<?php
    $empty = ( sizeof($sizes) && is_array($sizes) ) ? '' : 'empty';
    if ($empty)
        $sizes = array();
?>

<div class = "specs <?= $empty ?>">

    <?php
        foreach ($sizes as $key => $_size) {
            $show = 'disNone';
            if (!$key) $show = '';
            ?>
            <div class = "size_wrap <?= $show ?>" id = "sizes_<?= $key ?>" >
                <span class = "size_title">Размер</span>
                <span class = "size size_select">
                    <select class = "size_select_box disNone" id = "customPrice<?= $key ?>"
                            name = "customPrice[<?= $key ?>]">
                        <?php foreach ($_size as $_sizeOption) { ?>
                            <option value = "<?= $_sizeOption->id ?>"><?= $_sizeOption->size ?></option>
                        <?php } ?>
                    </select>
                    <div class = "size_selected"></div>
                </span>
            </div>

        <?php
        }
    ?>

    <div class = "size_table_wrap ">
        <span class = "size_table">Таблица размеров</span>

        <div class = "size_table_value">
            <span>?</span>
        </div>
    </div>

    <?php
        $empty = ( sizeof($colors) && is_array($colors) ) ? '' : 'empty';
        if ($empty)
            $colors = array();
    ?>

    <?php foreach ($colors as $num => $_color) {
        $show = ( $num ) ? 'disNone' : '';
        ?>
        <div class = "specs_color_wrap <?= $empty ?> <?= $show ?>" id = "color_<?= $num ?>" >
            <span class = "specs_color_title">Цвет</span>
            <span class = "specs_color color_select">
                <select class = "color_select_box" id = "customColor<?= $num ?>" name = "customColor[<?= $num ?>]"
                        style = "display: none;">
                    <?php
                        foreach ($_color as $_colorOption) {
                            ?>
                            <option value = "<?= $_colorOption->id ?>"
                                    selected = "selected"><?= $_colorOption->color ?></option>
                        <?php
                        }
                    ?>
                </select>
                <div class = "color_select_items_cont">
                    <div class = "color_select_item" data-value = "<?= $_colorOption->id ?>"
                         style = "width:324px; height:43px;"></div>
                </div>
                <div class = "color_selected"></div>
            </span>
        </div>
    <?php
    }
    ?>

    <?php
        $empty = ( sizeof($description) && is_array($description) ) ? '' : 'empty';
        if ($empty)
            $description = array();
    ?>
    <div class = "specs_details <?= $empty ?>">
        <?php
            foreach ($description as $num => $_description) {
                //        $show = ($num) ? 'disNone' : '';
                ?>
                <span class = "specs_detail_span <?= $show ?>" id = "description_<?= $num ?>" data="<?= $_description->id ?>">
                    <?= $_description->description ?>
                </span>


            <?php } ?>
        <span class = "specs_detail_span_title">Детали и уход</span>
    </div>


</div>
