<?php

    AddExtend::regCssFile(AddExtend::pathCss() . 'article.css', 'screen');
    $contacts = SiteController::getContacts();
    $this->breadcrumbs = array(
        'Оплата' => 'site/pay'
    );

?>
<div class = "item-page_pay">


    <div class = "simple_article">

        <div class = "left_column">
            <div class = "title_cont">
                <h1>Оплата товара
                    <br>
                    из Local moto
                </h1>
            </div>
        </div>

        <div class = "right_column">
            <div class = "img_cont">
                <img src = "<?= AddExtend::pathMedia() ?>img/pay_img.png">
            </div>
        </div>
        <div class = "clearfix"></div>

        <?php
            foreach ($pays as $_pay) {
                ?>
                <div class = "left_column">
                    <h2><?= $_pay->title ?></h2>
                </div>

                <div class = "right_column">
                    <p>
                        <?= $_pay->text ?>
                    </p>
                </div>
                <div class = "clearfix"></div>

            <?php
            }
        ?>

        <div class = "bottom">
            <p>
                Если ни один из вышеперечисленных способов вас не устраивает, то вы всегда можете написать нам на
                <a href = "mailto:<?= $contacts[ 'email' ] ?>">
                    <?= $contacts[ 'email' ] ?>
                </a>
            </p>
        </div>

    </div>


</div>
