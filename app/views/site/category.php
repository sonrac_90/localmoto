<?php
    Yii::app()->clientScript->registerCssFile(AddExtend::pathCss() . 'vmsite-ltr.css');
    $this->breadcrumbs = array();

    $keySort = '';

    if (Yii::app()->user->getState('sorting') == '1') {
        $keySort = 'Мужские товары. ';
    }

    if (Yii::app()->user->getState('sorting') == '0') {
        $keySort = 'Женские товары. ';
    }

    $setKeySort = false;

    if (!is_null($categoryChildName)) {
        $setKeySort = true;
        $this->breadcrumbs[ $keySort . $categoryChildName ] = array( 'showcat/id/' . $categoryChildID );
    }

    if (!$setKeySort)
        $keySort .= ' ';
    else
        $keySort = '';


    if (!is_null($categoryParentName)) {
        $setKeySort = true;
        $this->breadcrumbs[ $keySort . $categoryParentName ] = array( 'showcat/id/' . $categoryParentID );
    }

    if (!$setKeySort)
        $keySort .= ' ';
    else
        $keySort = '';


    if (!is_null($categoryName)) {
        $setKeySort = true;
        $this->breadcrumbs[ $keySort . $categoryName ] = array( 'showcat/id/' . $_GET[ 'id' ] );
    }

    if (!sizeof($this->breadcrumbs)) {
        $this->breadcrumbs['Все товары'] = array('all');
    }

    if (!$setKeySort)
        $keySort .= ' ';
    else
        $keySort = '';


    if (!is_null($producerName)) {
        $this->breadcrumbs[ $keySort . $producerName ] = array('showProducer/id/' . $_GET['id']);
    }

?>
<div id = "system-message-container">
</div>
<div class = "orderby-displaynumber">

    <div class = "vm-pagination-bottom">
        <div class = "totalPages-bottom"></div>
    </div>

    <div class = "clearfix"></div>
</div>
<?php

    $cnt = 0;
    $this->widget(
        'zii.widgets.CListView', array(
            'dataProvider'       => $goods,
            'itemView'           => '_listView',
            'itemsCssClass'      => 'list-view',
            'cssFile'            => false,
            'sortableAttributes' => false,
            'summaryText'        => false,
            'enableSorting'      => false,
            'pagerCssClass'      => 'vm-pagination-bottom',
            'template'           => '{pager}{items}{pager}',
            'htmlOptions'        => array(
                'class' => 'orderby-displaynumber',
            ),
            'pager'              => array(
                'htmlOptions' => array(
                    'class' => 'browse-view'
                ),
                'header' => '',
                'class' => 'CLinkCustom',
                'internalPageCssClass' => 'pagenav',
                'maxButtonCount' => 4,
                'lastPageCssClass' => '',
                'nextPageLabel' => 'Следующая',
                'prevPageLabel' => 'Предыдущая',
                'previousPageCssClass' => 'pagination-prev',
                'nextPageCssClass' => 'pagination-next'
            ),
        )
    );

?>
<div class = "clearfix"></div>