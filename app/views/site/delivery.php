<?php

    AddExtend::regCssFile(AddExtend::pathCss() . 'article.css', 'screen');
    $contacts = SiteController::getContacts();
    $this->breadcrumbs = array(
        'Доставка' => 'site/delivery'
    );

?>
<div class = "item-page_pay">


    <div class = "simple_article">

        <div class = "left_column">
            <div class = "title_cont">
                <h1>ДОСТАВКА ТОВАРОВ
                    <br>
                    из LOCAL MOTO
                </h1>
            </div>
        </div>

        <div class = "right_column">
            <div class = "img_cont">
                <img src = "<?= AddExtend::pathMedia() ?>img/deliv_img.png">
            </div>
        </div>
        <div class = "clearfix"></div>

        <?php
            foreach ($deliverys as $_delivery) {
                ?>
                <div class = "left_column">
                    <h2><?= $_delivery->title ?></h2>
                </div>

                <div class = "right_column">
                    <p>
                        <?= $_delivery->text ?>
                    </p>
                </div>
                <div class = "clearfix"></div>

            <?php
            }
        ?>

        <div class = "bottom">
            <p>
                Если ни один из вышеперечисленных способов вас не устраивает, то вы всегда можете написать нам на
                <a href = "mailto:<?= $contacts[ 'email' ] ?>">
                    <?= $contacts[ 'email' ] ?>
                </a>
            </p>
        </div>

    </div>


</div>
