<?php
    /**
     * @var array breadcrumbs
     * @var string $categoryChildName
     * @var string $categoryParentName
     * @var string $categoryName
     * @var string $producerName
     * @var Goods $good
     * @var Photo $photos
     * @var GoodsSize $sizes
     * @var ColorGoods $colors
     * @var DescriptionGoods $descriptions
     */
    $this->breadcrumbs = array();
    $keySort = '';

    if (Yii::app()->user->getState('sorting') == '1') {
        $keySort = 'Мужские товары. ';
    }

    if (Yii::app()->user->getState('sorting') == '0') {
        $keySort = 'Женские товары. ';
    }

    $setKeySort = false;

    if (!is_null($categoryChildName)) {
        $setKeySort = true;
        $this->breadcrumbs[ $keySort . " " . $categoryChildName ] = array( 'showcat/id/' . $categoryChildID );
    }

    if (!is_null($categoryParentName)) {
        $setKeySort = true;
        $this->breadcrumbs[ $keySort . $categoryParentName ] = array( 'showcat/id/' . $categoryParentID );
    }

    if (!$setKeySort)
        $keySort .= ' ';
    else
        $keySort = '';


    if (!is_null($categoryName)) {
        $this->breadcrumbs[ $categoryName ] = array( 'showcat/id/' . $categoryID );
    }

    if (!is_null($producerName)) {
        $this->breadcrumbs[ $keySort . $producerName ] = array( 'showProducer/id/' . $producerID );
    }
    $this->breadcrumbs[ $good->name ] = array( 'showGood/id/' . $_GET[ 'id' ] );
    AddExtend::regScriptFile(
        array(
            AddExtend::pathJs() . "libs/jquery.loupe.min.js",
            AddExtend::pathJs() . "slider_img_cur_product.js",
            AddExtend::pathJs() . "slider_rel_prod.js",
            AddExtend::pathJs() . "cur_prod_form.js",
            AddExtend::pathJs() . "admin/phpFunc.js",
            AddExtend::pathJs() . "buy.js",
        )
    );

    AddExtend::regCssFile(
        array(
            AddExtend::pathCss() . "slider_img_cur_product.css",
            AddExtend::pathCss() . "facebox.css",
            AddExtend::pathCss() . "modal.css",
        ), 'screen'
    );

?>

<div class = "productdetails-view productdetails" data="<?= $good->id ?>">
    <article class = "staff">
        <div class = "product_title">
            <h1 class = "bg_h1">
                <a href = "<?= AddExtend::baseUrl(true) ?>site/showProducer/id/<?= $good->producer_id ?>"><?= $good->producerName ?></a>
            </h1>

        </div>

        <div class = "product_model">
            <h2><?= $good->model ?></h2>
        </div>

        <div class = "main_slider cur_product_sl_cont">

            <div class = "content_slider" data-width = "1000" data-height = "450">
                <p class = "page_number">
                    <b>
                        1
                    </b>
                    /1
                </p>

                <div class = "arrow left hold topArrow"></div>
                <div class = "arrow right topArrow"></div>
                <div class = "slider_main_cont">
                    <div class = "content_band">

                        <?php
                            foreach ($photos as $numPhoto => $_photo) {
                                ?>
                                <div class = "item" id = "photoUrl<?= $numPhoto ?>">
                                    <div class = "photo_cont">
                                        <img
                                            src = "<?=
                                                AddExtend::baseUrl() . Photo::PATH_IMAGE . CropImage::dynamicPath($_photo->id) . "/" . $_photo->url ?>"
                                            style = "height: 450px;">
                                    </div>
                                </div>
                            <?php
                            }
                        ?>


                    </div>
                </div>
                <div class = "sl_p_cont">
                    <?php
                        foreach ($photos as $num => $_photo) {
                            $active = ( $num ) ? '' : 'sl_p_active';
                            ?>
                            <span class = "sl_pointer <?= $active ?>" rel = "<?= $num ?>"></span>
                        <?php
                        }
                    ?>
                </div>
            </div>

            <form method = "post" class = "product js-recalculate" action = "/sport" id = "buyForm" >
                <div class = "goods">
                    <?php

                        $empty = '';
                        if (!sizeof($sizes) && !sizeof($colors) && !sizeof($descriptions)) {
                            $empty = 'empty';
                        }

                        $this->renderPartial(
                            'goodInfo/sizes', array(
                                'sizes'       => $sizes,
                                'empty'       => $empty,
                                'colors'      => $colors,
                                'description' => $descriptions
                            )
                        );

                    ?>

                    <div class = "price">

                        <span class = "price_number"><?= $good->price ?></span>

                        <span class = "price_buy cp_button">
                            <div class = "addtocart-area">
                                <input name = "quantity" type = "hidden" value = "1">

                                <div class = "__addtocart-bar">

                                    <script type = "text/javascript">
                                        function check(obj) {
                                            // use the modulus operator '%' to see if there is a remainder
                                            remainder = obj.value % 1;
                                            quantity = obj.value;
                                            if (remainder != 0) {
                                                alert('You can buy this product only in multiples of 1 pieces!!');
                                                obj.value = quantity - remainder;
                                                return false;
                                            }
                                            return true;
                                        }
                                    </script>

                                    <span class = "quantity-box disNone" style = "display: none;">
                                        <input type = "text" class = "quantity-input js-recalculate" name = "quantity[]"
                                               onblur = "check(this);" value = "1" />
                                    </span>
                                    <span class = "quantity-controls js-recalculate " style = "display: none; ">
                                        <input type = "button" class = "quantity-controls quantity-plus" />
                                        <input type = "button" class = "quantity-controls quantity-minus" />
                                    </span>
                                    <span class = "__addtocart-button">
                                        <input type = "submit" name = "addtocart" class = "addtocart-button"
                                               value = "Купить" title = "Купить" />
                                    </span>

                                    <div class = "clear"></div>
                                </div>
                                <input type = "hidden" class = "pname" value = "<?= $good->name ?>">
                                <input type = "hidden" name = "virtuemart_product_id[]" value = "<?= $good->id ?>">

                                <div class = "clear"></div>
                            </div>
                        </span>
                    </div>

                    <div class = "model">

                        <div class = "model_wrap">
                            <span class = "model_title">
                                Модель
                            </span>

                            <span class = "model_number">
                                <?= $good->model ?>
                            </span>
                        </div>

                        <div class = "model_code_wrap">
                            <span class = "model_code_title">
                                Код
                            </span>
                        </div>

                        <span class = "model_code">
                            <?= $good->model ?>
                        </span>

                        <div class = "model_share_wrap">
                            <span class = "model_share_title">
                                Share
                            </span>

                            <span class = "soc_button_cur_prod">
                                <a href = "#" class = "fb">facebook</a>
                                <a href = "#" class = "vk">vkontakte</a>
                                <a href = "#" class = "tw">twitter</a>
                                <a href = "#" class = "pt">pinterest</a>
                            </span>
                        </div>

                    </div>
                </div>
            </form>

            <div class = "clearfix"></div>

        </div>

    </article>
</div>

<div id = "facebox" style = "position: fixed; display: none; left: 50%; margin-left: -147px; top: 50%; margin-top: -70px; width: 347px; height: 170px;">
    <div class = "popup">
        <div class = "content my-groovy-style">
            <div>
                <a class = "continue" href = "" onclick = "return buy.popupClose();">Продолжить покупки</a>
                <a class = "showcart floatright" href = "<?= AddExtend::baseUrl(true) ?>buy/cart">Показать корзину</a>
                <div>Количество товара было обновлено.</div>
                <h4> добавлен в корзину</h4></div>
        </div>
        <a href = "#" class = "close" onclick = "return buy.popupClose();"></a>
    </div>
</div>
<div id="facebox_overlay" class="facebox_hide facebox_overlayBG" onclick = "return buy.popupClose();" style="display: none; opacity: 0.2;"></div>

<?php
    foreach ($photos as $num => $_photo) {
        ?>
        <div class = "slider_img_zoom"
             style = "width: 300px; z-index: 15; height: 300px; position: absolute; overflow: hidden; left: 677px; top: 762px; display: none;">
            <div class = "sw_z"></div>
            <img
                src = "<?= AddExtend::baseUrl() . Photo::PATH_IMAGE . CropImage::dynamicPath($_photo->id) . "/" . $_photo->url ?>"
                style = "position: absolute; height: 675px; left: -414px; top: -466px;">
        </div>

    <?php } ?>
