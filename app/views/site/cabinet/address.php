<?php
    /**
     * @var Address $address
     * @var integer $countAddress
     * @var string  $empty
     */
    $empty = ( $countAddress ) ? 'pc_empty' : 'pc_empty';
    $emptyOne  = ( !$countAddress ) ? 'empty' : '';
?>


<div class = "pc_delivery <?= $emptyOne ?> " id = 'addressEmpty' ><!-- -|empty -->
    <div class = "pc_title">
        <span>Адреса доставки</span>
    </div>

    <div class = "<?= $empty ?> big-margin">Нет
        <br>
                                            адреса.
        <span onclick = "open_adress_popup('new');">Добавить?</span>
    </div>

    <div class = "pc_block big-margin no-border" id="addressBlock">
    <?php
        if (empty( $emptyOne )) {
            ?>

                <?php
                    /** @var Address $_addressNext */
                    foreach ($address as $number => $_addressNext) {
                        ?>
                        <span id="addressTitle<?= $_addressNext->id ?>" >
                            <span class = "small_button" onclick = "open_adress_popup(<?= $_addressNext->id ?>);">
                                Редактировать
                            </span>
                            <span class = "small_button" onclick = "delete_adress(<?= $_addressNext->id ?>);">Удалить</span>

                            <div class = "title"><?= intval($number + 1) ?> адрес</div>
                            <div><?= $_addressNext->country ?></div>
                            <div><?= $_addressNext->city ?></div>
                            <div><?= $_addressNext->street ?></div>
                            <div><?= $_addressNext->house ?></div>
                            <div><?= $_addressNext->department ?></div>
                        </span>
                    <?php
                    }
                ?>
        <?php } ?>
    </div>
    <div class = "pc_block no-border">
        <span class = "brown_button" onclick = "open_adress_popup('new');">Добавить адрес</span>
    </div>


                                             <!----------------------------- ADD ADRESS POPUP ------------------------------->
    <div id = "add_adress_popup" class = "pc_popup" style = "display: none;">
        <div class = "pc_popup_block">
            <div class = "hidden_inputs">

                <?php
                    if (empty ( $emptyOne )) {
                        foreach ($address as $number => $_addressNext) {
                            ?>
                            <input type = "text" id = "country_and_zip_<?= $_addressNext->id ?>"
                                   name = "country_and_zip_<? $_addressNext->id ?>" size = "0"
                                   value = "<?= $_addressNext->country ?>"
                                   class = "" aria-invalid = "false"
                                   placeholder = "Страна и Zip код (<?= intval($number + 1) ?> Адрес)">
                            <input type = "text" id = "city_<?= $_addressNext->id ?>"
                                   name = "city_<?= $_addressNext->id ?>" size = "0"
                                   value = "<?= $_addressNext->city ?>" class = ""
                                   aria-invalid = "false" placeholder = "Город (<?= intval($number + 1) ?> Адрес)">
                            <input type = "text" id = "street_<?= $_addressNext->id ?>"
                                   name = "street_<?= $_addressNext->id ?>" size = "0"
                                   value = "<?= $_addressNext->street ?>"
                                   class = "" aria-invalid = "false"
                                   placeholder = "Улица (<?= intval($number + 1) ?> Адрес)">
                            <input type = "text" id = "house_<?= $_addressNext->id ?>"
                                   name = "house_<?= $_addressNext->id ?>" size = "0"
                                   value = "<?= $_addressNext->house ?>" class = ""
                                   aria-invalid = "false"
                                   placeholder = "Дом (<?= intval($number + 1) ?> Адрес)">
                            <input type = "text" id = "department_<?= $_addressNext->id ?>"
                                   name = "department_<?= $_addressNext->id ?>" size = "0"
                                   value = "<?= $_addressNext->department ?>" class = ""
                                   aria-invalid = "false"
                                   placeholder = "Квартира (<?= intval($number + 1) ?> Адрес)">

                        <?php
                        }
                    }
                ?>

                <input type = "text" id = "country_and_zip_new" name = "country_and_zip_new" size = "0" value = ""
                       class = "" aria-invalid = "false" placeholder = "Страна и Zip код (Добавить Адрес)">
                <input type = "text" id = "city_new" name = "city_new" size = "0" value = "" class = ""
                       aria-invalid = "false" placeholder = "Город (Добавить Адрес)">
                <input type = "text" id = "street_new" name = "street_new" size = "0" value = ""
                       class = "" aria-invalid = "false" placeholder = "Улица (Добавить Адрес)">
                <input type = "text" id = "house_new" name = "metro_new" size = "0" value = "" class = ""
                       aria-invalid = "false" placeholder = "Дом (Добавить Адрес)">
                <input type = "text" id = "department_new" name = "metro_new" size = "0" value = "" class = ""
                       aria-invalid = "false" placeholder = "Квартира (Добавить Адрес)">
            </div>

            <div class = "pc_save">
                <button class = "brown_button" type = "submit" id = "save_adrs"
                        onclick = "javascript:return myValidator(userForm, 'saveAddress', this);">
                    Сохранить
                </button>
            </div>
            <div class = "clearfix"></div>
        </div>
        <div class = "pc_popup_slider" onclick = "close_personal_popup()"></div>
    </div>
</div>
