<?php
    $empty = ( $countService ) ? 'pc_empty' : 'pc_empty';
    $emptyOne  = ( !$countService ) ? 'empty' : '';
?>
<div class = "pc_history <?= $emptyOne ?>"><!-- -|empty -->
    <div class = "pc_title">
        <span>История заказов</span>
    </div>

    <div class = "<?= $empty ?> big-margin">Пока нет
        <br>
                                            ни одного заказа.
        <br>
        <span>
            <a href = "<?= AddExtend::baseUrl(true) ?>site/all" class = "choise">Выбрать!</a>
        </span>
    </div>

    <?php
        if (empty( $emptyOne )) {
            function getStatusOrder($status) {
                if ($status == 'ordered')
                    return 'В ожидании';
                if ($status == 'failure')
                    return 'Отменен';

                return 'Куплено';
            }

            function getStatusPay($status) {
                if ($status == 'pay')
                    return 'Оплачено';

                return 'Не оплачено';
            }

            function getStatusDelivery($status) {
                if ($status == 'delivery')
                    return 'Доставлено';

                return 'Не доставлено';
            }

            ?>
            <?php
            /** @var OrderMain $_orderNext */
            foreach ($order as $_orderNext) {
                $date = new DateTime($_orderNext->date_order);
                ?>
                <div class = "pc_block big-margin">
                    <div class = "pc_headline" onclick = "open_history_details(this)">
                        <div class = "pc_descr"><?= $date->format("H:i:s") ?></div>
                        <div class = "pc_descr"><?= $date->format("d-m-Y") ?></div>
                        <!--<div class="pc_name"><a href="/">N  </a></div>-->
                        <div class = "pc_name">
                            <span>N <?= $_orderNext->id ?></span>
                        </div>
                    </div>
                    <div class = "pc_details">

                        <div class = "pc_descr"><?= getStatusOrder($_orderNext->status) ?></div>
                        <div class = "pc_name">Статус:</div>
                    </div>
                    <div class = "pc_details">
                        <div class = "pc_descr"><?= $_orderNext->total_price ?></div>
                        <div class = "pc_name">Сумма:</div>
                    </div>
                    <div class = "pc_details">
                        <div class = "pc_descr"><?= getStatusPay($_orderNext->status_pay) ?></div>
                        <div class = "pc_name">Оплата:</div>
                    </div>
                    <div class = "pc_details">
                        <div class = "pc_descr"><?= getStatusPay($_orderNext->status_delivery) ?></div>
                        <div class = "pc_name">Доставка:</div>
                    </div>
                </div>
            <?php
            }
            ?>
        <?php
        }
    ?>
</div>
