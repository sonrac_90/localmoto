<?php


    if ($data[ 'num' ] == 1) {
        ?>
    <?php
    }

    if ($data[ 'num' ] == 1 || ( $data[ 'num' ] % 3 ) == 1) {
        echo "<div class='catalog_block'>";
    }

    $imgPath = CropImage::dynamicPath(
            str_replace(
                pathinfo($data[ 'photo' ], PATHINFO_EXTENSION), '', pathinfo($data[ 'photo' ], PATHINFO_FILENAME)
            )
        ) . "/" . str_replace('.', '-t.', $data[ 'photo' ]);
?>
    <div class = "catalog_item vertical-separator">
        <a title = "<?= $data[ 'name' ] ?>" rel = "vm-additional-images"
           href = "<?= AddExtend::baseUrl(true) . "site/showgood/id/{$data['id']}/goodID/" . $_GET[ 'id' ] ?>">
            <img width = "217" src = "<?= Photo::model()->getUrlImage($imgPath) ?>" />
        </a>
        <h2>
            <a class = "catalog_item_title"
               href = "<?= AddExtend::baseUrl(true) . "site/showProducer/id/{$data['producer_id']}" ?>">
                <?= $data[ 'producer_name' ] ?>
            </a>
        </h2>
        <h2>
            <a class = "catalog_item_model"
               href = "<?= AddExtend::baseUrl(true) . "site/showGood/id/{$data['id']}/goodID/" . $_GET[ 'id' ] ?>">
                <?= $data[ 'good_name' ] ?>
            </a>
        </h2>
        <div class = "catalog_item_price" id = "productPrice152">
            <?= $data[ 'price' ] ?>
        </div>
    </div>
<?php
    if (( $data[ 'num' ] % 3 ) == 0) {
        echo "</div>";
    }