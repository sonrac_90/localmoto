<?php
    AddExtend::regCssFile(AddExtend::pathCss() . 'article.css', 'screen');
    $contacts = SiteController::getContacts();
    $this->breadcrumbs = array(
        'Возврат товара' => 'site/change'
    );
?>


<div class = "item-page_change">


    <div class = "simple_article">

        <div class = "left_column">
            <div class = "title_cont2">
                <h1>Обмен
                    <br>
                    и Возврат товара
                </h1>
            </div>
        </div>

        <div class = "right_column">

        </div>
        <div class = "clearfix"></div>

        <?php

            foreach ($change as $_change) {
                ?>
                <div class = "left_column">
                    <h2><?= $_change->title ?></h2>
                </div>
                <div class = "right_column">
                    <p>
                        <?= $_change->text ?>
                    </p>
                </div>
                <div class = "clearfix"></div>
            <?php
            }

        ?>


        <div class = "bottom">
            <p>
                Если ни один из вышеперечисленных способов вас не устраивает, то вы всегда можете написать нам на
                <a href = "mailto:<?= $contacts[ 'email' ] ?>">
                    <?= $contacts[ 'email' ] ?>
                </a>
            </p>
        </div>

    </div>


</div>
