<?php
    AddExtend::regCssFile(AddExtend::pathCss() . 'article.css', 'screen');

    $phone = $contact->phone[ 0 ] . ' - ' . substr($contact->phone, 1, 3) . ' - ' . substr($contact->phone, 4, strlen($contact->phone) - 1);

    $this->breadcrumbs = array(
        'Контакты' => 'site/contacts'
    );


?>



<div class = "item-page_change">


    <div class = "simple_article">

        <div class = "left_column">
            <div class = "title_cont2">
                <h1>Контактная информация</h1>
            </div>
        </div>

        <div class = "clearfix"></div>
        <br />
        <br />

        <div class = "left_column">
            <h2>Город</h2>
        </div>
        <div class = "right_column">
            <p>
                <?= $contact->city ?>
            </p>
        </div>
        <div class = "clearfix"></div>

        <div class = "left_column">
            <h2>Адрес</h2>
        </div>
        <div class = "right_column">
            <p>
                <?= $contact->address ?>
            </p>
        </div>
        <div class = "clearfix"></div>

        <div class = "left_column">
            <h2>Телефон</h2>
        </div>
        <div class = "right_column">
            <p>
                <?= $phone ?>
            </p>
        </div>
        <div class = "clearfix"></div>

        <div class = "left_column">
            <h2>EMail</h2>
        </div>
        <div class = "right_column">
            <p>
                <a href = "mailto:<?= $contact->email ?>"><?= $contact->email ?></a>
            </p>
        </div>
        <div class = "clearfix"></div>


    </div>


</div>
