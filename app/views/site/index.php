<?php
    /**
     * @var Slider $slider
     * @var MainBlock $main
     */
?>
<div id = "ajaxscrl88" class = "ajaxscrl megaslider">
    <div id = "mNavLeft88" class = "mNavLeft"></div>
    <div id = "mNavRight88" class = "mNavRight"></div>
    <div id = "mContainer88" class = "mContainer">
        <div id = "mScroller88" class = "mScroller"
             style = "position: absolute; top: 0px; left: 0px; width: 100%; height: 100%;">
            <?php
                foreach ($slider as $key => $value) {
                    ?>
                    <div id = "m<?= $value->id ?>" class = "mScrollBlock">
                        <p>
                            <img width = "990" height="518px" src = "<?= Slider::getFullPath($value, '.') ?>" alt = "slide-03">
                        </p>
                    </div>
                    <?php
                }

            ?>
        </div>
    </div>
</div>


<div id = "system-message-container">
</div>
<div class = "blog-featured">


</div>


<?= $main->text ?>