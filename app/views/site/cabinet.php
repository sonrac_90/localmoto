<?php
    /**
     * @var Users     $userInfo
     * @var Address   $address
     * @var integer   $countGood
     * @var OrderMain $order
     */
    $countAddress = count($address);
    $countService = count($order);
    $countOrer = count($order);

    AddExtend::regCssFile(
        array(
            AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css',
            AddExtend::pathCss() . 'personal.css',
        ),
        'screen'
    );
    AddExtend::regScriptFile(
        array(
            AddExtend::pathJs() . 'libs/maskedinput.js',
            AddExtend::pathJs() . 'maskInit.js',
            AddExtend::pathJs() . 'libs/jquery.stylish-select.js',
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            AddExtend::pathJs() . 'admin/phpFunc.js',
            AddExtend::pathJs() . 'cabinet.js',
        )
    );
?>

<div id = "system-message-container">
</div>


<form method = "post" id = "adminForm" name = "userForm" action = ""
      class = "form-validate">

<div id = "personal_cabinet">
<div class = "main">
<!----------------------------- PERSONAL TITLE ------------------------------->
<div class = "pc_title">

    <span><?= $userInfo->first_name . " " . $userInfo->last_name ?></span>
    <br>
    <span>Личный кабинет</span>
</div>

<!----------------------------- PERSONAL CART ------------------------------->

<div class = "pc_cart_block">


    <a href = "javascript:void(0);" class = "cart_link basket_empty">
        <span class = "title-basket">Корзина</span>
        <span class = "number"><?= intval($countGood) ?></span>
    </a>

</div>

<!----------------------------- PERSONAL CABINET ------------------------------->
<div class = "pc_main">

    <!----------------------------- PERSONAL LEFT BLOCK ------------------------------->

    <div class = "pc_personal">
        <!----------------------------- MAIN POPUP ------------------------------->
        <div id = "personal_popup" class = "pc_popup" style = "display: none;">
            <div class = "pc_popup_block">

                <input type = "text" id = "personal-popup-name" name = "first_name"
                       value = "<?= $userInfo->first_name ?>"
                       class = "required"
                       aria-required = "true" required = "required" aria-invalid = "false" placeholder = "Имя">

                <input type = "text" id = "personal-popup-surname" name = "last_name" value = "<?= $userInfo->last_name ?>"
                       class = "required"
                       aria-required = "true" required = "required" aria-invalid = "false" placeholder = "Фамилия">

                <input type = "text" id = "personal-popup-email" name = "email" value = "<?= $userInfo->email ?>"
                       class = "required"
                       aria-required = "true" required = "required" aria-invalid = "false" placeholder = "E-mail">

                <input type = "text" id = "personal-popup-phone" name = "phone" value = "<?= $userInfo->phone ?>"
                       class = ""
                       aria-invalid = "false" placeholder = "Ведите свой номер телефона">

                <div class = "pc_birthday_title">Дата рождения</div>
                <div class = "pc_birthday">

                    <?php

                        $dateRange = '1920:' . date("Y", time());
                        $date = new DateTime();
                        $date->modify('- 18 year');
                        $date->modify('- 1 day');
                        $this->widget(
                            'zii.widgets.jui.CJuiDatePicker', array(
                                'name'        => 'birthday',
                                'value'       => date('d-m-Y', $userInfo->birthday),
                                'language'    => 'ru',
                                'options'     => array(
                                    'showAnim'        => 'slide',
                                    'dateFormat'      => 'dd-mm-yy',
                                    'changeMonth'     => true,
                                    'changeYear'      => true,
                                    'yearRange'       => $dateRange,
                                    'minDate'         => '01-01-1920',
                                    'maxDate'         => $date->format('d-m-Y'),
                                    'beforeShow'      => "js: function (input, inst) {
                                                inst.dpDiv.css({
                                                    'margin-left': '-16px',
                                                });
                                        }",
                                    'showButtonPanel' => true
                                ),
                                'htmlOptions' => array(
                                    'id' => 'birthday_date_field'
                                )
                            )
                        );
                    ?>

                </div>
                <div class = "pc_subscribe">
                    <input id = "check-personal-subscribe_pop_up" type = "checkbox" name = "subscribe"
                           value = "<?= $userInfo->subscribe ?>" class = "jq-checkbox <?= ($userInfo->subscribe) ? 'checked' : '' ?>" style = "position: absolute; left: -9999px;">

                    <label for = "check-personal-subscribe_pop_up">Подписаться на обновления</label>
                </div>
                <div class = "pc_save">
                    <button class = "brown_button" type = "submit"
                            onclick = "javascript:return myValidator(userForm, 'saveUser');">
                        Сохранить
                    </button>
                </div>
                <div class = "clearfix"></div>
            </div>
            <div class = "pc_popup_slider" onclick = "close_personal_popup()"></div>
        </div>

        <!----------------------------- PHONE POPUP ------------------------------->
        <div id = "phone_popup" class = "pc_popup" style = "display: none;">
            <div class = "pc_popup_block">
                <input type = "text" id = "add-personal-popup-phone" value = "+7(343)423-4234" aria-invalid = "false"
                       placeholder = "+7">

                <div class = "pc_save">
                    <button class = "brown_button" type = "submit"
                            onclick = "javascript:return myValidator(userForm, 'saveUser');">
                        Сохранить
                    </button>
                </div>
                <div class = "clearfix"></div>
            </div>
            <div class = "pc_popup_slider" onclick = "close_personal_popup()"></div>
        </div>

        <!----------------------------- BIRTHDAY POPUP ------------------------------->
        <div id = "birthday_popup" class = "pc_popup" style = "display: none;">
            <div class = "pc_popup_block">
                <div class = "pc_birthday_title">Дата рождения</div>
                <div class = "pc_birthday">
                    <input class = "pc_birth_day" id = "birthday-popup-day" type = "text" value = "">
                    <input class = "pc_birth_month" id = "birthday-popup-month" type = "text" value = "">
                    <input class = "pc_birth_year" id = "birthday-popup-year" type = "text" value = "">

                </div>
                <div class = "pc_save">
                    <button class = "brown_button" type = "submit"
                            onclick = "javascript:return myValidator(userForm, 'saveUser');">
                        Сохранить
                    </button>
                </div>
                <div class = "clearfix"></div>
            </div>
            <div class = "pc_popup_slider" onclick = "close_personal_popup()"></div>
        </div>

        <div class = "pc_title">
            <span>Персональные данные</span>
        </div>

        <div class = "pc_block big-margin">
            <div class = "title">Имя</div>
            <div class = "value"><?= $userInfo->first_name ?></div>
        </div>
        <div class = "pc_block">
            <div class = "title">Фамилия</div>
            <div class = "value"><?= $userInfo->last_name ?></div>
        </div>
        <div class = "pc_block">
            <div class = "title">Email</div>
            <div class = "value"><?= $userInfo->email ?></div>
        </div>
        <div class = "pc_block "><!-- -|pc_nodata -->
            <a href = "javascript:open_personal_popup(2);" class = "small_button">Добавить</a>

            <div class = "title">Телефон</div>
            <div class = "value"><?= Users::formatPhone($userInfo->phone) ?></div>
        </div>
        <div class = "pc_block "><!-- -|pc_nodata -->
            <a href = "javascript:open_personal_popup(3);" class = "small_button">Добавить</a>

            <div class = "title">Дата рождения</div>
            <div class = "value"><?= $userInfo->birthday ?></div>
        </div>
        <div class = "pc_subscribe">
            <input id = "check-personal-subscribe" type = "checkbox" name = "subscribe_to_updates" value = "<?= $userInfo->subscribe ?>"
                   class = "jq-checkbox <?= ($userInfo->subscribe) ? 'checked' : '' ?>" style = "position: absolute; left: -9999px;">
            <label for = "check-personal-subscribe" >Подписаться на обновления</label>
        </div>
        <div class = "pc_block no-border">
            <a href = "javascript:open_personal_popup(1);" class = "brown_button">Редактировать данные</a>
        </div>

    </div>
    <!----------------------------- PERSONAL CENTER BLOCK ------------------------------->

    <?php
        $this->renderPartial(
            'cabinet/address', array(
                'address'      => $address,
                'countAddress' => $countAddress
            )
        )
    ?>

    <!----------------------------- PERSONAL RIGHT BLOCK ------------------------------->

    <?php
        $this->renderPartial(
            'cabinet/order', array(
                'order'        => $order,
                'countService' => $countService
            )
        )
    ?>

    <div class = "clearfix"></div>
</div>
<input type = "hidden" name = "option" value = "com_virtuemart">
<input type = "hidden" name = "controller" value = "user">
<input type = "hidden" name = "ed623816943fb2be4186e7dab7136135" value = "1">
</form>
</div></div>