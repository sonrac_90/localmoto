<?php
    Statistics::setNewVisit();
?>

<?php
    $size = count(Yii::app()->session->get('goodBuy'));
    $empty = 'basket_empty';
    $link = 'javascript:void(0);';
    if ($size) {
        $empty = '';
        $link = AddExtend::baseUrl(true) . "buy/cart";
    }
?>

<html xmlns = "http://www.w3.org/1999/xhtml" xml:lang = "ru-ru" lang = "ru-ru" dir = "ltr" __fvdsurfcanyoninserted = "1"
      class = "dj_webkit dj_chrome dj_contentbox" slick-uniqueid = "3">

<?php $contacts = SiteController::getContacts(); ?>

<head>
    <script id = "tinyhippos-injected">
        var phraseBikers = <?= Phrase::model()->getAllLayout() ?>;
        if (window.top.ripple) {
            window.top.ripple("bootstrap").inject(window, document);
        }
        var baseUrl = '<?= AddExtend::baseUrl(true) ?>';
    </script>

    <style type = "text/css"></style>
    <!--[if lt IE 9]>
    <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
        document.createElement('hgroup');
    </script>
    <![endif]-->

    <!-- Стили -->
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>base.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>font.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>style.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>mobile.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>ajaxScroll/style88.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>ajaxScroll/style92.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>ajaxScroll/style115.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>auth.css" type = "text/css">


    <!-- Скрипты -->

    <?php AddExtend::regCoreScript('jquery') ?>

    <base href = "<?= AddExtend::baseUrl(true) ?>">
    <meta http-equiv = "content-type" content = "text/html; charset=utf-8">
    <title><?= @$this->title ?></title>

    <script src = "<?= AddExtend::pathJs() ?>libs/jquery.formstyler.min.js"></script>
    <script defer = "" src = "<?= AddExtend::pathJs() ?>app.js"></script>
    <script src = "<?= AddExtend::pathJs() ?>auth_menu.js"></script>
    <script src = "<?= AddExtend::pathJs() ?>libs/maskedinput.js"></script>
    <script src = "<?= AddExtend::pathJs() ?>maskInit.js"></script>
    <script src = "<?= AddExtend::pathJs() ?>libs/jquery.stylish-select.js"></script>
    <script src = "<?= AddExtend::pathJs() ?>jquery.megaslider.js"></script>

    <link href = "<?= AddExtend::pathMedia() ?>favicon.ico" rel = "shortcut icon" type = "image/vnd.microsoft.icon" />

    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>artmenu.css" type = "text/css">
    <link rel = "stylesheet" href = "<?= AddExtend::pathCss() ?>melon.datepicker.css" type = "text/css">


    <meta name = "viewport" content = "width=device-width, initial-scale=1.0" />
</head>
<body>

<div class = "wrapper">

<div class = "wwrapper">

<header style = "top: 0px; height: 370px;">

<div class = "address_h" style = "top: 0px;">
    <a class = "f_header" href = "http://www.facebook.com/localmoto.ru" target = "_blank">
        <span class = "desktop">f</span>
        <span class = "mobile">facebook</span>
    </a>

    <span class = "address_text"><?= $contacts[ 'address' ] ?></span>
    <span class = "address_phone">  <?= $contacts[ 'phone' ][ 0 ] ?>
        <span class = "desktop">
            -
        </span>
        <img class = "mobile" src = "<?= AddExtend::pathMedia() ?>img/phone_line.gif" />
        <?= $contacts[ 'phone' ][ 1 ] ?>
        <span class = "desktop"> —</span>
        <img class = "mobile" src = "<?= AddExtend::pathMedia() ?>img/phone_line.gif">
        <?= $contacts[ 'phone' ][ 2 ] ?>
    </span>
</div>

<div class = "search" style = "top: 20px;">

    <!--BEGIN Search Box -->
    <form action = "/all/search" method = "get">
        <div>
            <input class = "search_input" name = "keyword" id = "mod_virtuemart_search" maxlength = "20" alt = "Поиск"
                   type = "text" size = "20" value = " "><input class = "search_but" type = "image" value = " "
                                                                src = "<?= AddExtend::pathMedia() ?>images/vmgeneral/search.png"
                                                                onclick = "this.form.keyword.focus();"></div>
        <input type = "hidden" name = "limitstart" value = "0">
        <input type = "hidden" name = "option" value = "com_virtuemart">
        <input type = "hidden" name = "view" value = "category">
    </form>

    <!-- End Search Box -->


</div>

<a href = "<?= AddExtend::baseUrl(true) ?>" class = "logo" style = "width: 436px; height: 175px; bottom: 130px;">
<!--    <img src = "--><?//= AddExtend::pathMedia() ?><!--images/logo_new.png">-->
</a>

<div class = "motorcycle">
    <?php
        $category = Category::model()->getAllCategory();
        if (!isset( $_SESSION[ 'activeOneMenu' ] )) {
            $_SESSION[ 'activeOneMenu' ] = $category[ 0 ]->id;
            if (sizeof($category[ 0 ]->childNode))
                $_SESSION[ 'activeTwoMenu' ] = $category[ 0 ]->childNode[ 0 ]->id;
        }
        foreach ($category as $key => $_nextCat) {
            $sel = "text-decoration: underline;";
            if ($_SESSION[ 'activeOneMenu' ] != $_nextCat->id)
                $sel = "text-decoration: none;";
            echo "<span data='{$_nextCat['id']}' style='{$sel}'>" . $_nextCat->name . "</span>";
        }
    ?>
</div>

<?php
    /**
     *
     *
     *
     * BEGIN MENU
     *
     *
     *
     *
     */
    $cnt = 0;
    $display = "block;";

    function getUrlCat($id) {
        return AddExtend::baseUrl(true) . "site/showcat/id/" . $id;
    }

    foreach ($category as $_nextCat) {
        $display = 'block;';
        if ($_SESSION[ 'activeOneMenu' ] != $_nextCat->id)
            $display = 'none';
        echo '<nav class = "main_menu" id="main_menu' . $_nextCat->id . '" style="display: ' . $display . '"><ul id="nav">';

        if (isset( $_nextCat->childNode ) && sizeof($_nextCat->childNode)) {
            foreach ($_nextCat->childNode as $_child) {
                $active = '';

                if ($_child->id == $_SESSION[ 'activeTwoMenu' ]) {
                    $active = "class='active'";
                }
                $begin = false;
                if (!sizeof($_child->childNode) || !isset( $_child->childNode )) {
                    $_child->childNode = array();
                } else
                    $begin = true;

                echo "<li {$active}>";
                echo "<a href='" . getUrlCat($_child->id) . "'>{$_child['name']}</a>";

                if (isset( $_child->childNode ) && sizeof($_child->childNode)) {
                    if ($begin)
                        echo "<ul>";

                    foreach ($_child->childNode as $_childChild) {
                        echo "<li><a href='" . getUrlCat($_childChild->id) . "'>{$_childChild['name']}</a></li>";
                    }
                    if ($begin)
                        echo "</ul>";
                }
                echo "</li>";
            }
        }
        $cnt++;
        echo "</ul></nav>";
    }
    /**
     *
     *
     *
     * END MENU
     *
     *
     *
     *
     */
?>

<div class = "clearfix"></div>

<nav class = "second_main_menu" style = "opacity: 1;">
    <!-- The class on the root UL tag was changed to match the Blueprint nav style -->
    <ul class = "joomla-nav">
        <li class = "item132">
            <a href = "<?= AddExtend::baseUrl(true) ?>site/pay">Оплата</a>
        </li>
        <li class = "item133">
            <a href = "<?= AddExtend::baseUrl(true) ?>site/delivery">Доставка</a>
        </li>
        <li class = "item134">
            <a href = "<?= AddExtend::baseUrl(true) ?>site/change">Возврат товара</a>
        </li>
    </ul>

</nav>

<div class = "clearfix"></div>

<?php
    if ($this->action->id != 'index') {
        $this->widget(
            'zii.widgets.CBreadcrumbs', array(
                'links'     => $this->breadcrumbs,
                'homeLink'  => CHtml::link('Главная', AddExtend::baseUrl(true)),
                'separator' => ' > '
            )
        );
    }
?>

<div id = "mod_improved_ajax_login-111" style = "display: none;">

    <a id = "loginBtn" class = "selectBtn" onclick = "return false" href = "#">
        <span class = "loginBtn leftBtn">
            Войти
        </span>
        <span class = "loginBtn rightBtn">&nbsp;<img
                src = "<?= AddExtend::pathMedia("") ?>/images/arrow.png" alt = "\/" width = "10"
                height = "7">&nbsp;</span>
    </a>

    <a id = "regBtn" class = "selectBtn " href = "#">
        <span class = "loginBtn leftBtn">
            Регистрация
        </span>
        <span class = "loginBtn rightBtn">&nbsp;<img
                src = "<?= AddExtend::pathMedia("") ?>/images/arrow.png" alt = "\/" width = "10"
                height = "7">&nbsp;</span>
    </a>


</div>
<?php if (Yii::app()->user->isGuest) { ?>
    <!--------------------------------------------------- LOGIN REGISTER ------------------------------------------------>
    <div class = "basket_h">


        <a href = "<?= $link ?>" class = "cart_link <?= $empty ?>">
            <span class = "title-basket">Корзина</span>
            <span class = "number"><?= $size ?></span>
        </a>


        <div class = "enter" onclick = "auth_menu_show('auth');">Вход</div>
    </div>

<?php } else { ?>
    <div class="basket_h logged">



        <a href = "<?= $link ?>" class = "cart_link <?= $empty ?>">
            <span class = "title-basket">Корзина</span>
            <span class = "number"><?= $size ?></span>
        </a>




        <div class="enter" onclick="document.getElementById('logoutForm').submit();">Выход</div>
        <a class="my_cabinet" href="<?= AddExtend::baseUrl(true) ?>site/cabinet">Мой кабинет</a>

        <form action="<?= AddExtend::baseUrl(true) ?>/site/logout" method="post" name="login" id="logoutForm">
            <input type="hidden" name="return" value="Lw==">
            <input type="hidden" name="ac54bfbb458793f9ece9b7f86d2783ec" value="1">            <noscript>
                &lt;button class="loginBtn"&gt;Выйти&lt;/button&gt;
            </noscript>
        </form>

    </div>

     <?php } ?>

<div id = "auth-menu" style = "top: -500px;">
<div class = "menu-blocks">

<?php
    if (Yii::app()->user->isGuest) {
        ?>
        <div class = "g-block auth-block">
            <form id = "loginForm" action = "<?= AddExtend::baseUrl(true) ?>site/login" method = "post" name = "login">
                <div class = "form-block">
                    <div class = "g-left">
                        <input class = "button-1 reg-button" type = "submit" value = "Регистрация">
                    </div>
                    <div class = "g-right">
                        <div class = "block-1 b-item">
                            <div class = "invalid-text">Такого email не существует</div>
                            <div class = "invalid-slider"></div>
                            <input id = "auth-email" type = "text" value = "" placeholder = "EMAIL" name = "email"
                                   autocomplete = "off" />
                        </div>
                        <div class = "block-2 b-item">
                            <div class = "invalid-text">Пароль неверный</div>
                            <div class = "invalid-slider"></div>
                            <input id = "auth-password" type = "password" value = "" placeholder = "PASSWORD"
                                   name = "passwd"
                                   autocomplete = "off" />
                        </div>
                        <div class = "block-3">
                            <input class = "button-2 auth-ok-button" type = "submit" disabled = "" value = "Войти">
                        </div>
                    </div>
                    <div style = "clear:both;"></div>
                </div>
                <div class = "footer-block">
                    <div class = "g-left">
                        Нужна помощь?
                        <span class = "t-grey phone-num">
                            Звоните: <?= $contacts[ 'phone' ][ 0 ] . ' - ' . $contacts[ 'phone' ][ 1 ] . ' - ' . $contacts[ 'phone' ][ 2 ] ?> </span>
                    </div>
                    <div class = "g-right">
                        <div class = "block-1">
                            <a href = "#" class = "pass-remind-button" onclick = "auth_menu_show('pass-remind');">
                                Напомнить пароль?
                            </a>
                        </div>
                        <div class = "block-2">
                            <label for = "check-auth-save" class = "t-grey">Запомнить?</label>
                            <input id = "check-auth-save" type = "checkbox" class = "jq-checkbox" name = "remember"
                                   style = "position: absolute; left: -9999px;">
                        </div>
                    </div>
                    <div style = "clear:both;"></div>
                </div>

                <input type = "hidden" name = "return" value = "Lw==">
                <input type = "hidden" name = "ajax" value = "0">
                <input type = "hidden" name = "0b07f8424af411dd48f4eed6d25049dd" value = "1"></form>
        </div>
    <?php
    } else {
        ?>
        <div class = "basket_h logged">


            <a href = "<?= $link ?>" class = "cart_link <?= $empty ?>">
                <span class = "title-basket">Корзина</span>
                <span class = "number"><?= $size ?></span>
            </a>


            <div class = "enter" onclick = "document.getElementById('logoutForm').submit();">Выход</div>
            <a class = "my_cabinet" href = "<?= AddExtend::baseUrl(true) ?>site/cabinet">Мой кабинет</a>

            <form action = "<?= AddExtend::baseUrl(true) ?>site/logout" method = "post" name = "login"
                  id = "logoutForm">
                <input type = "hidden" name = "return" value = "Lw==">
                <input type = "hidden" name = "62bcb867b07ed298317e47d764e78e59" value = "1">
                <noscript>
                    &lt;button class="loginBtn"&gt;Выйти&lt;/button&gt;
                </noscript>
            </form>

        </div>
    <?php
    }
?>

<div class = "g-block reg-block">
    <form id = "regForm" action = "<?= AddExtend::baseUrl(true) ?>site/register" method = "post"
          name = "register">
        <div class = "form-block">
            <div class = "g-left">
                <span class = "t-grey">Регистрация
                    <br>
                                       нового пользователя
                    <br>
                                       проекта localmoto
                </span>
            </div>
            <div class = "g-right">
                <div class = "block-1 b-item">
                    <div class = "invalid-text">Неверное имя</div>
                    <div class = "invalid-slider"></div>
                    <input id = "reg-name" type = "text" value = "" placeholder = "ИМЯ"
                           style = "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
                </div>
                <div class = "block-2 b-item">
                    <div class = "invalid-text">Неверная фамилия</div>
                    <div class = "invalid-slider"></div>
                    <input id = "reg-surname" type = "text" value = "" placeholder = "ФАМИЛИЯ">
                </div>
                <div class = "block-3 b-item">
                    <div class = "invalid-text">Такого email не существует</div>
                    <div class = "invalid-slider"></div>
                    <input id = "reg-email" name = "email" type = "text" value = "" placeholder = "EMAIL"
                           class = "text-present"
                           style = "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDhss3LcOZQAAAU5JREFUOMvdkzFLA0EQhd/bO7iIYmklaCUopLAQA6KNaawt9BeIgnUwLHPJRchfEBR7CyGWgiDY2SlIQBT/gDaCoGDudiy8SLwkBiwz1c7y+GZ25i0wnFEqlSZFZKGdi8iiiOR7aU32QkR2c7ncPcljAARAkgckb8IwrGf1fg/oJ8lRAHkR2VDVmOQ8AKjqY1bMHgCGYXhFchnAg6omJGcBXEZRtNoXYK2dMsaMt1qtD9/3p40x5yS9tHICYF1Vn0mOxXH8Uq/Xb389wff9PQDbQRB0t/QNOiPZ1h4B2MoO0fxnYz8dOOcOVbWhqq8kJzzPa3RAXZIkawCenHMjJN/+GiIqlcoFgKKq3pEMAMwAuCa5VK1W3SAfbAIopum+cy5KzwXn3M5AI6XVYlVt1mq1U8/zTlS1CeC9j2+6o1wuz1lrVzpWXLDWTg3pz/0CQnd2Jos49xUAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
                </div>
                <div class = "block-4 b-item">
                    <div class = "invalid-text">Неверный пароль</div>
                    <div class = "invalid-slider"></div>
                    <input id = "reg-password" type = "password" value = "" placeholder = "PASSWORD"
                           name = "passwd"
                           style = "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;"
                           class = "text-present">
                </div>
                <div class = "block-5 b-item">
                    <span class = "brown-label">Повторите пароль</span>
                </div>
                <div class = "block-6 b-item">
                    <div class = "invalid-text">Неверный пароль</div>
                    <div class = "invalid-slider"></div>
                    <input id = "reg-password-confirm" type = "password" value = "" placeholder = "PASSWORD"
                           name = "passwd2"
                           style = "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
                </div>
                <div class = "block-7 b-item">
                    <input id = "check-auth-subscribe" type = "checkbox" class = "jq-checkbox" name = "subscribe"
                           style = "position: absolute; left: -9999px;">

                    <label for = "check-auth-subscribe">
                        <span class = "brown-label">Подписаться на обновления в localmoto</span>
                    </label>
                </div>
                <div class = "block-8 b-item"><input class = "button-2 reg-ok-button" type = "submit"
                                                     disabled = "" value = "Зарегистрироваться"></div>
            </div>
            <div style = "clear:both;"></div>
        </div>
        <div class = "footer-block">
            <div class = "g-left">
                Нужна помощь?
                <span class = "t-grey phone-num">Звоните: 8 — 499 — 5603080</span>
            </div>
            <div class = "g-right">
                <div class = "block-1">
                    <a href = "#" class = "back-button" onclick = "auth_menu_show('auth');">Назад</a>
                </div>
                <div class = "block-2">
                    <label for = "check-auth-regsave" class = "t-grey">Запомнить?</label>
                    <input id = "check-auth-regsave" class = "jq-checkbox" type = "checkbox"
                           style = "position: absolute; left: -9999px;">

                    <span id = "check-auth-regsave-styler" class = "jq-checkbox jq-checkbox"
                          style = "display: inline-block">
                        <span></span>
                    </span>
                </div>
            </div>
            <div style = "clear:both;"></div>
        </div>

        <input id = "reg-fullname" type = "hidden" name = "name" value = "">
        <input id = "reg-username" type = "hidden" name = "username" value = "admin@mail.com">
        <input id = "reg-email1" type = "hidden" name = "email" value = "admin@mail.com">
        <input id = "reg-email2" type = "hidden" name = "email2" value = "admin@mail.com">

        <input type = "hidden" name = "0b07f8424af411dd48f4eed6d25049dd" value = "1"> <input type = "hidden"
                                                                                             name = "ajax"
                                                                                             value = "">
        <input type = "hidden" name = "socialType" value = "">
        <input type = "hidden" name = "socialId" value = "">
    </form>
</div>

<div class = "g-block reg-complete-block">
    <div class = "block-1">
        <span class = "t-brown"></span>
    </div>
    <div class = "block-2">
        <span class = "brown-label"></span>
    </div>
    <div class = "block-3">
        <span class = "t-grey">Вы зарегистрированы
            <br>
                               спасибо, что вы с нами
        </span>
        <br>
        <a href = "#" class = "close-button">Продолжить</a>
    </div>
</div>

<div class = "g-block pass-remind-block">
    <div class = "form-block">
        <div class = "g-left t-brown">
            Пожалуйста, введите ваш e-mail,
            <br>
            на который будет отправлен новый пароль
            <br>
            или заново
            <span class = "reg-button" onclick = "auth_menu_show('reg');">зарегистрируйтесь</span>
        </div>
        <div class = "g-right">
            <div class = "block-1 b-item">
                <div class = "invalid-text">Пароль неверный</div>
                <div class = "invalid-slider"></div>
                <input id = "passremind-email" name = "email" type = "text" value = "" placeholder = "EMAIL"
                       class = "text-present"
                       style = "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
            </div>
            <div class = "block-2"><input class = "button-2 passremind-ok-button" type = "submit" disabled = "" id = "recPass"
                                          value = "Отправить"></div>
        </div>
        <div style = "clear:both;"></div>
    </div>
    <div class = "footer-block">
        <div class = "g-left">
            Нужна помощь?
            <span class = "t-grey phone-num">Звоните: 8 — 499 — 5603080</span>
        </div>
        <div class = "g-right">
            <div class = "block-1">
                <a href = "#" class = "back-button" onclick = "auth_menu_show('auth');">Назад</a>
            </div>
        </div>
        <div style = "clear:both;"></div>
    </div>
</div>

</div>

<div class = "menu-slider"></div>
</div>
<!--------------------------------------------------- #LOGIN REGISTER ------------------------------------------------>


</header>


<div class = "main">
    <?= $content ?>
</div>

</div>

<div class = "footer mobile_footer_main">


    <?php

        function getRedirect($sort) {
            return AddExtend::baseUrl(true) . 'site/changeSorting/sorting/' . $sort;
        }

        if (is_null($category) || count($category) == 0) {
            $category[] = new Category();
        }

        if (sizeof($category) == 1) {
            $category[] = new Category();
        }

        foreach ($category as $key => $_nextCat) {
            if ($key > 1) // Не отображаем больше 2 категорий
                break;
            echo '<nav class = "f_staff"><span class = "f_staff">' . $_nextCat->name . (($_nextCat->name) ? ':' : '' ) . '</span><p style="margin-top: 10px">';
            if (isset( $_nextCat->childNode ) && sizeof($_nextCat->childNode)) {
                foreach ($_nextCat->childNode as $_child) {
                    echo "<a href='" . getUrlCat($_child->id) . "'>{$_child['name']}</a>";
                }
            }
            if ($key == 1) {
                echo '<a class = "f_male" href = "' . getRedirect(2) . '" style="float: left;">В</a>
                          <a class = "f_male" href = "' . getRedirect(1) . '" style="float: left;">М</a>
                          <a class = "f_male" href = "' . getRedirect(0) . '" style="float: left;">Ж</a>';
            }
            echo "</p></nav>";
        }
    ?>

    <nav class = "f_nav_wrap">

        <a href = "<?= AddExtend::baseUrl() ?>" class = "f_logo"><img
                src = "<?= AddExtend::pathMedia() ?>images/logo_new.png" width="115"></a>

        <div class = "f_nav">
            <!-- The class on the root UL tag was changed to match the Blueprint nav style -->
            <ul class = "joomla-nav">
                <li class = "item169">
                    <a href = "<?= AddExtend::baseUrl(true) ?>site/about">
                        О компании
                    </a>
                </li>
                <li class = "item170">
                    <a href = "<?= AddExtend::baseUrl(true) ?>site/contacts">
                        Контакты
                    </a>
                </li>
                <li class = "item171">
                    <a href = "<?= AddExtend::baseUrl(true) ?>site/pay">
                        Оплата
                    </a>
                </li>
                <li class = "item172">
                    <a href = "<?= AddExtend::baseUrl(true) ?>site/delivery">Доставка</a>
                </li>
                <li class = "item173">
                    <a href = "<?= AddExtend::baseUrl(true) ?>site/change">Возврат товара</a>
                </li>
<!--                <li class = "item175">-->
<!--                    <a href = "#">Faq</a>-->
<!--                </li>-->
<!--                <li class = "item174">-->
<!--                    <a href = "#">Блог</a>-->
<!--                </li>-->
            </ul>


        </div>

    </nav>

    <div class = "f_contacts">

        <div class = "subscribe">
            <form>
                <span>Подписаться на обновления в Local Moto:</span>
                <input id = "subscribe_input" name = "email" type = "text" value = "" placeholder = "EMAIL"
                       autocomplete = "off">
                <input id = "subscribe_submit" type = "submit" value = "отправить">
            </form>
        </div>

        <script>

            jQuery(document).ready(function () {

                $('a.f_male').on('click', function (event) {

                    event.preventDefault();

                    var url = this.href;

                    $.ajax({

                               url     : url,
                               success : function (data) {
                                   document.location.reload();
                               }
                           });

                    return false;

                });

            });

        </script>

        <a href = "/index.php?option=com_content&amp;view=article&amp;id=10:contacts&amp;catid=2&amp;Itemid=121"
           class = "feedback">Обратная связь
        </a>
        <a href = "/lm/index.php?option=com_content&amp;view=article&amp;id=10:contacts&amp;catid=2&amp;Itemid=121"
           class = "desktop_site">Перейти на стандартное отображение сайта
        </a>

        <div class = "f_address">
            <span class = "f_address_text"><?= $contacts[ 'address' ] ?></span>
            <span class = "f_address_phone"><?= $contacts[ 'phone' ][ 0 ] ?> — <?= $contacts[ 'phone' ][ 1 ] ?>
                                                                             — <?= $contacts[ 'phone' ][ 2 ] ?></span>
        </div>

        <div class = "f_line2"></div>

    </div>


    <script>
        jQuery(document).ready(function () {
            jQuery(".footer").addClass("mobile_footer_main");
        });
    </script>

</div>

</div>


<div class = "blackBg" style = "position: fixed; opacity: 0;">

</div>
</body>
</html>
