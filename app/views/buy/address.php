<div class = "edit_ship_to order_page on" >

    <div class = "order_process">
        <!----------------------------- ADDRESS ------------------------------->
        <div class = "op_main" id = "addrBlock" >
            <?php
                /**
                 * @var int     $numAddr
                 * @var Address $_address
                 */
                foreach ($address as $numAddr => $_address) {
                    $selected = '';
                    if ($numAddr == 0)
                        $selected = 'op_selected';
                    ?>
                    <div class = "op_address_block <?= $selected ?> blocks_address" id = "attrID_<?= $_address['id'] ?>"><!-- -|op_selected-->
                        <div class = "op_address_right">
                            <a class = "small_button op_address_check" href = "javascript:void(0)"
                               onclick = "select_address(this)">
                                <span></span>
                            </a>
                            <a class = "small_button" href = "javascript: void(0)" onclick = "return editAddress(this);"> Редактировать</a>
                        </div>
                        <div class = "op_title"> <?= intval($numAddr + 1) ?> адрес</div>
                        <div class = "op_value" id="idAddr" style="display: none!important;"> <?= $_address['id'] ?> </div>
                        <div class = "op_value" id="countryAddr"> <?= $_address['country'] ?> </div>
                        <div class = "op_value" id="cityAddr"> <?= $_address['city'] ?></div>
                        <div class = "op_value" id="streetAddr"> <?= $_address['street'] ?></div>
                        <div class = "op_value" id="houseAddr"> <?= $_address['house'] ?> </div>
                        <div class = "op_value" id="departmentAddr"> <?= $_address['department'] ?> </div>
                    </div>
                <?php } ?>
            <div class = "op_address_block popup_address_begin">
                <div class = "op_address_right">
                    <a class = "small_button op_address_check" href = "javascript:void(0)"
                       onclick = "select_address(this)">
                        <span></span>
                    </a>
                </div>
                <div class = "op_title">Забрать самостоятельно</div>
            </div>
            <div class = "op_block">
                <div class = "op_block_right">
                    <a class = "brown_button" href = "javascript: void(0)" id = "add_address" >Добавить адрес</a>
                </div>
            </div>
        </div>

        <!----------------------------- ADDRESS FOOTER ------------------------------->

        <div class = "op_footer">
            <a class = "brown_button next_step" href = "javascript: void(0);" id = "ship_next">Далее</a>
        </div>
        <div class = "clearfix"></div>

        <div id = "personal_popup" class = "pc_popup address" style = "display: none;" >
            <div class = "pc_popup_block">
                <form method = "post" id = "addressUpdate" name = "userForm" action = "#"
                      _lpchecked = "1">

                    <input type = "text" id = "personal-popup-country" name = "country" value = "" placeholder = "Код Страна">
                    <input type = "text" id = "personal-popup-city" name = "city" value = ""
                           placeholder = "Город">

                    <input type = "text" id = "personal-popup-street" name = "street" value = ""
                           placeholder = "Улица">
                    <input type = "text" id = "personal-popup-house" name = "house" value = ""
                           placeholder = "Дом">
                    <input type = "text" id = "personal-popup-department" name = "department" value = ""
                           placeholder = "Квартира">

                    <div class = "mask" id = "mask-huw6v991" style = "display: none;"></div>

                </form>


                <div class = "pc_save">
                    <a href = "javascript: void(0)" class = "brown_button" id = "personal_save"
                       onclick = "saveAddress(this);">
                        Сохранить
                    </a>
                </div>

                <div class = "clearfix"></div>
            </div>
            <div class = "pc_popup_slider" onclick = "close_personal_popup()"></div>
        </div>
    </div>
</div>