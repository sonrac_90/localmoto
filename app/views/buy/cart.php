<?php

    AddExtend::regScriptFile(
        array(
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            AddExtend::pathJs() . 'cartHelper.js',
            AddExtend::pathJs() . 'cart_lm.js',
            AddExtend::pathJs() . 'admin/phpFunc.js',
        )
    );
    AddExtend::regCssFile(array (
            AddExtend::pathCss() . 'order.css',
            AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css',
        ), 'screen'
    );
    $this->breadcrumbs = array(
        'Корзина' => array( AddExtend::baseUrl() . 'buy/index' )
    );
?>

    <div class = "order_process">
        <!----------------------------- ORDER TITLE ------------------------------->


        <div class = "pc_title">
            <span></span>
            <br>
            <span>Оформление заказа</span>
        </div>

        <!----------------------------- ORDER CART ------------------------------->
        <div class = "pc_cart_block">
            <div class = "pc_cart active">
                <span class = "cart_title">Корзина</span>
                <span class = "cart_number"><?= count($goodBuy) ?></span>
            </div>
        </div>


        <!----------------------------- TITLE BLOCK ------------------------------->
        <div class = "op_title_block step_1"  ><!-- -|title_1...3 -->
            <div class = "op_title step_1" >
                <span class = "s_title next_step" id = "title_step_1">01</span>
                <br>
                <span class = "s_descr">Оформление</span>
            </div>
            <div class = "op_title step_2" >
                <span class = "s_title next_step" id = "title_step_2">02</span>
                <br>
                <span class = "s_descr">Личные данные</span>
            </div>
            <div class = "op_title step_3">
                <span class = "s_title next_step" id = "title_step_3">03</span>
                <br>
                <span class = "s_descr">Адресса доставки</span>
            </div>
        </div>
        <div class = "clearfix"></div>

    </div>

    <div class = "product_list order_page on">


        <div class = "order_process">

            <!----------------------------- PRODUCTS ------------------------------->
            <div class = "op_main">

                <?php
                    $sum = 0;
                    if (count($goodBuy))
                        foreach ($goodBuy as $key => $nextGood) {
                            $sum += $nextGood[ 'good_price' ] * $nextGood[ 'count' ];
                            ?>
                            <div class = "prow<?= $key ?> op_product" data-price = "<?= $nextGood[ 'good_price' ] ?>"
                                 data-total_price = "<?= $nextGood[ 'price' ] * $nextGood[ 'count' ] ?>"
                                 data-quantity = "<?= $nextGood[ 'count' ] ?>" data-key = "<?= $key ?>">

                                <div class = "prod_image"><img
                                        src = "<?= AddExtend::baseUrl() . Photo::PATH_IMAGE . CropImage::dynamicPath($nextGood[ 'photo_id' ]) . "/" . $nextGood[ 'photo_url' ] ?>"
                                        alt = "">
                                </div>
                                <div class = "prod_header">
                                    <div>Наименование</div>
                                    <div>Размер</div>
                                    <div>Цвет</div>
                                    <div>Код товара</div>
                                    <div>Производитель</div>
                                </div>
                                <div class = "prod_details">
                                    <div><?= $nextGood[ 'good_name' ] ?></div>
                                    <div><?= $nextGood[ 'good_size' ] ?></div>
                                    <div><?= $nextGood[ 'good_color' ] ?></div>
                                    <div><?= $nextGood[ 'good_model' ] ?></div>
                                    <div><?= $nextGood[ 'good_producer' ] ?></div>
                                </div>
                                <div class = "prod_kol">
                                    <div class = "prod_kol_1">Кол-во</div>
                                    <div class = "prod_kol_2"><?= $nextGood[ 'count' ] ?></div>
                                    <div class = "prod_kol_3">
                                        <a href = "javascript: void(0)" class = "small_button pm_but" data-pm = "+"
                                           data-row = "<?= $key ?>">+
                                        </a>
                                        <a href = "javascript: void(0)" class = "small_button pm_but" data-pm = "-"
                                           data-row = "<?= $key ?>">-
                                        </a>
                                    </div>

                                    <form action = "<?= AddExtend::baseUrl() ?>buy/updateCountGood/" method = "post"
                                          class = "quan_change_form">
                                        <input type = "hidden" name = "count" data = "<?= $key ?>" class = "input_quan"
                                               value = "1">
                                    </form>

                                </div>
                                <div class = "prod_price">
                                    <div class = "sum_value"><?= $nextGood[ 'count' ] * $nextGood[ 'good_price' ] ?>Р
                                    </div>
                                    <div class = "sum_title">Цена</div>
                                </div>
                                <div class = "prod_del">
                                    <a class = "small_button"
                                       href = "<?= AddExtend::baseUrl(true) ?>buy/cart/id/<?= $key ?>">Удалить
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
            </div>
            <!----------------------------- PRODUCTS FOOTER ------------------------------->

            <div class = "op_products_footer">
                <!--            <div class = "prod_promo">-->
                <!--                <div>Промо-код:</div>-->
                <!--                <form method = "post" id = "enterCouponCodeForm" name = "enterCouponCode"-->
                <!--                      action = "/index.php?option=com_virtuemart&amp;view=cart">-->
                <!--                    <input type = "text" name = "coupon_code" size = "20" maxlength = "50" class = "coupon"-->
                <!--                           alt = "Введите код" value = "Введите код"-->
                <!--                           onblur = "if(this.value=='') this.value='Введите код';"-->
                <!--                           onfocus = "if(this.value=='Введите код') this.value='';">-->
                <!---->
                <!--                    <input class = "brown_button" type = "submit" value = "Активировать">-->
                <!---->
                <!--                    <input type = "hidden" name = "option" value = "com_virtuemart">-->
                <!--                    <input type = "hidden" name = "view" value = "cart">-->
                <!--                    <input type = "hidden" name = "task" value = "setcoupon">-->
                <!--                    <input type = "hidden" name = "controller" value = "cart">-->
                <!--                </form>-->
                <!---->
                <!--            </div>-->

                <div class = "prod_sum" data-discount = "0" data-desc_per = "0">
                    <div class = "prod_sum_block prod_sum_1">
                        <div class = "sum_value"><?= $sum ?> Р</div>
                        <div class = "sum_title">Ваш заказ на сумму:</div>
                    </div>
                    <!--                <div class = "prod_sum_block prod_sum_2">-->
                    <!--                    <div class = "sum_value">0 P</div>-->
                    <!--                    <div class = "sum_title">Ваша скидка:</div>-->
                    <!--                </div>-->
                    <div class = "prod_sum_block prod_sum_3">
                        <div class = "sum_value"><?= $sum ?> P</div>
                        <div class = "sum_title">Итого:</div>
                    </div>
                    <div class = "clearfix"></div>
                    <div class = "prod_sum_4">
                        <a class = "brown_button next_step" href = "javascript: void(0);" id = "confirm_order">Оформить
                                                                                                               заказ
                        </a>
                    </div>
                </div>
            </div>
            <div class = "clearfix"></div>

        </div>

    </div>

<?php
    $this->renderPartial(
        'userInfo', array(
            'user' => $user
        )
    );
    $this->renderPartial(
        'address', array(
            'address' => $address
        )
    );
    $this->renderPartial('credit');
?>