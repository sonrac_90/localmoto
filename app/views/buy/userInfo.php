<div class = "edit_bill_to order_page">


    <div class = "order_process">


        <!----------------------------- PERSONAL DATA ------------------------------->
        <div class = "op_main">
            <div class = "op_block" id = "pinfo_name">
                <div class = "op_title">Имя</div>
                <div class = "op_value"><?= $user->first_name ?></div>
            </div>
            <div class = "op_block" id = "pinfo_surname">
                <div class = "op_title">Фамилия</div>
                <div class = "op_value"><?= $user->last_name ?></div>
            </div>
            <div class = "op_block" id = "pinfo_email">
                <div class = "op_title">Email</div>
                <div class = "op_value"><?= $user->email ?></div>
            </div>

            <?php
                $nodata = '';
                if (empty($user->phone))
                    $nodata = 'nodata';
            ?>

            <div class = "op_block  <?= $nodata ?>" id = "pinfo_phone"><!-- -|nodata -->
                <?php if ($nodata) { ?>
                <div class = "op_nodata">Добавьте пожалуйста свой номер телефона</div>
                <?php } ?>
                <div class = "op_title">Телефон</div>
                <div class = "op_value"><?= Users::formatPhone($user->phone) ?></div>
            </div>

            <?php
                $nodata = '';
                if (empty($user->birthday))
                    $nodata = 'nodata';
            ?>
            <div class = "op_block  <?= $nodata ?>" id = "pinfo_birthdate">
                <?php if ($nodata) ?>
                <div class = "op_nodata">Добавьте пожалуйста дату рождения</div>
                <div class = "op_title">Дата рождения</div>
                <div class = "op_value"><?= date('d-m-Y', $user->birthday) ?></div>
            </div>


            <div class = "op_block">
                <div class = "op_block_right">
                    <a class = "brown_button" href = "javascript:void(0)"
                       onclick = "javascript: open_personal_popup(1);">Редактировать
                                                                       данные
                    </a>
                </div>
            </div>
        </div>

        <!----------------------------- PERSONAL FOOTER ------------------------------->

        <div class = "op_footer">

            <a class = "brown_button next_step" href = "javascript: void(0);" id = "bill_next">Далее</a>
        </div>
        <div class = "clearfix"></div>

        <!----------------------------- MAIN POPUP ------------------------------->
        <div id = "personal_popup" class = "pc_popup" style = "display: none;">
            <div class = "pc_popup_block">
                <form method = "post" id = "all_update_user_info" name = "userForm"
                      action = "/index.php?option=com_virtuemart">

                    <input type = "text" id = "personal-popup-name" name = "first_name" value = "<?= $user->first_name ?>" placeholder = "Имя">
                    <input type = "text" id = "personal-popup-surname" name = "last_name" value = "<?= $user->last_name ?>"
                           placeholder = "Фамилия">

                    <input type = "text" id = "personal-popup-email" name = "email" value = "<?= $user->email ?>" placeholder = "E-mail">
                    <input type = "text" id = "personal-popup-phone" name = "phone" value = "<?= $user->phone ?>"
                           placeholder = "Ведите свой номер телефона">

                    <div class = "pc_birthday_title">Дата рождения</div>
                    <div class = "pc_birthday">
                        <?php
                            $dateRange = '1920:' . date("Y",time());
                            $date = new DateTime();
                            $date->modify('- 18 year');
                            $date->modify('- 1 day');
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array (
                                    'name'        => 'date_birth',
                                    'model'       => $user,
                                    'value'       => date('d-m-Y', $user->birthday),
                                    'language'    => 'ru',
                                    'options'     => array(
                                        'showAnim'        => 'slide',
                                        'dateFormat'      => 'dd-mm-yy',
                                        'changeMonth'     => true,
                                        'changeYear'      => true,
                                        'yearRange'       => $dateRange,
                                        'minDate'         => '01-01-1920',
                                        'maxDate'         => $date->format('d-m-Y'),
                                        'showButtonPanel' => true,
                                        'beforeShow'      => "js: function (input, inst) {
                                                inst.dpDiv.css({
                                                    'margin-left': '-16px',
                                                });
                                        }"
                                    ),
                                    'htmlOptions' => array(
                                        'id' => 'birthday_date_field'
                                    )
                                )
                            );
                        ?>
                    </div>
                </form>


                <div class = "pc_save">
                    <a href = "javascript: void(0)" class = "brown_button" id = "personal_save"
                       onclick = "close_personal_popup();">
                        Сохранить
                    </a>
                </div>

                <div class = "clearfix"></div>
            </div>
            <div class = "pc_popup_slider" onclick = "close_personal_popup()"></div>
        </div>

    </div>
</div>