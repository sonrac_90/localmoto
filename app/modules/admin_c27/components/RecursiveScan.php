<?php

    class RecursiveScan {
        public static function RemoveDir($path, $time = 48000, $removeParentFolder = false) {
            if (file_exists($path) && is_dir($path)) {
                $dirHandle = opendir($path);
                while (false !== ( $file = readdir($dirHandle) )) {
                    if ($file != '.' && $file != '..') { // исключаем папки с назварием '.' и '..'
                        $tmpPath = $path . '/' . $file;

                        if (is_dir($tmpPath)) { // если папка
                            $stat = stat($tmpPath);
                            if (date("d-m-Y H:i:s", $stat[ 'mtime' ]) < date("d-m-Y H:i:s", time() - $time))
                                self::RemoveDir($tmpPath);
                        } else {
                            if (file_exists($tmpPath)) { // удаляем файл
                                if (date("d-m-Y H:i:s", filectime($tmpPath)) < date("d-m-Y H:i:s", time() - $time))
                                    unlink($tmpPath);
                            }
                        }
                    }
                }
                closedir($dirHandle);

                // удаляем текущую папку
                if (file_exists($path) && $removeParentFolder) {
                    rmdir($path);
                }

                return true;
            }

            return false;
        }
    }