<?php

    class ValidateGoods {

        protected $validateResult = array();
        private   $params;
        private   $cat;
        private   $savingData     = array();

        public function __construct(&$params, &$cat) {
            $this->cat = $cat;
            $this->params = $params;
            $this->validateResult[ 'photo' ] = array();
            $this->validateResult[ 'good' ] = array();
            $this->validateResult[ 'CategoryGoods' ] = array();
        }

        /**
         * Возвращает объекты, которые необходимо сохранить и/или обновить
         *
         * @return array
         */
        public function getSaveData() {
            return $this->savingData;
        }

        public function getValidateResult() {
            return $this->validateResult;
        }

        public function validate() {
            $this->validateGood($this->params);
            $this->validateCategories($this->cat);
        }

        /**
         * Валидация модели товара
         *
         * @param array $goods
         */
        protected function validateGood($goods) {
            $good = new Goods();
            $good->price = $goods[ 'name' ];
            $good->sex_characters = $goods[ 'sex_characters' ];
            $good->producer_id = $goods[ 'producerName' ];
            $good->name = $goods[ 'name' ];
            if (intval($good->producer_id) <= 0)
                $good->producer_id = null;

            $this->savingData[ 'good' ] = $goods;

            $this->validateResult[ 'good' ] = $this->validateModel($good);
            if (isset( $goods[ 'photos' ] ) && is_array($goods[ 'photos' ])) {
                $this->validatePhotos($goods[ 'photos' ]);
            }

            if (isset( $this->params[ 'CategoryGoods' ] )) {
                $this->validateCategories($this->params[ 'CategoryGoods' ]);
            }
        }

        protected function validateModel($model) {
            return CJSON::decode(CActiveForm::validate($model));
        }

        /**
         * Валидация массива фотографий и данных с ней связанных (размеры, цвета, описания)
         *
         * @param array $photos
         */
        protected function validatePhotos($photos) {
            foreach ($photos as $keyPhoto => $_photo) {
                $photo = new Photo();
                $photo->url = $_photo[ 'url' ];
                if (isset( $_photo[ 'id' ] ))
                    $photo->id = $_photo[ 'id' ];

                if (isset( $_photo[ 'goods_id' ] ))
                    $photo->goods_id = $_photo[ 'goods_id' ];

                $this->validatePhoto($photo, $keyPhoto);
                if (isset( $_photo[ 'sizes' ] ) && is_array($_photo[ 'sizes' ]))
                    $this->validateSizes($_photo[ 'sizes' ], $keyPhoto);
                else {
                    $this->validateSizes(array(0 => array('size' => '')), $keyPhoto);
                }
                if (isset( $_photo[ 'color' ] )) {
                    $this->validateColors($_photo[ 'color' ], $keyPhoto);
                }
                if (isset( $_photo[ 'description' ] ) && is_array($_photo[ 'description' ]))
                    $this->validateDescription($_photo[ 'description' ], $keyPhoto);

            }
        }

        /**
         * Сохранение результата валидации очередной модели фотографии
         *
         * @param Photo   $photo
         * @param integer $numPhoto
         */
        private function validatePhoto($photo, $numPhoto) {
            if (!is_array($this->validateResult[ 'photo' ][ $numPhoto ]))
                $this->validateResult[ 'photo' ][ $numPhoto ] = array();

            if (!is_array($this->savingData[ 'photo' ][ $numPhoto ]))
                $this->savingData[ 'photo' ][ $numPhoto ] = array();

            $this->savingData[ 'photo' ][ $numPhoto ][ 'photo' ] = $photo;
            $this->validateResult[ 'photo' ][ $numPhoto ] = $this->validateModel($photo);
        }

        /**
         * Валидация массива размеров
         *
         * @param GoodsSize $sizes
         * @param number    $numPhoto
         */
        protected function validateSizes($sizes, $numPhoto) {
            foreach ($sizes as $kSize => $_size) {
                $size = new GoodsSize();
                $size->size = $_size;
                $this->validateSize($size, $numPhoto, $kSize);
            }
        }

        /**
         * Валидация модели размера товара
         *
         * @param GoodsSize $size
         * @param integer   $numPhoto
         * @param integer   $numSize
         */
        private function validateSize($size, $numPhoto, $numSize) {
            if (!is_array($this->validateResult[ 'photo' ][ $numPhoto ][ 'size' ])) {
                $this->validateResult[ 'photo' ][ $numPhoto ][ 'size' ] = array();
            }

            if (!is_array($this->savingData[ 'photo' ][ $numPhoto ][ 'size' ]))
                $this->savingData[ 'photo' ][ $numPhoto ][ 'size' ] = array();

            $this->savingData[ 'photo' ][ $numPhoto ][ 'size' ] = $size;
            $this->validateResult[ 'photo' ][ $numPhoto ][ 'size' ][ $numSize ] = $this->validateModel($size);
        }

        /**
         * Валидация моделей цвета
         *
         * @param ColorGoods $colors
         * @param integer    $numPhoto
         */
        protected function validateColors($colors, $numPhoto) {
//            foreach ($colors as $kColor => $_color) {
//                $color = new ColorGoods();
//                $color->attributes = $_color;
//
//                $this->validateColor($color, $numPhoto, $kColor);
//            }
            $color = new ColorGoods();
            $color->color = trim($colors);
            $this->validateColor($color, $numPhoto);

        }

        /**
         * Валидация модели цвета
         *
         * @param ColorGoods $color
         * @param integer    $numPhoto
         * @param integer    $numColor
         */
        protected function validateColor($color, $numPhoto) {
            if (!is_array($this->validateResult[ 'photo' ][ $numPhoto ][ 'color' ]))
                $this->validateResult[ 'photo' ][ $numPhoto ][ 'color' ] = array();

            if (!is_array($this->savingData[ 'photo' ][ $numPhoto ][ 'color' ]))
                $this->savingData[ 'photo' ][ $numPhoto ][ 'color' ] = array();

            $this->savingData[ 'photo' ][ $numPhoto ][ 'color' ] = $color;

            $this->validateResult[ 'photo' ][ $numPhoto ][ 'color' ] = $this->validateModel($color);
        }

        /**
         * Валидация описания изображения
         *
         * @param $description
         * @param $numPhoto
         */
        protected function validateDescription($description, $numPhoto) {
            $descript = new DescriptionGoods();
            $descript->attributes = $description;

            $this->savingData[ 'photo' ][ $numPhoto ][ 'description' ] = $descript;

            $this->validateResult[ 'photo' ][ $numPhoto ][ 'description' ] = $this->validateModel($descript);
        }

        /**
         * Валидация моделей категории
         *
         * @param GoodsCategory $categories
         */
        protected function validateCategories($categories) {
            foreach ($categories as $kCategory => $_category) {
                $category = new GoodsCategory();
                $category->attributes = $_category;

                $this->validateCategory($category, $kCategory);
            }

        }

        /**
         * Валидация модели категории
         *
         * @param GoodsCategory $category
         * @param integer       $numCategory
         */
        protected function validateCategory($category, $numCategory) {
            if (!is_array($this->validateResult[ 'CategoryGoods' ][ $numCategory ]))
                $this->validateResult[ 'CategoryGoods' ][ $numCategory ] = array();

            if (!is_array($this->savingData[ 'CategoryGoods' ][ $numCategory ]))
                $this->savingData[ 'CategoryGoods' ][ $numCategory ] = array();

            if (intval($category->category_id) <= 0)
                $category->category_id = null;

            $this->savingData[ 'CategoryGoods' ][ $numCategory ] = $category;

            $this->validateResult[ 'CategoryGoods' ][ $numCategory ] = $this->validateModel($category);
        }
    }