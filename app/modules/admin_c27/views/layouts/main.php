<!doctype html>
<html lang = "ru">

<head>
    <script>
        var adminUrl = '<?=AddExtend::getURL($this, '')?>';
        var baseUrl = '<?=AddExtend::baseUrl()?>';
        // Заглушка в IE для console
        if (typeof console === 'undefined')
            console = {
                log : function (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) {

                }
            };
        var controller = "<?=$this->id?>";
    </script>

    <?php AddExtend::regCoreScript('jquery'); ?>
    <meta charset = "UTF-8" />

    <link rel = "stylesheet" href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/bootstrap.min.css" />

    <link rel = "stylesheet" href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/AdminLTE.css" />

    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/font-awesome.min.css" rel = "stylesheet"
          type = "text/css" />
    <!-- Ionicons -->
    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/ionicons.min.css" rel = "stylesheet"
          type = "text/css" />
    <!-- Morris chart -->
    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/morris/morris.css" rel = "stylesheet"
          type = "text/css" />
    <!-- jvectormap -->
    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/jvectormap/jquery-jvectormap-1.2.2.css"
          rel = "stylesheet" type = "text/css" />
    <!-- fullCalendar -->
    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/fullcalendar/fullcalendar.css"
          rel = "stylesheet" type = "text/css" />
    <!-- Daterange picker -->
    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/daterangepicker/daterangepicker-bs3.css"
          rel = "stylesheet" type = "text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link
        href = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
        rel = "stylesheet" type = "text/css" />

    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/css/admin/admin.css" rel = "stylesheet"
          type = "text/css" />

    <!--    <script type="text/javascript" src="-->
    <? //=AddExtend::baseUrl(true)?><!--/bootstrap3/js/bootstrap.min.js"></script>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src = "https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src = "https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title><?= Yii::app()->name ?> - <?= $this->pageTitle ?></title>
    <script src = "<?= AddExtend::baseUrl(true) ?>/shared/media/js/admin/formStyle.js"></script>
    <link href = "<?= AddExtend::baseUrl(true) ?>shared/media/css/admin/dialog.css" rel = "stylesheet"
          type = "text/css" />

</head>

<body class = "skin-black fixed wysihtml5-supported pace-done">
<!-- header logo: style can be found in header.less -->
<header class = "header">
    <a href = "<?= AddExtend::getURL($this); ?>" class = "logo" style = "line-height: 0; padding: 0">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <img src = "<?= AddExtend::baseUrl(true); ?>shared/media/images/logo_new_admin.png">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class = "navbar navbar-static-top" role = "navigation">
        <!-- Sidebar toggle button-->
        <a href = "#" class = "navbar-btn sidebar-toggle" data-toggle = "offcanvas" role = "button">
            <span class = "sr-only">Toggle navigation</span>
            <span class = "icon-bar"></span>
            <span class = "icon-bar"></span>
            <span class = "icon-bar"></span>
        </a>
        <div class = "navbar-right">
            <ul class = "nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class = "dropdown user user-menu">
                    <div class = "pull-right" style = "margin-top: 7px;">
                        <a href = "<?= Yii::app()->createUrl('site/index') ?>"
                           class = "btn btn-default btn-flat" target = "_blank">На сайт
                        </a>
                    </div>
                </li>
                <li class = "dropdown user user-menu">
                    <div class = "pull-right" style = "margin-top: 7px;">
                        <a href = "<?= Yii::app()->createUrl(AddExtend::getURL($this, '/default/logout', false)) ?>"
                           class = "btn btn-default btn-flat">Выход (<?= Yii::app()->user->username ?>)
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class = "wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class = "left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class = "sidebar">
            <ul class = "sidebar-menu">
                <li>
                    <a href = "<?= AddExtend::getURL($this, '/users/index') ?>">
                        <i class = "fa fa-users<?= AddExtend::activeLink('/users') ?>"></i>
                        <span>Пользователи</span>
                    </a>
                </li>
                <li>
                    <a href = "<?= AddExtend::getURL($this, '/mainpage/index') ?>">
                        <i class = "fa fa-desktop<?= AddExtend::activeLink('/mainpage') ?>"></i>
                        <span>Главная страница</span>
                    </a>
                </li>
                <li>
                    <a href = "<?= AddExtend::getURL($this, '/category/index') ?>">
                        <i class = "fa fa-list<?= AddExtend::activeLink('/category') ?>"></i>
                        <span>Категории</span>
                    </a>
                </li>
                <li>
                    <a href = "<?= AddExtend::getURL($this, '/producer/index') ?>">
                        <i class = "fa fa-archive<?= AddExtend::activeLink('/producer') ?>"></i>
                        <span>Производители</span>
                    </a>
                </li>
                <li>
                    <a href = "<?= AddExtend::getURL($this, '/goods/index') ?>">
                        <i class = "fa fa-briefcase<?= AddExtend::activeLink('/goods') ?>"></i>
                        <span>Товары</span>
                    </a>
                </li>
                <li>
                    <a href = "<?= AddExtend::getURL($this, '/orders/index') ?>">
                        <i class = "fa fa-euro<?= AddExtend::activeLink('/orders') ?>"></i>
                        <span>Заказы</span>
                    </a>
                </li>
                <li class = "treeview<?= AddExtend::activeLink(array('/delivery', '/pay', '/change', '/contacts', '/phrase')) ?>">
                    <a href = "<?= AddExtend::getURL($this, '/contacts/index') ?>">
                        <i class = "fa fa-toggle-down<?= AddExtend::activeLink('/contacts') ?>"></i>
                        <span>О компании</span>
                        <i class = "fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class = "treeview-menu">
                        <li>
                            <a href = "<?= AddExtend::getURL($this, '/pay/index') ?>">
                                <i class = "fa fa-money<?= AddExtend::activeLink('/pay') ?>"></i>
                                <span>Способы оплаты</span>
                            </a>
                        </li>
                        <li>
                            <a href = "<?= AddExtend::getURL($this, '/delivery/index') ?>">
                                <i class = "fa fa-dashboard<?= AddExtend::activeLink('/delivery') ?>"></i>
                                <span>Доставка товара</span>
                            </a>
                        </li>
                        <li>
                            <a href = "<?= AddExtend::getURL($this, '/change/index') ?>">
                                <i class = "fa fa-bug<?= AddExtend::activeLink('/change') ?>"></i>
                                <span>Возврат товара</span>
                            </a>
                        </li>
                        <li>
                            <a href = "<?= AddExtend::getURL($this, '/contacts/index') ?>">
                                <i class = "fa fa-book<?= AddExtend::activeLink('/contacts') ?>"></i>
                                <span>Контактная информация</span>
                            </a>
                        </li>
                        <li>
                            <a href = "<?= AddExtend::getURL($this, '/phrase/index') ?>">
                                <i class = "fa fa-hand-o-right<?= AddExtend::activeLink('/phrase') ?>"></i>
                                <span>Фразы байкеров</span>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class = "right-side">
        <!-- Main content -->
        <section class = "content">
            <?= $content ?>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- ./wrapper -->

<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/jquery-ui-1.10.3.min.js"
        type = "text/javascript"></script>
<!-- Bootstrap -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/bootstrap.min.js"
        type = "text/javascript"></script>
<!-- AdminLTE App -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/AdminLTE/app.js"
        type = "text/javascript"></script>

</body>
</html>
