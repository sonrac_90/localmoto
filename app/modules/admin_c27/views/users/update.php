<?php
    AddExtend::regScriptFile(AddExtend::baseUrl() . "shared/media/js/admin/users.js");
    AddExtend::regCssFile(array(
                              AddExtend::pathCss() . 'admin/fix.css'
                          ), 'screen'
    );
?>
<script type = "text/javascript">
    var userID = <?= intval($user->id) ?>;
</script>
<div>

    <div>
        <?php
            echo '<div id="user_update_form_widget">';
            $form = $this->beginWidget(
                'CActiveForm', array(
                    'id'                   => 'user_update',
                    'action'               => AddExtend::getURL($this, $this->id . '/update/id/' . $_GET[ 'id' ]),
                    'enableAjaxValidation' => true,
                )
            );


            ?>
        <div class='row'>
            <div class="nextField">
                <?= $form->labelEx($user, 'last_name') ?>
                <?= $form->textField($user, 'last_name') ?>
                <?= $form->error($user, 'last_name'); ?>
            </div>
            <div class="nextField">
                <?= $form->labelEx($user, 'first_name') ?>
                <?= $form->textField($user, 'first_name') ?>
                <?= $form->error($user, 'first_name'); ?>
            </div>
            <div class="nextField">
                <?= $form->labelEx($user, 'username') ?>
                <?= $form->textField($user, 'username') ?>
                <?= $form->error($user, 'username'); ?>

            </div>
        </div>
        <div class="row">
            <div class="nextField">
                <?= $form->labelEx($user, 'role') ?>
                <?= $form->dropDownList($user, 'role', array(
                        'user'  => 'Пользователь',
                        'admin' => 'Администратор'
                    )
                ) ?>
                <?= $form->error($user, 'role'); ?>
            </div>
            <div class="nextField">
                <?= $form->labelEx($user, 'subscribe') ?>
                <?= $form->dropDownList($user, 'subscribe', array(
                        '0' => 'Не подписан',
                        '1' => 'Подписан'
                    )
                ) ?>
                <?= $form->error($user, 'last_name'); ?>
            </div>
            <div class="nextField">
                <?= $form->labelEx($user, 'birthday') ?>
                <?php
                    $dateRange = '1920:' . date("Y",time());
                    $date = new DateTime();
                    $date->modify('- 18 year');
                    $date->modify('- 1 day');
                    $this->widget(
                        'zii.widgets.jui.CJuiDatePicker', array(
                            'name'     => 'Users[birthday]',
                            'model'    => $user,
                            'value'    => date('d-m-Y', $user->birthday),
                            'language' => 'ru',
                            'options'  => array(
                                'showAnim'        => 'slide',
                                'dateFormat'      => 'dd-mm-yy',
                                'changeMonth'     => true,
                                'changeYear'      => true,
                                'yearRange'       => $dateRange,
                                'minDate'         => '01-01-1920',
                                'maxDate'         => $date->format('d-m-Y'),
                                'showButtonPanel' => true
                            ),
                        )
                    );
                ?>
                <?= $form->error($user, 'birthday'); ?>
            </div>
        </div>
        <div class="row">
            <div class="nextField">
                <?= $form->labelEx($user, 'email') ?>
                <?= $form->textField($user, 'email') ?>
                <?= $form->error($user, 'email'); ?>
            </div>
            <div class="nextField">
                <?= $form->labelEx($user, 'phone') ?>
                <?= $form->textField($user, 'phone') ?>
                <?= $form->error($user, 'phone'); ?>
            </div>
        </div>
    <?php
        echo "<div style='clear: both'>  </div>";
        echo '<div style="float: left;"><button id="addAddress" class="btn btn-primary" style="clear: both;">Добавить адрес</button></div>';
        echo "<div style='clear: both'></div>";


        echo "<table class='table table-bordered table-striped dataTable' style=\"width:777px\">";
        echo "<tr><td>№</td><td>Страна</td><td>Город</td><td>Улица</td><td>Дом</td><td>Квартира</td><td></td></tr>";
        foreach ($adresses as $keyAddress => $value) {
            if (isset( $value->attributes )) {
                $trShow = false;
                foreach ($value->attributes as $key => $attr) {
                    if ($key == 'id' || $key == 'user_id' || $key == 'date_add') {

                        echo $form->hiddenField($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' )) . "";

                    } else {

                        if (!$trShow)
                            echo "<tr><td class='numRowAddress'>" . intval($keyAddress + 1) . "</td>";
                        $trShow = true;

                        echo "<td>" . $form->textField($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' )) . "";

                        echo $form->error($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' )) . "</td>";

                    }
                }
                if ($trShow) {
                    echo "<td><div class='deleteIcon' number=\"{$value['id']}\"><img width=16 height=16 src='" . AddExtend::baseUrl() . "shared/media/css/admin/images/admin/delete.png'/></div></td>";
                    echo "</tr>";
                }
            }
        }
        echo "</table>";
    ?>
</div>
<?php echo "<div class='clear submitButton_user'>" . CHtml::submitButton('Сохранить', array( 'class' => 'btn btn-success' )) . "</button></div>";
    $this->endWidget();

?>
</div>