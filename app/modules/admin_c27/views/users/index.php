<a href = "<?= AddExtend::getURL($this, '/users/add') ?>" style = "color: white; text-decoration: none;"
   class = "btn btn-primary">
    Добавить нового
</a>
<?php
    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'               => 'users-list',
            'dataProvider'     => $users->search(),
            'ajaxType'         => 'POST',
            'filter'           => $users,
            'enablePagination' => true,
            'itemsCssClass'    => 'table table-bordered table-striped dataTable',
            'template'         => '{items}{pager}',
//            'filterCssClass'   => 'form-control',
            'pager'            => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'    => 'p',
            'columns'          => array(
                'id',
                'email',
                'username',
                'last_name',
                'first_name',
                'middle_name',
                'birthday',
                'city',
                'phone',
                array(
                    'name'   => 'role',
                    'value'  => function ($data) {
                        return ( ( ( $data->role == 'admin' ) ? 'Администратор' : 'Пользователь' ) );
                    },
                    'filter' => array(
                        '' => 'Все',
                        'admin' => 'Администратор',
                        'user'  => 'Пользователь',
                    ),
                ),
                array(
                    'name'   => 'subscribe',
                    'value'  => function ($data) {
                        return ( ( $data->subscribe ) ? 'Подписан' : 'Не подписан' );
                    },
                    'filter' => array(
                        '' => 'Все',
                        '0' => 'Не подписан',
                        '1' => 'Подписан'
                    )
                ),

                array(
                    'class'       => 'CButtonColumn',
                    'htmlOptions' => array(
                        'style' => 'width: 80px'
                    ),
                    'template'    => '<div style="width: 43px!important;">{update}{delete}</div>',
                    'buttons'     => array(
                        'update' => array(
                            'title' => 'Редактировать',
                            'imageUrl' => AddExtend::pathCss() . "admin/images/admin/edit.png"
                        ),
                        'delete' => array(
                            'title' => 'Удалить',
                            'imageUrl' => AddExtend::pathCss() . "admin/images/admin/delete.png"
                        ),
                    ),
                )
            )
        )
    );