<button id = "add" class = "btn btn-primary">
    Добавить нового
</button>
<?php

    AddExtend::regCssFile(
        array( AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css' ), 'screen'
    );

    AddExtend::regCoreScript('jquery.ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js');

    AddExtend::regScriptFile(
        array(
            AddExtend::baseUrl() . 'shared/media/js/admin/modal.js',
            AddExtend::baseUrl() . 'shared/media/js/admin/producer.js'
        )
    );

    $this->beginWidget(
        'zii.widgets.CListView', array(
            'dataProvider'       => $dataProvider,
            'itemView'           => '_producer',
            'id'                 => 'producerList',
            'ajaxUpdate'         => true,
            'htmlOptions'        => array(
                'class' => 'table table-stripped'
            ),
            'emptyText'          => 'Список производителей пуст',
            'template'           => '{sorter}<div style="clear: both;"></div> {items} <hr> {pager}{summary}',
            'sorterHeader'       => 'Сортировать по:',
            'sortableAttributes' => array(
                'name'
            ),

            'pager'              => array(
                'class'  => 'CLinkPager',
                'header' => false,
            )
        )
    );

    $this->endWidget();
?>

<div id = "modalProducer">

</div>