<!-- Small boxes (Stat box) -->
<div class = "row">
    <div class = "col-lg-4 col-xs-6">
        <!-- small box -->
        <div class = "small-box bg-aqua">
            <div class = "inner">
                <h3>
                    <?= $orders['all'] ?> / <?= $orders['buy'] ?>
                </h3>

                <p>
                    Количество заказов
                </p>
            </div>
            <div class = "icon">
                <i class = "ion ion-bag"></i>
            </div>
            <a href = "<?= AddExtend::getURL($this, 'orders/index') ?>" class = "small-box-footer">
                Перейти к заказам
                <i class = "fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
<!--    <div class = "col-lg-3 col-xs-6">-->
        <!-- small box -->
<!--        <div class = "small-box bg-green">-->
<!--            <div class = "inner">-->
<!--                <h3>-->
<!--                    53-->
<!--                    <sup style = "font-size: 20px">%</sup>-->
<!--                </h3>-->
<!--                <p>-->
<!--                    Bounce Rate-->
<!--                </p>-->
<!--            </div>-->
<!--            <div class = "icon">-->
<!--                <i class = "ion ion-stats-bars"></i>-->
<!--            </div>-->
<!--            <a href = "#" class = "small-box-footer">-->
<!--                More info-->
<!--                <i class = "fa fa-arrow-circle-right"></i>-->
<!--            </a>-->
<!--        </div>-->
<!--    </div>-->
    <!-- ./col -->
    <div class = "col-lg-4 col-xs-6">
        <!-- small box -->
        <div class = "small-box bg-yellow">
            <div class = "inner">
                <h3>
                    <?= $countUsers ?>
                </h3>

                <p>
                    Зарегистрировано пользователей
                </p>
            </div>
            <div class = "icon">
                <i class = "ion ion-person-add"></i>
            </div>
            <a href = "<?= AddExtend::getURL($this, 'users/index') ?>" class = "small-box-footer">
                Управление пользователями
                <i class = "fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class = "col-lg-4 col-xs-6">
        <!-- small box -->
        <div class = "small-box bg-red">
            <div class = "inner">
                <h3>
                    <?= $total ?>
                </h3>

                <p>
                    Количество посещений
                </p>
            </div>
            <div class = "icon">
                <i class = "ion ion-pie-graph"></i>
            </div>
            <a href = "#" class = "small-box-footer">
<!--                More info-->
                <i class = "fa "></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
</div><!-- /.row -->

<!-- top row -->
<div class = "row">
    <div class = "col-xs-12 connectedSortable">

    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Main row -->
<div class = "row">
<!-- Left col -->
<section class = "col-lg-6 connectedSortable">
    <!-- Box (with bar chart) -->
    <div class = "box box-danger" id = "loading-example">
        <div class = "box-header">
            <!-- tools box -->
            <div class = "pull-right box-tools">
<!--                <button class = "btn btn-danger btn-sm refresh-btn" data-toggle = "tooltip" title = "Reload">-->
<!--                    <i class = "fa fa-refresh"></i>-->
<!--                </button>-->
                <button class = "btn btn-danger btn-sm" data-widget = 'collapse' data-toggle = "tooltip"
                        title = "Свернуть">
                    <i class = "fa fa-minus"></i>
                </button>
<!--                <button class = "btn btn-danger btn-sm" data-widget = 'remove' data-toggle = "tooltip"-->
<!--                        title = "Remove">-->
<!--                    <i class = "fa fa-times"></i>-->
<!--                </button>-->
            </div>
            <!-- /. tools -->
            <i class = "fa fa-cloud"></i>

            <h3 class = "box-title">Загрузка сервера</h3>
        </div>
        <!-- /.box-header -->
        <div class = "box-body no-padding">
<!--            <div class = "row">-->
<!--                <div class = "col-sm-5">-->
<!--                    <div class = "pad">-->
<!--                         Progress bars -->
<!--                        <div class = "clearfix">-->
<!--                            <span class = "pull-left">Bandwidth</span>-->
<!--                            <small class = "pull-right">10/200 GB</small>-->
<!--                        </div>-->
<!--                        <div class = "progress xs">-->
<!--                            <div class = "progress-bar progress-bar-green" style = "width: 70%;"></div>-->
<!--                        </div>-->
<!--                        <div class = "clearfix">-->
<!--                            <span class = "pull-left">Transfered</span>-->
<!--                            <small class = "pull-right">10 GB</small>-->
<!--                        </div>-->
<!--                        <div class = "progress xs">-->
<!--                            <div class = "progress-bar progress-bar-red" style = "width: 70%;"></div>-->
<!--                        </div>-->
<!---->
<!--                        <div class = "clearfix">-->
<!--                            <span class = "pull-left">Activity</span>-->
<!--                            <small class = "pull-right">73%</small>-->
<!--                        </div>-->
<!--                        <div class = "progress xs">-->
<!--                            <div class = "progress-bar progress-bar-light-blue" style = "width: 70%;"></div>-->
<!--                        </div>-->
<!---->
<!--                        <div class = "clearfix">-->
<!--                            <span class = "pull-left">FTP</span>-->
<!--                            <small class = "pull-right">30 GB</small>-->
<!--                        </div>-->
<!--                        <div class = "progress xs">-->
<!--                            <div class = "progress-bar progress-bar-aqua" style = "width: 70%;"></div>-->
<!--                        </div>-->
                        <!-- Buttons -->
<!--                        <p>-->
<!--                            <button class = "btn btn-default btn-sm">-->
<!--                                <i class = "fa fa-cloud-download"></i>-->
<!--                                Generate PDF-->
<!--                            </button>-->
<!--                        </p>-->
<!--                    </div>-->
                    <!-- /.pad -->
<!--                </div>-->
                <!-- /.col -->
<!--            </div>-->
            <!-- /.row - inside box -->
        </div>
        <!-- /.box-body -->
        <div class = "box-footer">
            <div class = "row">
                <div class = "col-xs-4 text-center" style = "border-right: 1px solid #f4f4f4">
                    <input type = "text" class = "knob" data-readonly = "true" value = "<?= $systemInfo['cpu_load'] ?>" data-width = "60"
                           data-height = "60" data-fgColor = "#f56954" />

                    <div class = "knob-label">CPU</div>
                </div>
                <!-- ./col -->
                <div class = "col-xs-4 text-center" style = "border-right: 1px solid #f4f4f4">
                    <input type = "text" class = "knob" data-readonly = "true" value = "<?= $systemInfo['disk_percent'] ?>" data-width = "60"
                           data-height = "60" data-fgColor = "#00a65a" />

                    <div class = "knob-label">Disk</div>
                </div>
                <!-- ./col -->
                <div class = "col-xs-4 text-center">
                    <input type = "text" class = "knob" data-readonly = "true" value = "<?= $systemInfo['ram_percent'] ?>" data-width = "60"
                           data-height = "60" data-fgColor = "#3c8dbc" />

                    <div class = "knob-label">RAM</div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->

</section>
<!-- /.Left col -->
<!-- right col (We are only adding the ID to make the widgets sortable)-->
<section class = "col-lg-6 connectedSortable">
    <!-- Map box -->
    <div class = "box box-primary">
        <div class = "box-header">
            <!-- tools box -->
            <div class = "pull-right box-tools">
<!--                <button class = "btn btn-primary btn-sm daterange pull-right" data-toggle = "tooltip"-->
<!--                        title = "Date range">-->
<!--                    <i class = "fa fa-calendar"></i>-->
<!--                </button>-->
                <button class = "btn btn-primary btn-sm pull-right" data-widget = 'collapse' data-toggle = "tooltip"
                        title = "Свернуть" style = "margin-right: 5px;">
                    <i class = "fa fa-minus"></i>
                </button>
            </div>
            <!-- /. tools -->

            <i class = "fa fa-map-marker"></i>
            <h3 class = "box-title">
                Visitors
            </h3>
        </div>
        <div class = "box-body no-padding">
            <div id = "world-map" style = "height: 300px;"></div>
            <div class = "table-responsive">
                <!-- .table - Uses sparkline charts-->
                <table class = "table table-striped">
                    <tr>
                        <th>Страна</th>
                        <th>Количество</th>
                    </tr>
                    <?php
                        foreach ($visitors as $num=>$visitor) {
                            if ($num > 8)
                                break;
                            ?>
                            <tr>
                                <td>
                                    <a href="#" ><?= $visitor->country ?></a>
                                </td>
                                <td>
                                    <?= $visitor->id ?><div id = "sparkline-<?= intval($num + 1) ?>"></div>
                                </td>
                            </tr>
                            <?php
                        }
                    ?>

                </table>
                <!-- /.table -->
            </div>
        </div>
        <!-- /.box-body-->
<!--        <div class = "box-footer">-->
<!--            <button class = "btn btn-info">-->
<!--                <i class = "fa fa-download"></i>-->
<!--                Generate PDF-->
<!--            </button>-->
<!--            <button class = "btn btn-warning">-->
<!--                <i class = "fa fa-bug"></i>-->
<!--                Report Bug-->
<!--            </button>-->
<!--        </div>-->
    </div>
    <!-- /.box -->

</section>
<!-- right col -->
</div><!-- /.row (main row) -->

<!-- Morris.js charts -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/morris/raphael-min.js"
        type = "text/javascript"></script>
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/morris/morris.min.js"
        type = "text/javascript"></script>
<!-- Sparkline -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/sparkline/jquery.sparkline.min.js"
        type = "text/javascript"></script>

<script
    src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
    type = "text/javascript"></script>
<script
    src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
    type = "text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/jqueryKnob/jquery.knob.js"
        type = "text/javascript"></script>
<!-- daterangepicker -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/daterangepicker/daterangepicker.js"
        type = "text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script
    src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
    type = "text/javascript"></script>
<!-- iCheck -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/plugins/iCheck/icheck.min.js"
        type = "text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src = "<?= AddExtend::baseUrl(true) ?>shared/media/bootstrap3/js/AdminLTE/dashboard.js"
        type = "text/javascript"></script>

<script type = "text/javascript" >
    $(document).ready(function () {
        var visitorsData = {
            <?php
             $count = sizeof($visitors);
             foreach ($visitors as $num => $visitor) {
                $comma = '';
                if (intval($num + 1) < $count)
                    $comma = ',';
                echo '"' . $visitor->city . '" : ' . $visitor->id . $comma;
             }
            ?>
        };
        //World map by jvectormap
        $('#world-map').vectorMap({
                                      map               : 'world_mill_en',
                                      backgroundColor   : "#fff",
                                      regionStyle       : {
                                          initial : {
                                              fill             : '#e4e4e4',
                                              "fill-opacity"   : 1,
                                              stroke           : 'none',
                                              "stroke-width"   : 0,
                                              "stroke-opacity" : 1
                                          }
                                      },
                                      series            : {
                                          regions : [
                                              {
                                                  values            : visitorsData,
                                                  scale             : ["#3c8dbc", "#2D79A6"], //['#3E5E6B', '#A6BAC2'],
                                                  normalizeFunction : 'polynomial'
                                              }
                                          ]
                                      },
                                      onRegionLabelShow : function (e, el, code) {
                                          if (typeof visitorsData[code] != "undefined")
                                              el.html(el.html() + ': ' + visitorsData[code] + '');
                                      }
                                  });
    });
</script>