<?php
    /* @var $this SiteController */
    /* @var $model LoginForm */
    /* @var $form CActiveForm */
?>
<!doctype html>
<html lang = "ru">

<head>
    <?php AddExtend::regCoreScript('jquery'); ?>
    <meta charset = "UTF-8">
    <title><?= Yii::app()->name ?> - Login</title>

    <link rel = "stylesheet" href = "<?= AddExtend::baseUrl() ?>shared/media/bootstrap/css/bootstrap.min.css" />
    <link rel = "stylesheet"
          href = "<?= AddExtend::baseUrl() ?>shared/media/bootstrap/css/bootstrap-responsive.min.css" />
    <!--    <link rel="stylesheet" href="--><? //=AddExtend::baseUrl()?><!--shared/media/bootstrap/css/main.css"/>-->

    <script type = "text/javascript"
            src = "<?= AddExtend::baseUrl() ?>shared/media/bootstrap3/js/bootstrap.min.js"></script>
</head>

<body>
<?php
    $form = $this->beginWidget(
        'CActiveForm', array(
            'id'                     => 'login-form',
            'enableClientValidation' => true,
            'clientOptions'          => array(
                'validateOnSubmit' => true,
            ),
        )
    );
?>
<div class = "container">
    <div class = "row">
        <div class = "span4 offset4 well">
            <legend>
                <a href = "<?= Yii::app()->homeUrl ?>" title = "Вернуться">Вернуться на сайт</a>
            </legend>
            <legend>Пожалуйста авторизируйтесь для продолжения</legend>
            <?php
                $error_summary = $form->errorSummary($model);
                if (!empty( $model->errors )) {
                    ?>
                    <div class = "alert alert-error">
                        <a class = "close" data-dismiss = "alert" href = "javascript:void(0);"
                           onclick = "this.parentNode.style.display='none';">×
                        </a>
                        <?= $error_summary ?>
                    </div>
                <?php } ?>
            <form method = "POST" action = "" accept-charset = "UTF-8">
                <?php echo $form->textField(
                    $model, 'username', array(
                        'class'       => 'span4',
                        'placeholder' => 'Логин'
                    )
                ); ?>
                <?php echo $form->passwordField(
                    $model, 'password', array(
                        'class'       => 'span4',
                        'placeholder' => 'Пароль'
                    )
                ); ?>
                <!--<label class="checkbox">
                    <input type="checkbox" name="remember" value="1"> Remember Me
                </label>-->
                <?php echo CHtml::submitButton('Войти', array( 'class' => 'btn btn-info btn-block' )); ?>
            </form>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
</body>

</html>

