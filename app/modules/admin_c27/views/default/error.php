<?php
    /* @var $this SiteController */
    /* @var $error array */

    $this->pageTitle = Yii::app()->name . ' - Error';
    $this->breadcrumbs = array(
        'Error',
    );
?>

<section class = "content">

    <div class = "error-page">
        <h2 class = "headline text-info"> 404</h2>

        <div class = "error-content">
            <h3>
                <i class = "fa fa-warning text-yellow"></i>
                Страница не найдена.
            </h3>
            <p>
            <h5>Невозможно найти страницу, которую вы желаете открыть.
                Может, вы желаете
                <a href = '<?= $this->createUrl("default/index") ?>'> вернуться на главную</a>
                ?
            </h5>
            </p>
        </div>
    </div>

</section>