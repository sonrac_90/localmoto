<?php
    /** @var Slider $slider */

    AddExtend::regScriptFile(
        array(
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            // Валидация
            AddExtend::pathJs() . "admin/goods/jquery.pekeUpload2.js",
            // Валидация
            AddExtend::pathJs() . "admin/goods/pekeUploadEvent.js",
            AddExtend::pathJs() . 'libs/ckeditor/ckeditor.js',
            AddExtend::pathJs() . "admin/mainpage.js",
        )
    );


?>

<fieldset>
    <legend>Слайдер</legend>
    <div class = "slider">
        <input type = "file" id = "addImage" />
        <? if (!sizeof($slider)) : ?>
            <div class = "images">
                Нет изображений в слайдере
            </div>
        <? else : ?>
            <div class = "images">
                <?php
                    foreach ($slider as $_sliderNext) {
                        ?>
                        <div class = "nextImage" data-key = "<?= $_sliderNext->position ?>"
                             data = "<?= $_sliderNext->id ?>">
                            <img src = "<?= Slider::getFullPath($_sliderNext) ?>" width = "100" height = "100">
                            <div class="deleteImageSlider">Удалить</div>
                        </div>
                    <?php } ?>
            </div>
        <?endif ?>
    </div>
</fieldset>

<fieldset>
    <legend>
        Блок
    </legend>
    <form action="<?= AddExtend::getURL($this, $this->id . '/index') ?>" method="POST" >
        <textarea id="blockMain" name="text">
            <?= $main->text ?>
        </textarea>
        <input class="btn btn-success" type = "submit" value = "Сохранить" />
    </form>
</fieldset>
