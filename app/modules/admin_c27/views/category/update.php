<?php
    AddExtend::regScriptFile(AddExtend::baseUrl() . "shared/media/js/admin/category.js", CClientScript::POS_HEAD);
?>

<div>

    <div>
        <?php

            echo '<div id="category_update_form_widget">';
            $form = $this->beginWidget(
                'CActiveForm', array(
                    'id'                   => 'category_update',
                    'action'               => AddExtend::getURL($this, $this->id . '/update/id/' . $_GET[ 'id' ]),
                    'enableAjaxValidation' => true,
                )
            );

            echo '<div style="float:left">' . $form->errorSummary($category);

            foreach ($category->attributes as $key => $userAttr) {
                if ($key == 'id' || $key == 'date_create' || $key == 'parent')
                    continue;
                echo $form->labelEx($category, $key) . "<br/>";
                echo $form->textField($category, $key) . "<br/>";
                echo $form->error($category, $key) . "<br/>";
            }

            echo "Подкатегории: ";

            $this->endWidget();
        ?>

    </div>
</div>