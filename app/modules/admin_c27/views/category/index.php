<?php

    AddExtend::regCoreScript('jquery');
    AddExtend::regCssFile(
        array(
            AddExtend::baseUrl() . 'shared/media/bootstrap3/css/bootstrap.min.css',
            AddExtend::baseUrl() . 'shared/media/js/admin/fancytree/skin-themeroller/ui.fancytree.min.css',
            AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css',

        ), 'screen'
    );

    AddExtend::regScriptFile(
        array(
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            AddExtend::baseUrl() . 'shared/media/js/admin/fancytree/jquery.fancytree-all.min.js',
            AddExtend::baseUrl() . 'shared/media/js/admin/modal.js',
            AddExtend::baseUrl() . 'shared/media/js/admin/category.js'
        )
    );

?>

<div class = "preload">
    <span class = "preloadImage"></span>
</div>

<div>
    <button id = "addCat" class = "btn btn-primary">
        Добавить категорию
    </button>
</div>
<br />

<div id = "tree">
    <ul id = "treeData" style = "">
        <?php

            $previous = 0;
            $parent = 0;

            function getLI ($nextCat, $obj) {
                return "<li class='folder' id='{$nextCat->id}'>" . $nextCat->name . $obj->buttonBlock($nextCat->id);
            }

            foreach ($category as $nextCat) {
                if (count($nextCat->childNode) && isset($nextCat->childNode[0]->id)) {
                    echo getLI($nextCat, $this);
                    echo "<ul>";
                    foreach ($nextCat->childNode as $_child) {
                        echo getLI($_child, $this);
                        if (count($_child->childNode) && isset($_child->childNode[0]->id)) {
                            echo "<ul>";
                            foreach ($_child->childNode as $_childNext) {
                                echo getLI($_childNext, $this);
                            }
                            echo "</ul>";
                        }
                    }
                    echo "</ul>";
                } else {
                    $cat = false;
                    echo "<li id='{$nextCat->id}'>" . $nextCat->name . $this->buttonBlock($nextCat->id);
                }
            }

        ?>
    </ul>
</div>

<div id = "editModal">
    <input type = "text" value = "" name = "editCatName" />
</div>