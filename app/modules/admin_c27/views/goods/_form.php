<style>
    #tree li{
        width: 100%;
    }
    #btnMain button, input[type=submit] {
        float: right;
        position: relative;
        bottom: 0;
        margin: 0!important;
        margin-left: 4px!important;
    }

</style>
<?php

    $allSize = array(
        'XS'    => 'XS',
        'S'     => 'S',
        'M'     => 'M',
        'L'     => 'L',
        'XL'    => 'XL',
        'XXL'   => 'XXL',
        'XXXL'  => 'XXXL',
        'BXL'   => 'BXL',
        'BXXL'  => 'BXXL',
        'BXXXL' => 'BXXXL',
    );

    /**
     * @param array $sizes
     * @param string $size
     * @return string
     */
    function searchInSizes ($sizes, $size) {
        /** @var GoodsSize $_size */
        foreach ($sizes as $_size) {
            if (strcmp(trim($_size->size), trim($size)) == 0)
                return 'selected=selected';
        }

        return '';
    }


    function getOption($cat, $selected = '') {
        return "<option {$selected} value=\"" . $cat->id . "\">{$cat->name}</option>";
    }

    function getList($child = false, $implode = array()) {
        if ($child) {
            if (intval($implode[ 0 ]) > 0) {
                return array( Category::model()->getChild($child, null, $implode[ 0 ]) );
            }
        } else {
            return Category::model()->getCategoryById(null, $implode);
        }

        return array();
    }

    function getSelect($key, $id, $parent = null) {
        if ($parent) {
            $id = GoodsCategory::model()->searchUniqueId($id, $parent);
            $id = $id->id;
        }

        return '<select data-key="' . $key . '" data-old="' . $id . '" data="' . $id . '" data-parent="' . intval($parent) . '" name="CategoryGoods[' . $key . '][category_id]" >';
    }

    function returnSelect($list, $key, $child = false) {
        $ret = getSelect($key, $list->id, $child);
        $ret .= getOption($list, 'selected');

        $arrImplode = false;
        if (intval($list->id) > 0)
            $arrImplode = array( $list->id );

        $list = getList($child, $arrImplode);


        $next = sizeof($list[ 0 ]);
        $list[ 0 ][ $next ] = new Category();
        $list[ 0 ][ $next ]->id = -10;
        $list[ 0 ][ $next ]->name = "Добавить новую";

        foreach ($list[ 0 ] as $_nextCategory) {
            $name = trim($_nextCategory->name);
            if (empty( $name ))
                continue;
            $ret .= getOption($_nextCategory, '', $child);
        }

        $ret .= '</select>';

        return $ret;
    }

    AddExtend::regCssFile(
        array(
            AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css',
            AddExtend::baseUrl() . 'shared/media/css/admin/colorPicker/colorpicker.css',
        ), 'screen'
    );

    AddExtend::regScriptFile(
        array(
            // Jquery UI
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            // Реализация некоторых php функций в js
            AddExtend::baseUrl() . "shared/media/js/libs/jquery.scrollTo.js",
            AddExtend::baseUrl() . "shared/media/js/admin/phpFunc.js",
            // Модальные окна
            AddExtend::baseUrl() . "shared/media/js/admin/modal.js",
            // colorPicker
            AddExtend::baseUrl() . "shared/media/js/admin/colorPicker/eye.js",
            AddExtend::baseUrl() . "shared/media/js/admin/colorPicker/layout.js",
            AddExtend::baseUrl() . "shared/media/js/admin/colorPicker/utils.js",
            AddExtend::baseUrl() . "shared/media/js/admin/colorPicker/colorpicker.js",
            // Работа с объектами и массивами
            AddExtend::baseUrl() . "shared/media/js/admin/goods/event/array.js",
            // Удаление элементов
            AddExtend::baseUrl() . "shared/media/js/admin/goods/event/deleteIcon.js",
            // Навешивание событий
            AddExtend::baseUrl() . "shared/media/js/admin/goods/event/event.js",
            // Изменение состояний select|colorPicker
            AddExtend::baseUrl() . "shared/media/js/admin/goods/event/changeEvent.js",
            // Добавление элементов
            AddExtend::baseUrl() . "shared/media/js/admin/goods/event/createElement.js",
            // Главный скрипт, который связывает всю логику
            AddExtend::baseUrl() . "shared/media/js/admin/goods/goods.js",
            // Валидация
            AddExtend::baseUrl() . "shared/media/js/admin/goods/goodsValidate.js",
            // Валидация
            AddExtend::baseUrl() . "shared/media/js/admin/goods/jquery.pekeUpload2.js",
            // Валидация
            AddExtend::baseUrl() . "shared/media/js/admin/goods/pekeUploadEvent.js",
        )
    );

    $form = $this->beginWidget(
        'CActiveForm', array(
            'id'                   => 'user_update',
            'action'               => AddExtend::getURL($this, $this->id . '/' . $this->action->id . '/id/' . $_GET[ 'id' ]),
            'enableAjaxValidation' => true,
            'htmlOptions'          => array( 'style' => 'width: 100%' )
        )
    );
?>
    <div class = "good">
        <div class = "goodInfo">
            <div class = "relPos">
                <?php
                    echo $form->hiddenField($good, 'id', array( 'data' => 'id' ));
                    echo $form->labelEx($good, 'name');
                    echo $form->textField($good, 'name');
                ?>
                <div class = 'errors divError' id = 'good_Goods_name'></div>
            </div>
            <div class = "relPos">
                <?php
                    echo $form->labelEx($good, 'model');
                    echo $form->textField($good, 'model');
                ?>
                <div class = 'errors divError' id = 'good_Goods_model'></div>
            </div>
            <div class = "relPos">
                <?php
                    echo $form->labelEx($good, 'price');
                    echo $form->textField($good, 'price');
                ?>
                <div class = 'errors divError' id = 'good_Goods_price'></div>
            </div>
            <div class = "relPos">
                <?php
                    echo $form->labelEx($good, 'sex_characters');
                    echo $form->dropDownList(
                            $good, 'sex_characters', array(
                                '1' => 'Мужской',
                                '0' => 'Женский'
                            )
                        );
                ?>
                <div class = 'errors divError' id = 'good_Goods_sex_characters'></div>
            </div>
            <div class = "relPos">
                <?php
                    echo $form->labelEx($good, 'producerName');
                    $producerList = Producer::getList();
                    reset($producerList);
                    if (is_null($good->producer_id))
                        $good->producer_id = key($producerList);
                    echo $form->dropDownList(
                            $good, 'producerName', $producerList, array(
                                'data-old' => $good->producer_id,
                                'name111'  => 'asd'
                            )
                        );
                ?>
                <div class = 'errors divError' id = 'good_Goods_producer_id'></div>
            </div>
            <div class = "relPos">
                <?php
                    echo $form->labelEx($good, 'is_active');
                    echo $form->dropDownList(
                            $good, 'is_active', array(
                                1 => 'Доступен',
                                0 => 'Скрытый'
                            )
                        );
                ?>
                <div class = 'errors divError' id = 'good_Goods_producer_id'></div>
            </div>
        </div>
    </div>
<div class = "good">
<div class = "divSelect">
    <?php
        $this->renderPartial('cat_goods', array(
                'category' => Category::model()->getAllCategory()
            )
        );
    ?>
</div>


<?php    /**
 *
 *
 *
 *
 * Фотографии
 *
 */


?>
<br />
<div id = "photoInfo">
    <?php
        foreach ($photos as $key => $_photo) {
            if (AddExtend::checkSetVar($_photo[ 'url' ])) {
                ?>
                <div class = "photoInfo" data = "<?= $_photo->id ?>" data-key = "<?= $key ?>">
                    <fieldset>
                        <legend>
                            Фотография <?= intval($key + 1) ?>
                            <div class = "deletePhoto" data = "<?= $_photo->id ?>"
                                 dataname = "<?= $_photo->url ?>"></div>
                        </legend>
                        <div class = "photo">
                            <img width = "300px" height = "300px"
                                 src = "<?= Photo::model()->getUrlImage(CropImage::dynamicPath($_photo[ 'id' ]) . "/" . $_photo[ 'url' ]) ?>" />

                            <div class = 'errors divError' id = 'photo_<?= $key ?>_Photo_url'></div>
                            <input type = "file" value = "<?= $_photo[ 'url' ] ?>" data-photo = "<?= $key ?>"
                                   name = "Goods[photos][<?= $key ?>][url]" id = "file<?= $key ?>" />
                            <input type = "hidden" class = "hiddenUrl" value = "<?= $_photo[ 'url' ] ?>"
                                   data-photo = "<?= $key ?>" name = "Goods[photos][<?= $key ?>][url]" />
                            <input type = "hidden" data-photo = "<?= $key ?>" value = "<?= $_photo[ 'goods_id' ] ?>"
                                   name = "Goods[photos][<?= $key ?>][goods_id]" data = "id" />
                            <input type = "hidden" data-photo = "<?= $key ?>" value = "<?= $_photo[ 'id' ] ?>"
                                   name = "Goods[photos][<?= $key ?>][id]" />
                        </div>
                        <div class = 'photoAddInfo'>
                            <?php
                                /**
                                 *
                                 *
                                 *
                                 *
                                 * Размеры
                                 *
                                 *
                                 *
                                 */
                            ?>
                            <div class="sizeColor">
                            <fieldset>
                                <legend>Размеры</legend>

                                <?php
                                    if (AddExtend::checkSetVar($sizes[ $key ])){
                                ?>
                                <div class = "sizesContainer">
                                    <select multiple size = "10" name = "Goods[photos][<?= $key ?>][sizes][]">
                                        <?php
                                            foreach ($allSize as $_size) {
                                                $_size = trim($_size);
                                                echo "<option " . searchInSizes($sizes[$key], $_size) . ">{$_size}</option>";
                                            }
                                        ?>
                                    </select>
                                    <div class = 'errors divError'
                                         id = 'photo_<?= $key ?>_size_0'></div>
                                </div>
                            </fieldset>

                        <?php
                            }

                                /**
                                 *
                                 *
                                 * Цвета
                                 *
                                 *
                                 *
                                 *
                                 */
                            ?>
                            <fieldset>
                                <legend>Цвета
                                </legend>

                                <?php
                                    if (AddExtend::checkSetVar($colors[ $key ])){
                                ?>
                                <div class = "colorsContainer">
                                        <textarea rows="5" cols="20" name="Goods[photos][<?= $key ?>][color]"><?php
                                        foreach ($colors[ $key ] as $kColor => $_color) {
                                            echo $_color['color'] . ",\n";
                                        }
                                    ?>
                                    </textarea>
                                    <div class = 'errors divError'
                                         id = 'photo_<?= $key ?>_color_ColorGoods_color'></div>
                                    <?php
                                        }
                                    ?>
                            </fieldset>

                            <?php


                                /**
                                 *
                                 *
                                 * Описание
                                 *
                                 *
                                 *
                                 *
                                 */
                                if (AddExtend::checkSetVar($description[ $key ])) {
                                    $descr = $description[ $key ][ 'description' ];
                                    $dId = $description[ $key ][ 'id' ];
                                } else {
                                    $descr = '';
                                    $dId = null;
                                }
                            ?>
                            </div>
                            <fieldset>
                                <legend>Описание</legend>
                                <div class = "relPos">
                                    <textarea
                                        name = "Goods[photos][<?= $key ?>][description][description]"><?= $descr ?></textarea>
                                    <div class = 'errors divError'
                                         id = 'photo_<?= $key ?>_description_DescriptionGoods_description'></div>
                                    <input type = "hidden" value = "<?= $dId ?>"
                                           name = "Goods[photos][<?= $key ?>][description][id]" data = "id" />
                                </div>
                            </fieldset>
                        </div>
                </div>
            <?php

            }
        }
    ?>
</div>
    <div id="btnMain">
        <?php echo CHtml::submitButton('Сохранить', array( 'class' => 'btn btn-success' )); ?>
        <button id = "photoAdd" class = "btn btn-primary">
            Добавить фотографию
        </button>
    </div>

<div style = "width: 100%; text-align: center;">

</div>
<div id = "editModal">
    <input type = "text" value = "" name = "editCatName" />
</div>
<?php
    $this->endWidget();
?>

<script type="text/javascript" >
    <?php
    $cat = GoodsCategory::model()->getAllCategory($good->id);
    ?>
    var jsonCat = <?= CJSON::encode($cat); ?>;

</script>