<a href = "<?= AddExtend::getURL($this, '/goods/add') ?>" style = "color: white; text-decoration: none;"
   class = "btn btn-primary">
    Добавить новый
</a>
<?php
    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'               => 'users-list',
            'dataProvider'     => $goods->search($id, $type),
            'filter'           => $goods,
            'ajaxType'         => 'POST',
            'ajaxUpdate'       => true,
            'enablePagination' => true,
            'itemsCssClass'    => 'table table-bordered table-striped dataTable',
            'template'         => '{items}{pager}',
            'pager'            => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'    => 'p',
            'columns'          => array(
                'id',
                'name',
                'price',
                'model',
                array(
                    'name' => 'sex_characters',
                    'value' => function ($data) {
                        switch ($data->sex_characters) {
                            case 1 :
                                return 'Мужские';
                                break;
                            case 0 :
                                return 'Женские';
                                break;
                        }
                    },
                    'filter' => array(
                        '1' => 'Мужские',
                        '0' => 'Женские'
                    )
                ),
                array(
                    'name'  => 'producerName',
                    'value' => '$data->producer->name',
                    'filter' => Producer::model()->getAllProducer()
                ),
                array(
                    'header' => 'Фото',
                    'type'   => 'html',
                    'value'  => function ($data) {
                        $url = Goods::model()->getFirstPhoto($data->id);
                        if (empty( $url[ 0 ] )) {
                            $urlImg = AddExtend::pathCss() . "admin/images/admin/no_image.png";
                        } else {
                            $urlImg = AddExtend::baseUrl() . Photo::PATH_IMAGE . CropImage::dynamicPath($url[ 1 ]) . "/" . $url[ 0 ];
                        }

                        return "<img src='{$urlImg}' width=40 height=40 alt='Нет изображения'/>";
                    },
                    'filter' => false
                ),
                array(
                    'name'   => 'is_active',
                    'type'   => 'html',
                    'value'  => function ($data) {
                        return ( ( $data->is_active == 1 ) ? 'Доступен' : 'Скрытый' );
                    },
                    'filter' => array(
                        '0' => 'Скрытый',
                        1   => 'Доступен'
                    )
                ),

                array(
                    'class'       => 'CButtonColumn',
                    'template'    => '{update}{delete}',
                    'buttons'     => array(
                        'update' => array(
                            'title' => 'Редактировать',
                            'imageUrl' => AddExtend::pathCss() . "admin/images/admin/edit.png"
                        ),
                        'delete' => array(
                            'title' => 'Удалить',
                            'imageUrl' => AddExtend::pathCss() . "admin/images/admin/delete.png"
                        ),
                    ),
                    'htmlOptions' => array( 'rows' => 40 )
                )
            )
        )
    );
