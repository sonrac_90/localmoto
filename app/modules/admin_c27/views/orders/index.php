<?php
    AddExtend::regCoreScript('jquery');
    AddExtend::regCssFile(
        array(
            AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css',

        ), 'screen'
    );

    AddExtend::regScriptFile(
        array(
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            AddExtend::baseUrl() . 'shared/media/js/admin/modal.js',
        )
    );

    $statusOrder = array(
        'ordered' => 'В ожидании',
        'failure' => 'Отменен',
        'buy'     => 'Завершен',
    );

    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'                    => 'service-list',
            'dataProvider'          => $order->search(),
            'ajaxType'              => 'POST',
            'filter'                => $order,
            'rowCssClassExpression' => '$data->status . "-" . $data->status_pay . "-" . $data->status_delivery',
            'enablePagination'      => true,
            'itemsCssClass'         => 'table table-bordered table-striped dataTable',
            'template'              => '{items}{pager}',
            'pager'                 => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'         => 'p',
            'columns'               => array(
                'id',
                'user_email',
                'user_firstname',
                'user_lastname',
                'user_phone',
                'address',
                'total_count',
                'date_order',
                'total_price',
                'failure_cause',
                array(
                    'name'   => 'type_order',
                    'value'  => function ($data) {
                        return ( ( $data->type_order == 'pickup' ) ? 'Самовывоз' : 'Доставка' );
                    },
                    'filter' => array(
                        ''         => 'Все',
                        'pickup'   => 'Самовывоз',
                        'delivery' => 'Доставка'
                    )
                ),
                array(
                    'name'   => 'status',
                    'value'  => function ($data) {
                        $status = '';
                        switch ($data->status) {
                            case 'ordered' :
                                $status = 'В ожидании';
                                break;
                            case 'failure' :
                                $status = 'Отменен';
                                break;
                            case 'buy' :
                                $status = 'Завершен';
                                break;
                        }

                        return $status;
                    },
                    'filter' => array(
                        ''        => 'Все',
                        'ordered' => 'В ожидании',
                        'failure' => 'Отменен',
                        'buy'     => 'Завершен'

                    ),
                ),
                array(
                    'name'   => 'status_pay',
                    'value'  => function ($data) {
                        $status = '';
                        switch ($data->status_pay) {
                            case 'pay' :
                                $status = 'Оплачен';
                                break;
                            case 'not_pay' :
                                $status = 'Не оплачен';
                                break;
                        }

                        return $status;
                    },
                    'filter' => array(
                        ''        => 'Все',
                        'pay'     => 'Оплачен',
                        'not_pay' => 'Не оплачен'
                    ),
                ),
                array(
                    'name'   => 'status_delivery',
                    'value'  => function ($data) {
                        $status = '';
                        switch ($data->status_delivery) {
                            case 'delivery' :
                                $status = 'Доставлен';
                                break;
                            case 'not_delivery' :
                                $status = 'Не доставлен';
                                break;
                        }

                        return $status;
                    },
                    'filter' => array(
                        ''             => 'Все',
                        'delivery'     => 'Доставлен',
                        'not_delivery' => 'Не доставлен',
                    ),
                ),
                array(
                    'class'       => 'CButtonColumn',
                    'htmlOptions' => array(
                        'style' => 'width: 80px;'
                    ),
                    'template'    => '<div style="width:65px!important">{detail}{update}{delete}</div>',
                    'buttons'     => array(
                        'detail' => array(
                            'title'    => 'Просмотреть',
                            'url'      => 'AddExtend::baseUrl(true) . \'admin_c27/orders/view/id/\' . $data->id',
                            'imageUrl' => AddExtend::pathMedia() . 'css/admin/images/admin/visible.png',
                        ),
                        'update' => array(
                            'title'       => 'Редактировать',
                            'click'       => 'function (event) {event.preventDefault(); updateStatus(this, \'' . $data->status . '\'); return false; }',
                            'htmlOptions' => array(
                                'onclick' => 'return false'
                            ),
                            'imageUrl'    => AddExtend::pathCss() . "admin/images/admin/edit.png"
                        ),
                        'delete' => array(
                            'title'    => 'Удалить',
                            'imageUrl' => AddExtend::pathCss() . "admin/images/admin/delete.png"
                        ),
                    ),
                )
            )
        )
    );

?>


<script type = "text/javascript">
    $(document).ready(function () {
        $('aside.right-side').css({'overflow' : 'auto'});

    });

    function updateStatus(elem) {
        var modal = $('#modal');
        var tr = $('tr').has(elem).eq(0);
        if (!tr.size())
            return;
        var inpVal = tr.attr('class').split('-');
        var cntTR = 0;
        modal.find('select').each(function () {
            $(this).val(inpVal[cntTR]);
            cntTR++;
        });

        function saveProducer(event) {
            event.preventDefault();
            var tr = $(this).parent().find('select');
            var inpVal1 = tr.eq(0).val();
            var inpVal2 = tr.eq(1).val();
            var inpVal3 = tr.eq(2).val();
            var action = elem.href;

            var modal = $('#modal');

            $.ajax({
                       url     : action,
                       data    : 'status=' + inpVal1 + '&statusPay=' + inpVal2 + '&statusDelivery=' + inpVal3,
                       type    : 'POST',
                       success : function (data) {
                           $(modal).dialog('close');
                           $(modal).attr('class', '');
                           $.fn.yiiGridView.update("service-list");
                           $('.ui-dialog').remove();
                       },
                       error   : function (xhr, err) {
                           console.log(xhr, err);
                       }
                   });
        }

        var param = {
            'title' : 'Изменение статуса заказа',
            buttons : [
                {
                    text  : 'Изменить',
                    click : saveProducer
                }
            ]
        };

        addModal(modal, saveProducer, param);
    }
</script>

<div id = "modal" style = "display: none;">
    <form id = "changeInfo">
        <select name = 'status' id = 'statusOrder'>
            <?php
                foreach ($statusOrder as $value => $text) {
                    ?>
                    <option value = "<?= $value ?>"> <?= $text ?></option>
                <?php
                }
            ?>
        </select>
        <br />
        <select name = "pay" id = "statusPay">
            <option value = "pay">Оплачен</option>
            <option value = "not_pay">Не оплачен</option>
        </select>
        <br />
        <select name = "delivery" id = "statusDelivery">
            <option value = "delivery">Доставлен</option>
            <option value = "not_delivery">Не доставлен</option>
        </select>
    </form>
</div>