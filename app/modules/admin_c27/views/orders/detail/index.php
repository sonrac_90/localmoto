<?php
    AddExtend::regCoreScript('jquery');
    AddExtend::regCssFile(
        array(
            AddExtend::getCoreScriptPath() . '/jui/css/base/jquery-ui.css',

        ), 'screen'
    );

    AddExtend::regScriptFile(
        array(
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            AddExtend::baseUrl() . 'shared/media/js/admin/modal.js',
        )
    );

    $statusOrder = array(
        'ordered'  => 'В ожидании',
        'not_paid' => 'Не подтвержден',
        'paid'     => 'Оплачен',
        'failure'  => 'Отменен'
    );

    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'               => 'order-list',
            'dataProvider'     => $order->search(),
            'ajaxType'         => 'POST',
            'filter'           => $order,
            'enablePagination' => true,
            'itemsCssClass'    => 'table table-bordered table-striped dataTable',
            'rowCssClassExpression' => '$data->status',
            'template'         => '{items}{pager}',
            'pager'            => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'    => 'p',
            'columns'          => array(
                'id',
                'user_email',
                'user_firstname',
                'user_lastname',
                'user_phone',
                'address',
                'good_model',
                'good_name',
                'good_price',
                'good_producer',
                array(
                    'name' => 'photo_url',
                    'type' => 'html',
                    'value' => function ($data) {
                        $url = explode('.', $data->photo_url);
                        $src = '<img src="' . AddExtend::baseUrl(true) . Photo::PATH_IMAGE . CropImage::dynamicPath($url[0]) . '/';
                        $url[0] .= '-t';
                        $src .= implode('.', $url) . '" width=100 />';
                        return $src;

                    },
                    'filter' => false
                ),
                'size',
                'color',
                'description',
                'total_count',
                'date_order',
                'total_price',
                'failure_cause',
                array(
                    'name' => 'type_order',
                    'value' => function ($data) {
                        return (($data->type_order == 'pickup') ? 'Самовывоз' : 'Доставка');
                    },
                    'filter' => array(
                        ''         => 'Все',
                        'pickup'   => 'Самовывоз',
                        'delivery' => 'Доставка'
                    )
                ),
                array(
                    'name' => 'status',
                    'value' => function ($data) {
                        $status = '';
                        switch ($data->status) {
                            case 'ordered' :
                                $status = 'В ожидании';
                                break;
                            case 'not_paid' :
                                $status = 'Не подтвержден';
                                break;
                            case 'paid' :
                                $status = 'Оплачен';
                                break;
                            case 'failure' :
                                $status = 'Отменен';
                                break;
                        }
                        return $status;
                    },
                    'filter' => array_merge(array('' => 'Все'), $statusOrder)
                ),
//                array(
//                    'class'       => 'CButtonColumn',
//                    'htmlOptions' => array(
//                        'style' => 'width: 66px;'
//                    ),
//                    'template'    => '{update}{delete}',
//                    'buttons'     => array(
//                        'update' => array(
//                            'title' => 'Редактировать',
//                            'click' => 'function (event) {event.preventDefault(); updateStatus(this, \'' . $data->status . '\'); return false; }',
//                            'htmlOptions' => array(
//                                'onclick' => 'return false'
//                            ),
//                        ),
//                        'delete' => array(
//                            'title' => 'Удалить',
//                        ),
//                    ),
//                )
            )
        )
    );



?>

<script type="text/javascript">
    $(document).ready(function () {
        $('aside.right-side').css({'overflow' : 'auto'});

    })

    function updateStatus(elem) {
        var modal = $('#modal');
        modal.find('select').eq(0).val($('tr').has(elem).eq(0).attr('class'));

        var param = {
            'title' : 'Изменение статуса заказа'

        };

        function saveProducer(event) {
            event.preventDefault();
            var inpVal = $(this).parent().find('select').eq(0).val();
            var action = adminUrl + controller + '/updateChildStatus/id/' + <?=$_GET['id']?>;
            var id = basename(elem.href);

            var modal = $('#modal');

            $.ajax({
                       url     : action,
                       data    : 'id=' + id + '&status=' + inpVal,
                       type    : 'POST',
                       success : function (data) {

                           $(modal).dialog('close');
                           $(modal).html('');
                           $(modal).attr('class', '');
                           updateYiiList('order-list', bindProducerPage);
                           $('.ui-dialog').remove();
                       },
                       error   : function (xhr, err) {
                           console.log(xhr, err);
                       }
                   });

        }

        addModal(modal, saveProducer, param);
    }
</script>

<div id = "modal" style = "display: none;">
    <select name = 'status' id = 'statusOrder'>
       <?php
           foreach ($statusOrder as $value => $text) {
               ?>
               <option value = "<?=$value?>"> <?=$text?></option>
               <?php
           }
       ?>
    </select>
</div>