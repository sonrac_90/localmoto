<?php
    AddExtend::regScriptFile(AddExtend::baseUrl() . "shared/media/js/admin/users.js");
    AddExtend::regCssFile(array(
                              AddExtend::pathCss() . 'admin/fix.css'
                          ), 'screen'
    );
?>
<script type = "text/javascript">
    var userID =
    <?=intval($user->id)?>
</script>
<div>

    <div>
        <?php
            echo '<div style="clear: both"><button id="addAddress" class="btn btn-success" style="clear: both;">Добавить адресс</button></div><br/>';

            echo '<div id="user_update_form_widget">';
            $form = $this->beginWidget(
                'CActiveForm', array(
                    'id'                   => 'user_update',
                    'action'               => AddExtend::getURL($this, $this->id . '/update/id/' . $_GET[ 'id' ]),
                    'enableAjaxValidation' => true,
                )
            );

            echo '<div style="float:left">' . $form->errorSummary($user);

            foreach ($user->attributes as $key => $userAttr) {
                if ($key == 'id' || $key == 'password' || $key == 'register_date' || $key == 'last_visit' || $key == 'email_activate')
                    continue;
                echo $form->labelEx($user, $key) . "<br/>";
                if ($key == 'role') {
                    echo $form->dropDownList(
                            $user, $key, array(
                                'user'  => 'Пользователь',
                                'admin' => 'Администратор'
                            )
                        ) . "<br/>";
                }else if ($key == 'birthday') {
                    $dateRange = '1920:' . date("Y",time());
                    $date = new DateTime();
                    $date->modify('- 18 year');
                    $date->modify('- 1 day');
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array (
                            'name'=> 'Users[' . $key . ']',
                            'model' => $user,
                            'value' => date('d-m-Y', $user->birthday),
                            'language' => 'ru',
                            'options' => array(
                                'showAnim' => 'slide',
                                'dateFormat' => 'dd-mm-yy',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => $dateRange,
                                'minDate' => '01-01-1920',
                                'maxDate' => $date->format('d-m-Y'),
                                'showButtonPanel' => true
                            ),
                        )
                    );
                    echo "<br/>";
                } else if ($key == 'subscribe'){
                    echo $form->dropDownList($user, $key, array(
                            '0' => 'Не подписан',
                            '1' => 'Подписан'
                        )
                    );
                } else {
                    echo $form->textField($user, $key) . "<br/>";
                }
                echo $form->error($user, $key) . "<br/>";
            }
        ?>
    </div>

    <?php

        foreach ($adresses as $keyAddress => $value) {
            if (isset( $value->attributes )) {
                echo "<div class=\"address\" id=\"addrBlock{$value['id']}\">";
                echo "<fieldset>";
                echo "<legend>Адрес " . intval($keyAddress + 1) . "<div class='deleteIcon' number=\"{$value['id']}\"><img width=20 height=20 src='" . AddExtend::baseUrl() . "shared/media/css/admin/images/admin/delete.png'/></div></legend>";
                foreach ($value->attributes as $key => $attr) {
                    if ($key == 'id' || $key == 'user_id' || $key == 'date_add') {
                        echo $form->hiddenField($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' ));
                    } else {
                        echo $form->labelEx($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' )) . "<br/>";
                        echo $form->textField($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' )) . "<br/>";
                        echo $form->error($value, $key, array( 'name' => 'Address[' . $keyAddress . '][' . $key . ']' )) . "<br/>";
                    }
                }
                echo "</fieldset>";
                echo "</div>";
            }
        }
    ?>
</div>
<?php echo "<div class='clear submitButton_user'>" . CHtml::submitButton('Сохранить', array( 'class' => 'btn btn-success' )) . "</button></div>";
    $this->endWidget();

?>
</div>