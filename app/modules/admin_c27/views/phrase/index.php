<a href = "<?= AddExtend::getURL($this, '/phrase/add') ?>" style = "color: white; text-decoration: none;"
   class = "btn btn-primary">
    Добавить новую
</a>
<?php
    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'               => 'phrase-list',
            'dataProvider'     => $phrase->search(),
            'ajaxType'         => 'POST',
            'filter'           => $phrase,
            'enablePagination' => true,
            'itemsCssClass'    => 'table table-bordered table-striped dataTable',
            'template'         => '{items}{pager}',
            'pager'            => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'    => 'p',
            'columns'          => array(
                'id',
                'author',
                'phrase',

                array(
                    'class'       => 'CButtonColumn',
                    'template'    => '{update}{delete}',
                    'buttons'     => array(
                        'update' => array(
                            'title' => 'Редактировать',
                            'imageUrl' => AddExtend::pathCss() . 'admin/images/admin/edit.png'
                        ),
                        'delete' => array(
                            'title' => 'Удалить',
                            'imageUrl' => AddExtend::pathCss() . 'admin/images/admin/delete.png'
                        ),
                    ),
                    'htmlOptions' => array( 'rows' => 40 )
                )
            )
        )
    );