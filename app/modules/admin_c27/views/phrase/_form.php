
<?php
    /* @var $this PhraseController */
    /* @var $model Phrase */
    /* @var $form CActiveForm */
    AddExtend::regCssFile(array(
                              AddExtend::pathCss() . 'admin/fix.css'
                          ), 'screen'
    );
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'phrase-_form-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>true,
        )); ?>

    <p class="note">Поля помеченные <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->hiddenField($model,'id'); ?>
        <?php echo $form->error($model,'id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'author'); ?>
        <?php echo $form->textField($model,'author', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model,'author'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'phrase'); ?>
        <?php echo $form->textArea($model,'phrase', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model,'phrase'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-success')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->