<button id = "add" class = "btn btn-primary">
    Добавить новый
</button>
<?php


    AddExtend::regCoreScript('jquery.ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js');
    AddExtend::regScriptFile(AddExtend::pathJs() . 'admin/sortable.js');


    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'                    => 'delivery',
            'dataProvider'          => $delivery->search(),
            'filter'                => $delivery,
            'enableHistory'         => true,
            'enablePagination'      => true,
            'ajaxUpdate'            => 'POST',
            'itemsCssClass'         => 'table table-bordered table-striped dataTable',
            'template'              => '{items}{pager}',
            'rowCssClassExpression' => '"items[]_{$data->id}"',
            'pager'                 => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'         => 'p',
            'columns'               => array(
                'id',
                'title',
                'text',
                array(
                    'class'       => 'CButtonColumn',
                    'template'    => '{update}{delete}',
                    'buttons'     => array(
                        'update' => array(
                            'title' => 'Редактировать',
                            'imageUrl' => AddExtend::pathCss() . 'admin/images/admin/edit.png'
                        ),
                        'delete' => array(
                            'title' => 'Удалить',
                            'imageUrl' => AddExtend::pathCss() . 'admin/images/admin/delete.png'
                        ),
                    ),
                    'htmlOptions' => array( 'rows' => 40 )
                )
            )
        )
    );
?>

<script type = "text/javascript">
    $(document).ready(function () {
        $('#add').click(function () {
            document.location.href = adminUrl + controller + '/add';
        });

        $('#delivery').sortableGrid();
    });
</script>
