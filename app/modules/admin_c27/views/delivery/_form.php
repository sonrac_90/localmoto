<?php

    /* @var $this DeliveryController */
    /* @var $model Delivery */
    /* @var $form CActiveForm */
    AddExtend::regCssFile(array(
                              AddExtend::pathCss() . 'admin/fix.css'
                          ), 'screen'
    );
?>

<div class = "form">

    <?php $form = $this->beginWidget(
        'CActiveForm', array(
            'id'                   => 'pay-_form-form',
            'enableAjaxValidation' => true,
            'clientOptions'        => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType'   => true
            ),
        )
    ); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class = "row">
        <?php echo $form->hiddenField($model, 'id'); ?>
    </div>

    <div class = "row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class = "row">
        <?php echo $form->labelEx($model, 'text'); ?>
        <?php echo $form->textArea($model, 'text', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model, 'text'); ?>
    </div>


    <div class = "row buttons">
        <?php
            echo CHtml::submitButton(
                'Сохранить', array(
                    'class' => 'btn btn-success'
                )
            );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div>
