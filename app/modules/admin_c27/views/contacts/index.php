<?php

    $this->widget(
        'zii.widgets.grid.CGridView', array(
            'id'               => 'pay',
            'dataProvider'     => $contact->search(),
            'filter'           => $contact,
            'enableHistory'    => true,
            'enablePagination' => true,
            'ajaxUpdate'       => 'POST',
            'itemsCssClass'    => 'table table-bordered table-striped dataTable',
            'template'         => '{items}{pager}',
            'pager'            => array(
                'header'               => '',
                'prevPageLabel'        => '<',
                'nextPageLabel'        => '>',
                'lastPageLabel'        => '>>',
                'firstPageLabel'       => '<<',
                'hiddenPageCssClass'   => '',
                'pageSize'             => 50,
                'htmlOptions'          => array( 'class' => 'pagination pagination-centered' ),
                'selectedPageCssClass' => 'active',
            ),
            'pagerCssClass'    => 'p',
            'columns'          => array(
                'id',
                array(
                    'name'  => 'phone',
                    'value' => function ($data) {
                        return $data->phone[ 0 ] . ' - ' . substr($data->phone, 1, 3) . ' - ' . substr($data->phone, 4, strlen($data->phone) - 1);
                    },
                ),
                'email',
                'address',
                array(
                    'name'  => 'about',
                    'value' => function ($data) {
                        $len = strlen($data->about);

                        return ( $len <= 200 ) ? $data->about : substr($data->about, 0, 200) . " ... ";
                    }
                ),
                array(
                    'class'       => 'CButtonColumn',
                    'template'    => '{update}',
                    'buttons'     => array(
                        'update' => array(
                            'title' => 'Редактировать',
                            'imageUrl' => AddExtend::pathCss() . 'admin/images/admin/edit.png'
                        ),
                    ),
                    'htmlOptions' => array( 'rows' => 40 )
                )
            )
        )
    );
?>

<script type = "text/javascript">
    $(document).ready(function () {
        $('#add').click(function () {
            document.location.href = adminUrl + controller + '/add';
        })
    });
</script>
