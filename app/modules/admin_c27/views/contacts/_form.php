<?php

    /* @var $this ContactsController */
    /* @var $model Contacts */
    /* @var $form CActiveForm */
    AddExtend::regCssFile(array(
                              AddExtend::pathCss() . 'admin/fix.css'
                          ), 'screen'
    );

    AddExtend::regScriptFile(
        array(
            AddExtend::pathJs() . 'libs/ckeditor/ckeditor.js',
            AddExtend::pathJs() . 'libs/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        )
    );
?>

<div class = "form">

    <?php $form = $this->beginWidget(
        'CActiveForm', array(
            'id'                   => 'pay-_form-form',
            'enableAjaxValidation' => true,
            'clientOptions'        => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType'   => true
            ),
        )
    ); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class = "row">
        <?php echo $form->hiddenField($model, 'id'); ?>
    </div>

    <div class = "row">
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->textField($model, 'city', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model, 'city'); ?>
    </div>

    <div class = "row">
        <?php echo $form->labelEx($model, 'address'); ?>
        <?php echo $form->textField($model, 'address', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model, 'address'); ?>
    </div>

    <div class = "row">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php
            $this->widget(
                'CMaskedTextField', array(
                    'model'       => $model,
                    'attribute'   => 'phone',
                    'mask'        => '8-999-9999999',
                    'htmlOptions' => array(
                        'size'      => 15,
                        'maxlength' => 11,
                        'class'     => 'inputWidth'
                    )
                )
            );

            echo $form->error($model, 'phone');
        ?>
    </div>

    <div class = "row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('class' => 'inputWidth')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class = "row">
        <div class = 'col-md-12'>
            <div class = 'box box-info'>
                <div class = 'box-header'>
                    <h3 class = 'box-title'>
                        <?php echo $form->labelEx($model, 'about'); ?>
                    </h3>
                    <!-- tools box -->
                    <div class = "pull-right box-tools">
                        <button onclick = "return false;" class = "btn btn-info btn-sm" data-widget = 'collapse'
                                data-toggle = "tooltip" title = "Collapse">
                            <i class = "fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class = 'box-body pad'>
                    <?php echo $form->textArea(
                        $model, 'about', array(
                            'id'    => 'editor1',
                            'style' => 'width: 100%; height: 250px;',
                        )
                    ); ?>
                </div>
            </div>
            <!-- /.box -->
            <?php echo $form->error($model, 'about'); ?>
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
    <div class = "row buttons">
        <?php
            echo CHtml::submitButton(
                'Сохранить', array(
                    'class' => 'btn btn-success'
                )
            );
        ?>
    </div>

    <?php $this->endWidget(); ?>

    <script type = "text/javascript">
        $(function () {
            CKEDITOR.replace('editor1', {
                contestLanguage          : 'ru',
                contentsCss              : [
                    '<?= AddExtend::pathJs(true) ?>libs/ckeditor/contents.css',
                    '<?= AddExtend::pathCss(true) ?>base.css',
                    '<?= AddExtend::pathCss(true) ?>style.css',
                    '<?= AddExtend::pathCss(true) ?>article.css'
                ],
                scayt_sLang              : 'ru_RU',
                language                 : 'ru',
                contentsLanguage         : 'ru',
                charset                  : 'utf-8',
                templates_replaceContent : false,
//                autoUpdateElement        : false,
//                autoUpdateElementJquery  : false,
                autocomplete             : true,
                allowedContent           : true,
                entities                 : false,
                entities_latin           : false,
                entities_greek           : false,
                entities_additional      : false,
                basicEntities            : false
            });
        });


        if(typeof(CKEDITOR) !== 'undefined') {
            for(var name in CKEDITOR.instances) {
                CKEDITOR.instances[name].on("instanceReady", function() {
                    // Set keyup event
                    this.document.on("keyup", updateValue);
                    // Set paste event
                    this.document.on("paste", updateValue);
                });

                function updateValue() {
                    CKEDITOR.instances[name].updateElement();
                    $('#editor1').trigger('keyup');
                }
            }
        }
    </script>

</div>


<script type="text/javascript" >
    $(document).ready(function () {
        console.log($('.pull-right .fa-minus'));
        $('.pull-right .btn-sm').on('click', function () {
            var self = $(this).find('.fa-minus');
            if (!self.size())
                self = $(this).find('.fa-minus1');

            if ($(self).attr('data-class') == 'plus') {
                $(self).attr('data-class', 'minus');
                $(self).removeClass('fa-minus1').addClass('fa-minus');
            } else {
                $(self).attr('data-class', 'plus');
                $(self).removeClass('fa-minus').addClass('fa-minus1');
            }
        });
    });
</script>