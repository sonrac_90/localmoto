<html>
<head>
    <title>
        FancyTree Example
    </title>
    <script src = "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel = "stylesheet" href = "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src = "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script src = "<?= AddExtend::baseUrl() ?>shared/media/js/admin/fancytree/jquery.fancytree-all.min.js"></script>
    <link rel = "stylesheet" href = "<? AddExtend::baseUrl() ?>shared/media/bootstrap3/css/bootstrap.min.css" />
    <link rel = "stylesheet"
          href = "<? AddExtend::baseUrl() ?>shared/media/js/admin/fancytree/skin-themeroller/ui.fancytree.min.css" />
    <script type = "text/javascript">

        $(function () {
            // Attach the fancytree widget to an existing <div id="tree"> element
            // and pass the tree options as an argument to the fancytree() function:
            $("#tree").fancytree({
                                     extensions : ["dnd"],
//                source: {
//                    url: "ajax-tree-fs.json"
//                },
                                     dnd        : {
                                         preventVoidMoves      : true, // Prevent dropping nodes 'before self', etc.
                                         preventRecursiveMoves : true, // Prevent dropping nodes on own descendants
                                         autoExpandMS          : 400,
                                         dragStart             : function (node, data) {
                                             /** This function MUST be defined to enable dragging for the tree.
                                              *  Return false to cancel dragging of node.
                                              */
                                             return true;
                                         },
                                         dragEnter             : function (node, data) {
                                             /** data.otherNode may be null for non-fancytree droppables.
                                              *  Return false to disallow dropping on node. In this case
                                              *  dragOver and dragLeave are not called.
                                              *  Return 'over', 'before, or 'after' to force a hitMode.
                                              *  Return ['before', 'after'] to restrict available hitModes.
                                              *  Any other return value will calc the hitMode from the cursor position.
                                              */
                                             // Prevent dropping a parent below another parent (only sort
                                             // nodes under the same parent)
                                             /* 					if(node.parent !== data.otherNode.parent){
                                              return false;
                                              }
                                              // Don't allow dropping *over* a node (would create a child)
                                              return ["before", "after"];
                                              */
                                             return true;
                                         },
                                         dragDrop              : function (node, data) {
                                             /** This function MUST be defined to enable dropping of items on
                                              *  the tree.
                                              */
                                             data.otherNode.moveTo(node, data.hitMode);
                                         }
                                     },
                                     activate   : function (event, data) {
//				alert("activate " + data.node);
                                     }
//                lazyLoad: function(event, data) {
//                    data.result = {url: "ajax-sub2.json"}
//                }
                                 });
        });

    </script>
</head>

<div id = "tree">
    <ul id = "treeData" style = "">
        <li id = "1">Node 1
        <li id = "2" class = "folder">Folder 2
            <ul>
                <li id = "3">Node 2.1
                <li id = "4">Node 2.2
            </ul>
    </ul>
</div>
</html>