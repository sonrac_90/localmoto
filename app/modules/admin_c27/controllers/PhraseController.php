<?php

    class PhraseController extends AdminController {

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $phrase = new Phrase('search');
            $phrase->unsetAttributes();
            if (isset($_POST[ 'Phrase' ])) {
                $phrase->attributes = $_POST[ 'Phrase' ];
            }

            $this->render('index', array(
                    'phrase' => $phrase
                )
            );
        }

        public function actionAdd() {
            $this->actionUpdate(null);
        }

        public function actionUpdate($id) {
            if (is_null($id))
                $phrase = new Phrase();
            else
                $phrase = Phrase::model()->findByPk($id);

            if (AddExtend::isAjax()) {
                $phrase->attributes = $_POST['Phrase'];
                echo CActiveForm::validate($phrase);
                exit;
            }

            if (isset($_POST['Phrase'])) {
                $phrase->attributes = $_POST['Phrase'];
                if ($phrase->validate()) {
                    if (!is_null($id)) {
                        if ($phrase->update()) {
                            $this->redirect($this->createUrl('phrase/index'));
                        }
                    } else
                        if ($phrase->save()) {
                            $this->redirect($this->createUrl('phrase/index'));
                        }
                }
            }

            $this->render('_form', array(
                    'model' => $phrase
                )
            );

        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }