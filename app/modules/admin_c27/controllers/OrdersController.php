<?php

    class OrdersController extends AdminController {

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $order = new OrderMain('search');
            $order->unsetAttributes();
            if (isset($_POST[ 'OrderMain' ]))
                $order->attributes = $_POST['OrderMain'];

            $this->render('index', array(
                    'order' => $order
                )
            );
        }

        public function actionView ($id) {
            $order = new Order('search');
            $order->unsetAttributes();

            if (isset($_POST[ 'Order' ])) {
                $order->attributes = $_POST[ 'Order' ];
            }

            $order->order_id = $id;

            $this->render('detail/index', array(
                    'order' => $order
                )
            );
        }

        public function actionDelete ($id) {
            Order::model()->deleteByPk($id);
        }

        public function actionDeleteOrderMain ($id) {
            OrderMain::model()->deleteByPk($id);
        }

        public function actionUpdate ($id) {
            if (isset($_POST[ 'status' ])) {
                /** @var OrderMain $order */
                $order = OrderMain::model()->findByPk($id);
                if ($order) {
                    $order->status = $_POST['status'];
                    if (isset($_POST['statusPay']))
                        $order->status_pay = $_POST['statusPay'];

                    if (isset($_POST['statusPay']))
                        $order->status_delivery = $_POST['statusDelivery'];

                    $order->update();
                    $allChildOrder = Order::model()->findAllByAttributes(array('order_id' => $order->getPrimaryKey()));
                    /** @var Order $_child */
                    foreach ($allChildOrder as $_child) {
                        $_child->status = $_POST[ 'status' ];
                    }
                }
            }
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }