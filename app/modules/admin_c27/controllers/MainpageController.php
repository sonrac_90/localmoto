<?php

    class MainpageController extends AdminController {
        private $md5prefix = "SE34dwe4xk034sdk5mep23bsEN";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionDeleteSliderPhoto ($id) {
            Slider::model()->deleteByPk($id);
        }

        public function actionIndex() {

            $criteria = new CDbCriteria();
            $criteria->order = "position ASC";

            $slider = Slider::model()->findAll($criteria);

            $mainBlock = MainBlock::model()->findByPk(1);

            if (isset($_POST['text']) && !empty($_POST['text'])) {
                $mainBlock->text = $_POST['text'];
                $mainBlock->update();
            }

            $this->render('index',
                         array(
                             'slider' => $slider,
                             'main'   => $mainBlock,
                        )
            );
        }

        public function actionChangeIndex () {
            if (isset($_POST['id'])) {
                $id = CJSON::decode($_POST['id'], true);

                $position = 0;
                foreach ($id as $key => $value) {
                    $position++;
                    Slider::model()->updateByPk($value, array('position' => $position));
                }
            }
        }

        public function actionUploadSlider() {
            $path = AddExtend::basePath() . "/../" . Slider::PathImage;
            // Удаляю все загруженные, но не сохраненные фото
            //            RecursiveScan::RemoveDir($path . "tmp/");

            if (!is_writable($path)) {
                AddExtend::answer(
                    false, array( 'textError' => 'Директория защищена от записи', 'dir' => $path )
                );
                exit;
            }
            if (!sizeof($_FILES)) {
                AddExtend::answer(
                    false, array( 'textError' => 'Не передано ни одного файла' )
                );
                exit;
            }

            $file = $_FILES[ 'file' ];
            $nameFile = array();
            foreach ($file[ 'name' ] as $key => $_file) {
                $tmpName = $file[ 'tmp_name' ][ $key ];
                $newName = $path . "/tmp/" . CropImage::addSalt($_file);
                CropImage::renameFile($tmpName, $newName);
                $nameFile[ $key ] = CropImage::cropImagegick(
                    $newName, array(
                        'width'  => 900,
                        'height' => 518
                    ), array(
                        'width'  => 250,
                        'height' => 250 * 0.75
                    )
                );
                @unlink($newName);
            }

            $url = Slider::PathImage . "/tmp/" . basename($nameFile[ 0 ][ 'black' ]);

            $sliderPhoto = new Slider();
            $sliderPhoto->ext = pathinfo($url, PATHINFO_EXTENSION);
            $sliderPhoto->position = 1;

            $save = false;
            if ($sliderPhoto->validate()) {
                if ($sliderPhoto->save()) {
                    $save = true;
                    $slashes = "/..";
                    if (AddExtend::baseUrl())
                        $slashes = "/../";
                    $newFileName = $sliderPhoto->id . "." . pathinfo($url, PATHINFO_EXTENSION);

                    $path .= "/" . CropImage::dynamicPath($sliderPhoto->getPrimaryKey()) . "/";
                    CropImage::rmkdir($path);

                    $nameFileArr = array(
                        'file1Old' => AddExtend::basePath() . $slashes . $url,
                        'file1New' => $path . "/" . $newFileName,
                        'file2Old' => AddExtend::basePath() . $slashes . str_replace('-b.', '-t.', $url),
                        'file2New' => $path . "/" . $sliderPhoto->id . "-t." . pathinfo($url, PATHINFO_EXTENSION)
                    );

                    $res1 = @rename($nameFileArr['file1Old'], $nameFileArr['file1New']);
                    $res2 = @rename($nameFileArr['file2Old'], $nameFileArr['file2New']);
                    if (!$res1 || !$res2) {
                        AddExtend::answer(false, $nameFileArr);
                        exit();
                    }

                }
            }

            if (!$save) {
                AddExtend::answer(
                    false, array(
                        'textError' => 'Не удалоь сохранить',
                        'error'     => $sliderPhoto->getErrors()
                    )
                );
                foreach ($nameFile as $_nextFileName) {
                    @unlink($_nextFileName[ 'black' ]);
                    @unlink($_nextFileName[ 'color' ]);
                    exit();
                }
            }

            AddExtend::answer(
                true, array(
                    'image_url' => Yii::app()->getBaseUrl(true) . "/" . Slider::PathImage . '/' . CropImage::dynamicPath($sliderPhoto->id) . '/' . $sliderPhoto->id . "-t." . $sliderPhoto->ext,
                    'id'        => $sliderPhoto->id,
                    'position'  => $sliderPhoto->position,
                )
            );
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    $url = Yii::app()->user->returnUrl;
                    if ($url == '/' || $url == '/index.php')
                        $this->redirect(Yii::app()->createUrl($this->module->id . '/default/index'));
                    $this->redirect($url);
                }
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        public function actionLogout() {
            Yii::app()->user->logout(true);
            $this->redirect(AddExtend::getURL($this, "/default/login"));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ];
                else
                    $this->render('error', $error);
            }
        }

    }