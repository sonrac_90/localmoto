<?php

    class PayController extends AdminController {
        private $md5prefix = "SE34dwe4xk034sdk5mep23bsEN";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $pay = new Pay('search');
            $pay->unsetAttributes();

            if (isset( $_POST[ 'Pay' ] ))
                $pay->attributes = $_POST[ 'Pay' ];

            $this->render(
                'index', array(
                    'pay' => $pay
                )
            );
        }

        public function actionAdd() {

            $pay = new Pay();
            $pay->unsetAttributes();

            if (AddExtend::isAjax()) {
                $pay->attributes = $_POST[ 'Pay' ];
                CJSON::decode(
                    CActiveForm::validate($pay)
                );
                exit;
            }

            if (isset( $_POST[ 'Pay' ] )) {
                $pay->attributes = $_POST[ 'Pay' ];

                if ($pay->validate()) {
                    if ($pay->save()) {
                        $this->redirect($this->createUrl('pay/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $pay
                )
            );
        }

        public function actionUpdate($id) {

            /** @var Pay $pay */
            $pay = Pay::model()->findByPk($id);

            if (AddExtend::isAjax()) {
                $pay->attributes = $_POST[ 'Pay' ];
                echo CActiveForm::validate($pay);
                exit;
            }

            if (isset( $_POST[ 'Pay' ] )) {
                $pay->attributes = $_POST[ 'Pay' ];
                $pay->position = $pay->getMaxPosition();
                $pay->attributes[ 'position' ] = $pay->position;

                if ($pay->validate()) {
                    if ($pay->update()) {
                        $this->redirect($this->createUrl('pay/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $pay
                )
            );

        }

        public function actionChangeIndex() {
            if (isset( $_POST[ 'items' ] ) && is_array($_POST[ 'items' ])) {
                $i = 0;
                foreach ($_POST[ 'items' ] as $item) {
                    $pay = Pay::model()->findByPk($item);
                    $pay->position = $i;
                    $pay->update();
                    $i++;
                }
            }

            exit;
        }

        public function actionDelete($id) {
            Pay::model()->deleteByPk($id);
            $this->redirect($this->createUrl('pay/index'));
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        public function actionLogout() {
            Yii::app()->user->logout(true);
            $this->redirect(AddExtend::getURL($this, "/default/login"));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }
