<?php

    class ContactsController extends AdminController {
        private $md5prefix = "SE34dwe4xk034sdk5mep23bsEN";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $contact = new Contacts('search');
            $contact->unsetAttributes();

            if (isset ( $_POST[ 'Contacts' ] ))
                $contact->attributes = $_POST[ 'Contacts' ];
            $this->render(
                'index', array(
                    'contact' => $contact
                )
            );
        }

        public function actionAdd() {

            $contact = new Contacts();
            $contact->unsetAttributes();

            if (AddExtend::isAjax()) {
                $contact->attributes = $_POST[ 'Contacts' ];

                $contact->phone = intval(str_replace('-', '', $contact->phone));

                CJSON::decode(
                    CActiveForm::validate($contact)
                );
                exit;
            }

            if (isset( $_POST[ 'Contacts' ] )) {
                $contact->attributes = $_POST[ 'Contacts' ];

                $contact->phone = intval(str_replace('-', '', $contact->phone));

                if ($contact->validate()) {
                    if ($contact->save()) {
                        $this->redirect($this->createUrl('contacts/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $contact
                )
            );
        }

        public function actionUpdate($id) {

            $contact = Contacts::model()->findByPk($id);

            if (AddExtend::isAjax()) {
                $_POST[ 'Contacts' ][ 'phone' ] = intval(str_replace('-', '', $_POST[ 'Contacts' ][ 'phone' ]));

                $contact->attributes = $_POST[ 'Contacts' ];

                echo CActiveForm::validate($contact);
                exit;
            }

            if (isset( $_POST[ 'Contacts' ] )) {

                $_POST[ 'Contacts' ][ 'phone' ] = intval(str_replace('-', '', $_POST[ 'Contacts' ][ 'phone' ]));

                $contact->attributes = $_POST[ 'Contacts' ];

                if ($contact->validate()) {
                    if ($contact->update()) {
                        $this->redirect($this->createUrl('contacts/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $contact
                )
            );

        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        public function actionLogout() {
            Yii::app()->user->logout(true);
            $this->redirect(AddExtend::getURL($this, "/default/login"));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }
