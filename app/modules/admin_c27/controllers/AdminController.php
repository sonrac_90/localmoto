<?php

    class AdminController extends Controller {
        public $title         = '';
        public $layout        = "/layouts/main";
        public $defaultAction = 'index';


        public function filters() {
            return array( 'accessControl' );
        }

        public function accessRules() {
            return array(
                array(
                    'allow',
                    // allow all users to access 'index' and 'view' actions.
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    // deny all users
                    'users' => array( '*' ),
                ),
            );
        }
    }