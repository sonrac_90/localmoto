<?php

    class ChangeController extends AdminController {
        private $md5prefix = "SE34dwe4xk034sdk5mep23bsEN";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $change = new Change('search');
            $change->unsetAttributes();

            if (isset( $_POST[ 'Change' ] ))
                $change->attributes = $_POST[ 'Change' ];

            $this->render(
                'index', array(
                    'change' => $change
                )
            );
        }

        public function actionAdd() {

            $change = new Change();
            $change->unsetAttributes();

            if (AddExtend::isAjax()) {
                $change->attributes = $_POST[ 'Change' ];
                CJSON::decode(
                    CActiveForm::validate($change)
                );
                exit;
            }

            if (isset( $_POST[ 'Change' ] )) {
                $change->attributes = $_POST[ 'Change' ];

                $change->position = $change->getMaxPosition();
                $change->attributes[ 'position' ] = $change->position;

                if ($change->validate()) {
                    if ($change->save()) {
                        $this->redirect($this->createUrl('change/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $change
                )
            );
        }

        public function actionUpdate($id) {

            $change = Change::model()->findByPk($id);

            if (AddExtend::isAjax()) {
                $change->attributes = $_POST[ 'Change' ];
                echo CActiveForm::validate($change);
                exit;
            }

            if (isset( $_POST[ 'Change' ] )) {
                $change->attributes = $_POST[ 'Change' ];

                if ($change->validate()) {
                    if ($change->update()) {
                        $this->redirect($this->createUrl('change/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $change
                )
            );

        }

        public function actionDelete($id) {
            Delivery::model()->deleteByPk($id);
            $this->redirect($this->createUrl('change/index'));
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        public function actionLogout() {
            Yii::app()->user->logout(true);
            $this->redirect(AddExtend::getURL($this, "/default/login"));
        }

        public function actionChangeIndex() {
            if (isset( $_POST[ 'items' ] ) && is_array($_POST[ 'items' ])) {
                $i = 0;
                foreach ($_POST[ 'items' ] as $item) {
                    $change = Change::model()->findByPk($item);
                    $change->position = $i;
                    $change->update();
                    $i++;
                }
            }

            exit;
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }
