<?php

    class UsersController extends AdminController {

        public $pageTitle = "Users";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $users = new Users('search');
            $users->unsetAttributes();

            if (isset( $_POST[ 'Users' ] )) {
                $users->attributes = $_POST[ 'Users' ];
            }

            $this->render(
                'index', array(
                    'users' => $users
                )
            );
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

        public function actionAdd() {
            $this->actionUpdate(0);
        }

        public function actionUpdate($id) {
            //        $id = intval($id);
            if ($id == 0) {
                $user = new Users();
                $address[ 0 ] = new Address();
            } else {
                $user = Users::model()->findByPk($id);
                $address = Address::model()->findAllByAttributes(array(), "user_id=:uId", array( ':uId' => $id ));
            }

            if (AddExtend::isAjax() && !isset( $_POST[ 'saveData' ] )) {
                if (isset( $_POST[ 'Users' ] )) {
                    $user->attributes = $_POST[ 'User' ];
                    echo CActiveForm::validate($user);
                }
                Yii::app()->end();
            }
            $ok = false;

            if (isset( $_POST[ 'Users' ] )) {
                foreach ($_POST[ 'Users' ] as $key => $val)
                    $user[ $key ] = $val;

                $user->birthday = date('Y-m-d', $user->birthday);

                if (is_null($user->register_date))
                    $user->register_date = date('d-m-y H:m:s');

                if ($user->validate())
                    if ($id > 0) {
                        if ($user->update()) {
                            $ok = true;
                        }
                    } else {
                        if ($user->save())
                            $ok = true;
                    }
            }

            if (isset( $_POST[ 'Address' ] )) {
                $save = true;
                foreach ($_POST[ 'Address' ] as $key => $value) {
                    if (!isset( $address[ $key ] ))
                        $address[ $key ] = new Address();

                    $address[ $key ]->attributes = $value;
                    if ($id <= 0) {
                        $address[ $key ][ 'user_id' ] = $user->getPrimaryKey();
                    }

                    if (is_null($address[ $key ][ 'date_add' ]))
                        $address[ $key ][ 'date_add' ] = date('d-m-y H:m:s');

                    if (!$address[ $key ]->validate())
                        $save = false; else if ($id > 0)
                        if (isset( $address[ 'key' ]->id ) && !is_null($address[ 'key' ]->id) && $id > 0) {
                            if (!$address[ $key ]->update())
                                $save = false;
                        } else {
                            if (!$address[ $key ]->save())
                                $save = false;
                        } else {
                        if (!$address[ $key ]->save()) {
                            $save = false;
                        }
                    }
                }
                if ($save && $ok)
                    $ok = true;
                else
                    $ok = false;
            }

            if ($ok)
                if (!isset( $_POST[ 'saveData' ] ))
                    $this->redirect(AddExtend::getURL($this, "/users/index"));
                else
                    exit( json_encode(
                        array(
                            'url' => AddExtend::getURL($this, "users/index")
                        )
                    ) );
            else {
            }
            $this->render(
                'update', array(
                    'user'     => $user,
                    'adresses' => $address
                )
            );
        }

        public function actionDeleteAddress($id) {
            if (!is_numeric($id)) {
                exit;
            } else {
                $userAddress = Address::model()->findByPk($id);
                $userAddress->delete();
            }
        }

        public function actionDelete($id) {
            $user = Users::model()->findByPk($id);
            $user->delete();
        }
    }