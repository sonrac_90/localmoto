<?php

    class TestController extends AdminController {

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $image = new Imagick(Yii::app()->basePath . "/..shared/uploads/goods/tmp/bell1_2_2.png");
            $image->scaleimage(100, 100);
            $newName = Yii::app()->basePath . "/..shared/uploads/goods/tmp/bell1_2_2_scale.png";
            $newNameUrl = AddExtend::baseUrl(true) . "shared/uploads/goods/tmp/bell1_2_2_scale.png";
            $image->writeimage($newName);

            echo "<img src='{$newNameUrl}' />";
            echo "<pre>";
            print_r($image);
            echo "</pre>";
            exit;
            //        $this->render('index', array());
        }

        private function recursiveChmod($path, $filePerm = 0777, $dirPerm = 0777) {
            if (!file_exists($path)) {
                return ( false );
            }
            if (is_file($path)) {
                chmod($path, $filePerm);
            } elseif (is_dir($path)) {
                $foldersAndFiles = scandir($path);
                $entries = array_slice($foldersAndFiles, 2);
                foreach ($entries as $entry) {
                    $this->recursiveChmod($path . "/" . $entry, $filePerm, $dirPerm);
                }
                $a = chmod($path, $dirPerm);
                var_dump($a);
            }

            return ( true );
        }

        public function actionChmod () {
            $path = AddExtend::basePath() . '/..' . Photo::PATH_IMAGE;
            var_dump($this->recursiveChmod($path));
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }