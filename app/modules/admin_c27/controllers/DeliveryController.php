<?php

    class DeliveryController extends AdminController {
        private $md5prefix = "SE34dwe4xk034sdk5mep23bsEN";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $delivery = new Delivery('search');
            $delivery->unsetAttributes();

            if (isset( $_POST[ 'Delivery' ] ))
                $delivery->attributes = $_POST[ 'Delivery' ];

            $this->render(
                'index', array(
                    'delivery' => $delivery
                )
            );
        }

        public function actionAdd() {

            $delivery = new Delivery();
            $delivery->unsetAttributes();

            if (AddExtend::isAjax()) {
                $delivery->attributes = $_POST[ 'Delivery' ];
                CJSON::decode(
                    CActiveForm::validate($delivery)
                );
                exit;
            }

            if (isset( $_POST[ 'Delivery' ] )) {
                $delivery->attributes = $_POST[ 'Delivery' ];

                $delivery->position = $delivery->getMaxPosition();
                $delivery->attributes[ 'position' ] = $delivery->position;

                if ($delivery->validate()) {
                    if ($delivery->save()) {
                        $this->redirect($this->createUrl('delivery/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $delivery
                )
            );
        }

        public function actionUpdate($id) {

            $delivery = Delivery::model()->findByPk($id);

            if (AddExtend::isAjax()) {
                $delivery->attributes = $_POST[ 'Delivery' ];
                echo CActiveForm::validate($delivery);
                exit;
            }

            if (isset( $_POST[ 'Delivery' ] )) {
                $delivery->attributes = $_POST[ 'Delivery' ];

                if ($delivery->validate()) {
                    if ($delivery->update()) {
                        $this->redirect($this->createUrl('delivery/index'));
                    }
                }
            }

            $this->render(
                '_form', array(
                    'model' => $delivery
                )
            );

        }

        public function actionDelete($id) {
            Delivery::model()->deleteByPk($id);
            $this->redirect($this->createUrl('delivery/index'));
        }

        public function actionChangeIndex() {
            if (isset( $_POST[ 'items' ] ) && is_array($_POST[ 'items' ])) {
                $i = 0;
                foreach ($_POST[ 'items' ] as $item) {
                    $delivery = Delivery::model()->findByPk($item);
                    $delivery->position = $i;
                    $delivery->update();
                    $i++;
                }
            }

            exit;
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        public function actionLogout() {
            Yii::app()->user->logout(true);
            $this->redirect(AddExtend::getURL($this, "/default/login"));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }
