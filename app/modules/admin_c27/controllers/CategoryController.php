<?php

    class CategoryController extends AdminController {

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $category = Category::model()->getAllCategory();

            $this->render(
                'index', array(
                    'category' => $category
                )
            );
        }

        public function actionUpdateIndex() {
            $data = json_decode($_POST[ 'data' ], true);

            $position = 0;
            foreach ($data as $_next) {
                $position++;
                $id = intval($_next[ 'key' ]);
                Category::model()->updateByPk(
                    $id, array(
                        'position'        => $position,
                        'parent_position' => null,
                        'parent'          => null
                    )
                );
                if (isset( $_next[ 'children' ] )) {
                    $positionChild = 1;
                    foreach ($_next[ 'children' ] as $_child) {
                        $position++;
                        $idChild = intval($_child[ 'key' ]);
                        Category::model()->updateByPk(
                            intval($_child[ 'key' ]), array(
                                'position'        => $position,
                                'parent'          => $id,
                                'parent_position' => $positionChild
                            )
                        );

                        $positionChild++;

                        if (isset( $_child[ 'children' ] )) {
                            $positionChildTwo = 1;
                            foreach ($_child[ 'children' ] as $_childTwo) {
                                $position++;
                                Category::model()->updateByPk(
                                    intval($_childTwo[ 'key' ]), array(
                                        'position'        => $position,
                                        'parent'          => $idChild,
                                        'parent_position' => $positionChildTwo
                                    )
                                );
                                $positionChildTwo++;
                            }
                        }
                    }
                }
            }

        }

        public function actionUpdate() {
            $id = $_POST[ 'id' ];
            $newName = $_POST[ 'newName' ];

            Category::model()->updateByPk(intval($id), array( 'name' => $newName ));
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

        public function actionDelete($id) {
            Category::model()->deleteByPk($id);
            $allChild = Category::model()->findAllByAttributes(array( 'parent' => $id ));
            foreach ($allChild as $_child) {
                $_child->parent = null;
                Category::model()->updateByPk(
                    $_child->id, array(
                        'parent' => null,
                    )
                );
            }
        }

        public function actionAdd() {
            $name = $_POST[ 'name' ];
            $category = new Category();

            $category->name = $name;
            if (isset( $_POST[ 'id' ] ))
                $category->parent = $_POST[ 'id' ];

            $category->position = $category->getMaxPosition() + 1;
            $category->date_create = AddExtend::getDateNow();
            if (!$category->save()) {
                exit( CJSON::encode(array_merge($category->getErrors(), array( 'error' => 'Некорректно заполнены поля' ))) );
            } else {
                exit( CJSON::encode($category->attributes) );
            }
        }

        /**
         * Return buttons for elem
         *
         * @param $id
         *
         * @return string
         */
        public function buttonBlock($id) {
            return '
        <div class="btnCat">
            <span class="view' . $id . '">
                <a title="Просмотеть товары категории" href="' . AddExtend::getURL($this, "/goods/index/id/" . $id . "/type/cat") . '">
                    <img class = "view" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/visible.png" class="viewEYE"/>
                </a>
            </span>
            <span class="edit' . $id . '">
                <img title="Переименовать" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/edit.png"/>
            </span>
            <span class="delete' . $id . '">
                <img title="Удалить" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/delete.png"/>
            </span>
        </div>';
        }
    }
