<?php

    class GoodsController extends AdminController {

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex($id = null, $type = null) {
            $goods = new Goods('search');
            $goods->unsetAttributes();

            if (isset( $_POST[ 'Goods' ] ))
                $goods->attributes = $_POST[ 'Goods' ];

            $this->render(
                'index', array(
                    'goods' => $goods,
                    'id'    => $id,
                    'type'  => $type
                )
            );
        }

        public function actionAdd() {
            // Товар
            $good = new Goods();

            // Категории
            $category = Category::model()->getCategoryById($good->id, null, true);

            // Фотки
            $photos = array();
            $photos[ 0 ] = new Photo ();

            $this->validateGoods();

            $this->saveAndRedirect();

            $this->render(
                '_form', array(
                    'good'     => $good,
                    'photos'   => $photos,
                    'category' => $category,
                )
            );
        }

        private function validateGoods() {
            if (AddExtend::isAjax()) {
                if (isset( $_POST[ 'Goods' ] )) {
                    // Валицация Информации о товаре
                    $validate = new ValidateGoods($_POST[ 'Goods' ], $_POST[ 'CategoryGoods' ]);
                    $validate->validate();
                    exit( CJSON::encode($validate->getValidateResult()) );
                }
            }
        }

        private function saveAndRedirect() {
            if (isset ( $_POST[ 'Goods' ] )) {
                $this->saveData();
                $this->redirect($this->createUrl('goods/index'));
            }
        }

        private function saveData() {
            if (isset( $_POST[ 'Goods' ][ 'id' ] )) {
                $goods = Goods::model()->findByPk($_POST[ 'Goods' ][ 'id' ]);
                GoodsCategory::model()->deleteAllByAttributes(array('goods_id' => $_POST[ 'Goods' ][ 'id' ]));
            } else {
                $goods = new Goods();
            }

            if (is_null($goods))
                $goods = new Goods();

            $goods->attributes = $_POST[ 'Goods' ];
            $goods->producer_id = $_POST[ 'Goods' ][ 'producerName' ];
            if ($goods->getIsNewRecord()) {
                $goods->save();
            } else
                $goods->update();

            foreach ($_POST[ 'CategoryGoods' ] as $_category) {
                $catNew = null;
                if (isset( $_category[ 'id' ] ) && intval($_category[ 'id' ]) > 0)
                    $catNew = GoodsCategory::model()->findByPk($_category[ 'id' ]); else
                    $catNew = new GoodsCategory();

                if (is_null($catNew))
                    $catNew = new GoodsCategory();
                $catNew->attributes = $_category;
                $catNew->goods_id = $goods->getPrimaryKey();
                if ($catNew->getIsNewRecord())
                    $catNew->save(); else
                    $catNew->update();
            }
            if (isset( $_POST[ 'Goods' ][ 'photos' ] )) {
                foreach ($_POST[ 'Goods' ][ 'photos' ] as $numPhoto => $_photo) {
                    $photo = null;
                    if (isset( $_photo[ 'id' ] ))
                        $photo = Photo::model()->findByPk($_photo[ 'id' ]); else
                        $photo = new Photo();

                    if (is_null($photo))
                        $photo = new Photo();

                    $photo->goods_id = $goods->getPrimaryKey();
                    $url = $_photo[ 'url' ];
                    $photo->url = $url;

                    if ($photo->getIsNewRecord()) {
                        $photo->save();
                    } else
                        $photo->update();

                    if (!$photo->getPrimaryKey()) {
                        echo 'Ошибка сохранения фотографии!<pre>';
                        exit( print_r($photo->getErrors()) );
                    }


                    // Переименовывание изображений!!!!!

                    $path = AddExtend::basePath() . "/../" . Photo::PATH_IMAGE . CropImage::dynamicPath($photo->id) . "/";

                    $newFileName = $photo->id . "." . pathinfo($url, PATHINFO_EXTENSION);
                    $newFileNameThumb = $photo->id . "-t." . pathinfo($url, PATHINFO_EXTENSION);

                    CropImage::rmkdir($path);

                    if (strlen($url) > 25) {
                        $slashes = "/..";
                        if (AddExtend::baseUrl())
                            $slashes = "/../";
                        $res1 = @rename(AddExtend::basePath() . $slashes . $url, $path . $newFileName);
                        $res2 = @rename(AddExtend::basePath() . $slashes . str_replace('-b.', '-t.', $url), $path . $newFileNameThumb);

                        if (!$res1 || !$res2) {
                            echo AddExtend::basePath() . $slashes . $url . " " . $path . $newFileName . "<br/>";
                            echo AddExtend::basePath() . $slashes . str_replace('-b.', '-t.', $url) . " " . $path . $newFileName . "<br/>";
                            exit( 'Неудалось переименовать файлы' );
                        }

                        $photo->url = $newFileName;

                        $photo->update();
                    }

                    if (isset( $_photo[ 'description' ][ 'id' ] ) && intval($_photo[ 'description' ][ 'id' ]) > 0)
                        $description = DescriptionGoods::model()->findByPk($_photo[ 'description' ][ 'id' ]); else
                        $description = new DescriptionGoods();

                    $description->photo_id = $photo->id;
                    $description->description = $_photo[ 'description' ][ 'description' ];

                    if (sizeof($description->getErrors())) {
                        echo "<pre>";
                        print_r($description->getErrors());
                        echo "</pre>";
                        exit;
                    }

                    if ($description->getIsNewRecord())
                        $description->save(); else
                        $description->update();

                    $keyPhoto =  $photo->getPrimaryKey();

                    if (isset( $_photo[ 'sizes' ] ) && is_array($_photo[ 'sizes' ]))
                        GoodsSize::model()->deleteAllByAttributes(array('photo_id' => $keyPhoto));
                        foreach ($_photo[ 'sizes' ] as $_size) {
                            $size = new GoodsSize();
                            $size->size = $_size;
                            $size->photo_id = $keyPhoto;

                            $size->save();
                        }

                    if (isset( $_photo[ 'color' ] )) {
                        $color = trim($_photo[ 'color' ]);
                        $color = str_replace(' ', '', str_replace("\n", '', str_replace("\r\n", "", $color)));
                        $colors = explode(',', $color);
                        ColorGoods::model()->deleteAllByAttributes(array('id_photo' => $keyPhoto));
                        foreach ($colors as $_color) {
                            $color = new ColorGoods();
                            $color->color = $_color;
                            $color->id_photo = $keyPhoto;

                            $color->save();
                        }
                    }
                }
            }
        }

        public function actionUpdate($id) {
            list( $good, $categories, $photos, $sizes, $colors, $descriptions ) = Goods::model()->getGoodInfo($id);

            $this->validateGoods();

            $this->saveAndRedirect();

            $this->render(
                '_form', array(
                    'good'        => $good,
                    'category'    => $categories,
                    'photos'      => $photos,
                    'sizes'       => $sizes,
                    'colors'      => $colors,
                    'description' => $descriptions
                )
            );
        }

        public function actionUploadPhoto() {
            $path = AddExtend::basePath() . "/../" . Photo::PATH_IMAGE;
            // Удаляю все загруженные, но не сохраненные фото
//            RecursiveScan::RemoveDir($path . "tmp/");

            if (!is_writable($path)) {
                AddExtend::answer(
                    false, array( 'textError' => 'Директория защищена от записи' )
                );
                exit;
            }
            if (!sizeof($_FILES)) {
                AddExtend::answer(
                    false, array( 'textError' => 'Не передано ни одного файла' )
                );
                exit;
            }

            $file = $_FILES[ 'file' ];
            $nameFile = array();
            foreach ($file[ 'name' ] as $key => $_file) {
                $tmpName = $file[ 'tmp_name' ][ $key ];
                $newName = $path . "tmp/" . CropImage::addSalt($_file);
                CropImage::renameFile($tmpName, $newName);
                $nameFile[ $key ] = CropImage::cropImagegick(
                    $newName, array(
                        'width'  => 700,
                        'height' => 700
                    ), array(
                        'width'  => 195,
                        'height' => 195
                    )
                );
                unlink($newName);
            }

            $url = AddExtend::baseUrl() . Photo::PATH_IMAGE . "tmp/" . basename($nameFile[ 0 ][ 'black' ]);

            AddExtend::answer(
                true, array( 'url' => $url )
            );
        }

        public function actionDeleteSize($id) {
            GoodsSize::model()->deleteByPk($id);
            AddExtend::answer();
        }

        public function actionDeleteColor($id) {
            ColorGoods::model()->deleteByPk($id);
            AddExtend::answer();
        }

        public function actionDeletePhoto($id) {
            if (intval($id) > 0) {
                $photo = Photo::model()->findByPk($id);
                if ($photo) {
                    @unlink(
                        AddExtend::basePath() . "/.." . Photo::PATH_IMAGE . CropImage::dynamicPath($photo->primaryKey()) . "/" . $photo->url
                    );
                    Photo::model()->deleteByPk($id);
                }
            } else {
                AddExtend::answer(
                    false, array(
                        'status'    => false,
                        'textError' => 'Неправильно передан идентификатор!'
                    )
                );
                exit;
            }
            AddExtend::answer();
        }

        public function actionDeleteSubCategory($id, $goodID) {
            GoodsCategory::model()->deleteAllByAttributes(
                array(
                    'category_id' => $id,
                    'goods_id'    => $goodID
                )
            );
            AddExtend::answer();
        }

        public function actionDelete($id) {
            Goods::model()->deleteByPk($id);
        }

        public function actionGetCategoryList() {
            $sets = array();
            if (isset( $_POST[ 'sets' ] )) {
                $sets = CJSON::decode($_POST[ 'sets' ], true);
            }

            $catList = Category::model()->getParentCategory(null, $sets);

            $newCat = array();

            foreach ($catList as $key => $_cat) {
                $newCat[ $key ] = array();
                $newCat[ $key ] = $_cat->attributes;
            }

            if (!sizeof($newCat))
                $newCat[ ] = array(
                    'id'   => -9,
                    'name' => ''
                );

            $newCat[ ] = array(
                'id'   => -10,
                'name' => 'Добавить новую'
            );

            $newCat[ 0 ][ 'child' ] = array();
            $child = Category::model()->getChild($catList[ 0 ]->id);

            foreach ($child as $_child) {
                $newCat[ 0 ][ 'child' ][ ] = $_child->attributes;
            }

            $newCat[ 0 ][ 'child' ][ ] = array(
                'id'   => -10,
                'name' => 'Добавить новую'
            );

            if (empty( $newCat[ key($newCat) ] )) {
                AddExtend::answer(
                    false, array( 'textError' => 'Категорий больше нет!' )
                );
                exit;
            }

            AddExtend::answer(true, $newCat);

            exit;
        }

        public function actionGetSubCat($id) {
            if (is_numeric($id) && intval($id) > 0) {
                $sets = null;
                if (isset( $_POST[ 'sets' ] )) {
                    $sets = CJSON::decode($_POST[ 'sets' ], true);
                    if (sizeof($sets) == 0)
                        $sets = null;
                }
                $category = Category::model()->getChild($id, null, $sets);
                $cat = $category[ 0 ];
                if (is_null($cat))
                    $category = array(
                        0 => array(
                            'id'   => '-9',
                            'name' => ''
                        )
                    );
                $arr = array(
                    -10 => array(
                        'id'   => -10,
                        'name' => 'Добавить новую'
                    )
                );

                exit( CJSON::encode(
                    array_merge(
                        $category, $arr
                    )
                ) );
            } else if ($id == -10) {
                exit( CJSON::encode(
                    array(
                        array(
                            'id'   => -9,
                            'name' => ''
                        ),
                        array(
                            'id'   => -10,
                            'name' => 'Добавить новую'
                        ),
                    )
                ) );
            } else {
                exit( CJSON::encode(
                    array(
                        array(
                            'id'   => -9,
                            'name' => ''
                        ),
                        array(
                            'id'   => -10,
                            'name' => 'Добавить новую'
                        ),
                    )
                ) );
            }
        }

        public function actionDeleteCategory($id, $goodID) {
            $condition = array( 'category_id' => $id );
            if ($goodID) {
                $condition[ 'goods_id' ] = $goodID;
            }

            GoodsCategory::model()->deleteAllByAttributes($condition);
            $child = Category::model()->getChild($id, null, array());
            if (sizeof($child[ 0 ])) {
                $allID = array();
                foreach ($child[ 0 ] as $key => $value) {
                    $allID[ ] = $value->id;
                }

                $condition[ 'category_id' ] = ' IN (' . implode(',', $allID);
                GoodsCategory::model()->deleteAllByAttributes($condition);
            }

            AddExtend::answer();
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

        /**
         * Return buttons for elem
         *
         * @param $id
         *
         * @return string
         */
        public function buttonBlock($id) {
            return '
                    <div class="btnCat">
                        <span class="edit' . $id . '">
                            <img title="Переименовать" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/edit.png"/>
                        </span>
                        <span class="delete' . $id . '">
                            <img title="Удалить" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/delete.png"/>
                        </span>
                    </div>';
        }

        public function actionUpdateIndex() {
            $data = json_decode($_POST[ 'data' ], true);

            $position = 0;
            foreach ($data as $_next) {
                $position++;
                $id = intval($_next[ 'key' ]);
                Category::model()->updateByPk(
                    $id, array(
                        'position'        => $position,
                        'parent_position' => null,
                        'parent'          => null
                    )
                );
                if (isset( $_next[ 'children' ] )) {
                    $positionChild = 1;
                    foreach ($_next[ 'children' ] as $_child) {
                        $position++;
                        $idChild = intval($_child[ 'key' ]);
                        Category::model()->updateByPk(
                            intval($_child[ 'key' ]), array(
                                'position'        => $position,
                                'parent'          => $id,
                                'parent_position' => $positionChild
                            )
                        );

                        $positionChild++;

                        if (isset( $_child[ 'children' ] )) {
                            $positionChildTwo = 1;
                            foreach ($_child[ 'children' ] as $_childTwo) {
                                $position++;
                                Category::model()->updateByPk(
                                    intval($_childTwo[ 'key' ]), array(
                                        'position'        => $position,
                                        'parent'          => $idChild,
                                        'parent_position' => $positionChildTwo
                                    )
                                );
                                $positionChildTwo++;
                            }
                        }
                    }
                }
            }

        }

    }
