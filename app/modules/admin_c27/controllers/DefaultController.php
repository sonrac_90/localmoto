<?php

    class DefaultController extends AdminController {
        private $md5prefix = "SE34dwe4xk034sdk5mep23bsEN";

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {

            $criteria = new CDbCriteria();
            $criteria->select = 'count(id) as id, country, city';
            $criteria->group = 'country';
            $criteria->order = "id DESC";
            $visitors = Visitors::model()->findAll($criteria);

            $totalCount = Yii::app()->db->createCommand("SELECT count(id) as id FROM " . Visitors::model()->tableName())->queryScalar();

            $order = array(
                'all' => OrderMain::getCountOrder(),
                'buy' => OrderMain::getCountOrder('status_pay = \'pay\''),
            );

            $countUser = Yii::app()->db->createCommand("SELECT count(id) as id FROM " . Users::model()->tableName())->queryScalar();


            $systemInfo = AddExtend::getSystemInfo();

            $this->render('index', array(
                    'visitors'   => $visitors,
                    'orders'     => $order,
                    'countUsers' => $countUser,
                    'systemInfo' => $systemInfo,
                    'total'      => $totalCount
                )
            );
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    $url = Yii::app()->user->returnUrl;
                    if ($url == '/' || $url == '/index.php')
                        $this->redirect(Yii::app()->createUrl($this->module->id . '/default/index'));
                    $this->redirect($url);
                }
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        public function actionLogout() {
            Yii::app()->user->logout(true);
            $this->redirect(AddExtend::getURL($this, "/default/login"));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

    }