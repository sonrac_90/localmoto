<?php

    class ProducerController extends AdminController {

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'actions' => array(
                        'login',
                        'logout',
                        'error'
                    ),
                    'users'   => array( '*' ),
                ),
                array(
                    'allow',
                    'roles' => array( 'admin' ),
                ),
                array(
                    'deny',
                    'users' => array( '*' ),
                ),
            );
        }

        public function actionIndex() {
            $producer = new Producer();
            $producer->unsetAttributes();

            if (isset( $_POST[ 'producerName' ] )) {
                $producer->name = $_POST[ 'producerName' ];
                $producer->save();
            }

            $criteria = new CDbCriteria();
            $criteria->select = 'name, id';

            $sort = new CSort();
            $sort->sortVar = "name";
            $sort->defaultOrder = " id ASC";

            $sort->attributes = array(
                'name' => array(
                    'label' => 'Название',
                    'asc'   => 'name ASC',
                    'desc'  => 'name DESC'
                ),
                'id'   => array(
                    'label' => 'ID',
                    'asc'   => 'id ASC',
                    'desc'  => 'id DESC'
                )
            );

            $sort->multiSort = true;

            $dataProvider = new CActiveDataProvider(
                Producer::model(), array(
                    'criteria'   => $criteria,
                    'sort'       => $sort,
                    'pagination' => array(
                        'pageSize' => 30,
                    )
                )
            );

            $this->render(
                'index', array(
                    'dataProvider' => $dataProvider,
                    'producer'     => $producer
                )
            );
        }

        public function actionUpdate($id) {
            $newName = $_POST[ 'newName' ];

            Producer::model()->updateByPk(
                $id, array(
                    'name' => $newName
                )
            );
        }

        public function actionAdd() {
            $newName = $_POST[ 'newName' ];

            $producer = new Producer();
            $producer->name = $newName;

            if (!$producer->validate())
                exit( CJSON::encode(array_merge($producer->getErrors(), array( 'error' => 'Такой производитель уже существует' ))) );
            if ($producer->save()) {
                exit( CJSON::encode($producer->attributes) );
            } else {
                exit( CJSON::encode(
                    array(
                        array_merge($producer->getErrors(), array( 'error' => 'Ошибка валидации' ))
                    )
                ) );
            }
        }

        public function actionGetListProducers() {
            if (AddExtend::isAjax()) {
                exit( json_encode(Producer::model()->getList()) );
            }

            return Producer::model()->getList();
        }

        public function actionDelete($id) {
            Producer::model()->deleteByPk($id);
        }

        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login())
                    $this->redirect(Yii::app()->user->returnUrl);
            }
            // display the login form
            $this->renderPartial('login', array( 'model' => $model ));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ]; else
                    $this->render('error', $error);
            }
        }

        /**
         * Return buttons for elem
         *
         * @param $id
         *
         * @return string
         */
        public function buttonBlock($id) {
            return '
        <div class="btnCat">
            <span class="view' . $id . '"><a title="Просмотеть товары производителя" href="' . AddExtend::getURL($this, "/goods/index/id/" . $id) . '"><img class="view" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/visible.png" class="viewEYE"/></span></a>
            <span class="edit' . $id . '"><img title="Переименовать" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/edit.png"/></span>
            <span class="delete' . $id . '"><img title="Удалить" src="' . AddExtend::baseUrl() . 'shared/media/css/admin/images/admin/delete.png"/></span>
        </div>';
        }
    }
