<?php

    class Admin_c27Module extends CWebModule {
        public function init() {
            // import the module-level models and components
            $this->setImport(
                array(
                    'admin_c27.models.*',
                    'admin_c27.controllers.*',
                    'admin_c27.components.*',
                )
            );

            Yii::app()->setComponents(
                array(
                    'errorHandler' => array(
                        'errorAction' => 'admin_c27/default/error',
                    ),
                    'user'         => array(
                        'class'          => 'WebUser',
                        'loginUrl'       => Yii::app()->createUrl('admin_c27/default/login'),
                    ),
                )
            );

            Yii::app()->user->setStateKeyPrefix('_admin_c27');

        }

        public function beforeControllerAction($controller, $action) {
            //        Controller::checkAdminUrl();
            if (parent::beforeControllerAction($controller, $action)) {
                if (!Yii::app()->user->isGuest) {
                    if (!Yii::app()->user->checkAccess('admin')) {
                        //В случае, если пользователь авторизировался с правами user, перенаправляем его на основную страницу
                        $controller->redirect(Yii::app()->createUrl(AddExtend::baseUrl(true)));
                    }
                }

                // this method is called before any module controller action is performed
                // you may place customized code here
                return true;
            } else
                return false;
        }
    }
