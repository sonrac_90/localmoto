<?php

    // uncomment the following to define a path alias
    Yii::setPathOfAlias('shared', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'shared');
    Yii::setPathOfAlias('yiiTemp', Yii::getPathOfAlias('shared') . '/yii');

    function getBaseUrl() {
        $dir = dirname($_SERVER[ 'SCRIPT_NAME' ]);
        $dir = ( $dir == '/' ) ? '' : $dir;

        return $dir;
    }

    // This is the main Web application configuration. Any writable
    // CWebApplication properties can be configured here.
    return array(
        'basePath'       => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
        'name'           => 'LocalMoto',
        'runtimePath'    => Yii::getPathOfAlias('yiiTemp') . '/runtime',
        'sourceLanguage' => 'ru',

        'preload'        => array( 'log' ),

        'import'         => array(
            'application.models.*',
            'application.components.*',
            'application.components.ipgeobase.*',
        ),

        'modules'        => array(

            'gii'   => array(
                'class'     => 'system.gii.GiiModule',
                'password'  => 'p',
                'ipFilters' => array(
                    '127.0.0.1',
                    '::1'
                ),
            ),
            'admin_c27' => array()

        ),

        'components'     => array(
            'assetManager' => array(
                'basePath' => Yii::getPathOfAlias('yiiTemp') . '/assets',
                'baseUrl'  => getBaseUrl() . '/shared/yii/assets'
            ),

            'user'         => array(
                'class'          => 'WebUser',
                'allowAutoLogin' => true,
            ),
            'authManager'  => array(
                'class'        => 'PhpAuthManager',
                'defaultRoles' => array( 'guest' ),
            ),

            'urlManager'   => array(
                'urlFormat'      => 'path',
                'showScriptName' => false,
                'rules'          => array(
                    '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
                ),
            ),

            'db'           => array(
                'connectionString' => 'mysql:host=' . DATABASE_HOST . ';dbname=' . DATABASE_NAME,
                'emulatePrepare'   => true,
                'username'         => DATABASE_USER,
                'password'         => DATABASE_USER_PASSWORD,
                'tablePrefix'      => DATABASE_TABLE_PREFIX,
                'charset'          => 'utf8',
            ),
            'errorHandler' => array(
                // use 'site/error' action to display errors
                'errorAction' => 'site/error',
            ),

            'log'          => array(
                'class'  => 'CLogRouter',
                'routes' => array(
                    array(
                        'class'   => 'CFileLogRoute',
                        'levels'  => 'error, warning, log',
                        'enabled' => true,
                    ),
                ),
            ),
        ),

        'params'         => array(
            'adminEmail' => 'admin@lm.ru',
        ),
    );
