<?php
/***************************************************************************\
| Sypex Geo                  version 2.2.0                                  |
| (c)2006-2014 zapimir       zapimir@zapimir.net       http://sypex.net/    |
| (c)2006-2014 BINOVATOR     info@sypex.net                                 |
|---------------------------------------------------------------------------|
|     created: 2006.10.17 18:33              modified: 2014.05.02 23:00     |
|---------------------------------------------------------------------------|
| Sypex Geo is released under the terms of the BSD license                  |
|   http://sypex.net/bsd_license.txt                                        |
\***************************************************************************/

define ('SXGEO_FILE', 0);
define ('SXGEO_MEMORY', 1);
define ('SXGEO_BATCH',  2);
class SxGeo {
	protected $fh;
	protected $ip1c;
	protected $info;
	protected $range;
	protected $db_begin;
	protected $b_idx_str;
	protected $m_idx_str;
	protected $b_idx_arr;
	protected $m_idx_arr;
	protected $m_idx_len;
	protected $db_items;
	protected $country_size;
	protected $db;
	protected $regions_db;
	protected $cities_db;

	public $id2iso = array(
		'', 'AP', 'EU', 'AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'CW', 'AO', 'AQ', 'AR', 'AS', 'AT', 'AU',
		'AW', 'AZ', 'BA', 'BB', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BM', 'BN', 'BO', 'BR', 'BS',
		'BT', 'BV', 'BW', 'BY', 'BZ', 'CA', 'CC', 'CD', 'CF', 'CG', 'CH', 'CI', 'CK', 'CL', 'CM', 'CN',
		'CO', 'CR', 'CU', 'CV', 'CX', 'CY', 'CZ', 'DE', 'DJ', 'DK', 'DM', 'DO', 'DZ', 'EC', 'EE', 'EG',
		'EH', 'ER', 'ES', 'ET', 'FI', 'FJ', 'FK', 'FM', 'FO', 'FR', 'SX', 'GA', 'GB', 'GD', 'GE', 'GF',
		'GH', 'GI', 'GL', 'GM', 'GN', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GW', 'GY', 'HK', 'HM', 'HN',
		'HR', 'HT', 'HU', 'ID', 'IE', 'IL', 'IN', 'IO', 'IQ', 'IR', 'IS', 'IT', 'JM', 'JO', 'JP', 'KE',
		'KG', 'KH', 'KI', 'KM', 'KN', 'KP', 'KR', 'KW', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LI', 'LK', 'LR',
		'LS', 'LT', 'LU', 'LV', 'LY', 'MA', 'MC', 'MD', 'MG', 'MH', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP',
		'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NC', 'NE', 'NF', 'NG', 'NI',
		'NL', 'NO', 'NP', 'NR', 'NU', 'NZ', 'OM', 'PA', 'PE', 'PF', 'PG', 'PH', 'PK', 'PL', 'PM', 'PN',
		'PR', 'PS', 'PT', 'PW', 'PY', 'QA', 'RE', 'RO', 'RU', 'RW', 'SA', 'SB', 'SC', 'SD', 'SE', 'SG',
		'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SR', 'ST', 'SV', 'SY', 'SZ', 'TC', 'TD', 'TF',
		'TG', 'TH', 'TJ', 'TK', 'TM', 'TN', 'TO', 'TL', 'TR', 'TT', 'TV', 'TW', 'TZ', 'UA', 'UG', 'UM',
		'US', 'UY', 'UZ', 'VA', 'VC', 'VE', 'VG', 'VI', 'VN', 'VU', 'WF', 'WS', 'YE', 'YT', 'RS', 'ZA',
		'ZM', 'ME', 'ZW', 'A1', 'XK', 'O1', 'AX', 'GG', 'IM', 'JE', 'BL', 'MF', 'BQ', 'SS'
	);

	public $id2isoFull = array(
        "AB" => array
        (
            "country" => "Абхазия",
            "fullCountry" => "Республика Абхазия",
            "english" => "Abkhazia",
            "alpha2" => "AB",
            "alpha3Iso" => "ABH",
            "worldPlace" => "895",
            "places" => "Азия",
        ),

        "AU" => array
        (
            "country" => "Австралия",
            "fullCountry" => "",
            "english" => "Australia",
            "alpha2" => "AU",
            "alpha3Iso" => "AUS",
            "worldPlace" => "036",
            "places" => "Океания",
        ),

        "AT" => array
        (
            "country" => "Австрия",
            "fullCountry" => "Австрийская Республика",
            "english" => "Austria",
            "alpha2" => "AT",
            "alpha3Iso" => "AUT",
            "worldPlace" => "040",
            "places" => "Европа",
        ),

        "AZ" => array
        (
            "country" => "Азербайджан",
            "fullCountry" => "Республика Азербайджан",
            "english" => "Azerbaijan",
            "alpha2" => "AZ",
            "alpha3Iso" => "AZE",
            "worldPlace" => "031",
            "places" => "Азия",
        ),

        "AL" => array
        (
            "country" => "Албания",
            "fullCountry" => "Республика Албания",
            "english" => "Albania",
            "alpha2" => "AL",
            "alpha3Iso" => "ALB",
            "worldPlace" => "008",
            "places" => "Европа",
        ),

        "DZ" => array
        (
            "country" => "Алжир",
            "fullCountry" => "Алжирская Народная Демократическая Республика",
            "english" => "Algeria",
            "alpha2" => "DZ",
            "alpha3Iso" => "DZA",
            "worldPlace" => "012",
            "places" => "Африка",
        ),

        "AS" => array
        (
            "country" => "Американское Самоа",
            "fullCountry" => "",
            "english" => "American Samoa",
            "alpha2" => "AS",
            "alpha3Iso" => "ASM",
            "worldPlace" => "016",
            "places" => "Океания",
        ),

        "AI" => array
        (
            "country" => "Ангилья",
            "fullCountry" => "",
            "english" => "Anguilla",
            "alpha2" => "AI",
            "alpha3Iso" => "AIA",
            "worldPlace" => "660",
            "places" => "Америка",
        ),

        "AO" => array
        (
            "country" => "Ангола",
            "fullCountry" => "Республика Ангола",
            "english" => "Angola",
            "alpha2" => "AO",
            "alpha3Iso" => "AGO",
            "worldPlace" => "024",
            "places" => "Африка",
        ),

        "AD" => array
        (
            "country" => "Андорра",
            "fullCountry" => "Княжество Андорра",
            "english" => "Andorra",
            "alpha2" => "AD",
            "alpha3Iso" => "AND",
            "worldPlace" => "020",
            "places" => "Европа",
        ),

        "AQ" => array
        (
            "country" => "Антарктида",
            "fullCountry" => "",
            "english" => "Antarctica",
            "alpha2" => "AQ",
            "alpha3Iso" => "ATA",
            "worldPlace" => "010",
            "places" => "Антарктика",
        ),

        "AG" => array
        (
            "country" => "Антигуа и Барбуда",
            "fullCountry" => "",
            "english" => "Antigua and Barbuda",
            "alpha2" => "AG",
            "alpha3Iso" => "ATG",
            "worldPlace" => "028",
            "places" => "Америка",
        ),

        "AR" => array
        (
            "country" => "Аргентина",
            "fullCountry" => "Аргентинская Республика",
            "english" => "Argentina",
            "alpha2" => "AR",
            "alpha3Iso" => "ARG",
            "worldPlace" => "032",
            "places" => "Америка",
        ),

        "AM" => array
        (
            "country" => "Армения",
            "fullCountry" => "Республика Армения",
            "english" => "Armenia",
            "alpha2" => "AM",
            "alpha3Iso" => "ARM",
            "worldPlace" => "051",
            "places" => "Азия",
        ),

        "AW" => array
        (
            "country" => "Аруба",
            "fullCountry" => "",
            "english" => "Aruba",
            "alpha2" => "AW",
            "alpha3Iso" => "ABW",
            "worldPlace" => "533",
            "places" => "Америка",
        ),

        "AF" => array
        (
            "country" => "Афганистан",
            "fullCountry" => "Переходное Исламское Государство Афганистан",
            "english" => "Afghanistan",
            "alpha2" => "AF",
            "alpha3Iso" => "AFG",
            "worldPlace" => "004",
            "places" => "Азия",
        ),

        "BS" => array
        (
            "country" => "Багамы",
            "fullCountry" => "Содружество Багамы",
            "english" => "Bahamas",
            "alpha2" => "BS",
            "alpha3Iso" => "BHS",
            "worldPlace" => "044",
            "places" => "Америка",
        ),

        "BD" => array
        (
            "country" => "Бангладеш",
            "fullCountry" => "Народная Республика Бангладеш",
            "english" => "Bangladesh",
            "alpha2" => "BD",
            "alpha3Iso" => "BGD",
            "worldPlace" => "050",
            "places" => "Азия",
        ),

        "BB" => array
        (
            "country" => "Барбадос",
            "fullCountry" => "",
            "english" => "Barbados",
            "alpha2" => "BB",
            "alpha3Iso" => "BRB",
            "worldPlace" => "052",
            "places" => "Америка",
        ),

        "BH" => array
        (
            "country" => "Бахрейн",
            "fullCountry" => "Королевство Бахрейн",
            "english" => "Bahrain",
            "alpha2" => "BH",
            "alpha3Iso" => "BHR",
            "worldPlace" => "048",
            "places" => "Азия",
        ),

        "BY" => array
        (
            "country" => "Беларусь",
            "fullCountry" => "Республика Беларусь",
            "english" => "Belarus",
            "alpha2" => "BY",
            "alpha3Iso" => "BLR",
            "worldPlace" => "112",
            "places" => "Европа",
        ),

        "BZ" => array
        (
            "country" => "Белиз",
            "fullCountry" => "",
            "english" => "Belize",
            "alpha2" => "BZ",
            "alpha3Iso" => "BLZ",
            "worldPlace" => "084",
            "places" => "Америка",
        ),

        "BE" => array
        (
            "country" => "Бельгия",
            "fullCountry" => "Королевство Бельгии",
            "english" => "Belgium",
            "alpha2" => "BE",
            "alpha3Iso" => "BEL",
            "worldPlace" => "056",
            "places" => "Европа",
        ),

        "BJ" => array
        (
            "country" => "Бенин",
            "fullCountry" => "Республика Бенин",
            "english" => "Benin",
            "alpha2" => "BJ",
            "alpha3Iso" => "BEN",
            "worldPlace" => "204",
            "places" => "Африка",
        ),

        "BM" => array
        (
            "country" => "Бермуды",
            "fullCountry" => "",
            "english" => "Bermuda",
            "alpha2" => "BM",
            "alpha3Iso" => "BMU",
            "worldPlace" => "060",
            "places" => "Америка",
        ),

        "BG" => array
        (
            "country" => "Болгария",
            "fullCountry" => "Республика Болгария",
            "english" => "Bulgaria",
            "alpha2" => "BG",
            "alpha3Iso" => "BGR",
            "worldPlace" => "100",
            "places" => "Европа",
        ),

        "BO" => array
        (
            "country" => "Боливия, Многонациональное Государство",
            "fullCountry" => "Многонациональное Государство Боливия",
            "english" => "Bolivia, plurinational state of",
            "alpha2" => "BO",
            "alpha3Iso" => "BOL",
            "worldPlace" => "068",
            "places" => "Америка",
        ),

        "BQ" => array
        (
            "country" => "Бонайре, Саба и Синт-Эстатиус",
            "fullCountry" => "",
            "english" => "Bonaire, Sint Eustatius and Saba",
            "alpha2" => "BQ",
            "alpha3Iso" => "BES",
            "worldPlace" => "535",
            "places" => "Америка",
        ),

        "BA" => array
        (
            "country" => "Босния и Герцеговина",
            "fullCountry" => "",
            "english" => "Bosnia and Herzegovina",
            "alpha2" => "BA",
            "alpha3Iso" => "BIH",
            "worldPlace" => "070",
            "places" => "Европа",
        ),

        "BW" => array
        (
            "country" => "Ботсвана",
            "fullCountry" => "Республика Ботсвана",
            "english" => "Botswana",
            "alpha2" => "BW",
            "alpha3Iso" => "BWA",
            "worldPlace" => "072",
            "places" => "Африка",
        ),

        "BR" => array
        (
            "country" => "Бразилия",
            "fullCountry" => "Федеративная Республика Бразилия",
            "english" => "Brazil",
            "alpha2" => "BR",
            "alpha3Iso" => "BRA",
            "worldPlace" => "076",
            "places" => "Америка",
        ),

        "IO" => array
        (
            "country" => "Британская территория в Индийском океане",
            "fullCountry" => "",
            "english" => "British Indian Ocean Territory",
            "alpha2" => "IO",
            "alpha3Iso" => "IOT",
            "worldPlace" => "086",
            "places" => "Океания",
        ),

        "BN" => array
        (
            "country" => "Бруней-Даруссалам",
            "fullCountry" => "",
            "english" => "Brunei Darussalam",
            "alpha2" => "BN",
            "alpha3Iso" => "BRN",
            "worldPlace" => "096",
            "places" => "Азия",
        ),

        "BF" => array
        (
            "country" => "Буркина-Фасо",
            "fullCountry" => "",
            "english" => "Burkina Faso",
            "alpha2" => "BF",
            "alpha3Iso" => "BFA",
            "worldPlace" => "854",
            "places" => "Африка",
        ),

        "BI" => array
        (
            "country" => "Бурунди",
            "fullCountry" => "Республика Бурунди",
            "english" => "Burundi",
            "alpha2" => "BI",
            "alpha3Iso" => "BDI",
            "worldPlace" => "108",
            "places" => "Африка",
        ),

        "BT" => array
        (
            "country" => "Бутан",
            "fullCountry" => "Королевство Бутан",
            "english" => "Bhutan",
            "alpha2" => "BT",
            "alpha3Iso" => "BTN",
            "worldPlace" => "064",
            "places" => "Азия",
        ),

        "VU" => array
        (
            "country" => "Вануату",
            "fullCountry" => "Республика Вануату",
            "english" => "Vanuatu",
            "alpha2" => "VU",
            "alpha3Iso" => "VUT",
            "worldPlace" => "548",
            "places" => "Океания",
        ),

        "HU" => array
        (
            "country" => "Венгрия",
            "fullCountry" => "Венгерская Республика",
            "english" => "Hungary",
            "alpha2" => "HU",
            "alpha3Iso" => "HUN",
            "worldPlace" => "348",
            "places" => "Европа",
        ),

        "VE" => array
        (
            "country" => "Венесуэла Боливарианская Республика",
            "fullCountry" => "Боливарийская Республика Венесуэла",
            "english" => "Venezuela",
            "alpha2" => "VE",
            "alpha3Iso" => "VEN",
            "worldPlace" => "862",
            "places" => "Америка",
        ),

        "VG" => array
        (
            "country" => "Виргинские острова, Британские",
            "fullCountry" => "Британские Виргинские острова",
            "english" => "Virgin Islands, British",
            "alpha2" => "VG",
            "alpha3Iso" => "VGB",
            "worldPlace" => "092",
            "places" => "Америка",
        ),

        "VI" => array
        (
            "country" => "Виргинские острова, США",
            "fullCountry" => "Виргинские острова Соединенных Штатов",
            "english" => "Virgin Islands, U.S.",
            "alpha2" => "VI",
            "alpha3Iso" => "VIR",
            "worldPlace" => "850",
            "places" => "Америка",
        ),

        "VN" => array
        (
            "country" => "Вьетнам",
            "fullCountry" => "Социалистическая Республика Вьетнам",
            "english" => "Vietnam",
            "alpha2" => "VN",
            "alpha3Iso" => "VNM",
            "worldPlace" => "704",
            "places" => "Азия",
        ),

        "GA" => array
        (
            "country" => "Габон",
            "fullCountry" => "Габонская Республика",
            "english" => "Gabon",
            "alpha2" => "GA",
            "alpha3Iso" => "GAB",
            "worldPlace" => "266",
            "places" => "Африка",
        ),

        "HT" => array
        (
            "country" => "Гаити",
            "fullCountry" => "Республика Гаити",
            "english" => "Haiti",
            "alpha2" => "HT",
            "alpha3Iso" => "HTI",
            "worldPlace" => "332",
            "places" => "Америка",
        ),

        "GY" => array
        (
            "country" => "Гайана",
            "fullCountry" => "Республика Гайана",
            "english" => "Guyana",
            "alpha2" => "GY",
            "alpha3Iso" => "GUY",
            "worldPlace" => "328",
            "places" => "Америка",
        ),

        "GM" => array
        (
            "country" => "Гамбия",
            "fullCountry" => "Республика Гамбия",
            "english" => "Gambia",
            "alpha2" => "GM",
            "alpha3Iso" => "GMB",
            "worldPlace" => "270",
            "places" => "Африка",
        ),

        "GH" => array
        (
            "country" => "Гана",
            "fullCountry" => "Республика Гана",
            "english" => "Ghana",
            "alpha2" => "GH",
            "alpha3Iso" => "GHA",
            "worldPlace" => "288",
            "places" => "Африка",
        ),

        "GP" => array
        (
            "country" => "Гваделупа",
            "fullCountry" => "",
            "english" => "Guadeloupe",
            "alpha2" => "GP",
            "alpha3Iso" => "GLP",
            "worldPlace" => "312",
            "places" => "Америка",
        ),

        "GT" => array
        (
            "country" => "Гватемала",
            "fullCountry" => "Республика Гватемала",
            "english" => "Guatemala",
            "alpha2" => "GT",
            "alpha3Iso" => "GTM",
            "worldPlace" => "320",
            "places" => "Америка",
        ),

        "GN" => array
        (
            "country" => "Гвинея",
            "fullCountry" => "Гвинейская Республика",
            "english" => "Guinea",
            "alpha2" => "GN",
            "alpha3Iso" => "GIN",
            "worldPlace" => "324",
            "places" => "Африка",
        ),

        "GW" => array
        (
            "country" => "Гвинея-Бисау",
            "fullCountry" => "Республика Гвинея-Бисау",
            "english" => "Guinea-Bissau",
            "alpha2" => "GW",
            "alpha3Iso" => "GNB",
            "worldPlace" => "624",
            "places" => "Африка",
        ),

        "DE" => array
        (
            "country" => "Германия",
            "fullCountry" => "Федеративная Республика Германия",
            "english" => "Germany",
            "alpha2" => "DE",
            "alpha3Iso" => "DEU",
            "worldPlace" => "276",
            "places" => "Европа",
        ),

        "GG" => array
        (
            "country" => "Гернси",
            "fullCountry" => "",
            "english" => "Guernsey",
            "alpha2" => "GG",
            "alpha3Iso" => "GGY",
            "worldPlace" => "831",
            "places" => "Европа",
        ),

        "GI" => array
        (
            "country" => "Гибралтар",
            "fullCountry" => "",
            "english" => "Gibraltar",
            "alpha2" => "GI",
            "alpha3Iso" => "GIB",
            "worldPlace" => "292",
            "places" => "Европа",
        ),

        "HN" => array
        (
            "country" => "Гондурас",
            "fullCountry" => "Республика Гондурас",
            "english" => "Honduras",
            "alpha2" => "HN",
            "alpha3Iso" => "HND",
            "worldPlace" => "340",
            "places" => "Америка",
        ),

        "HK" => array
        (
            "country" => "Гонконг",
            "fullCountry" => "Специальный  административный  регион Китая Гонконг",
            "english" => "Hong Kong",
            "alpha2" => "HK",
            "alpha3Iso" => "HKG",
            "worldPlace" => "344",
            "places" => "Азия",
        ),

        "GD" => array
        (
            "country" => "Гренада",
            "fullCountry" => "",
            "english" => "Grenada",
            "alpha2" => "GD",
            "alpha3Iso" => "GRD",
            "worldPlace" => "308",
            "places" => "Америка",
        ),

        "GL" => array
        (
            "country" => "Гренландия",
            "fullCountry" => "",
            "english" => "Greenland",
            "alpha2" => "GL",
            "alpha3Iso" => "GRL",
            "worldPlace" => "304",
            "places" => "Америка",
        ),

        "GR" => array
        (
            "country" => "Греция",
            "fullCountry" => "Греческая Республика",
            "english" => "Greece",
            "alpha2" => "GR",
            "alpha3Iso" => "GRC",
            "worldPlace" => "300",
            "places" => "Европа",
        ),

        "GE" => array
        (
            "country" => "Грузия",
            "fullCountry" => "",
            "english" => "Georgia",
            "alpha2" => "GE",
            "alpha3Iso" => "GEO",
            "worldPlace" => "268",
            "places" => "Азия",
        ),

        "GU" => array
        (
            "country" => "Гуам",
            "fullCountry" => "",
            "english" => "Guam",
            "alpha2" => "GU",
            "alpha3Iso" => "GUM",
            "worldPlace" => "316",
            "places" => "Океания",
        ),

        "DK" => array
        (
            "country" => "Дания",
            "fullCountry" => "Королевство Дания",
            "english" => "Denmark",
            "alpha2" => "DK",
            "alpha3Iso" => "DNK",
            "worldPlace" => "208",
            "places" => "Европа",
        ),

        "JE" => array
        (
            "country" => "Джерси",
            "fullCountry" => "",
            "english" => "Jersey",
            "alpha2" => "JE",
            "alpha3Iso" => "JEY",
            "worldPlace" => "832",
            "places" => "Европа",
        ),

        "DJ" => array
        (
            "country" => "Джибути",
            "fullCountry" => "Республика Джибути",
            "english" => "Djibouti",
            "alpha2" => "DJ",
            "alpha3Iso" => "DJI",
            "worldPlace" => "262",
            "places" => "Африка",
        ),

        "DM" => array
        (
            "country" => "Доминика",
            "fullCountry" => "Содружество Доминики",
            "english" => "Dominica",
            "alpha2" => "DM",
            "alpha3Iso" => "DMA",
            "worldPlace" => "212",
            "places" => "Америка",
        ),

        "DO" => array
        (
            "country" => "Доминиканская Республика",
            "fullCountry" => "",
            "english" => "Dominican Republic",
            "alpha2" => "DO",
            "alpha3Iso" => "DOM",
            "worldPlace" => "214",
            "places" => "Америка",
        ),

        "EG" => array
        (
            "country" => "Египет",
            "fullCountry" => "Арабская Республика Египет",
            "english" => "Egypt",
            "alpha2" => "EG",
            "alpha3Iso" => "EGY",
            "worldPlace" => "818",
            "places" => "Африка",
        ),

        "ZM" => array
        (
            "country" => "Замбия",
            "fullCountry" => "Республика Замбия",
            "english" => "Zambia",
            "alpha2" => "ZM",
            "alpha3Iso" => "ZMB",
            "worldPlace" => "894",
            "places" => "Африка",
        ),

        "EH" => array
        (
            "country" => "Западная Сахара",
            "fullCountry" => "",
            "english" => "Western Sahara",
            "alpha2" => "EH",
            "alpha3Iso" => "ESH",
            "worldPlace" => "732",
            "places" => "Африка",
        ),

        "ZW" => array
        (
            "country" => "Зимбабве",
            "fullCountry" => "Республика Зимбабве",
            "english" => "Zimbabwe",
            "alpha2" => "ZW",
            "alpha3Iso" => "ZWE",
            "worldPlace" => "716",
            "places" => "Африка",
        ),

        "IL" => array
        (
            "country" => "Израиль",
            "fullCountry" => "Государство Израиль",
            "english" => "Israel",
            "alpha2" => "IL",
            "alpha3Iso" => "ISR",
            "worldPlace" => "376",
            "places" => "Азия",
        ),

        "IN" => array
        (
            "country" => "Индия",
            "fullCountry" => "Республика Индия",
            "english" => "India",
            "alpha2" => "IN",
            "alpha3Iso" => "IND",
            "worldPlace" => "356",
            "places" => "Азия",
        ),

        "ID" => array
        (
            "country" => "Индонезия",
            "fullCountry" => "Республика Индонезия",
            "english" => "Indonesia",
            "alpha2" => "ID",
            "alpha3Iso" => "IDN",
            "worldPlace" => "360",
            "places" => "Азия",
        ),

        "JO" => array
        (
            "country" => "Иордания",
            "fullCountry" => "Иорданское Хашимитское Королевство",
            "english" => "Jordan",
            "alpha2" => "JO",
            "alpha3Iso" => "JOR",
            "worldPlace" => "400",
            "places" => "Азия",
        ),

        "IQ" => array
        (
            "country" => "Ирак",
            "fullCountry" => "Республика Ирак",
            "english" => "Iraq",
            "alpha2" => "IQ",
            "alpha3Iso" => "IRQ",
            "worldPlace" => "368",
            "places" => "Азия",
        ),

        "IR" => array
        (
            "country" => "Иран, Исламская Республика",
            "fullCountry" => "Исламская Республика Иран",
            "english" => "Iran, Islamic Republic of",
            "alpha2" => "IR",
            "alpha3Iso" => "IRN",
            "worldPlace" => "364",
            "places" => "Азия",
        ),

        "IE" => array
        (
            "country" => "Ирландия",
            "fullCountry" => "",
            "english" => "Ireland",
            "alpha2" => "IE",
            "alpha3Iso" => "IRL",
            "worldPlace" => "372",
            "places" => "Европа",
        ),

        "IS" => array
        (
            "country" => "Исландия",
            "fullCountry" => "Республика Исландия",
            "english" => "Iceland",
            "alpha2" => "IS",
            "alpha3Iso" => "ISL",
            "worldPlace" => "352",
            "places" => "Европа",
        ),

        "ES" => array
        (
            "country" => "Испания",
            "fullCountry" => "Королевство Испания",
            "english" => "Spain",
            "alpha2" => "ES",
            "alpha3Iso" => "ESP",
            "worldPlace" => "724",
            "places" => "Европа",
        ),

        "IT" => array
        (
            "country" => "Италия",
            "fullCountry" => "Итальянская Республика",
            "english" => "Italy",
            "alpha2" => "IT",
            "alpha3Iso" => "ITA",
            "worldPlace" => "380",
            "places" => "Европа",
        ),

        "YE" => array
        (
            "country" => "Йемен",
            "fullCountry" => "Йеменская Республика",
            "english" => "Yemen",
            "alpha2" => "YE",
            "alpha3Iso" => "YEM",
            "worldPlace" => "887",
            "places" => "Азия",
        ),

        "CV" => array
        (
            "country" => "Кабо-Верде",
            "fullCountry" => "Республика Кабо-Верде",
            "english" => "Cape Verde",
            "alpha2" => "CV",
            "alpha3Iso" => "CPV",
            "worldPlace" => "132",
            "places" => "Африка",
        ),

        "KZ" => array
        (
            "country" => "Казахстан",
            "fullCountry" => "Республика Казахстан",
            "english" => "Kazakhstan",
            "alpha2" => "KZ",
            "alpha3Iso" => "KAZ",
            "worldPlace" => "398",
            "places" => "Азия",
        ),

        "KH" => array
        (
            "country" => "Камбоджа",
            "fullCountry" => "Королевство Камбоджа",
            "english" => "Cambodia",
            "alpha2" => "KH",
            "alpha3Iso" => "KHM",
            "worldPlace" => "116",
            "places" => "Азия",
        ),

        "CM" => array
        (
            "country" => "Камерун",
            "fullCountry" => "Республика Камерун",
            "english" => "Cameroon",
            "alpha2" => "CM",
            "alpha3Iso" => "CMR",
            "worldPlace" => "120",
            "places" => "Африка",
        ),

        "CA" => array
        (
            "country" => "Канада",
            "fullCountry" => "",
            "english" => "Canada",
            "alpha2" => "CA",
            "alpha3Iso" => "CAN",
            "worldPlace" => "124",
            "places" => "Америка",
        ),

        "QA" => array
        (
            "country" => "Катар",
            "fullCountry" => "Государство Катар",
            "english" => "Qatar",
            "alpha2" => "QA",
            "alpha3Iso" => "QAT",
            "worldPlace" => "634",
            "places" => "Азия",
        ),

        "KE" => array
        (
            "country" => "Кения",
            "fullCountry" => "Республика Кения",
            "english" => "Kenya",
            "alpha2" => "KE",
            "alpha3Iso" => "KEN",
            "worldPlace" => "404",
            "places" => "Африка",
        ),

        "CY" => array
        (
            "country" => "Кипр",
            "fullCountry" => "Республика Кипр",
            "english" => "Cyprus",
            "alpha2" => "CY",
            "alpha3Iso" => "CYP",
            "worldPlace" => "196",
            "places" => "Азия",
        ),

        "KG" => array
        (
            "country" => "Киргизия",
            "fullCountry" => "Киргизская Республика",
            "english" => "Kyrgyzstan",
            "alpha2" => "KG",
            "alpha3Iso" => "KGZ",
            "worldPlace" => "417",
            "places" => "Азия",
        ),

        "KI" => array
        (
            "country" => "Кирибати",
            "fullCountry" => "Республика Кирибати",
            "english" => "Kiribati",
            "alpha2" => "KI",
            "alpha3Iso" => "KIR",
            "worldPlace" => "296",
            "places" => "Океания",
        ),

        "CN" => array
        (
            "country" => "Китай",
            "fullCountry" => "Китайская Народная Республика",
            "english" => "China",
            "alpha2" => "CN",
            "alpha3Iso" => "CHN",
            "worldPlace" => "156",
            "places" => "Азия",
        ),

        "CC" => array
        (
            "country" => "Кокосовые (Килинг) острова",
            "fullCountry" => "",
            "english" => "Cocos (Keeling) Islands",
            "alpha2" => "CC",
            "alpha3Iso" => "CCK",
            "worldPlace" => "166",
            "places" => "Океания",
        ),

        "CO" => array
        (
            "country" => "Колумбия",
            "fullCountry" => "Республика Колумбия",
            "english" => "Colombia",
            "alpha2" => "CO",
            "alpha3Iso" => "COL",
            "worldPlace" => "170",
            "places" => "Америка",
        ),

        "KM" => array
        (
            "country" => "Коморы",
            "fullCountry" => "Союз Коморы",
            "english" => "Comoros",
            "alpha2" => "KM",
            "alpha3Iso" => "COM",
            "worldPlace" => "174",
            "places" => "Африка",
        ),

        "CG" => array
        (
            "country" => "Конго",
            "fullCountry" => "Республика Конго",
            "english" => "Congo",
            "alpha2" => "CG",
            "alpha3Iso" => "COG",
            "worldPlace" => "178",
            "places" => "Африка",
        ),

        "CD" => array
        (
            "country" => "Конго, Демократическая Республика",
            "fullCountry" => "Демократическая Республика Конго",
            "english" => "Congo, Democratic Republic of the",
            "alpha2" => "CD",
            "alpha3Iso" => "COD",
            "worldPlace" => "180",
            "places" => "Африка",
        ),

        "KP" => array
        (
            "country" => "Корея, Народно-Демократическая Республика",
            "fullCountry" => "Корейская Народно-Демократическая Республика",
            "english" => "Korea, Democratic People's republic of",
            "alpha2" => "KP",
            "alpha3Iso" => "PRK",
            "worldPlace" => "408",
            "places" => "Азия",
        ),

        "KR" => array
        (
            "country" => "Корея, Республика",
            "fullCountry" => "Республика Корея",
            "english" => "Korea, Republic of",
            "alpha2" => "KR",
            "alpha3Iso" => "KOR",
            "worldPlace" => "410",
            "places" => "Азия",
        ),

        "CR" => array
        (
            "country" => "Коста-Рика",
            "fullCountry" => "Республика Коста-Рика",
            "english" => "Costa Rica",
            "alpha2" => "CR",
            "alpha3Iso" => "CRI",
            "worldPlace" => "188",
            "places" => "Америка",
        ),

        "CI" => array
        (
            "country" => "Кот д'Ивуар",
            "fullCountry" => "Республика Кот д'Ивуар",
            "english" => "Cote d'Ivoire",
            "alpha2" => "CI",
            "alpha3Iso" => "CIV",
            "worldPlace" => "384",
            "places" => "Африка",
        ),

        "CU" => array
        (
            "country" => "Куба",
            "fullCountry" => "Республика Куба",
            "english" => "Cuba",
            "alpha2" => "CU",
            "alpha3Iso" => "CUB",
            "worldPlace" => "192",
            "places" => "Америка",
        ),

        "KW" => array
        (
            "country" => "Кувейт",
            "fullCountry" => "Государство Кувейт",
            "english" => "Kuwait",
            "alpha2" => "KW",
            "alpha3Iso" => "KWT",
            "worldPlace" => "414",
            "places" => "Азия",
        ),

        "CW" => array
        (
            "country" => "Кюрасао",
            "fullCountry" => "",
            "english" => "Curaçao",
            "alpha2" => "CW",
            "alpha3Iso" => "CUW",
            "worldPlace" => "531",
            "places" => "Америка",
        ),

        "LA" => array
        (
            "country" => "Лаос",
            "fullCountry" => "Лаосская Народно-Демократическая Республика",
            "english" => "Lao People's Democratic Republic",
            "alpha2" => "LA",
            "alpha3Iso" => "LAO",
            "worldPlace" => "418",
            "places" => "Азия",
        ),

        "LV" => array
        (
            "country" => "Латвия",
            "fullCountry" => "Латвийская Республика",
            "english" => "Latvia",
            "alpha2" => "LV",
            "alpha3Iso" => "LVA",
            "worldPlace" => "428",
            "places" => "Европа",
        ),

        "LS" => array
        (
            "country" => "Лесото",
            "fullCountry" => "Королевство Лесото",
            "english" => "Lesotho",
            "alpha2" => "LS",
            "alpha3Iso" => "LSO",
            "worldPlace" => "426",
            "places" => "Африка",
        ),

        "LB" => array
        (
            "country" => "Ливан",
            "fullCountry" => "Ливанская Республика",
            "english" => "Lebanon",
            "alpha2" => "LB",
            "alpha3Iso" => "LBN",
            "worldPlace" => "422",
            "places" => "Азия",
        ),

        "LY" => array
        (
            "country" => "Ливийская Арабская Джамахирия",
            "fullCountry" => "Социалистическая Народная Ливийская Арабская Джамахирия",
            "english" => "Libyan Arab Jamahiriya",
            "alpha2" => "LY",
            "alpha3Iso" => "LBY",
            "worldPlace" => "434",
            "places" => "Африка",
        ),

        "LR" => array
        (
            "country" => "Либерия",
            "fullCountry" => "Республика Либерия",
            "english" => "Liberia",
            "alpha2" => "LR",
            "alpha3Iso" => "LBR",
            "worldPlace" => "430",
            "places" => "Африка",
        ),

        "LI" => array
        (
            "country" => "Лихтенштейн",
            "fullCountry" => "Княжество Лихтенштейн",
            "english" => "Liechtenstein",
            "alpha2" => "LI",
            "alpha3Iso" => "LIE",
            "worldPlace" => "438",
            "places" => "Европа",
        ),

        "LT" => array
        (
            "country" => "Литва",
            "fullCountry" => "Литовская Республика",
            "english" => "Lithuania",
            "alpha2" => "LT",
            "alpha3Iso" => "LTU",
            "worldPlace" => "440",
            "places" => "Европа",
        ),

        "LU" => array
        (
            "country" => "Люксембург",
            "fullCountry" => "Великое Герцогство Люксембург",
            "english" => "Luxembourg",
            "alpha2" => "LU",
            "alpha3Iso" => "LUX",
            "worldPlace" => "442",
            "places" => "Европа",
        ),

        "MU" => array
        (
            "country" => "Маврикий",
            "fullCountry" => "Республика Маврикий",
            "english" => "Mauritius",
            "alpha2" => "MU",
            "alpha3Iso" => "MUS",
            "worldPlace" => "480",
            "places" => "Африка",
        ),

        "MR" => array
        (
            "country" => "Мавритания",
            "fullCountry" => "Исламская Республика Мавритания",
            "english" => "Mauritania",
            "alpha2" => "MR",
            "alpha3Iso" => "MRT",
            "worldPlace" => "478",
            "places" => "Африка",
        ),

        "MG" => array
        (
            "country" => "Мадагаскар",
            "fullCountry" => "Республика Мадагаскар",
            "english" => "Madagascar",
            "alpha2" => "MG",
            "alpha3Iso" => "MDG",
            "worldPlace" => "450",
            "places" => "Африка",
        ),

        "YT" => array
        (
            "country" => "Майотта",
            "fullCountry" => "",
            "english" => "Mayotte",
            "alpha2" => "YT",
            "alpha3Iso" => "MYT",
            "worldPlace" => "175",
            "places" => "Африка",
        ),

        "MO" => array
        (
            "country" => "Макао",
            "fullCountry" => "Специальный административный регион Китая Макао",
            "english" => "Macao",
            "alpha2" => "MO",
            "alpha3Iso" => "MAC",
            "worldPlace" => "446",
            "places" => "Азия",
        ),

        "MW" => array
        (
            "country" => "Малави",
            "fullCountry" => "Республика Малави",
            "english" => "Malawi",
            "alpha2" => "MW",
            "alpha3Iso" => "MWI",
            "worldPlace" => "454",
            "places" => "Африка",
        ),

        "MY" => array
        (
            "country" => "Малайзия",
            "fullCountry" => "",
            "english" => "Malaysia",
            "alpha2" => "MY",
            "alpha3Iso" => "MYS",
            "worldPlace" => "458",
            "places" => "Азия",
        ),

        "ML" => array
        (
            "country" => "Мали",
            "fullCountry" => "Республика Мали",
            "english" => "Mali",
            "alpha2" => "ML",
            "alpha3Iso" => "MLI",
            "worldPlace" => "466",
            "places" => "Африка",
        ),

        "UM" => array
        (
            "country" => "Малые Тихоокеанские отдаленные острова Соединенных Штатов",
            "fullCountry" => "",
            "english" => "United States Minor Outlying Islands",
            "alpha2" => "UM",
            "alpha3Iso" => "UMI",
            "worldPlace" => "581",
            "places" => "Океания",
        ),

        "MV" => array
        (
            "country" => "Мальдивы",
            "fullCountry" => "Мальдивская Республика",
            "english" => "Maldives",
            "alpha2" => "MV",
            "alpha3Iso" => "MDV",
            "worldPlace" => "462",
            "places" => "Азия",
        ),

        "MT" => array
        (
            "country" => "Мальта",
            "fullCountry" => "Республика Мальта",
            "english" => "Malta",
            "alpha2" => "MT",
            "alpha3Iso" => "MLT",
            "worldPlace" => "470",
            "places" => "Европа",
        ),

        "MA" => array
        (
            "country" => "Марокко",
            "fullCountry" => "Королевство Марокко",
            "english" => "Morocco",
            "alpha2" => "MA",
            "alpha3Iso" => "MAR",
            "worldPlace" => "504",
            "places" => "Африка",
        ),

        "MQ" => array
        (
            "country" => "Мартиника",
            "fullCountry" => "",
            "english" => "Martinique",
            "alpha2" => "MQ",
            "alpha3Iso" => "MTQ",
            "worldPlace" => "474",
            "places" => "Америка",
        ),

        "MH" => array
        (
            "country" => "Маршалловы острова",
            "fullCountry" => "Республика Маршалловы острова",
            "english" => "Marshall Islands",
            "alpha2" => "MH",
            "alpha3Iso" => "MHL",
            "worldPlace" => "584",
            "places" => "Океания",
        ),

        "MX" => array
        (
            "country" => "Мексика",
            "fullCountry" => "Мексиканские Соединенные Штаты",
            "english" => "Mexico",
            "alpha2" => "MX",
            "alpha3Iso" => "MEX",
            "worldPlace" => "484",
            "places" => "Америка",
        ),

        "FM" => array
        (
            "country" => "Микронезия, Федеративные Штаты",
            "fullCountry" => "Федеративные штаты Микронезии",
            "english" => "Micronesia, Federated States of",
            "alpha2" => "FM",
            "alpha3Iso" => "FSM",
            "worldPlace" => "583",
            "places" => "Океания",
        ),

        "MZ" => array
        (
            "country" => "Мозамбик",
            "fullCountry" => "Республика Мозамбик",
            "english" => "Mozambique",
            "alpha2" => "MZ",
            "alpha3Iso" => "MOZ",
            "worldPlace" => "508",
            "places" => "Африка",
        ),

        "MD" => array
        (
            "country" => "Молдова, Республика",
            "fullCountry" => "Республика Молдова",
            "english" => "Moldova",
            "alpha2" => "MD",
            "alpha3Iso" => "MDA",
            "worldPlace" => "498",
            "places" => "Европа",
        ),

        "MC" => array
        (
            "country" => "Монако",
            "fullCountry" => "Княжество Монако",
            "english" => "Monaco",
            "alpha2" => "MC",
            "alpha3Iso" => "MCO",
            "worldPlace" => "492",
            "places" => "Европа",
        ),

        "MN" => array
        (
            "country" => "Монголия",
            "fullCountry" => "",
            "english" => "Mongolia",
            "alpha2" => "MN",
            "alpha3Iso" => "MNG",
            "worldPlace" => "496",
            "places" => "Азия",
        ),

        "MS" => array
        (
            "country" => "Монтсеррат",
            "fullCountry" => "",
            "english" => "Montserrat",
            "alpha2" => "MS",
            "alpha3Iso" => "MSR",
            "worldPlace" => "500",
            "places" => "Америка",
        ),

        "MM" => array
        (
            "country" => "Мьянма",
            "fullCountry" => "Союз Мьянма",
            "english" => "Myanmar",
            "alpha2" => "MM",
            "alpha3Iso" => "MMR",
            "worldPlace" => "104",
            "places" => "Азия",
        ),

        "NA" => array
        (
            "country" => "Намибия",
            "fullCountry" => "Республика Намибия",
            "english" => "Namibia",
            "alpha2" => "NA",
            "alpha3Iso" => "NAM",
            "worldPlace" => "516",
            "places" => "Африка",
        ),

        "NR" => array
        (
            "country" => "Науру",
            "fullCountry" => "Республика Науру",
            "english" => "Nauru",
            "alpha2" => "NR",
            "alpha3Iso" => "NRU",
            "worldPlace" => "520",
            "places" => "Океания",
        ),

        "NP" => array
        (
            "country" => "Непал",
            "fullCountry" => "Федеративная Демократическая Республика Непал",
            "english" => "Nepal",
            "alpha2" => "NP",
            "alpha3Iso" => "NPL",
            "worldPlace" => "524",
            "places" => "Азия",
        ),

        "NE" => array
        (
            "country" => "Нигер",
            "fullCountry" => "Республика Нигер",
            "english" => "Niger",
            "alpha2" => "NE",
            "alpha3Iso" => "NER",
            "worldPlace" => "562",
            "places" => "Африка",
        ),

        "NG" => array
        (
            "country" => "Нигерия",
            "fullCountry" => "Федеративная Республика Нигерия",
            "english" => "Nigeria",
            "alpha2" => "NG",
            "alpha3Iso" => "NGA",
            "worldPlace" => "566",
            "places" => "Африка",
        ),

        "NL" => array
        (
            "country" => "Нидерланды",
            "fullCountry" => "Королевство Нидерландов",
            "english" => "Netherlands",
            "alpha2" => "NL",
            "alpha3Iso" => "NLD",
            "worldPlace" => "528",
            "places" => "Европа",
        ),

        "NI" => array
        (
            "country" => "Никарагуа",
            "fullCountry" => "Республика Никарагуа",
            "english" => "Nicaragua",
            "alpha2" => "NI",
            "alpha3Iso" => "NIC",
            "worldPlace" => "558",
            "places" => "Америка",
        ),

        "NU" => array
        (
            "country" => "Ниуэ",
            "fullCountry" => "Республика Ниуэ",
            "english" => "Niue",
            "alpha2" => "NU",
            "alpha3Iso" => "NIU",
            "worldPlace" => "570",
            "places" => "Океания",
        ),

        "NZ" => array
        (
            "country" => "Новая Зеландия",
            "fullCountry" => "",
            "english" => "New Zealand",
            "alpha2" => "NZ",
            "alpha3Iso" => "NZL",
            "worldPlace" => "554",
            "places" => "Океания",
        ),

        "NC" => array
        (
            "country" => "Новая Каледония",
            "fullCountry" => "",
            "english" => "New Caledonia",
            "alpha2" => "NC",
            "alpha3Iso" => "NCL",
            "worldPlace" => "540",
            "places" => "Океания",
        ),

        "NO" => array
        (
            "country" => "Норвегия",
            "fullCountry" => "Королевство Норвегия",
            "english" => "Norway",
            "alpha2" => "NO",
            "alpha3Iso" => "NOR",
            "worldPlace" => "578",
            "places" => "Европа",
        ),

        "AE" => array
        (
            "country" => "Объединенные Арабские Эмираты",
            "fullCountry" => "",
            "english" => "United Arab Emirates",
            "alpha2" => "AE",
            "alpha3Iso" => "ARE",
            "worldPlace" => "784",
            "places" => "Азия",
        ),

        "OM" => array
        (
            "country" => "Оман",
            "fullCountry" => "Султанат Оман",
            "english" => "Oman",
            "alpha2" => "OM",
            "alpha3Iso" => "OMN",
            "worldPlace" => "512",
            "places" => "Азия",
        ),

        "BV" => array
        (
            "country" => "Остров Буве",
            "fullCountry" => "",
            "english" => "Bouvet Island",
            "alpha2" => "BV",
            "alpha3Iso" => "BVT",
            "worldPlace" => "074",
            "places" => "",
        ),

        "IM" => array
        (
            "country" => "Остров Мэн",
            "fullCountry" => "",
            "english" => "Isle of Man",
            "alpha2" => "IM",
            "alpha3Iso" => "IMN",
            "worldPlace" => "833",
            "places" => "Европа",
        ),

        "NF" => array
        (
            "country" => "Остров Норфолк",
            "fullCountry" => "",
            "english" => "Norfolk Island",
            "alpha2" => "NF",
            "alpha3Iso" => "NFK",
            "worldPlace" => "574",
            "places" => "Океания",
        ),

        "CX" => array
        (
            "country" => "Остров Рождества",
            "fullCountry" => "",
            "english" => "Christmas Island",
            "alpha2" => "CX",
            "alpha3Iso" => "CXR",
            "worldPlace" => "162",
            "places" => "Азия",
        ),

        "HM" => array
        (
            "country" => "Остров Херд и острова Макдональд",
            "fullCountry" => "",
            "english" => "Heard Island and McDonald Islands",
            "alpha2" => "HM",
            "alpha3Iso" => "HMD",
            "worldPlace" => "334",
            "places" => "",
        ),

        "KY" => array
        (
            "country" => "Острова Кайман",
            "fullCountry" => "",
            "english" => "Cayman Islands",
            "alpha2" => "KY",
            "alpha3Iso" => "CYM",
            "worldPlace" => "136",
            "places" => "Америка",
        ),

        "CK" => array
        (
            "country" => "Острова Кука",
            "fullCountry" => "",
            "english" => "Cook Islands",
            "alpha2" => "CK",
            "alpha3Iso" => "COK",
            "worldPlace" => "184",
            "places" => "Океания",
        ),

        "TC" => array
        (
            "country" => "Острова Теркс и Кайкос",
            "fullCountry" => "",
            "english" => "Turks and Caicos Islands",
            "alpha2" => "TC",
            "alpha3Iso" => "TCA",
            "worldPlace" => "796",
            "places" => "Америка",
        ),

        "PK" => array
        (
            "country" => "Пакистан",
            "fullCountry" => "Исламская Республика Пакистан",
            "english" => "Pakistan",
            "alpha2" => "PK",
            "alpha3Iso" => "PAK",
            "worldPlace" => "586",
            "places" => "Азия",
        ),

        "PW" => array
        (
            "country" => "Палау",
            "fullCountry" => "Республика Палау",
            "english" => "Palau",
            "alpha2" => "PW",
            "alpha3Iso" => "PLW",
            "worldPlace" => "585",
            "places" => "Океания",
        ),

        "PS" => array
        (
            "country" => "Палестинская территория, оккупированная",
            "fullCountry" => "Оккупированная Палестинская территория",
            "english" => "Palestinian Territory, Occupied",
            "alpha2" => "PS",
            "alpha3Iso" => "PSE",
            "worldPlace" => "275",
            "places" => "Азия",
        ),

        "PA" => array
        (
            "country" => "Панама",
            "fullCountry" => "Республика Панама",
            "english" => "Panama",
            "alpha2" => "PA",
            "alpha3Iso" => "PAN",
            "worldPlace" => "591",
            "places" => "Америка",
        ),

        "VA" => array
        (
            "country" => "Папский Престол (Государство — город Ватикан)",
            "fullCountry" => "",
            "english" => "Holy See (Vatican City State)",
            "alpha2" => "VA",
            "alpha3Iso" => "VAT",
            "worldPlace" => "336",
            "places" => "Европа",
        ),

        "PG" => array
        (
            "country" => "Папуа-Новая Гвинея",
            "fullCountry" => "",
            "english" => "Papua New Guinea",
            "alpha2" => "PG",
            "alpha3Iso" => "PNG",
            "worldPlace" => "598",
            "places" => "Океания",
        ),

        "PY" => array
        (
            "country" => "Парагвай",
            "fullCountry" => "Республика Парагвай",
            "english" => "Paraguay",
            "alpha2" => "PY",
            "alpha3Iso" => "PRY",
            "worldPlace" => "600",
            "places" => "Америка",
        ),

        "PE" => array
        (
            "country" => "Перу",
            "fullCountry" => "Республика Перу",
            "english" => "Peru",
            "alpha2" => "PE",
            "alpha3Iso" => "PER",
            "worldPlace" => "604",
            "places" => "Америка",
        ),

        "PN" => array
        (
            "country" => "Питкерн",
            "fullCountry" => "",
            "english" => "Pitcairn",
            "alpha2" => "PN",
            "alpha3Iso" => "PCN",
            "worldPlace" => "612",
            "places" => "Океания",
        ),

        "PL" => array
        (
            "country" => "Польша",
            "fullCountry" => "Республика Польша",
            "english" => "Poland",
            "alpha2" => "PL",
            "alpha3Iso" => "POL",
            "worldPlace" => "616",
            "places" => "Европа",
        ),

        "PT" => array
        (
            "country" => "Португалия",
            "fullCountry" => "Португальская Республика",
            "english" => "Portugal",
            "alpha2" => "PT",
            "alpha3Iso" => "PRT",
            "worldPlace" => "620",
            "places" => "Европа",
        ),

        "PR" => array
        (
            "country" => "Пуэрто-Рико",
            "fullCountry" => "",
            "english" => "Puerto Rico",
            "alpha2" => "PR",
            "alpha3Iso" => "PRI",
            "worldPlace" => "630",
            "places" => "Америка",
        ),

        "MK" => array
        (
            "country" => "Республика Македония",
            "fullCountry" => "",
            "english" => "Macedonia, The Former Yugoslab Republic Of",
            "alpha2" => "MK",
            "alpha3Iso" => "MKD",
            "worldPlace" => "807",
            "places" => "Европа",
        ),

        "RE" => array
        (
            "country" => "Реюньон",
            "fullCountry" => "",
            "english" => "Reunion",
            "alpha2" => "RE",
            "alpha3Iso" => "REU",
            "worldPlace" => "638",
            "places" => "Африка",
        ),

        "RU" => array
        (
            "country" => "Россия",
            "fullCountry" => "Российская Федерация",
            "english" => "Russian Federation",
            "alpha2" => "RU",
            "alpha3Iso" => "RUS",
            "worldPlace" => "643",
            "places" => "Европа",
        ),

        "RW" => array
        (
            "country" => "Руанда",
            "fullCountry" => "Руандийская Республика",
            "english" => "Rwanda",
            "alpha2" => "RW",
            "alpha3Iso" => "RWA",
            "worldPlace" => "646",
            "places" => "Африка",
        ),

        "RO" => array
        (
            "country" => "Румыния",
            "fullCountry" => "",
            "english" => "Romania",
            "alpha2" => "RO",
            "alpha3Iso" => "ROU",
            "worldPlace" => "642",
            "places" => "Европа",
        ),

        "WS" => array
        (
            "country" => "Самоа",
            "fullCountry" => "Независимое Государство Самоа",
            "english" => "Samoa",
            "alpha2" => "WS",
            "alpha3Iso" => "WSM",
            "worldPlace" => "882",
            "places" => "Океания",
        ),

        "SM" => array
        (
            "country" => "Сан-Марино",
            "fullCountry" => "Республика Сан-Марино",
            "english" => "San Marino",
            "alpha2" => "SM",
            "alpha3Iso" => "SMR",
            "worldPlace" => "674",
            "places" => "Европа",
        ),

        "ST" => array
        (
            "country" => "Сан-Томе и Принсипи",
            "fullCountry" => "Демократическая Республика Сан-Томе и Принсипи",
            "english" => "Sao Tome and Principe",
            "alpha2" => "ST",
            "alpha3Iso" => "STP",
            "worldPlace" => "678",
            "places" => "Африка",
        ),

        "SA" => array
        (
            "country" => "Саудовская Аравия",
            "fullCountry" => "Королевство Саудовская Аравия",
            "english" => "Saudi Arabia",
            "alpha2" => "SA",
            "alpha3Iso" => "SAU",
            "worldPlace" => "682",
            "places" => "Азия",
        ),

        "SZ" => array
        (
            "country" => "Свазиленд",
            "fullCountry" => "Королевство Свазиленд",
            "english" => "Swaziland",
            "alpha2" => "SZ",
            "alpha3Iso" => "SWZ",
            "worldPlace" => "748",
            "places" => "Африка",
        ),

        "SH" => array
        (
            "country" => "Святая Елена, Остров вознесения, Тристан-да-Кунья",
            "fullCountry" => "",
            "english" => "Saint Helena, Ascension And Tristan Da Cunha",
            "alpha2" => "SH",
            "alpha3Iso" => "SHN",
            "worldPlace" => "654",
            "places" => "Африка",
        ),

        "MP" => array
        (
            "country" => "Северные Марианские острова",
            "fullCountry" => "Содружество Северных Марианских островов",
            "english" => "Northern Mariana Islands",
            "alpha2" => "MP",
            "alpha3Iso" => "MNP",
            "worldPlace" => "580",
            "places" => "Океания",
        ),

        "BL" => array
        (
            "country" => "Сен-Бартельми",
            "fullCountry" => "",
            "english" => "Saint Barthélemy",
            "alpha2" => "BL",
            "alpha3Iso" => "BLM",
            "worldPlace" => "652",
            "places" => "Америка",
        ),

        "MF" => array
        (
            "country" => "Сен-Мартен",
            "fullCountry" => "",
            "english" => "Saint Martin (French Part)",
            "alpha2" => "MF",
            "alpha3Iso" => "MAF",
            "worldPlace" => "663",
            "places" => "Америка",
        ),

        "SN" => array
        (
            "country" => "Сенегал",
            "fullCountry" => "Республика Сенегал",
            "english" => "Senegal",
            "alpha2" => "SN",
            "alpha3Iso" => "SEN",
            "worldPlace" => "686",
            "places" => "Африка",
        ),

        "VC" => array
        (
            "country" => "Сент-Винсент и Гренадины",
            "fullCountry" => "",
            "english" => "Saint Vincent and the Grenadines",
            "alpha2" => "VC",
            "alpha3Iso" => "VCT",
            "worldPlace" => "670",
            "places" => "Америка",
        ),

        "LC" => array
        (
            "country" => "Сент-Люсия",
            "fullCountry" => "",
            "english" => "Saint Lucia",
            "alpha2" => "LC",
            "alpha3Iso" => "LCA",
            "worldPlace" => "662",
            "places" => "Америка",
        ),

        "KN" => array
        (
            "country" => "Сент-Китс и Невис",
            "fullCountry" => "",
            "english" => "Saint Kitts and Nevis",
            "alpha2" => "KN",
            "alpha3Iso" => "KNA",
            "worldPlace" => "659",
            "places" => "Америка",
        ),

        "PM" => array
        (
            "country" => "Сент-Пьер и Микелон",
            "fullCountry" => "",
            "english" => "Saint Pierre and Miquelon",
            "alpha2" => "PM",
            "alpha3Iso" => "SPM",
            "worldPlace" => "666",
            "places" => "Америка",
        ),

        "RS" => array
        (
            "country" => "Сербия",
            "fullCountry" => "Республика Сербия",
            "english" => "Serbia",
            "alpha2" => "RS",
            "alpha3Iso" => "SRB",
            "worldPlace" => "688",
            "places" => "Европа",
        ),

        "SC" => array
        (
            "country" => "Сейшелы",
            "fullCountry" => "Республика Сейшелы",
            "english" => "Seychelles",
            "alpha2" => "SC",
            "alpha3Iso" => "SYC",
            "worldPlace" => "690",
            "places" => "Африка",
        ),

        "SG" => array
        (
            "country" => "Сингапур",
            "fullCountry" => "Республика Сингапур",
            "english" => "Singapore",
            "alpha2" => "SG",
            "alpha3Iso" => "SGP",
            "worldPlace" => "702",
            "places" => "Азия",
        ),

        "SX" => array
        (
            "country" => "Синт-Мартен",
            "fullCountry" => "",
            "english" => "Sint Maarten",
            "alpha2" => "SX",
            "alpha3Iso" => "SXM",
            "worldPlace" => "534",
            "places" => "Америка",
        ),

        "SY" => array
        (
            "country" => "Сирийская Арабская Республика",
            "fullCountry" => "",
            "english" => "Syrian Arab Republic",
            "alpha2" => "SY",
            "alpha3Iso" => "SYR",
            "worldPlace" => "760",
            "places" => "Азия",
        ),

        "SK" => array
        (
            "country" => "Словакия",
            "fullCountry" => "Словацкая Республика",
            "english" => "Slovakia",
            "alpha2" => "SK",
            "alpha3Iso" => "SVK",
            "worldPlace" => "703",
            "places" => "Европа",
        ),

        "SI" => array
        (
            "country" => "Словения",
            "fullCountry" => "Республика Словения",
            "english" => "Slovenia",
            "alpha2" => "SI",
            "alpha3Iso" => "SVN",
            "worldPlace" => "705",
            "places" => "Европа",
        ),

        "GB" => array
        (
            "country" => "Соединенное Королевство",
            "fullCountry" => "Соединенное Королевство Великобритании и Северной Ирландии",
            "english" => "United Kingdom",
            "alpha2" => "GB",
            "alpha3Iso" => "GBR",
            "worldPlace" => "826",
            "places" => "Европа",
        ),

        "US" => array
        (
            "country" => "Соединенные Штаты",
            "fullCountry" => "Соединенные Штаты Америки",
            "english" => "United States",
            "alpha2" => "US",
            "alpha3Iso" => "USA",
            "worldPlace" => "840",
            "places" => "Америка",
        ),

        "SB" => array
        (
            "country" => "Соломоновы острова",
            "fullCountry" => "",
            "english" => "Solomon Islands",
            "alpha2" => "SB",
            "alpha3Iso" => "SLB",
            "worldPlace" => "090",
            "places" => "Океания",
        ),

        "SO" => array
        (
            "country" => "Сомали",
            "fullCountry" => "Сомалийская Республика",
            "english" => "Somalia",
            "alpha2" => "SO",
            "alpha3Iso" => "SOM",
            "worldPlace" => "706",
            "places" => "Африка",
        ),

        "SD" => array
        (
            "country" => "Судан",
            "fullCountry" => "Республика Судан",
            "english" => "Sudan",
            "alpha2" => "SD",
            "alpha3Iso" => "SDN",
            "worldPlace" => "736",
            "places" => "Африка",
        ),

        "SR" => array
        (
            "country" => "Суринам",
            "fullCountry" => "Республика Суринам",
            "english" => "Suriname",
            "alpha2" => "SR",
            "alpha3Iso" => "SUR",
            "worldPlace" => "740",
            "places" => "Америка",
        ),

        "SL" => array
        (
            "country" => "Сьерра-Леоне",
            "fullCountry" => "Республика Сьерра-Леоне",
            "english" => "Sierra Leone",
            "alpha2" => "SL",
            "alpha3Iso" => "SLE",
            "worldPlace" => "694",
            "places" => "Африка",
        ),

        "TJ" => array
        (
            "country" => "Таджикистан",
            "fullCountry" => "Республика Таджикистан",
            "english" => "Tajikistan",
            "alpha2" => "TJ",
            "alpha3Iso" => "TJK",
            "worldPlace" => "762",
            "places" => "Азия",
        ),

        "TH" => array
        (
            "country" => "Таиланд",
            "fullCountry" => "Королевство Таиланд",
            "english" => "Thailand",
            "alpha2" => "TH",
            "alpha3Iso" => "THA",
            "worldPlace" => "764",
            "places" => "Азия",
        ),

        "TW" => array
        (
            "country" => "Тайвань (Китай)",
            "fullCountry" => "",
            "english" => "Taiwan, Province of China",
            "alpha2" => "TW",
            "alpha3Iso" => "TWN",
            "worldPlace" => "158",
            "places" => "Азия",
        ),

        "TZ" => array
        (
            "country" => "Танзания, Объединенная Республика",
            "fullCountry" => "Объединенная Республика Танзания",
            "english" => "Tanzania, United Republic Of",
            "alpha2" => "TZ",
            "alpha3Iso" => "TZA",
            "worldPlace" => "834",
            "places" => "Африка",
        ),

        "TL" => array
        (
            "country" => "Тимор-Лесте",
            "fullCountry" => "Демократическая Республика Тимор-Лесте",
            "english" => "Timor-Leste",
            "alpha2" => "TL",
            "alpha3Iso" => "TLS",
            "worldPlace" => "626",
            "places" => "Азия",
        ),

        "TG" => array
        (
            "country" => "Того",
            "fullCountry" => "Тоголезская Республика",
            "english" => "Togo",
            "alpha2" => "TG",
            "alpha3Iso" => "TGO",
            "worldPlace" => "768",
            "places" => "Африка",
        ),

        "TK" => array
        (
            "country" => "Токелау",
            "fullCountry" => "",
            "english" => "Tokelau",
            "alpha2" => "TK",
            "alpha3Iso" => "TKL",
            "worldPlace" => "772",
            "places" => "Океания",
        ),

        "TO" => array
        (
            "country" => "Тонга",
            "fullCountry" => "Королевство Тонга",
            "english" => "Tonga",
            "alpha2" => "TO",
            "alpha3Iso" => "TON",
            "worldPlace" => "776",
            "places" => "Океания",
        ),

        "TT" => array
        (
            "country" => "Тринидад и Тобаго",
            "fullCountry" => "Республика Тринидад и Тобаго",
            "english" => "Trinidad and Tobago",
            "alpha2" => "TT",
            "alpha3Iso" => "TTO",
            "worldPlace" => "780",
            "places" => "Америка",
        ),

        "TV" => array
        (
            "country" => "Тувалу",
            "fullCountry" => "",
            "english" => "Tuvalu",
            "alpha2" => "TV",
            "alpha3Iso" => "TUV",
            "worldPlace" => "798",
            "places" => "Океания",
        ),

        "TN" => array
        (
            "country" => "Тунис",
            "fullCountry" => "Тунисская Республика",
            "english" => "Tunisia",
            "alpha2" => "TN",
            "alpha3Iso" => "TUN",
            "worldPlace" => "788",
            "places" => "Африка",
        ),

        "TM" => array
        (
            "country" => "Туркмения",
            "fullCountry" => "Туркменистан",
            "english" => "Turkmenistan",
            "alpha2" => "TM",
            "alpha3Iso" => "TKM",
            "worldPlace" => "795",
            "places" => "Азия",
        ),

        "TR" => array
        (
            "country" => "Турция",
            "fullCountry" => "Турецкая Республика",
            "english" => "Turkey",
            "alpha2" => "TR",
            "alpha3Iso" => "TUR",
            "worldPlace" => "792",
            "places" => "Азия",
        ),

        "UG" => array
        (
            "country" => "Уганда",
            "fullCountry" => "Республика Уганда",
            "english" => "Uganda",
            "alpha2" => "UG",
            "alpha3Iso" => "UGA",
            "worldPlace" => "800",
            "places" => "Африка",
        ),

        "UZ" => array
        (
            "country" => "Узбекистан",
            "fullCountry" => "Республика Узбекистан",
            "english" => "Uzbekistan",
            "alpha2" => "UZ",
            "alpha3Iso" => "UZB",
            "worldPlace" => "860",
            "places" => "Азия",
        ),

        "UA" => array
        (
            "country" => "Украина",
            "fullCountry" => "",
            "english" => "Ukraine",
            "alpha2" => "UA",
            "alpha3Iso" => "UKR",
            "worldPlace" => "804",
            "places" => "Европа",
        ),

        "WF" => array
        (
            "country" => "Уоллис и Футуна",
            "fullCountry" => "",
            "english" => "Wallis and Futuna",
            "alpha2" => "WF",
            "alpha3Iso" => "WLF",
            "worldPlace" => "876",
            "places" => "Океания",
        ),

        "UY" => array
        (
            "country" => "Уругвай",
            "fullCountry" => "Восточная Республика Уругвай",
            "english" => "Uruguay",
            "alpha2" => "UY",
            "alpha3Iso" => "URY",
            "worldPlace" => "858",
            "places" => "Америка",
        ),

        "FO" => array
        (
            "country" => "Фарерские острова",
            "fullCountry" => "",
            "english" => "Faroe Islands",
            "alpha2" => "FO",
            "alpha3Iso" => "FRO",
            "worldPlace" => "234",
            "places" => "Европа",
        ),

        "FJ" => array
        (
            "country" => "Фиджи",
            "fullCountry" => "Республика островов Фиджи",
            "english" => "Fiji",
            "alpha2" => "FJ",
            "alpha3Iso" => "FJI",
            "worldPlace" => "242",
            "places" => "Океания",
        ),

        "PH" => array
        (
            "country" => "Филиппины",
            "fullCountry" => "Республика Филиппины",
            "english" => "Philippines",
            "alpha2" => "PH",
            "alpha3Iso" => "PHL",
            "worldPlace" => "608",
            "places" => "Азия",
        ),

        "FI" => array
        (
            "country" => "Финляндия",
            "fullCountry" => "Финляндская Республика",
            "english" => "Finland",
            "alpha2" => "FI",
            "alpha3Iso" => "FIN",
            "worldPlace" => "246",
            "places" => "Европа",
        ),

        "FK" => array
        (
            "country" => "Фолклендские острова (Мальвинские)",
            "fullCountry" => "",
            "english" => "Falkland Islands (Malvinas)",
            "alpha2" => "FK",
            "alpha3Iso" => "FLK",
            "worldPlace" => "238",
            "places" => "Америка",
        ),

        "FR" => array
        (
            "country" => "Франция",
            "fullCountry" => "Французская Республика",
            "english" => "France",
            "alpha2" => "FR",
            "alpha3Iso" => "FRA",
            "worldPlace" => "250",
            "places" => "Европа",
        ),

        "GF" => array
        (
            "country" => "Французская Гвиана",
            "fullCountry" => "",
            "english" => "French Guiana",
            "alpha2" => "GF",
            "alpha3Iso" => "GUF",
            "worldPlace" => "254",
            "places" => "Америка",
        ),

        "PF" => array
        (
            "country" => "Французская Полинезия",
            "fullCountry" => "",
            "english" => "French Polynesia",
            "alpha2" => "PF",
            "alpha3Iso" => "PYF",
            "worldPlace" => "258",
            "places" => "Океания",
        ),

        "TF" => array
        (
            "country" => "Французские Южные территории",
            "fullCountry" => "",
            "english" => "French Southern Territories",
            "alpha2" => "TF",
            "alpha3Iso" => "ATF",
            "worldPlace" => "260",
            "places" => "",
        ),

        "HR" => array
        (
            "country" => "Хорватия",
            "fullCountry" => "Республика Хорватия",
            "english" => "Croatia",
            "alpha2" => "HR",
            "alpha3Iso" => "HRV",
            "worldPlace" => "191",
            "places" => "Европа",
        ),

        "CF" => array
        (
            "country" => "Центрально-Африканская Республика",
            "fullCountry" => "",
            "english" => "Central African Republic",
            "alpha2" => "CF",
            "alpha3Iso" => "CAF",
            "worldPlace" => "140",
            "places" => "Африка",
        ),

        "TD" => array
        (
            "country" => "Чад",
            "fullCountry" => "Республика Чад",
            "english" => "Chad",
            "alpha2" => "TD",
            "alpha3Iso" => "TCD",
            "worldPlace" => "148",
            "places" => "Африка",
        ),

        "ME" => array
        (
            "country" => "Черногория",
            "fullCountry" => "Республика Черногория",
            "english" => "Montenegro",
            "alpha2" => "ME",
            "alpha3Iso" => "MNE",
            "worldPlace" => "499",
            "places" => "Европа",
        ),

        "CZ" => array
        (
            "country" => "Чешская Республика",
            "fullCountry" => "",
            "english" => "Czech Republic",
            "alpha2" => "CZ",
            "alpha3Iso" => "CZE",
            "worldPlace" => "203",
            "places" => "Европа",
        ),

        "CL" => array
        (
            "country" => "Чили",
            "fullCountry" => "Республика Чили",
            "english" => "Chile",
            "alpha2" => "CL",
            "alpha3Iso" => "CHL",
            "worldPlace" => "152",
            "places" => "Америка",
        ),

        "CH" => array
        (
            "country" => "Швейцария",
            "fullCountry" => "Швейцарская Конфедерация",
            "english" => "Switzerland",
            "alpha2" => "CH",
            "alpha3Iso" => "CHE",
            "worldPlace" => "756",
            "places" => "Европа",
        ),

        "SE" => array
        (
            "country" => "Швеция",
            "fullCountry" => "Королевство Швеция",
            "english" => "Sweden",
            "alpha2" => "SE",
            "alpha3Iso" => "SWE",
            "worldPlace" => "752",
            "places" => "Европа",
        ),

        "SJ" => array
        (
            "country" => "Шпицберген и Ян Майен",
            "fullCountry" => "",
            "english" => "Svalbard and Jan Mayen",
            "alpha2" => "SJ",
            "alpha3Iso" => "SJM",
            "worldPlace" => "744",
            "places" => "Европа",
        ),

        "LK" => array
        (
            "country" => "Шри-Ланка",
            "fullCountry" => "Демократическая Социалистическая Республика Шри-Ланка",
            "english" => "Sri Lanka",
            "alpha2" => "LK",
            "alpha3Iso" => "LKA",
            "worldPlace" => "144",
            "places" => "Азия",
        ),

        "EC" => array
        (
            "country" => "Эквадор",
            "fullCountry" => "Республика Эквадор",
            "english" => "Ecuador",
            "alpha2" => "EC",
            "alpha3Iso" => "ECU",
            "worldPlace" => "218",
            "places" => "Америка",
        ),

        "GQ" => array
        (
            "country" => "Экваториальная Гвинея",
            "fullCountry" => "Республика Экваториальная Гвинея",
            "english" => "Equatorial Guinea",
            "alpha2" => "GQ",
            "alpha3Iso" => "GNQ",
            "worldPlace" => "226",
            "places" => "Африка",
        ),

        "AX" => array
        (
            "country" => "Эландские острова",
            "fullCountry" => "",
            "english" => "Åland Islands",
            "alpha2" => "AX",
            "alpha3Iso" => "ALA",
            "worldPlace" => "248",
            "places" => "Европа",
        ),

        "SV" => array
        (
            "country" => "Эль-Сальвадор",
            "fullCountry" => "Республика Эль-Сальвадор",
            "english" => "El Salvador",
            "alpha2" => "SV",
            "alpha3Iso" => "SLV",
            "worldPlace" => "222",
            "places" => "Америка",
        ),

        "ER" => array
        (
            "country" => "Эритрея",
            "fullCountry" => "",
            "english" => "Eritrea",
            "alpha2" => "ER",
            "alpha3Iso" => "ERI",
            "worldPlace" => "232",
            "places" => "Африка",
        ),

        "EE" => array
        (
            "country" => "Эстония",
            "fullCountry" => "Эстонская Республика",
            "english" => "Estonia",
            "alpha2" => "EE",
            "alpha3Iso" => "EST",
            "worldPlace" => "233",
            "places" => "Европа",
        ),

        "ET" => array
        (
            "country" => "Эфиопия",
            "fullCountry" => "Федеративная Демократическая Республика Эфиопия",
            "english" => "Ethiopia",
            "alpha2" => "ET",
            "alpha3Iso" => "ETH",
            "worldPlace" => "231",
            "places" => "Африка",
        ),

        "ZA" => array
        (
            "country" => "Южная Африка",
            "fullCountry" => "Южно-Африканская Республика",
            "english" => "South Africa",
            "alpha2" => "ZA",
            "alpha3Iso" => "ZAF",
            "worldPlace" => "710",
            "places" => "Африка",
        ),

        "GS" => array
        (
            "country" => "Южная Джорджия и Южные Сандвичевы острова",
            "fullCountry" => "",
            "english" => "South Georgia and the South Sandwich Islands",
            "alpha2" => "GS",
            "alpha3Iso" => "SGS",
            "worldPlace" => "239",
            "places" => "",
        ),

        "OS" => array
        (
            "country" => "Южная Осетия",
            "fullCountry" => "Республика Южная Осетия",
            "english" => "South Ossetia",
            "alpha2" => "OS",
            "alpha3Iso" => "OST",
            "worldPlace" => "896",
            "places" => "Азия",
        ),

        "SS" => array
        (
            "country" => "Южный Судан",
            "fullCountry" => "",
            "english" => "South Sudan",
            "alpha2" => "SS",
            "alpha3Iso" => "SSD",
            "worldPlace" => "728",
            "places" => "Африка",
        ),

        "JM" => array
        (
            "country" => "Ямайка",
            "fullCountry" => "",
            "english" => "Jamaica",
            "alpha2" => "JM",
            "alpha3Iso" => "JAM",
            "worldPlace" => "388",
            "places" => "Америка",
        ),

        "JP" => array
        (
            "country" => "Япония",
            "fullCountry" => "",
            "english" => "Japan",
            "alpha2" => "JP",
            "alpha3Iso" => "JPN",
            "worldPlace" => "392",
            "places" => "Азия",
        ),
    );

	public $batch_mode  = false;
	public $memory_mode = false;

	public function __construct($db_file = 'SxGeo.dat', $type = SXGEO_FILE){
        $file = dirname(__FILE__);
		$this->fh = fopen($file . DIRECTORY_SEPARATOR . $db_file, 'rb');
		// Сначала убеждаемся, что есть файл базы данных
		$header = fread($this->fh, 40); // В версии 2.2 заголовок увеличился на 8 байт
		if(substr($header, 0, 3) != 'SxG') die("Can't open {$db_file}\n");
		$info = unpack('Cver/Ntime/Ctype/Ccharset/Cb_idx_len/nm_idx_len/nrange/Ndb_items/Cid_len/nmax_region/nmax_city/Nregion_size/Ncity_size/nmax_country/Ncountry_size/npack_size', substr($header, 3));
		if($info['b_idx_len'] * $info['m_idx_len'] * $info['range'] * $info['db_items'] * $info['time'] * $info['id_len'] == 0) die("Wrong file format {$db_file}\n");
		$this->range       = $info['range'];
		$this->b_idx_len   = $info['b_idx_len'];
		$this->m_idx_len   = $info['m_idx_len'];
		$this->db_items    = $info['db_items'];
		$this->id_len      = $info['id_len'];
		$this->block_len   = 3 + $this->id_len;
		$this->max_region  = $info['max_region'];
		$this->max_city    = $info['max_city'];
		$this->max_country = $info['max_country'];
		$this->country_size= $info['country_size'];
		$this->batch_mode  = $type & SXGEO_BATCH;
		$this->memory_mode = $type & SXGEO_MEMORY;
		$this->pack        = $info['pack_size'] ? explode("\0", fread($this->fh, $info['pack_size'])) : '';
		$this->b_idx_str   = fread($this->fh, $info['b_idx_len'] * 4);
		$this->m_idx_str   = fread($this->fh, $info['m_idx_len'] * 4);

		$this->db_begin = ftell($this->fh);
		if ($this->batch_mode) {
			$this->b_idx_arr = array_values(unpack("N*", $this->b_idx_str)); // Быстрее в 5 раз, чем с циклом
			unset ($this->b_idx_str);
			$this->m_idx_arr = str_split($this->m_idx_str, 4); // Быстрее в 5 раз чем с циклом
			unset ($this->m_idx_str);
		}
		if ($this->memory_mode) {
			$this->db  = fread($this->fh, $this->db_items * $this->block_len);
			$this->regions_db = fread($this->fh, $info['region_size']);
			$this->cities_db  = fread($this->fh, $info['city_size']);
		}
		$this->info = $info;
		$this->info['regions_begin'] = $this->db_begin + $this->db_items * $this->block_len;
		$this->info['cities_begin']  = $this->info['regions_begin'] + $info['region_size'];
	}

	protected function search_idx($ipn, $min, $max){
		if($this->batch_mode){
			while($max - $min > 8){
				$offset = ($min + $max) >> 1;
				if ($ipn > $this->m_idx_arr[$offset]) $min = $offset;
				else $max = $offset;
			}
			while ($ipn > $this->m_idx_arr[$min] && $min++ < $max){};
		}
		else {
			while($max - $min > 8){
				$offset = ($min + $max) >> 1;
				if ($ipn > substr($this->m_idx_str, $offset*4, 4)) $min = $offset;
				else $max = $offset;
			}
			while ($ipn > substr($this->m_idx_str, $min*4, 4) && $min++ < $max){};
		}
		return  $min;
	}

	protected function search_db($str, $ipn, $min, $max){
		if($max - $min > 1) {
			$ipn = substr($ipn, 1);
			while($max - $min > 8){
				$offset = ($min + $max) >> 1;
				if ($ipn > substr($str, $offset * $this->block_len, 3)) $min = $offset;
				else $max = $offset;
			}
			while ($ipn >= substr($str, $min * $this->block_len, 3) && $min++ < $max){};
		}
		else {
			return hexdec(bin2hex(substr($str, $min * $this->block_len + 3 , 3)));
		}
		return hexdec(bin2hex(substr($str, $min * $this->block_len - $this->id_len, $this->id_len)));
	}

	public function get_num($ip){
		$ip1n = (int)$ip; // Первый байт
		if($ip1n == 0 || $ip1n == 10 || $ip1n == 127 || $ip1n >= $this->b_idx_len || false === ($ipn = ip2long($ip))) return false;
		$ipn = pack('N', $ipn);
		$this->ip1c = chr($ip1n);
		// Находим блок данных в индексе первых байт
		if ($this->batch_mode){
			$blocks = array('min' => $this->b_idx_arr[$ip1n-1], 'max' => $this->b_idx_arr[$ip1n]);
		}
		else {
			$blocks = unpack("Nmin/Nmax", substr($this->b_idx_str, ($ip1n - 1) * 4, 8));
		}
		if ($blocks['max'] - $blocks['min'] > $this->range){
			// Ищем блок в основном индексе
			$part = $this->search_idx($ipn, floor($blocks['min'] / $this->range), floor($blocks['max'] / $this->range)-1);
			// Нашли номер блока в котором нужно искать IP, теперь находим нужный блок в БД
			$min = $part > 0 ? $part * $this->range : 0;
			$max = $part > $this->m_idx_len ? $this->db_items : ($part+1) * $this->range;
			// Нужно проверить чтобы блок не выходил за пределы блока первого байта
			if($min < $blocks['min']) $min = $blocks['min'];
			if($max > $blocks['max']) $max = $blocks['max'];
		}
		else {
			$min = $blocks['min'];
			$max = $blocks['max'];
		}
		$len = $max - $min;
		// Находим нужный диапазон в БД
		if ($this->memory_mode) {
			return $this->search_db($this->db, $ipn, $min, $max);
		}
		else {
			fseek($this->fh, $this->db_begin + $min * $this->block_len);
			return $this->search_db(fread($this->fh, $len * $this->block_len), $ipn, 0, $len-1);
		}
	}

	protected function readData($seek, $max, $type){
		$raw = '';
		if($seek && $max) {
			if ($this->memory_mode) {
				$raw = substr($type == 1 ? $this->regions_db : $this->cities_db, $seek, $max);
			} else {
				fseek($this->fh, $this->info[$type == 1 ? 'regions_begin' : 'cities_begin'] + $seek);
				$raw = fread($this->fh, $max);
			}
		}
		return $this->unpack($this->pack[$type], $raw);
	}

	protected function parseCity($seek, $full = false){
		if(!$this->pack) return false;
		$only_country = false;
		if($seek < $this->country_size){
			$country = $this->readData($seek, $this->max_country, 0);
			$city = $this->unpack($this->pack[2]);
			$city['lat'] = $country['lat'];
			$city['lon'] = $country['lon'];
			$only_country = true;
		}
		else {
			$city = $this->readData($seek, $this->max_city, 2);
			$country = array('id' => $city['country_id'], 'iso' => $this->id2iso[$city['country_id']]);
			unset($city['country_id']);
		}
		if($full) {
			$region = $this->readData($city['region_seek'], $this->max_region, 1);
			if(!$only_country) $country = $this->readData($region['country_seek'], $this->max_country, 0);
			unset($city['region_seek']);
			unset($region['country_seek']);
			return array('city' => $city, 'region' => $region, 'country' => $country);
		}
		else {
			unset($city['region_seek']);
			return array('city' => $city, 'country' => array('id' => $country['id'], 'iso' => $country['iso']));
		}
	}

	protected function unpack($pack, $item = ''){
		$unpacked = array();
		$empty = empty($item);
		$pack = explode('/', $pack);
		$pos = 0;
		foreach($pack AS $p){
			list($type, $name) = explode(':', $p);
			$type0 = $type{0};
			if($empty) {
				$unpacked[$name] = $type0 == 'b' || $type0 == 'c' ? '' : 0;
				continue;
			}
			switch($type0){
				case 't':
				case 'T': $l = 1; break;
				case 's':
				case 'n':
				case 'S': $l = 2; break;
				case 'm':
				case 'M': $l = 3; break;
				case 'd': $l = 8; break;
				case 'c': $l = (int)substr($type, 1); break;
				case 'b': $l = strpos($item, "\0", $pos)-$pos; break;
				default: $l = 4;
			}
			$val = substr($item, $pos, $l);
			switch($type0){
				case 't': $v = unpack('c', $val); break;
				case 'T': $v = unpack('C', $val); break;
				case 's': $v = unpack('s', $val); break;
				case 'S': $v = unpack('S', $val); break;
				case 'm': $v = unpack('l', $val . (ord($val{2}) >> 7 ? "\xff" : "\0")); break;
				case 'M': $v = unpack('L', $val . "\0"); break;
				case 'i': $v = unpack('l', $val); break;
				case 'I': $v = unpack('L', $val); break;
				case 'f': $v = unpack('f', $val); break;
				case 'd': $v = unpack('d', $val); break;

				case 'n': $v = current(unpack('s', $val)) / pow(10, $type{1}); break;
				case 'N': $v = current(unpack('l', $val)) / pow(10, $type{1}); break;

				case 'c': $v = rtrim($val, ' '); break;
				case 'b': $v = $val; $l++; break;
			}
			$pos += $l;
			$unpacked[$name] = is_array($v) ? current($v) : $v;
		}
		return $unpacked;
	}

	public function get($ip){
		return $this->max_city ? $this->getCity($ip) : $this->getCountry($ip);
	}
	public function getCountry($ip){
		return $this->id2iso[$this->get_num($ip)];
	}

    public function getFullCountry ($ip) {
        $number = $this->id2iso[$this->get_num($ip)];
        return $this->id2isoFull[$number];
    }

	public function getCountryId($ip){
		return $this->get_num($ip);
	}
	public function getCity($ip){
		$seek = $this->get_num($ip);
		return $seek ? $this->parseCity($seek) : false;
	}
	public function getCityFull($ip){
		$seek = $this->get_num($ip);
		return $seek ? $this->parseCity($seek, 1) : false;
	}
	public function about(){
		$charset = array('utf-8', 'latin1', 'cp1251');
		$types   = array('n/a', 'SxGeo Country', 'SxGeo City RU', 'SxGeo City EN', 'SxGeo City', 'SxGeo City Max RU', 'SxGeo City Max EN', 'SxGeo City Max');
		return array(
			'Created' => date('Y.m.d', $this->info['time']),
			'Timestamp' => $this->info['time'],
			'Charset' => $charset[$this->info['charset']],
			'Type' => $types[$this->info['type']],
			'Byte Index' => $this->b_idx_len,
			'Main Index' => $this->m_idx_len,
			'Blocks In Index Item' => $this->range,
			'IP Blocks' => $this->db_items,
			'Block Size' => $this->block_len,
			'City' => array(
				'Max Length' => $this->max_city,
				'Total Size' => $this->info['city_size'],
			),
			'Region' => array(
				'Max Length' => $this->max_region,
				'Total Size' => $this->info['region_size'],
			),
			'Country' => array(
				'Max Length' => $this->max_country,
				'Total Size' => $this->info['country_size'],
			),
		);
	}
}