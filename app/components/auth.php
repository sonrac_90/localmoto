<?php

    return array(
        'guest'   => array(
            'type'        => CAuthItem::TYPE_ROLE,
            'description' => 'Guest',
            'bizRule'     => null,
            'data'        => null
        ),
        'user'    => array(
            'type'        => CAuthItem::TYPE_ROLE,
            'description' => 'User',
            'children'    => array(
                'guest',
                // унаследуемся от гостя
            ),
            'bizRule'     => null,
            'data'        => null
        ),
        'editor'  => array(
            'type'        => CAuthItem::TYPE_ROLE,
            'description' => 'Editor',
            'children'    => array(
                'user',
                // позволим модератору всё, что позволено пользователю
            ),
            'bizRule'     => null,
            'data'        => null
        ),
        'manager' => array(
            'type'        => CAuthItem::TYPE_ROLE,
            'description' => 'Manager',
            'children'    => array(
                'editor',
                // позволим модератору всё, что позволено редактору
            ),
            'bizRule'     => null,
            'data'        => null
        ),
        'admin'   => array(
            'type'        => CAuthItem::TYPE_ROLE,
            'description' => 'Admin',
            'children'    => array(
                'moderator',
                // позволим админу всё, что позволено менеджеру
            ),
            'bizRule'     => null,
            'data'        => null
        ),
    );