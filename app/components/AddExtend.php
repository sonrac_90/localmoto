<?php

    class AddExtend {

        /**
         * Get Url
         *
         * @param CController $controller
         * @param             mixed {string|null} $addLink
         * @param bool        $absolute
         *
         * @return string
         */
        public static function getURL($controller, $addLink = null, $absolute = true) {
            $module = '';
            if (isset( $controller->module ) && !empty( $controller->module->id )) {
                $module = $controller->module->id . "/" . ( ( is_string($addLink) ) ? $addLink : '' );
            }

            return Yii::app()->getBaseUrl($absolute) . self::trimSlashes($module);
        }

        /**
         * Split url vs /
         *
         * @param string $url
         *
         * @return mixed|string
         */
        public static function trimSlashes($url) {
            $url = str_replace("//", "/", $url);

            if ($url[ 0 ] != '/')
                $url = "/" . $url;

            return $url;
        }

        /**
         * Get Home Url
         *
         * @return string
         */
        public static function homeURL() {
            return Yii::app()->getHomeUrl();
        }

        public static function isAjax() {
            return Yii::app()->request->isAjaxRequest;
        }

        public static function getDateNow($time = true, $timeConvert = null) {
            if (is_null($timeConvert))
                $timeConvert = time();
            return ( ( $time ) ? date('d-m-y H:m:s', $timeConvert) : date('d-m-y', $timeConvert) );
        }

        public static function timeNowDB($time = true, $timeConvert=null) {
            if (is_null($timeConvert))
                $timeConvert = time();

            return ( ( $time ) ? date('Y-m-d H:i:s', $timeConvert) : date('Y-m-d', $timeConvert) );
        }

        public static function destroyVar(&$var) {
            $var = null;
            unset( $var );
        }

        public static function basePath() {
            return Yii::app()->basePath;
        }

        public static function regScriptFile($file, $position = CClientScript::POS_HEAD) {
            self::regFile($file, $position, true);
        }

        private static function regFile($file, $position, $type = true) {
            $tmp = $file;
            if (!is_array($tmp))
                $tmp = array( $file );

            foreach ($tmp as $value) {
                if ($type)
                    Yii::app()->clientScript->registerScriptFile($value, $position); else
                    Yii::app()->clientScript->registerCssFile($value, $position);
            }
        }

        public static function regCoreScript($script, $link = null) {
            Yii::app()->getClientScript()->registerCoreScript($script);
            if (!is_null($link))
                Yii::app()->getClientScript()->scriptMap[ $script ] = $link;
        }

        public static function regCssFile($file, $position = CClientScript::POS_HEAD) {
            self::regFile($file, $position, false);
        }

        public static function getCoreScriptPath() {
            return Yii::app()->clientScript->getCoreScriptUrl();
        }

        public static function checkSetVar($var) {
            if (isset ( $var ) && !empty( $var ) && !is_null($var))
                return true;

            return false;
        }

        public static function implodeOBJ($obj, $attr, $separator = ",") {
            $returnString = '';
            foreach ($obj as $nextAttr) {
                if (isset ( $nextAttr[ $attr ] ) && !is_resource($nextAttr) && !is_array($nextAttr) && !is_object($attr)) {
                    if (!empty( $returnString ))
                        $returnString .= $separator;
                    $returnString .= $nextAttr[ $attr ];
                }
            }

            return $returnString;
        }

        public static function pathCss() {
            return self::pathMedia() . "css/";
        }

        public static function pathMedia($slashes = "/") {
            return self::baseUrl() . "shared/media{$slashes}";
        }

        public static function baseUrl($absolute = false) {
            return Yii::app()->getBaseUrl($absolute) . '/';
        }

        public static function pathJs() {
            return self::pathMedia() . "js/";
        }

        public static function answer($status = true, $mergeArray = array()) {
            echo CJSON::encode(
                array_merge(
                    array(
                        'status' => $status
                    ), $mergeArray
                )
            );
        }

        public static function clearBreadCrumbs() {
            $_SESSION[ 'activeOneMenu' ] = null;
            $_SESSION[ 'activeTwoMenu' ] = null;
        }

        public static function getSystemInfo () {
            $loadCPU = sys_getloadavg();

            $data['cpu_load'] = round((($loadCPU[0]+$loadCPU[1]+$loadCPU[2])/3), 1);

            $meminfo = `cat /proc/meminfo`;
            preg_match_all('/[\S]+/',$meminfo, $ram);
            $data['ram_total'] = floor(intval(@$ram[0][1]) / 1024);
            $data['ram_free'] = floor((intval(@$ram[0][4]) + intval(@$ram[0][10])) / 1024);
            $data['ram_percent'] = round((($data['ram_free'] / $data['ram_total'] * 100)), 1);

            $diskinfo = `df -h /`;

            preg_match_all('/[\S]+/',$diskinfo, $disk_space);
            $disk_space = $disk_space[0];
            $length = count($disk_space) - 1;

            $data['disk_total']     = $disk_space[$length - 4];
            $data['disk_used']      = $disk_space[$length - 3];
            $data['disk_free']      = $disk_space[$length - 2];
            $data['disk_percent']   = ($data['disk_used'] > 0) ? round(floatval($data['disk_used']) / floatval($data['disk_total']) * 100, 1) : 0;

            return $data;
        }

        public static function activeLink($component) {
            $allWord = array();
            if (is_string($component)) {
                $allWord[] = $component;
            }

            if (is_array($component)) {
                $allWord = $component;
            }

            $return = '';

            foreach ($allWord as $_word) {
                if (strpos(Yii::app()->request->requestUri, $_word) !== false) {
                    $return = ' active';
                }
            }
            return $return;
        }
    }