<?php

    class UserIdentity extends CUserIdentity {
        // Будем хранить id.
        protected $_id;
        protected $_username;
        public $hash = false;

        // Данный метод вызывается один раз при аутентификации пользователя.
        public function authenticate() {
            // Производим стандартную аутентификацию, описанную в руководстве.
            $user = Users::model()->find(
                'LOWER(username)=:e OR LOWER(email)=:e', array(
                    ':id' => strtolower($this->username),
                    ':e'  => strtolower($this->username)
                )
            );

            $pass = md5(Users::$md5prefix . $this->password);
            if ($this->hash)
                $pass = $this->password;

            if (( $user === null ) || ($pass !== $user->password )) {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            } else {
                // В качестве идентификатора будем использовать id, а не username,
                // как это определено по умолчанию. Обязательно нужно переопределить
                // метод getId(см. ниже).
                $this->_id = $user->id;

                // Далее логин нам не понадобится, зато имя может пригодится
                // в самом приложении. Используется как Yii::app()->user->name.
                // realName есть в нашей модели. У вас это может быть name, firstName
                // или что-либо ещё.
                $this->setState('username', $user->username);

                $this->errorCode = self::ERROR_NONE;
            }

            return !$this->errorCode;
        }

        public function getId() {
            return $this->_id;
        }

        public function getUsername() {
            return $this->_username;
        }
    }