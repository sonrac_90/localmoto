<?php

    class CropImage {

        /**
         * Путь к картинке
         *
         * @param $id
         *
         * @return string
         */
        public static function dynamicPath($id) {
            $n = (int) $id;

            $a = (int) ( $n / 30000 );
            $b = (int) ( ( $n - $a * 30000 ) / 300 );

            return "{$a}/{$b}";
        }

        //создает рекурсивно путь каталоги по указанному пути
        public static function rmkdir($path, $mode = 0777) {
            echo $path . "<br/><br/>";

            return is_dir($path) || ( self::rmkdir(dirname($path), $mode) && mkdir($path, $mode) );
        }

        /**
         * Create thumbnail
         *
         * @param $image
         * @param $newSize
         *
         * @return mixed
         */
        public static function createThumb($image, $newSize) {
            $imgSize = self::getSizeImage($image);
            $coord = self::getCoordCropCenter($imgSize, $newSize);

            $rect = array(
                'x'      => $coord[ 'x' ],
                'y'      => $coord[ 'y' ],
                'width'  => $newSize[ 'width' ],
                'height' => $newSize[ 'height' ]
            );
            if (function_exists('imagecrop'))
                return imagecrop($image, $rect); else {
                return $image;
            }
        }

        /**
         * Get image size
         *
         * @param $imageSource
         *
         * @return array
         */
        public static function getSizeImage($imageSource) {
            return array(
                'width'  => self::getWidthImage($imageSource),
                'height' => self::getHeightImage($imageSource)
            );
        }

        /**
         * Get image width
         *
         * @param $imageSource
         *
         * @return int
         */
        public static function getWidthImage($imageSource) {
            return imagesx($imageSource);
        }

        /**
         * Get image height
         *
         * @param $imageSource
         *
         * @return int
         */
        public static function getHeightImage($imageSource) {
            return imagesy($imageSource);
        }

        /**
         * Get center crop coordinate
         *
         * @param $imgSize
         * @param $thumbSize
         *
         * @return array
         */
        public static function getCoordCropCenter($imgSize, $thumbSize) {
            $cropX = $imgSize[ 'width' ] / 2;
            $cropY = $imgSize[ 'height' ] / 2;

            $x = ( ( ( $cropX - $thumbSize[ 'width' ] / 2 ) < 0 ) ? 0 : ( $cropX - $thumbSize[ 'width' ] / 2 ) );
            $y = ( ( ( $cropY - $thumbSize[ 'height' ] / 2 < 0 ) ) ? 0 : ( $cropY - $thumbSize[ 'height' ] / 2 ) );

            //        echo "<br/>$x - $y";
            return array(
                'x' => $x,
                'y' => $y
            );
        }

        /**
         * Generate black and color preview with width $scalar and height $scalar*$number
         *
         * @param $image
         * @param $scalar
         * @param $number
         *
         * @return array
         */
        public static function generatePreviewBlackColorResize($image, $scalar, $number = 0.75) {
            $img = self::getNewName($image);
            $imageSource = self::cropImageSize($image, $scalar, $number);

            foreach ($imageSource as $key => $imgNext) {
                self::saveImage($imgNext, $img[ $key ]);
            }

            return $img;
        }

        /**
         * Get new name of output files
         *
         * @param        $image
         * @param string $first
         * @param string $second
         *
         * @return array
         */
        public static function getNewName($image, $first = 'b', $second = 'c') {
            $dir = dirname($image);
            $imageName = explode('.', basename($image));
            $nImg = $imageName[ sizeof($imageName) - 2 ];
            $imageName[ sizeof($imageName) - 2 ] = $nImg . $first;
            $img = array();
            $img[ 'black' ] = $dir . "/" . implode(".", $imageName);
            $imageName[ sizeof($imageName) - 2 ] = $nImg . $second;
            $img[ 'color' ] = $dir . "/" . implode(".", $imageName);

            return $img;
        }

        /**
         * Resize Image
         *
         * @param       $image
         * @param       $size
         * @param float $multi
         *
         * @return array
         */
        public static function cropImageSize($image, $size, $multi = 0.75) {
            $imgSource = self::retSource($image);
            $newSize = array();
            if (is_array($size))
                $newSize = array(
                    'width'  => $size[ 'width' ],
                    'height' => $size[ 'height' ]
                ); else {
                $newSize = array(
                    'width'  => $size,
                    'height' => $size * $multi
                );
            }
            $destination = self::createImg($newSize);

            self::resempledImage($imgSource, $destination, $newSize);

            return array(
                'color' => &$destination,
                'black' => self::cropImageBlack($imgSource, $newSize)
            );
        }

        /**
         * Get image source
         *
         * @param $imgPath
         *
         * @return null|resource
         */
        public static function retSource($imgPath) {
            $imgExt = self::getExtension($imgPath);
            switch ($imgExt) {
                case 'jpg':
                case'jpeg':
                    return imagecreatefromjpeg($imgPath);
                    break;
                case 'png' :
                    return imagecreatefrompng($imgPath);
                    break;
                default:
                    return null;
            }
        }

        /**
         * Get image extension
         *
         * @param $imgPath
         *
         * @return mixed
         */
        public static function getExtension($imgPath) {
            return pathinfo($imgPath, PATHINFO_EXTENSION);
        }

        /**
         * Get source image from file
         *
         * @param $newSize
         *
         * @return resource
         */
        public static function createImg($newSize) {
            return imagecreatetruecolor($newSize[ 'width' ], $newSize[ 'height' ]);
        }

        /**
         * Resize image with new size
         *
         * @param $image
         * @param $destination
         * @param $newSize
         *
         * @return bool
         */
        public static function resempledImage($image, &$destination, $newSize) {
            $imgSize = self::getSizeImage($image);

            $original_aspect = $imgSize[ 'width' ] / $imgSize[ 'height' ];
            $thumb_aspect = $newSize[ 'width' ] / $newSize[ 'height' ];

            if ($original_aspect >= $thumb_aspect) {
                $new_height = $newSize[ 'height' ];
                $new_width = $imgSize[ 'width' ] / ( $imgSize[ 'height' ] / $newSize[ 'height' ] );
            } else {
                $new_width = $newSize[ 'width' ];
                $new_height = $imgSize[ 'height' ] / ( $imgSize[ 'width' ] / $newSize[ 'width' ] );
            }

            $destination = imagecreatetruecolor($newSize[ 'width' ], $newSize[ 'height' ]);
            imagecopyresampled(
                $destination, $image, 0 - ( $new_width - $newSize[ 'width' ] ) / 2, 0 - ( $new_height - $newSize[ 'height' ] ) / 2, 0, 0, $new_width, $new_height, $imgSize[ 'width' ], $imgSize[ 'height' ]
            );
        }

        /**
         * Create GrayScale image
         *
         * @param $image
         * @param $newSize
         *
         * @return resource
         */
        public static function cropImageBlack($image, $newSize) {
            imagefilter($image, IMG_FILTER_GRAYSCALE);
            $destination = self::createImg($newSize);
            self::resempledImage($image, $destination, $newSize);

            return $destination;
        }

        /**
         * Save Image
         *
         * @param      $imgSource
         * @param      $imgPath
         * @param null $quality
         * @param null $filter
         */
        public static function saveImage($imgSource, $imgPath, $quality = null, $filter = null) {
            $extension = self::getExtension($imgPath);

            switch ($extension) {
                case 'jpg':
                case 'jpeg' :
                    if ($quality)
                        imagejpeg($imgSource, $imgPath, $quality); else
                        imagejpeg($imgSource, $imgPath);
                    break;
                case 'png' :
                    if ($quality && $filter)
                        imagepng($imgSource, $imgPath, $quality, $filter); else if ($quality)
                        imagepng($imgSource, $imgPath, $quality); else
                        imagepng($imgSource, $imgPath);

                    break;
            }
        }

        /**
         * Generate two image with $sizeSlider size and $sizeThumb, size
         *
         * @param $image
         * @param $sizeSlider
         * @param $sizeThumb
         *
         * @return array
         */
        public static function generateNewImageAndThumb($image, $sizeSlider, $sizeThumb) {
            $newName = self::getNewName($image, '-b', '-t');

            $destination = self::createImg($sizeSlider);
            self::resempledImage(self::retSource($image), $destination, $sizeSlider);
            self::saveImage($destination, $newName[ 'black' ]);

            imagedestroy($destination);
            $destination = self::createImg($sizeSlider);
            self::resempledImage(self::retSource($image), $destination, $sizeThumb);
            self::saveImage($destination, $newName[ 'color' ]);

            imagedestroy($destination);

            return $newName;
        }

        /**
         * Save image with thumb and normal size. Depends - imageMagic php
         *
         * @param $image
         * @param $sizeSlider
         * @param $sizeThumb
         *
         * @return string
         */
        public static function cropImagegick($image, $sizeSlider, $sizeThumb) {
            $newName = self::getNewName($image, '-b', '-t');
            self::saveImageick($image, $newName[ 'black' ], $sizeSlider);
            self::saveImageick($image, $newName[ 'color' ], $sizeThumb);

            return $newName;
        }

        /**
         * Save image with resizing. Depends: imageMagic php
         *
         * @param $image
         * @param $newName
         * @param $size
         */
        public static function saveImageick($image, $newName, $size) {
            $imageSlider = new Imagick($image);
            $imageSlider->scaleimage($size[ 'width' ], $size[ 'height' ]);
            $imageSlider->writeimage($newName);
        }

        public static function renameFile($oldPath, $newPath) {
            return @rename($oldPath, $newPath);
        }

        public static function addSalt($file, $salt = null) {
            if (is_null($salt))
                $salt = time();
            $fFile = explode('.', $file);
            $fFile[ sizeof($fFile) - 2 ] = $fFile[ sizeof($fFile) - 2 ] . md5($salt);

            return implode('.', str_replace(' ', '', $fFile));
        }
    }