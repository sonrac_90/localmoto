<?php

class GHelperMail {

    public static $link = 'http://lm.devup.cc';

    public static function send ($subject, $to, $message, $from="system@lm.ru") {
        return mail($to, $subject, $message, self::headers());
    }

    public static function headers() {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Дополнительные заголовки
        $headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
        $headers .= 'From: Birthday Reminder <birthday@example.com>' . "\r\n";
        $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
        $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
        return $headers;
    }

    public static function sendMailRegister ($userInfo, $pass=null) {
        $text = 'Уважаемая(-ый), ' . $userInfo['first_name'] . $userInfo['last_name'] . "!" . "<br/><br/> Благодарим за то что выбрали наш магазин. Данные для доступа к учетной записи:<br/><br/>Login: " . $userInfo['email'] . "<br/>Password: " . ((is_null($pass)) ? $userInfo['password'] : $pass) . "<br/><br/>Зазазывайте у нас: <a href='" . self::$link . '">Перейти в магазин</a>';
        return self::send('Регистрация на LocalMoto', $userInfo->email, self::template($text));
    }

    public static function passwordRecovery ($userInfo, $pass=null) {
        $text = 'Уважаемая(-ый), ' . $userInfo['first_name'] . $userInfo['last_name'] . "!" . "<br/><br/> Благодарим за то что выбрали наш магазин. Данные для доступа к учетной записи:<br/><br/>Login: " . $userInfo['email'] . "<br/>Password: " . ((is_null($pass)) ? $userInfo['password'] : $pass) . "<br/><br/>Зазазывайте у нас: <a href='" . self::$link . '">Перейти в магазин</a>';
        return self::send('Восстановление пароля на LocalMoto', $userInfo->email, self::template($text));
    }

    public static function template($text) {
        $url = Yii::app()->createAbsoluteUrl('/');
        return "<!doctype html>
            <html lang=\"ru\">
                <head>
                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                    <title></title>
                </head>
                <body style=\"margin:0; padding:0;\">
                    <table width=\"673px\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: Arial, Tahoma, sans-serif; font-size:16px;\">
                        <tr>
                            <td>{$text}</td>
                        </tr>
                    </table>
                </body>
            </html>
        ";
    }
}