<?php

    class SiteController extends Controller {
        public $title       = "Главная страница";
        public $layout      = "/layouts/main";
        public $breadcrumbs = array();

        public function Init () {

            if (!Yii::app()->user->hasState('sorting')) {
                Yii::app()->user->setState('sorting', 2);
            }

        }

        public function filters() {
            return array( 'accessControl' );
        }

        public function accessRules() {
            return array(
                array(
                    'allow',
                    'roles' => array( '*' ),
                ),
                array(
                    'deny',
                    'actions' => array( 'cabinet' ),
                    'users' => array( '?' ),
                ),
            );
        }

        public static function getContacts() {
            $contact = Contacts::model()->find();
            $phone = array (
                $contact->phone[ 0 ],
                substr($contact->phone, 1, 3),
                substr($contact->phone, 4, strlen($contact->phone) - 1)
            );

            $contRet = array(
                'phone'   => $phone,
                'email'   => $contact->email,
                'address' => $contact->address,
                'city'    => $contact->city,
            );

            return $contRet;
        }

        /**
         * Declares class-based actions.
         */
        public function actions() {
            return array(
                // captcha action renders the CAPTCHA image displayed on the contact page
                'captcha' => array(
                    'class'     => 'CCaptchaAction',
                    'backColor' => 0xFFFFFF,
                ),
                // page action renders "static" pages stored under 'protected/views/site/pages'
                // They can be accessed via: index.php?r=site/page&view=FileName
                'page'    => array(
                    'class' => 'CViewAction',
                ),
            );
        }

        /**
         * This is the default 'index' action that is invoked
         * when an action is not explicitly requested by users.
         */
        public function actionIndex() {

            $criteria = new CDbCriteria();
            $criteria->order = "position ASC";

            $sliders = Slider::model()->findAll($criteria);

            $mainBlock = MainBlock::model()->find();

            AddExtend::clearBreadCrumbs();

            $this->render('index', array(
                    'slider' => $sliders,
                    'main'   => $mainBlock,
                )
            );
        }

        public function getBreadCrumbsCategory ($id) {
            $category = Category::model()->findByPk($id);

            $this->title = "Товары категории '" . $category->name . "'";

            $categoryParent = Category::model()->findByAttributes(array( 'id' => $category->parent ));

            if (!is_null($categoryParent->parent)) {
                $_SESSION[ 'activeTwoMenu' ] = $categoryParent->id;
                $_SESSION[ 'activeOneMenu' ] = $categoryParent->parent;
                $categoryChild = Category::model()->findByAttributes(array( 'id' => $categoryParent->parent ));
            } else {
                $_SESSION[ 'activeTwoMenu' ] = $id;
                $_SESSION[ 'activeOneMenu' ] = $category->parent;
            }

            return array(
                $category,
                $categoryParent,
                $categoryChild
            );
        }

        public function actionShowCat($id) {
            $this->title = 'Просмотр товаров категории';
            list ($category, $categoryParent, $categoryChild) = $this->getBreadCrumbsCategory($id);

            $producer = new Producer();

            $sql = "SELECT
                    mg.id as id,
                    (@cnt:=@cnt+1) as num,
                    mg.`name` as good_name,
                    mg.price as price,
                    mg.sex_characters as sex_characters,
                    (SELECT `name` FROM motolocal_producer WHERE id=mg.producer_id) as producer_name,
                    mg.producer_id as producer_id,
                    mg.is_active as is_active,
                    (SELECT url FROM motolocal_photo WHERE goods_id = mg.id LIMIT 1) as photo
                FROM
                    (SELECT @cnt:=0) r, motolocal_goods mg
                INNER JOIN motolocal_goods_category mgc ON mgc.category_id = " . $id . " AND mg.id = mgc.goods_id";
                if (intval(Yii::app()->user->getState('sorting')) != 2) {
                    $sql .= " WHERE mg.sex_characters = '" . Yii::app()->user->getState('sorting') . "'";
                }
            $this->title = $category->name;

            $total = Yii::app()->db->createCommand("SELECT count(s.id) FROM (" . $sql . ") s")->queryScalar();

            $goods = new CSqlDataProvider(
                $sql, array(
                    'totalItemCount' => $total,
                    'pagination'     => array(
                        'pageSize' => 9,
                    ),
                )
            );

            $this->render(
                'category', array(
                    'goods'              => $goods,
                    'categoryName'       => $category->name,
                    'categoryParentName' => $categoryParent->name,
                    'categoryParentID'   => $categoryParent->id,
                    'categoryChildName'  => $categoryChild->name,
                    'categoryChildID'    => $categoryChild->id,
                    'producerID'         => $producer->id,
                    'producerName'       => $producer->name,
                )
            );
        }

        public function actionAll() {
            $this->title = 'Просмотр всех товаров';
            list ($category, $categoryParent, $categoryChild) = $this->getBreadCrumbsCategory(-100);

            $producer = new Producer();

            $sql = "SELECT
                    mg.id as id,
                    (@cnt:=@cnt+1) as num,
                    mg.`name` as good_name,
                    mg.price as price,
                    mg.sex_characters as sex_characters,
                    (SELECT `name` FROM motolocal_producer WHERE id=mg.producer_id) as producer_name,
                    mg.producer_id as producer_id,
                    mg.is_active as is_active,
                    (SELECT url FROM motolocal_photo WHERE goods_id = mg.id LIMIT 1) as photo
                FROM
                    (SELECT @cnt:=0) r, motolocal_goods mg";

                if (intval(Yii::app()->user->getState('sorting')) != 2) {
                    $sql .= " WHERE mg.sex_characters = '" . Yii::app()->user->getState('sorting') . "'";
                }
            $total = Yii::app()->db->createCommand("SELECT count(s.id) FROM (" . $sql . ") s")->queryScalar();

            $goods = new CSqlDataProvider(
                $sql, array(
                    'totalItemCount' => $total,
                    'pagination'     => array(
                        'pageSize' => 9,
                    ),
                )
            );

            $this->render(
                'category', array(
                    'goods'              => $goods,
                    'categoryName'       => $category->name,
                    'categoryParentName' => $categoryParent->name,
                    'categoryParentID'   => $categoryParent->id,
                    'categoryChildName'  => $categoryChild->name,
                    'categoryChildID'    => $categoryChild->id,
                    'producerID'         => $producer->id,
                    'producerName'       => $producer->name,
                )
            );
        }

        public function actionShowGood($id, $goodID) {

            $this->title = "Просмотр товаров";
            list( $good, $categories, $photos, $sizes, $colors, $descriptions ) = Goods::model()->getGoodInfo($id);

            list ($category, $categoryParent, $categoryChild) = $this->getBreadCrumbsCategory($goodID);

            $producer = new Producer();

            if (is_null($categoryChild)){
                $producer = Producer::model()->findByPk($good->producer_id);
            }

            $this->render(
                'good', array(
                    'good'         => $good,
                    'photos'       => $photos,
                    'sizes'        => $sizes,
                    'colors'       => $colors,
                    'descriptions' => $descriptions,
                    'categoryName'       => $category->name,
                    'categoryID'         => $category->id,
                    'categoryParentName' => $categoryParent->name,
                    'categoryParentID'   => $categoryParent->id,
                    'categoryChildName'  => $categoryChild->name,
                    'categoryChildID'    => $categoryChild->id,
                    'producerID'         => $producer->id,
                    'producerName'       => $producer->name,
                )
            );
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            $this->title = 'Ошибка';
            if ($error = Yii::app()->errorHandler->error) {

                if (Yii::app()->request->isAjaxRequest)
                    echo $error[ 'message' ];
                else
                    $this->render(
                        'error', array(
                            'error' => $error
                        )
                    );
            }else {
                $error = array(
                    'message' => 'Неавторизированный пользователь не может просматривать данную страницу. Авторизируйтесь, используя ваш email и пароль!'
                );
                $this->render(
                    'error', array(
                        'error' => $error
                    )
                );
            }
        }

        /**
         * Displays the contact page
         */
        public function actionContact() {
            $this->title = "Контактная информация";

            $model = new ContactForm;
            if (isset( $_POST[ 'ContactForm' ] )) {
                $model->attributes = $_POST[ 'ContactForm' ];
                if ($model->validate()) {
                    $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                    $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                    $headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" . "MIME-Version: 1.0\r\n" . "Content-Type: text/plain; charset=UTF-8";

                    mail(Yii::app()->params[ 'adminEmail' ], $subject, $model->body, $headers);
                    Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                    $this->refresh();
                }
            }
            $this->render('contact', array( 'model' => $model ));
        }

        /**
         * Displays the login page
         */
        public function actionLogin() {
            $model = new LoginForm;

            // if it is ajax validation request
            if (AddExtend::isAjax()) {

                $model->username   = $_POST[ 'email' ];
                $model->password   = $_POST[ 'passwd' ];
                $model->rememberMe = ($_POST[ 'remember' ] == 'on' ) ? true : false;

                $resultValidation = CJSON::decode(CActiveForm::validate($model));

                $error = 0;
                if (sizeof($resultValidation) && is_array($resultValidation))
                    $error = 111;
                else {
                    $model->login();
                }
                echo CJSON::encode(array_merge(array(
                            'error' => $error
                        ),
                        $resultValidation
                    )
                );
                Yii::app()->end();
            }

            // collect user input data
            if (isset( $_POST[ 'LoginForm' ] )) {
                $model->attributes = $_POST[ 'LoginForm' ];
                // validate user input and redirect to the previous page if valid
                $address = Yii::app()->session->get('address');
                $good    = Yii::app()->session->get('goodBuy');
                if ($model->validate() && $model->login()) {
                    Yii::app()->session->add('address', $address);
                    Yii::app()->session->add('goodBuy', $good);

                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
            // display the login form
            $this->redirect($this->createUrl('site/error'));
        }

        /**
         * Logs out the current user and redirect to homepage.
         */
        public function actionLogout() {
            $address = Yii::app()->session->get('address');
            $good    = Yii::app()->session->get('goodBuy');

            Yii::app()->user->logout();

            Yii::app()->session->add('address', $address);
            Yii::app()->session->add('goodBuy', $good);
            $this->redirect(Yii::app()->homeUrl);
        }

        public function actionPay() {
            $this->title = "Способы оплаты";
            $pays = Pay::model()->getAll();

            $this->render(
                'pay', array(

                    'pays' => $pays
                )
            );
        }

        public function actionDelivery() {
            $this->title = "Способы доставки";
            $deliverys = Delivery::model()->getAll();

            $this->render(
                'delivery', array(

                    'deliverys' => $deliverys,
                )
            );
        }

        public function actionChange() {
            $this->title = "Возврат товара";
            $change = Change::model()->getAll();

            $this->render(
                'change', array(
                    'change' => $change
                )
            );
        }

        public function actionAbout() {
            $this->title = "О компании";
            $about = Contacts::model()->find();

            $this->render(
                'about', array(
                    'about' => $about->about
                )
            );
        }

        public function actionContacts() {
            $this->title = "Контактная информация";
            $contact = Contacts::model()->find();

            $this->render(
                'contacts', array(
                    'contact' => $contact
                )
            );
        }

        public function actionRegister() {
            $user = Users::model()->findByAttributes(array( 'email' => $_POST[ 'email' ] ));
            if (is_null($user) || !$user) {
                $user = new Users();

                $name = explode(" ", $_POST[ 'name' ]);

                $user->first_name    = $name[ 0 ];
                $user->username      = $_POST[ 'username' ];
                $user->last_name     = $name[ 1 ];
                $user->email         = $_POST[ 'email' ];
                $pass                = $_POST[ 'passwd' ];
                $user->password      = md5(Users::$md5prefix . $_POST[ 'passwd' ]);
                $user->subscribe     = ($_POST[ 'subscribe' ] == 'on') ? 1 : 0;
                $user->register_date = AddExtend::getDateNow();
                $user->last_visit    = AddExtend::getDateNow();

                if ($user->validate()) {
                    if ($user->save()) {
                        if ($user->subscribe == 1) {
                            $subscribe = new Subscribe();
                            $subscribe->email = $user->email;
                            $subscribe->date_subscribe = AddExtend::getDateNow();
                            $subscribe->user_id = $user->getPrimaryKey();

                        }
                        GHelperMail::sendMailRegister($user, $pass);
                        echo CJSON::encode(array(
                                'error' => 0
                           )
                        );
                    } else {
                        echo CJSON::encode(array(
                               'error'        => 1,
                               'errorsDetail' => $user->getErrors()
                           )
                       );

                    }
                } else {
                    echo CJSON::encode(array(
                           'error'        => 1,
                           'errorsDetail' => $user->getErrors()
                       )
                    );
                }
            }else {
                echo CJSON::encode(array(
                       'error'     => 2,
                       'textError' => 'Пользователь с таким email существует. Воспользуйтесь формой восстановления пароля'
                   )
                );
            }
        }

        public function actionPassRecovery () {
            $email = $_POST['email'];

            $criteria = new CDbCriteria();
            $criteria->addCondition("email = :email");
            $criteria->params = array(
                ':email' => $email
            );

            /** @var Users $user */
            $user = Users::model()->find($criteria);

            if ($user) {
                $pass = Users::passwordGenerate(15);
                $user->password = md5(Users::$md5prefix . $pass);
                $result = GHelperMail::passwordRecovery($user, $pass);
                if ($result) {
                    $user->update();
                    echo $email;
                }
            }
            exit;
        }

        public function actionChangeSorting ($sorting) {
            Yii::app()->user->setState('sorting', $sorting);
        }

        public function actionShowProducer($id) {
            $this->title = "Просмотр категории товаров производителя";
            list ($category, $categoryParent, $categoryChild) = $this->getBreadCrumbsCategory(-3);

            if (is_null($categoryChild))
                $categoryChild = new Category();

            $sql = "SELECT
                    mg.id as id,
                    (@cnt:=@cnt+1) as num,
                    mg.`name` as good_name,
                    mg.price as price,
                    mg.sex_characters as sex_characters,
                    (SELECT `name` FROM motolocal_producer WHERE id=mg.producer_id) as producer_name,
                    mg.producer_id as producer_id,
                    mg.is_active as is_active,
                    (SELECT url FROM motolocal_photo WHERE goods_id = mg.id LIMIT 1) as photo
                FROM
                    (SELECT @cnt:=0) r, motolocal_goods mg";
                if (intval(Yii::app()->user->getState('sorting')) != 2) {
                    $sql .= " WHERE mg.sex_characters = '" . Yii::app()->user->getState('sorting') . "' and mg.producer_id = " . $id;
                }

            $this->title = $category->name;

            $producer = Producer::model()->findByPk($id);

            $total = Yii::app()->db->createCommand("SELECT count(s.id) FROM (" . $sql . ") s")->queryScalar();

            $goods = new CSqlDataProvider(
                $sql, array(
                    'totalItemCount' => $total,
                    'pagination'     => array(
                        'pageSize' => 9,
                    ),
                )
            );

            $this->render(
                'category', array(
                    'goods'              => $goods,
                    'categoryName'       => $category->name,
                    'categoryParentName' => $categoryParent->name,
                    'categoryParentID'   => $categoryParent->id,
                    'categoryChildName'  => $categoryChild->name,
                    'categoryChildID'    => $categoryChild->id,
                    'producerID'         => $producer->id,
                    'producerName'       => $producer->name,
                )
            );
        }

        public function actionCabinet () {
            $this->title = "Личный кабинет";
            $userID = Yii::app()->user->id;
            /** @var Users $user */
            $user = Users::model()->findByPk($userID);
            $address = Address::model()->getAllUserAddress($userID);
            $order = OrderMain::model()->findAllByAttributes(
                array(
                    'user_email' => $user->email
                )
            );
            $goodCart = Yii::app()->session->get('goodBuy');

            $countGood = count($goodCart);

            $this->render(
                'cabinet', array(
                    'userInfo' => $user,
                    'address'  => $address,
                    'countGood' => $countGood,
                    'order'     => $order
                )
            );
        }

        public function actionSubscribe(){
            $subscribe = new Subscribe();

            $subscribe->email          = $_POST[ 'email' ];
            $subscribe->date_subscribe = AddExtend::getDateNow();
            $error = 0;

            $valResult = $subscribe->validate();

            if ($valResult) {
                $subscribe->save();
            } else
                $error = 112;

            $valResult = $subscribe->getErrors();

            echo CJSON::encode(
                array_merge(
                    array(
                        'error' => $error
                    ), $valResult
                )
            );
        }
    }
