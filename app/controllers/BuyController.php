<?php

    class BuyController extends CController {

        private $sessionVarName = 'goodBuy';

        public $layout = '/layouts/main';
        public $title = 'Корзина';
        public $breadcrumbs = array();

        public function Init () {
//            Yii::app()->session->destroy();
            require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'controllers/SiteController.php';
        }

        private function getData($key = null) {
            if (is_null($key))
                $key = $this->sessionVarName;
            return Yii::app()->session->get($key);
        }

        private function addNewToSession($data, $number, $keySession = null) {
            if (is_null($keySession))
                $keySession = $this->sessionVarName;
            $oldData = $this->getData($keySession);
            if (!is_array($oldData[ $number ]))
                $oldData[ $number ] = array();

            foreach ($data as $key => $value) {
                $oldData[ $number ][ $key ] = $value;
            }

            Yii::app()->session->add($keySession, $oldData);
        }

        private function identicalGood($data, $dataSearch, $listImplode) {

            $position = false;

            if (is_array($data))
                foreach ($data as $pos => $next) {
                    $all = true;
                    foreach ($next as $key => $value) {
                        if (substr_count($listImplode, $key . ',') == 0) {
                            if (isset( $dataSearch[ $key ] ) && $dataSearch[ $key ] != $value) {
                                $all = false;
                            }
                        }
                    }

                    if ($all)
                        $position = $pos;
                }

            return $position;
        }

        public function actionRegisterData() {

            $data = CJSON::decode($_POST[ 'data' ], true);
            $newGood = $data;
            $sessionData = $this->getData();
            $positionGood = $this->identicalGood($sessionData, $data, 'count, ');
            if (is_bool($positionGood) || !count($sessionData)) {
                $next = 0;
                if ($sessionData) {
                    end($sessionData);
                    $next = key($sessionData) + 1;
                    reset($sessionData);
                }
                $this->addNewToSession($newGood, $next);
            }else {
                $sessionData[$positionGood]['count']++;
                $sessionData[$positionGood]['good_price'] += $data['good_price'];
            }
        }

        public function actionUpdateCountGood($id, $count) {
            $data = $this->getData();
            $this->destroyVar();
            $data[$id]['count'] = $count;
            $data[$id]['total_price'] = $count * $data[$id]['price'];
            Yii::app()->session->add('goodBuy', $data);
        }

        private function destroySessVar ($var = null) {
            if (is_null($var))
                $var = $this->sessionVarName;

            Yii::app()->session->remove($var);
        }

        private function destroyVar ($num=0, $key = null) {
            if (is_null($key))
                $key = $this->sessionVarName;

            $old = $this->getData();
            $old[$num] = null;
            unset($old[$num]);
            $this->destroySessVar($key);
            Yii::app()->session->add($key, $old);
        }

        public function actionCart($id = null) {
            if (!is_null($id)) {
                $this->destroyVar($id);
                $this->redirect($this->createUrl('buy/cart'));
            }

            $user = new Users();
            $id = Yii::app()->user->id;

            if (!Yii::app()->user->isGuest) {
                $user = Users::model()->findByPk($id);
            }

//            $address = $this->getData('address');
            $addrSess = array();

//            if (is_null($address) || !isset($address) || !count($address)) {
                $address = Address::model()->getAllUserAddress($id);
                /**
                 * @var integer $key
                 * @var Address $value
                 */
                foreach ($address as $key => $value) {
                    $addrSess[ $key ][ 'id' ]         = $value->id;
                    $addrSess[ $key ][ 'country' ]    = $value->country;
                    $addrSess[ $key ][ 'city' ]       = $value->city;
                    $addrSess[ $key ][ 'street' ]     = $value->street;
                    $addrSess[ $key ][ 'house' ]      = $value->house;
                    $addrSess[ $key ][ 'department' ] = $value->department;
                }

                $this->destroySessVar('address');
//            } else {
//                $addrSess = $address;
//            }

            Yii::app()->session->add('address', $addrSess);

            if (is_null($address))
                $address = array();
            $data = $this->getData();

            if (!count($data))
                $this->redirect($this->createUrl('site/index'));

            $this->render(
                'cart', array(
                    'goodBuy' => $data,
                    'user'    => $user,
                    'address' => $address,
                )
            );
        }

        public function actionAddAddress () {
            $id = null;
            if (isset($_POST['id']) && intval($_POST['id'])) {
                $id = intval($_POST['id']);
            }

            if (is_null($id)) {
                $address = new Address();
                foreach ($_POST as $key => $value) {
                    if ($key != 'id')
                        $address->{$key} = $value;
                }
            } else {
                $address = Address::model()->findByPk($id);
                foreach ($_POST as $key => $value) {
                    if ($key != 'id')
                        $address->{$key} = $value;
                }
            }

            Yii::app()->session->remove('address');

            $address->user_id = Yii::app()->user->id;
            $address->date_add = AddExtend::getDateNow();

            if ($address->validate()) {

                $result = ($id) ? $address->update() : $address->save();

                if ($result) {
                    echo CJSON::encode(
                        array(
                            'error'      => 0,
                            'id'         => $address->getPrimaryKey(),
                            'country'    => $address->country,
                            'city'       => $address->city,
                            'street'     => $address->street,
                            'house'      => $address->house,
                            'department' => $address->department,
                        )
                    );
                    exit;
                }
            }

            echo CJSON::encode(
                array(
                    'error'     => 112,
                    'textError' => 'Невозможно сохранить',
                    'errors'    => $address->getErrors()
                )
            );
        }

        public function actionDeleteAddress ($id) {
            Address::model()->deleteByPk($id);
        }

        public function actionUpdateUserInfo () {
            if (!Yii::app()->user->isGuest) {
                /** @var Users $userInfo */
                $userID = Yii::app()->user->id;
                $userInfo = Users::model()->findByPk($userID);

                if (!is_null($userInfo)) {
                    $data = CJSON::decode($_POST['userInfo']);
                    $data['phone'] = preg_replace ("/[^0-9\s]/", "", $data['phone']);
                    $data['subscribe'] = intval($data['subscribe']);

                    $criteria = new CDbCriteria();
                    $criteria->addCondition('email NOT LIKE \'' . $userInfo->email . '\' AND id <>' . $userID);

                    $oldUSER = Users::model()->find($criteria);

                    $email = '';
                    if ($oldUSER)
                        $email = $userInfo->email;

                    foreach ($data as $key => $value) {
                        $userInfo->{$key} = $value;
                    }

                    if ($email)
                        $userInfo->email = $email;

                    if ($userInfo->validate()) {
                        $userInfo->update();
                    } else {
                        echo CJSON::encode($userInfo->getErrors());
                    }
                }
            }else {
                $data = CJSON::decode($_POST['userInfo']);
                $data['phone'] = preg_replace ("/[^0-9\s]/", "", $data['phone']);

                /** @var Users $userInfo */
                $userInfo = Users::model()->findByAttributes(array('email' => $data['email']));
                if ($userInfo) { // Обновляю данные о пользователе
                    foreach ($data as $key => $value) {
                        $userInfo->{$key} = $value;
                    }

                    if ($userInfo->validate()) { // Обновляю данные о пользователе
                        $userInfo->update();
                    } else {
                        echo "<pre>";   print_r($userInfo->getErrors()); echo "</pre>";
                        exit;
                    }
                    Yii::app()->user->id = $userInfo->getPrimaryKey();
                }else { // Регистрирую пользователя
                    $userInfo = new Users();

                    $userInfo->attributes = $data;
                    $pass = Users::passwordGenerate(10);
                    $userInfo->password = md5(Users::$md5prefix . $pass);
                    $userInfo->register_date = date('d-m-Y H:i:s', time());

                    if ($userInfo->validate()) {
                        $userInfo->save();
                        Yii::app()->user->id = $userInfo->getPrimaryKey();
                    }else {
                        echo "<pre>";   print_r($userInfo->getErrors()); echo "</pre>";
                        exit;
                    }
                }
            }
        }

        public function actionAddOrder () {
            $userInfo = json_decode($_POST[ 'userInfo' ], true);
            $address  = json_decode($_POST[ 'address' ] , true);

            $allGoods = $this->getData();

            foreach ($allGoods as $key => $value) {
                $allGoods[ $key ][ 'good_id' ]          = intval(str_replace('"', '', $value[ 'good_id' ]));
                $allGoods[ $key ][ 'size_id' ]          = intval(str_replace('"', '', $value[ 'size_id' ]));
                $allGoods[ $key ][ 'color_id' ]         = intval(str_replace('"', '', $value[ 'color_id' ]));
                $allGoods[ $key ][ 'description_id' ]   = intval(str_replace('"', '', $value[ 'description_id' ]));
                $allGoods[ $key ][ 'good_producer_id' ] = intval(str_replace('"', '', $value[ 'good_producer_id' ]));
                $allGoods[ $key ][ 'good_producer_id' ] = intval(str_replace('"', '', $value[ 'good_producer_id' ]));
            }

            $criteria = new CDbCriteria();
            $criteria->select = "id";
            $criteria->addCondition("email = :email and first_name = :firstName and last_name = :lastName");
            $criteria->params = array(
                ':email' => $userInfo['user_email'],
                ':firstName' => $userInfo['user_firstname'],
                ':lastName' => $userInfo['user_lastname'],
            );

            $userID = Users::model()->find($criteria);

            $id = null;
            if (!is_null($userID))
                $id = $userID->id;
            else
                $id = Yii::app()->user->id;

            $orderMain = new OrderMain();
            $orderMain->user_birthday  = $userInfo['user_birthday'];
            $orderMain->user_firstname = $userInfo['user_firstname'];
            $orderMain->user_email     = $userInfo['user_email'];
            $orderMain->user_lastname  = $userInfo['user_lastname'];
            $orderMain->user_phone     = preg_replace ("/[^0-9\s]/", "", $userInfo['user_phone']);
            $orderMain->user_id        = $id;
            $orderMain->address_id     = $allGoods[0]['address_id'];

            $addressID = null;
            if (isset($address['city'])) {
                $orderMain->address = $address[ 'country' ] . ", " . $address[ 'city' ] . ", "
                                        . $address[ 'street' ] . ", " . $address[ 'department' ];
                $criteria = new CDbCriteria();
                $criteria->select = 'id';
                $criteria->addCondition("country = :country");
                $criteria->addCondition("city = :city");
                $criteria->addCondition("department = :department");
                $criteria->addCondition("street = :street");
                $criteria->params = array(
                    ':country'    => $address[ 'country' ],
                    ':city'       => $address[ 'city' ],
                    ':street'     => $address[ 'street' ],
                    ':department' => $address[ 'department' ],
                );
                $aID = Address::model()->find($criteria);
                if (!is_null($aID))
                    $addressID = $aID->id;
            }

            $orderMain->address_id = $addressID;
            $orderMain->date_order = AddExtend::timeNowDB();
            $orderMain->type_order = ($address['pickUp']) ? 'pickup' : 'delivery';

            $totalCount = 0;
            $totalPrice = 0;

            $orderAll = array();

            foreach ($allGoods as $_nextGood) {
                $count = $_nextGood['count'];
                $_nextGood['count']     = null;

                $order                  = new Order();
                $order->attributes      = $_nextGood;

                $size = GoodsSize::model()->findByPk($order->size_id);
                $order->size = $size->size;

                $color = ColorGoods::model()->findByPk($order->color_id);
                $order->color = $color->color;

                $description = DescriptionGoods::model()->findByPk($order->description_id);
                $order->description = $description->description;

                $order->user_birthday   = $orderMain->user_birthday;
                $order->user_firstname  = $orderMain->user_firstname;
                $order->user_email      = $orderMain->user_email;
                $order->user_lastname   = $orderMain->user_lastname;
                $order->user_phone      = $orderMain->user_phone;
                $order->user_id         = $orderMain->user_id;
                $order->address_id      = $orderMain->address_id;
                $order->address         = $orderMain->address;
                $order->total_count     = $count;
                $photoID                = explode('.', $order->photo_url);
                $order->photo_id        = $photoID[0];
                $order->total_price     = $count * $order->good_price;
                $order->date_order      = $orderMain->date_order;
                $totalCount            += $count;
                $totalPrice            += $order->total_price;

                $orderAll[]             = $order;
            }

            $orderMain->total_count = $totalCount;
            $orderMain->total_price = $totalPrice;

            if ($orderMain->validate()) {
                if ($orderMain->save()) {

                    /** @var Order $_nextOrder */
                    foreach ($orderAll as $_nextOrder) {
                        $_nextOrder->order_id = $orderMain->getPrimaryKey();

                        if ($_nextOrder->validate()) {
                            $_nextOrder->save();
                        }

                        Yii::app()->session->remove($this->sessionVarName);
                        Yii::app()->session->remove('address');
                    }

                }
            }
        }

        public function actionUpdateAddress() {
            $address = Address::model()->findByPk($_POST[ 'id' ]);
            if ($address) {
                unset($_POST[ 'id' ]);
                $address->attributes = $_POST;
                if ($address->validate()) {
                    $address->update();
                    exit( CJSON::encode(
                        array(
                            'error'     => 0,
                        )
                    ) );
                } else {
                    exit( CJSON::encode(
                        array(
                            'error'     => 112,
                            'errors'    => $address->getErrors(),
                            'textError' => 'Невозможно обновить данные',
                        )
                    ) );
                }
            }

            exit( CJSON::encode(
                array(
                    'error'     => 112,
                    'errors'    => array('Нет такого адресса'),
                    'textError' => 'Невозможно обновить данные',
                )
            ) );

        }

    }