<?php

    /**
     * This is the model class for table "{{order_main}}".
     *
     * The followings are the available columns in table '{{order_main}}':
     *
     * @property integer $id
     * @property integer $user_id
     * @property string  $user_email
     * @property string  $user_firstname
     * @property string  $user_lastname
     * @property string  $user_middlename
     * @property string  $user_birthday
     * @property integer $user_phone
     * @property integer $address_id
     * @property string  $address
     * @property integer $total_count
     * @property string  $date_order
     * @property integer $total_price
     * @property string  $status
     * @property string  $failure_cause
     * @property string  $type_order
     * @property string  $status_pay
     * @property string  $status_delivery
     *
     * The followings are the available model relations:
     * @property Order[] $orders
     * @property Address $address0
     * @property Users   $user
     */
    class OrderMain extends CActiveRecord {
        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{order_main}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'user_email, user_firstname, user_lastname, user_birthday, user_phone, total_count, date_order',
                    'required'
                ),
                array(
                    'user_id, address_id, total_count, total_price',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'user_email, user_phone, user_firstname, user_lastname, user_middlename, address, failure_cause',
                    'length',
                    'max' => 255
                ),
                array(
                    'status, status_pay',
                    'length',
                    'max' => 7
                ),
                array(
                    'type_order',
                    'length',
                    'max' => 8
                ),
                array(
                    'status_delivery',
                    'length',
                    'max' => 12
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, user_id, user_email, user_firstname, user_lastname, user_middlename, user_birthday, user_phone, address_id, address, total_count, date_order, total_price, status, failure_cause, type_order, status_pay, status_delivery',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'orders'   => array(
                    self::HAS_MANY,
                    'Order',
                    'order_id'
                ),
                'address0' => array(
                    self::BELONGS_TO,
                    'Address',
                    'address_id'
                ),
                'user'     => array(
                    self::BELONGS_TO,
                    'Users',
                    'user_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'              => 'ID',
                'user_id'         => 'Пользователь',
                'user_email'      => 'Email',
                'user_firstname'  => 'Имя',
                'user_lastname'   => 'Фамилия',
                'user_middlename' => 'Отчество',
                'user_birthday'   => 'Дата рождения',
                'user_phone'      => 'Телефон',
                'address_id'      => 'Address',
                'address'         => 'Адрес доставки',
                'total_count'     => 'Количество',
                'date_order'      => 'Дата заказа',
                'total_price'     => 'Общая стоимость',
                'status'          => 'Статус заказа',
                'failure_cause'   => 'Причина отказа',
                'type_order'      => 'Тип доставки',
                'status_pay'      => 'Статус оплаты',
                'status_delivery' => 'Статус доставки',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('user_id', $this->user_id);
            $criteria->compare('user_email', $this->user_email, true);
            $criteria->compare('user_firstname', $this->user_firstname, true);
            $criteria->compare('user_lastname', $this->user_lastname, true);
            $criteria->compare('user_middlename', $this->user_middlename, true);
            $criteria->compare('user_birthday', $this->user_birthday, true);
            $criteria->compare('user_phone', $this->user_phone);
            $criteria->compare('address_id', $this->address_id);
            $criteria->compare('address', $this->address, true);
            $criteria->compare('total_count', $this->total_count);
            $criteria->compare('date_order', $this->date_order, true);
            $criteria->compare('total_price', $this->total_price);
            $criteria->compare('status', $this->status, true);
            $criteria->compare('failure_cause', $this->failure_cause, true);
            $criteria->compare('type_order', $this->type_order, true);
            $criteria->compare('status_pay', $this->status_pay, true);
            $criteria->compare('status_delivery', $this->status_delivery, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return OrderMain the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        public static function getCountOrder($where=null) {
            $sql = "SELECT count(id) as id FROM " . self::tableName();
            if (is_string($where))
                $sql .= " WHERE " . $where;

            return Yii::app()->db->createCommand($sql)->queryScalar();
        }

    }