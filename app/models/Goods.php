<?php

    /**
     * This is the model class for table "{{goods}}".
     *
     * The followings are the available columns in table '{{goods}}':
     *
     * @property integer         $id
     * @property string          $name
     * @property integer         $price
     * @property string          $model
     * @property string          $sex_characters
     * @property integer         $producer_id
     * @property integer         $is_active
     *
     * The followings are the available model relations:
     * @property Producer        $producer
     * @property GoodsCategory[] $goodsCategories
     * @property Photo[]         $photos
     * @property Goods[]         $producerGoods
     */
    class Goods extends CActiveRecord {
        public $producerName;
        public $photo;

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Goods the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{goods}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'name, price, model, producer_id',
                    'required'
                ),
                array(
                    'price, producer_id, is_active',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'name, model',
                    'length',
                    'max' => 255
                ),
                array(
                    'sex_characters',
                    'length',
                    'max' => 1
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, name, price, model, sex_characters, is_active, producer_id, producerName',
                    'safe',
                    'on' => 'search'
                ),
                array(
                    'id, name, price, model, sex_characters, is_active, producer_id, producerName',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'producer'        => array(
                    self::BELONGS_TO,
                    'Producer',
                    'producer_id'
                ),
                'goodsCategories' => array(
                    self::HAS_MANY,
                    'GoodsCategory',
                    'producer_id'
                )
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'             => 'ID',
                'name'           => 'Название',
                'price'          => 'Цена',
                'model'          => 'Модель',
                'sex_characters' => 'М/Ж',
                'producer_id'    => 'Producer',
                'is_active'      => 'Видимость',
                'producerName'   => 'Производитель',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @param $id
         * @param $type
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search($id = null, $type = null) {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->with = array( 'producer' );

            if (!is_null($id)) {
                if ($type == 'cat') {
                    $criteria->join = "INNER JOIN " . GoodsCategory::model()->tableName() . " cat ON cat.goods_id = t.id and cat.category_id = $id";
                }else {
                    $criteria->addCondition("producer_id = $id");
                }
            }


            $criteria->compare('t.id', $this->id, true);
            $criteria->compare('t.name', $this->name, true);
            $criteria->compare('t.price', $this->price);
            $criteria->compare('t.model', $this->model, true);
            $criteria->compare('t.sex_characters', $this->sex_characters, true);
            $criteria->compare('t.is_active', $this->is_active, true);
            $criteria->compare('producer.name', $this->producerName, true);
            $criteria->compare('t.producer_id', $this->producer_id, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria'   => $criteria,
                    'sort'       => array(
                        'defaultOrder' => array( 't.id DESC' ),
                        'attributes'   => array(
                            '*',
                            'producerName' => array(
                                'asc'  => 'producer.name ASC',
                                'desc' => 'producer.name DESC'
                            )
                        )
                    ),
                    'pagination' => array(
                        'pageSize' => 10
                    )
                )
            );
        }

        public function getFirstPhoto($good_id) {
            $photo = Photo::model()->findByAttributes(array( 'goods_id' => $good_id ));

            return array(
                $photo->url,
                $photo->id
            );
        }

        // Сбор данных о товаре
        public function getGoodInfo($id) {
            // Собственно сам товар
            $goods = $this->findByPk($id);
            $prod = Producer::model()->findByPk($goods->producer_id);
            if ($prod)
                $goods->producerName = $prod->name;
            // Категории
            $categories = array( Category::model()->getCategoryById($id, false, true) );

            // Фотографии
            $photos = Photo::model()->getAllPhotoGood($id);

            // Размеры фотографий
            $sizes = GoodsSize::model()->getAllSizesPhotos($photos);

            // Цвета фотографий
            $colors = ColorGoods::model()->getAllColorPhotos($photos);

            // Описания фотографий
            $descriptions = DescriptionGoods::model()->getAllDescriptionPhotos($photos);

            return array(
                $goods,
                $categories,
                $photos,
                $sizes,
                $colors,
                $descriptions
            );
        }

        public function getSizes() {
            return array(
                'XS'    => 'XS',
                'S'     => 'S',
                'M'     => 'M',
                'L'     => 'L',
                'XL'    => 'XL',
                'XXL'   => 'XXL',
                'XXXL'  => 'XXXL',
                'BXL'   => 'BXL',
                'BXXL'  => 'BXXL',
                'BXXXL' => 'BXXXL',
            );
        }

    }
