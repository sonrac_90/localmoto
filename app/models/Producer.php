<?php

    /**
     * This is the model class for table "{{producer}}".
     *
     * The followings are the available columns in table '{{producer}}':
     *
     * @property integer $id
     * @property string  $name
     *
     * The followings are the available model relations:
     * @property Goods[] $goods
     */
    class Producer extends CActiveRecord {
        public static function getList($listExplode = array()) {
            $criteria = new CDbCriteria();
            $criteria->select = 'id, name';
            if (sizeof($listExplode))
                $criteria->condition = 'id NOT IN (' . implode(', ', $listExplode) . ')';

            $criteria->order = "id ASC";

            $producer = array();
            $prod = self::model()->findAll($criteria);

            foreach ($prod as $_producer) {
                $producer[ $_producer[ 'id' ] ] = $_producer[ 'name' ];
            }
            if (sizeof($producer) == 0)
                $producer[ -9 ] = " ";
            $producer[ -10 ] = "Добавить нового";

            return $producer;
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Producer the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{producer}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'name',
                    'required'
                ),
                array(
                    'name',
                    'unique'
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, name',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'goods' => array(
                    self::BELONGS_TO,
                    'Goods',
                    'goods_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'   => 'ID',
                'name' => 'Проивзодитель',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('name', $this->name, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria'   => $criteria,
                    'pagination' => array(
                        'pageSize' => 30
                    )
                )
            );
        }

        public function getAllProducer () {
            $prod = $this->findAll();
            $prodRet = array();
            foreach ($prod as $key => $value) {
                $prodRet[$value->name] = $value->name;
            }
            return $prodRet;
        }
    }
