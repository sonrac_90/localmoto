<?php

    /**
     * This is the model class for table "{{address}}".
     *
     * The followings are the available columns in table '{{address}}':
     *
     * @property integer $id
     * @property integer $user_id
     * @property string  $country
     * @property string  $city
     * @property string  $street
     * @property string  $house
     * @property string  $department
     * @property string  $date_add
     *
     * The followings are the available model relations:
     * @property Users   $user
     */
    class Address extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Address the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{address}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'country, city, street, house, department',
                    'required'
                ),
                array(
                    'user_id',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'country, city, street, house, department',
                    'length',
                    'max' => 255
                ),
                array(
                    'date_add',
                    'safe'
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, user_id, country, city, street, department, date_add',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'user' => array(
                    self::BELONGS_TO,
                    'Users',
                    'user_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'         => 'ID',
                'user_id'    => 'Пользователь',
                'country'    => 'Страна',
                'city'       => 'Город',
                'street'     => 'Улица',
                'house'      => 'Дом',
                'department' => 'Квартира',
                'date_add'   => 'Дата добавления',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('user_id', $this->user_id);
            $criteria->compare('country', $this->country, true);
            $criteria->compare('city', $this->city, true);
            $criteria->compare('street', $this->street, true);
            $criteria->compare('house', $this->house, true);
            $criteria->compare('department', $this->department, true);
            $criteria->compare('date_add', $this->date_add, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        public function getAllUserAddress ($userID) {
            $criteria = new CDbCriteria();

//            $criteria->order = "position ASC";
            $criteria->addCondition("user_id = '$userID'");

            return $this->findAll($criteria);
        }
    }
