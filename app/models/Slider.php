<?php

/**
 * This is the model class for table "{{slider}}".
 *
 * The followings are the available columns in table '{{slider}}':
 * @property integer $id
 * @property string $ext
 * @property integer $position
 */
class Slider extends CActiveRecord
{
    const PathImage = 'shared/uploads/sliders';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{slider}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ext, position', 'required'),
			array('position', 'numerical', 'integerOnly'=>true),
			array('ext', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ext, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ext' => 'Расширение',
			'position' => 'Позиция',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->order = "position ASC";
		$criteria->compare('id',$this->id);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Slider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @param Slider $model
     * @return string
     */
    public static function getFullPath($model, $addition='-t.') {
        return AddExtend::baseUrl(true) . self::PathImage . '/' . CropImage::dynamicPath($model->id) . '/' . $model->id . $addition . $model->ext;
    }

    public function getMaxPosition () {
        return Yii::app()->db->createCommand("SELECT MAX(position) as POSITION FROM " . $this->tableName())->queryScalar();
    }

    public function beforeValidate () {
        if ($this->isNewRecord) {
            $position = $this->getMaxPosition();
            $position = $position ? $position : 1;
            $this->position = $position + 1;
        }

        return parent::beforeValidate();
    }
}
