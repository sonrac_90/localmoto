<?php

    /**
     * This is the model class for table "{{order}}".
     *
     * The followings are the available columns in table '{{order}}':
     *
     * @property integer          $id
     * @property integer          $user_id
     * @property string           $user_email
     * @property string           $user_firstname
     * @property string           $user_lastname
     * @property string           $user_middlename
     * @property string           $user_birthday
     * @property integer          $user_phone
     * @property integer          $photo_id
     * @property string           $photo_url
     * @property integer          $size_id
     * @property string           $size
     * @property integer          $color_id
     * @property string           $color
     * @property integer          $description_id
     * @property string           $description
     * @property integer          $address_id
     * @property string           $address
     * @property integer          $good_id
     * @property string           $good_model
     * @property string           $good_price
     * @property string           $good_name
     * @property integer          $good_producer_id
     * @property string           $good_producer
     * @property integer          $total_count
     * @property integer          $total_price
     * @property string           $date_order
     * @property string           $status
     * @property string           $failure_cause
     * @property integer          $order_id
     * @property string           $type_order
     *
     * The followings are the available model relations:
     * @property Address          $address0
     * @property ColorGoods       $color0
     * @property DescriptionGoods $description0
     * @property OrderMain        $order
     * @property Photo            $photo
     * @property GoodsSize        $size0
     * @property Users            $user
     */
    class Order extends CActiveRecord {
        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{order}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'user_email, user_firstname, user_lastname, user_birthday, user_phone, photo_url, good_model, good_price, good_name, good_producer, total_count, date_order, order_id',
                    'required'
                ),
                array(
                    'user_id, photo_id, size_id, color_id, description_id, address_id, good_id, good_producer_id, total_count, total_price, order_id',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'user_email, user_phone, user_firstname, user_lastname, user_middlename, photo_url, size, color, address, good_model, good_name, good_producer, failure_cause',
                    'length',
                    'max' => 255
                ),
                array(
                    'description',
                    'length',
                    'max' => 11
                ),
                array(
                    'good_price',
                    'length',
                    'max' => 10
                ),
                array(
                    'status',
                    'length',
                    'max' => 7
                ),
                array(
                    'type_order',
                    'length',
                    'max' => 8
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, user_id, user_email, user_firstname, user_lastname, user_middlename, user_birthday, user_phone, photo_id, photo_url, size_id, size, color_id, color, description_id, description, address_id, address, good_id, good_model, good_price, good_name, good_producer_id, good_producer, total_count, total_price, date_order, status, failure_cause, order_id, type_order',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'address0'     => array(
                    self::BELONGS_TO,
                    'Address',
                    'address_id'
                ),
                'color0'       => array(
                    self::BELONGS_TO,
                    'ColorGoods',
                    'color_id'
                ),
                'description0' => array(
                    self::BELONGS_TO,
                    'DescriptionGoods',
                    'description_id'
                ),
                'order'        => array(
                    self::BELONGS_TO,
                    'OrderMain',
                    'order_id'
                ),
                'photo'        => array(
                    self::BELONGS_TO,
                    'Photo',
                    'photo_id'
                ),
                'size0'        => array(
                    self::BELONGS_TO,
                    'GoodsSize',
                    'size_id'
                ),
                'user'         => array(
                    self::BELONGS_TO,
                    'Users',
                    'user_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'               => 'ID',
                'user_id'          => 'Пользователь',
                'user_email'       => 'Email',
                'user_firstname'   => 'Имя',
                'user_lastname'    => 'Фамилия',
                'user_middlename'  => 'Отчество',
                'user_birthday'    => 'Дата рождения',
                'user_phone'       => 'Телефон',
                'photo_id'         => 'Photo',
                'photo_url'        => 'Фотография',
                'size_id'          => 'Size',
                'size'             => 'Размер',
                'color_id'         => 'Color',
                'color'            => 'Цвет',
                'description_id'   => 'Description',
                'description'      => 'Описание товара',
                'address_id'       => 'Address',
                'address'          => 'Адрес доставки',
                'good_id'          => 'Good',
                'good_model'       => 'Модель',
                'good_price'       => 'Цена',
                'good_name'        => 'Название',
                'good_producer_id' => 'Good Producer',
                'good_producer'    => 'Производитель',
                'total_count'      => 'Количество',
                'total_price'      => 'Общая стоимость',
                'date_order'       => 'Дата заказа',
                'status'           => 'Статус заказа',
                'failure_cause'    => 'Причина отказа',
                'order_id'         => 'Order',
                'type_order'       => 'Тип заказа',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('user_id', $this->user_id);
            $criteria->compare('user_email', $this->user_email, true);
            $criteria->compare('user_firstname', $this->user_firstname, true);
            $criteria->compare('user_lastname', $this->user_lastname, true);
            $criteria->compare('user_middlename', $this->user_middlename, true);
            $criteria->compare('user_birthday', $this->user_birthday, true);
            $criteria->compare('user_phone', $this->user_phone);
            $criteria->compare('photo_id', $this->photo_id);
            $criteria->compare('photo_url', $this->photo_url, true);
            $criteria->compare('size_id', $this->size_id);
            $criteria->compare('size', $this->size, true);
            $criteria->compare('color_id', $this->color_id);
            $criteria->compare('color', $this->color, true);
            $criteria->compare('description_id', $this->description_id);
            $criteria->compare('description', $this->description, true);
            $criteria->compare('address_id', $this->address_id);
            $criteria->compare('address', $this->address, true);
            $criteria->compare('good_id', $this->good_id);
            $criteria->compare('good_model', $this->good_model, true);
            $criteria->compare('good_price', $this->good_price, true);
            $criteria->compare('good_name', $this->good_name, true);
            $criteria->compare('good_producer_id', $this->good_producer_id);
            $criteria->compare('good_producer', $this->good_producer, true);
            $criteria->compare('total_count', $this->total_count);
            $criteria->compare('total_price', $this->total_price);
            $criteria->compare('date_order', $this->date_order, true);
            $criteria->compare('status', $this->status, true);
            $criteria->compare('failure_cause', $this->failure_cause, true);
            $criteria->compare('order_id', $this->order_id);
            $criteria->compare('type_order', $this->type_order, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Order the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }