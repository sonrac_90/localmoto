<?php

    /**
     * This is the model class for table "{{description_goods}}".
     *
     * The followings are the available columns in table '{{description_goods}}':
     *
     * @property integer $id
     * @property string  $description
     * @property integer $photo_id
     *
     * The followings are the available model relations:
     * @property Photo   $photo
     */
    class DescriptionGoods extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return DescriptionGoods the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{description_goods}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'description, photo_id',
                    'required'
                ),
                array(
                    'photo_id',
                    'numerical',
                    'integerOnly' => true
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, description, photo_id',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'photo' => array(
                    self::BELONGS_TO,
                    'Photo',
                    'photo_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'          => 'ID',
                'description' => 'Описание',
                'photo_id'    => 'ID фото',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('description', $this->description, true);
            $criteria->compare('photo_id', $this->photo_id);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        public function getAllDescriptionPhotos($photos) {
            if (!is_array($photos))
                return array();

            $descriptions = array();

            foreach ($photos as $numPhoto => $photo) {
                $criteria = new CDbCriteria();
                $criteria->addCondition("photo_id={$photo->id}");
                $descriptions[ $numPhoto ] = $this->find($criteria);
            }

            return $descriptions;

        }
    }
