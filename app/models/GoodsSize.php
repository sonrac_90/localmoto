<?php

    /**
     * This is the model class for table "{{goods_size}}".
     *
     * The followings are the available columns in table '{{goods_size}}':
     *
     * @property integer $id
     * @property string  $size
     * @property integer $photo_id
     *
     * The followings are the available model relations:
     * @property Photo   $photo
     */
    class GoodsSize extends CActiveRecord {
        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return GoodsSize the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{goods_size}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'size',
                    'required'
                ),
                array(
                    'photo_id',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'size',
                    'length',
                    'max' => 25
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, size, photo_id',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'photo' => array(
                    self::BELONGS_TO,
                    'Photo',
                    'photo_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'       => 'ID',
                'size'     => 'Размер',
                'photo_id' => 'ID фото',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('size', $this->size, true);
            $criteria->compare('photo_id', $this->photo_id);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * @param Photo[] $photos
         *
         * @return GoodsSize[]
         */
        public function getAllSizesPhotos($photos) {
            if (!is_array($photos))
                return array();

            $sizes = array();

            foreach ($photos as $numPhoto => $photo) {
                $criteria = new CDbCriteria();
                $criteria->addCondition('photo_id=' . $photo->id);
                $sizes[ $numPhoto ] = $this->findAll($criteria);
            }

            return $sizes;
        }
    }
