<?php

    /**
     * This is the model class for table "{{photo}}".
     *
     * The followings are the available columns in table '{{photo}}':
     *
     * @property integer            $id
     * @property string             $url
     * @property integer            $goods_id
     *
     * The followings are the available model relations:
     * @property ColorGoods[]       $colorGoods
     * @property DescriptionGoods[] $descriptionGoods
     * @property GoodsSize[]        $goodsSizes
     * @property Goods              $goods
     *
     * @const string PATH_IMAGE
     */
    class Photo extends CActiveRecord {
        const PATH_IMAGE = "shared/uploads/goods/";

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Photo the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{photo}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'url, goods_id',
                    'required'
                ),
                array(
                    'goods_id',
                    'numerical',
                    'integerOnly' => true
                ),
                array(
                    'url',
                    'length',
                    'max' => 255
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, url, goods_id',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'colorGoods'       => array(
                    self::HAS_MANY,
                    'ColorGoods',
                    'id_photo'
                ),
                'descriptionGoods' => array(
                    self::HAS_MANY,
                    'DescriptionGoods',
                    'photo_id'
                ),
                'goodsSizes'       => array(
                    self::HAS_MANY,
                    'GoodsSize',
                    'photo_id'
                ),
                'goods'            => array(
                    self::BELONGS_TO,
                    'Goods',
                    'goods_id'
                ),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'       => 'ID',
                'url'      => 'Изображение',
                'goods_id' => 'Товар',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('url', $this->url, true);
            $criteria->compare('goods_id', $this->goods_id);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        public function getAllPhotoGood($id) {
            $criteria = new CDbCriteria();
            $criteria->addCondition('goods_id=' . $id);

            return $this->findAll($criteria);
        }

        public function getUrlImage($url) {
            $fullPath = AddExtend::basePath() . "/../" . Photo::PATH_IMAGE . $url;
            if (is_file($fullPath)) {
                return AddExtend::baseUrl() . $this::PATH_IMAGE . $url;
            }

            return AddExtend::pathMedia() . "css/admin/images/admin/no-img.png";
        }
    }
