<?php

    /**
     * This is the model class for table "{{visitors}}".
     *
     * The followings are the available columns in table '{{visitors}}':
     *
     * @property integer $id
     * @property string  $ip
     * @property string  $date
     * @property string  $country
     * @property string  $city
     */
    class Visitors extends CActiveRecord {
        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{visitors}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'ip, country, city',
                    'length',
                    'max' => 255
                ),
                array(
                    'date',
                    'safe'
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, ip, date, country, city',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array();
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'      => 'ID',
                'ip'      => 'IP-адрес',
                'date'    => 'Дата визита',
                'country' => 'Страна',
                'city'    => 'Город',
            );
        }

        public function beforeSave() {
            $this->ip = $_SERVER[ 'REMOTE_ADDR' ];
            $geoBase = new SxGeo();
            $country = $geoBase->getFullCountry($this->ip);
            if ($country) {
                $this->country = $country[ 'country' ];
                $this->city = $country[ 'alpha2' ];
                $date = new DateTime();
                $this->date = $date->format("d-m-y H:i:s");

                $date->modify('-1 day');
                $criteria = new CDbCriteria();
                $criteria->select = '*';
                $criteria->addCondition("ip = '{$this->ip}' AND `date` >= '" . $date->format("d-m-Y H:i:s") . "'");
                $identical = $this->findAll($criteria);
                if (!count($identical)) {
                    return true;
                }
            }

            return false;

        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('ip', $this->ip, true);
            $criteria->compare('date', $this->date, true);
            $criteria->compare('country', $this->country, true);
            $criteria->compare('city', $this->city, true);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Visitors the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }
