<?php

    /**
     * This is the model class for table "{{category}}".
     *
     * The followings are the available columns in table '{{category}}':
     *
     * @property integer $id
     * @property string  $name
     * @property integer $parent
     * @property string  $date_create
     * @property integer $parent_position
     * @property integer $position
     */
    class Category extends CActiveRecord {

        public $catsID    = null;
        public $childNode = array();

        public $child = array();

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Category the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array(
                    'name, date_create',
                    'required'
                ),
                array(
                    'parent, parent_position, position',
                    'numerical',
                    'integerOnly' => true
                ),
                //            array('name', 'length', 'max'=>255),
                array(
                    'name',
                    'unique'
                ),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array(
                    'id, name, parent, date_create, parent_position, position',
                    'safe',
                    'on' => 'search'
                ),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array();
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'              => 'ID',
                'name'            => 'Name',
                'parent'          => 'Родитель',
                'date_create'     => 'Дата создания',
                'parent_position' => 'Позиция в подкатегории',
                'position'        => 'Позиция',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id);
            $criteria->compare('name', $this->name, true);
            $criteria->compare('parent', $this->parent);
            $criteria->compare('date_create', $this->date_create, true);
            $criteria->compare('parent_position', $this->parent_position);
            $criteria->compare('position', $this->position);

            return new CActiveDataProvider(
                $this, array(
                    'criteria' => $criteria,
                )
            );
        }

        /**
         * Get max position category
         *
         * @return mixed
         */
        public function getMaxPosition() {
            return Yii::app()->db->createCommand("SELECT max(position) FROM " . $this->tableName() . " as position")->queryScalar();
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return '{{category}}';
        }

        /**
         * @param null|number $id
         * @param array|bool  $listExplode
         * @param bool        $getChild
         * @param integer     $goodID
         *
         * @return CActiveRecord[]
         */
        public function getCategoryById($id = null, $listExplode = false, $getChild = false, $goodID = null) {
            $criteria = new CDbCriteria();
            if (!is_null($id) && !empty( $id )) {
                $criteria->join = "INNER JOIN " . GoodsCategory::model()->tableName() . " cat ON cat.category_id=t.id";
                $criteria->addCondition("cat.goods_id=$id");
                $criteria->addCondition("t.parent IS NULL");
            } else {
                $criteria->addCondition("t.parent is NULL");
            }

            if (is_array($listExplode) && sizeof($listExplode)) {
                if (is_null($criteria)) {
                    $criteria = new CDbCriteria();
                }
                $criteria->addCondition("t.id NOT IN (" . implode(',', $listExplode) . ")");
            }

            if (!is_null($id)) {
                $criteria->select = 't.id, cat.id as catsID, t.name, t.parent, t.date_create, t.parent_position, t.position';
                $ret = $this->findAll($criteria);

                foreach ($ret as $num => $cat) {
                    if (!isset ( $ret[ $num ][ 'childNode' ] ))
                        $ret[ $num ][ 'childNode' ] = array();
                    $ret[ $num ][ 'childNode' ] = $this->getChild($cat->id, $id, array(), $getChild);
                }
            } else {
                if (is_array($listExplode))
                    $cat = $this->findAll($criteria); else
                    $cat = array( $this->find($criteria) );
                foreach ($cat as $num => $_cat) {
                    if (!isset ( $cat[ $num ][ 'childNode' ] ))
                        $cat[ $num ][ 'childNode' ] = array();
                    $cat[ $num ][ 'childNode' ] = $this->getChild($_cat->id, $id, array(), $getChild);
                }
                $ret = array(
                    $cat
                );
            }

            return $ret;
        }

        /**
         * @param null $id
         * @param null $goodID
         * @param null $implode
         * @param bool $getChild
         * @param null $idParent
         *
         * @return CActiveRecord[]
         */
        public function getChild($id = null, $goodID = null, $implode = null, $getChild = false, $idParent = null) {
            if (intval($id) <= 0)
                return array();

            $criteria = new CDbCriteria();
            $criteria->select = 't.id, t.name, t.parent, t.date_create, t.parent_position, t.position';
            $criteria->addCondition("t.parent=$id");

            if (!is_null($implode) && intval($implode) > 0 && !is_array($implode))
                $criteria->addCondition("t.id!={$implode}"); else if (is_array($implode) && sizeof($implode))
                $criteria->addCondition("t.id NOT IN (" . implode(',', $implode) . ")");

            if (is_null($goodID)) {
                if (is_array($implode)) {
                    $cat = $this->findAll($criteria);
                }else
                    $cat = array( $this->find($criteria) );

                if ($getChild) {
                    foreach ($cat as $key => $value) {
                        $cat[ $key ][ 'childNode' ] = $this->getChild($value->id, $goodID, $implode, $getChild);
                    }
                }

                return $cat;
            } else {
                if (!is_null($goodID)) {
                    $criteria->select .= ', cat.id as catsID';
                    $criteria->join = "INNER JOIN " . GoodsCategory::model()->tableName() . " cat ON $id = t.parent";
                    $criteria->addCondition("cat.goods_id=$goodID");
                    $criteria->addCondition("cat.category_id=t.id");
                }

                $child = $this->findAll($criteria);

                if (( isset ( $child[ 0 ] ) && isset ( $child[ 0 ]->id ) ))
                    $criteria->addCondition("t.id IN (" . AddExtend::implodeOBJ($child, 'id') . ")");
                if (is_object($child) && isset ( $child->id ))
                    $criteria->addCondition("t.id={$child->id}");
            }

            $all = $this->findAll($criteria);

            if ($getChild) {
                foreach ($all as $key => $value) {
                    $all[ $key ][ 'childNode' ] = $this->getChild($value->id, $goodID, $implode, $getChild);
                }
            }

            return $all;
        }

        public function getParentCategory($id, $listExplode = false) {

            $criteria = new CDbCriteria();
            if (is_array($listExplode) && sizeof($listExplode))
                $criteria->addCondition("t.id NOT IN (" . implode(',', $listExplode) . ")");

            if (intval($id) > 0)
                $criteria->addCondition("parent=$id"); else
                $criteria->addCondition("parent IS NULL");

            return $this->findAll($criteria);
        }

        public function getAllCategory($id = null, $implode = null) {
            $id = intval($id);

            $criteria = new CDbCriteria;
            $criteria->select = "name, t.id, position, parent";
            $criteria->order = 'position, parent, parent_position ASC';

            if (is_array($implode) && sizeof($implode)) {
                $criteria->addCondition("id NOT IN (" . implode(',', $implode) . ")");
            }

            if (intval($id) > 0) {
                $criteria->addCondition("parent=$id");
            }

            $criteria->addCondition('parent IS NULL ');

            $cat = $this->findAll($criteria);

            $catNew = array();

            $cnt = 0;
            foreach ($cat as $_cat) {
                $catNew[$cnt] = $_cat;
                $catNew[$cnt]['childNode'] = $this->getChild($_cat->id, null, array(), true);
                $cnt++;
            }

            return $catNew;
        }
    }
