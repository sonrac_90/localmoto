//alert('asd');
app_start(jQuery);
var menu_timeout = 0;
var gl_window;

$(document).ready(function () {
    $('.megaslider').megaslider({
                                    'slider_content_class'        : 'mScrollBlock',
                                    'slider_content_active_class' : 'mScrollBlockActive',

                                    slider_arrow_left_class  : 'mNavLeft',
                                    slider_arrow_right_class : 'mNavRight',

                                    'pause_time'      : 5000,
                                    'transition_time' : 700,
                                    'autostart'       : false,
                                    'transition_type' : 'margin'
                                });

    $('.megaslider2').megaslider({
                                     'slider_content_class'        : 'mScrollBlock',
                                     'slider_content_active_class' : 'mScrollBlockActive',

                                     slider_arrow_left_class  : 'mNavLeft',
                                     slider_arrow_right_class : 'mNavRight',

                                     'pause_time'      : 5000,
                                     'transition_time' : 700,
                                     'autostart'       : false,
                                     'transition_type' : 'margin'
                                 });
});

function app_start($) {

    $(document).ready(function () {
        var selSpan = $('.motorcycle span');
        selSpan.on('click', function (event) {
            event.preventDefault();
            selSpan.css('text-decoration', 'none');
            $(this).css('text-decoration', 'underline');
            $('nav[id^=main_menu]').hide();
            $('#main_menu' + $(this).attr('data')).show();
            return false;
        })
    });

    gl_window = $(window);
    show_hide_second_level_menu();
    viewport_detect();
    k_animate_header_wishlist();
    mobile_menu_();

    //resize_header_onscroll();

    function k_animate_header_wishlist() {

        var ww = $(window).width();

        if (ww > 990) {
            $('.search_input').focus(function () {
                $('.basket_h').animate({'top' : '-28px'}, 200);
                $(this).css('width', '300px');
            });

            $('.search_input').blur(function () {
                $('.basket_h').animate({
                                           'top' : '0'}, 200);
                $(this).css('width', '85px');
            });

            $('.catalog_item').hover(
                function () {
                    $(this).find('.addtocart-button-catalog').css('display', 'block');
                },
                function () {
                    $(this).find('.addtocart-button-catalog').css('display', 'none');
                }
            );
        }

        $('.catalog_item').hover(
            function () {
                $(this).addClass('catalog_item_hover');
            },
            function () {
                $(this).removeClass('catalog_item_hover');
            }
        );
    }

    function viewport_detect() {
        var min_width = 400;
        var sw = screen.width;
        var sh = screen.height;
        var dev_width = Math.min(sw, sh);
        var dev_height = Math.max(sw, sh);
        var vp_content = '';
        var iscale;

        if (dev_width < min_width) {
            iscale = Math.round(dev_width / min_width * 10) / 10;
            vp_content = 'width=' + dev_width + ', initial-scale=' + iscale;
        } else {
            vp_content = 'width=device-width, initial-scale=1.0';
        }

        $("head").append('<meta name=\"viewport\" content=\"' + vp_content + '\" />');
    }

    function mobile_menu_() {
        $(".item122").click(function () {
            $(this).toggleClass("active_mobile_menu");
            $(".item122 #sub_item").toggleClass("sub_item_active");
            return false;
        });
        $(".item123").click(function () {
            $(this).toggleClass("active_mobile_menu");
            $(".item123 #sub_item").toggleClass("sub_item_active");
            return false;
        });
        $(".item124").click(function () {
            $(this).toggleClass("active_mobile_menu");
            $(".item124 #sub_item").toggleClass("sub_item_active");
            return false;
        });
        $(".item125").click(function () {
            $(this).toggleClass("active_mobile_menu");
            $(".item125 #sub_item").toggleClass("sub_item_active");
            return false;
        });
        $(".item126").click(function () {
            $(this).toggleClass("active_mobile_menu");
            $(".item126 #sub_item").toggleClass("sub_item_active");
            return false;
        });
    }


    function show_hide_second_level_menu() {

        $('#nav > li').hover(
            function () {
                var li_list = $('#nav > li > ul').css('display', 'none');

                var qwe = $(this);

                clearTimeout(menu_timeout);
                qwe.find('ul').css('display', 'block');
                setTimeout(function () {
                    qwe.find('ul').css('opacity', 1)
                }, 10);
            },
            function () {
                var qwe = $(this);
                qwe.find('ul').css('opacity', 0);
                clearTimeout(menu_timeout);
                menu_timeout = setTimeout(function () {
                    qwe.find('ul').css('display', 'none');
                }, 250);
            }
        );

    }

    function resize_header_onscroll() {
        gl_window.resize(function () {
            var WW = gl_window.width();
            if (WW >= 990) {
                var cur_scroll = gl_window.scrollTop();


                if ((cur_scroll < 190) && (cur_scroll > 0)) {
                    $('header').css({
                                        'top' : -cur_scroll
                                    });
                    $('header .address_h').css({
                                                   'opacity' : Math.min(1 - (cur_scroll / (60 - cur_scroll)), 0)
                                               })

                    $('header .search').css({
                                                'top' : 20 + cur_scroll
                                            })

                    $('header .logo').css({
                                              'width'  : 436 / (1 + cur_scroll / 390),
                                              'height' : 175 / (1 + cur_scroll / 390),
                                              'bottom' : 130 / (1 + cur_scroll / 390)
                                          })
                }

                if (cur_scroll >= 190) {
                    $('header').css({
                                        'top' : -190
                                    })
                    $('header .address_h').css({
                                                   'opacity' : 0
                                               })

                    $('header .search').css({
                                                'top' : 20 + 190
                                            })


                    $('header .logo').css({
                                              'width'  : 140,
                                              'height' : 88,
                                              'bottom' : 78
                                          })

                }

                if (cur_scroll == 0) {
                    $('header').css({
                                        'top' : 0
                                    })
                    $('header .address_h').css({
                                                   'opacity' : 1
                                               })


                    $('header .search').css({
                                                'top' : 20
                                            })
                    $('header .logo').css({
                                              'width'  : 436,
                                              'height' : 175,
                                              'bottom' : 130
                                          })
                }
            } else {
                $('header').removeAttr('style');
                $('header .address_h').removeAttr('style');
                $('header .search').removeAttr('style');
                $('header .logo').removeAttr('style');
            }
        })
        gl_window.scroll(function () {
            var WW = gl_window.width();

            if (WW >= 990) {
                var cur_scroll = gl_window.scrollTop();


                if ((cur_scroll < 190) && (cur_scroll > 0)) {
                    $('header').css({
                                        'top'    : -cur_scroll,
                                        'height' : '318px'
                                    })
                    $('header .address_h').css({
                                                   'top' : cur_scroll
                                               })

                    $('header .search').css({
                                                'top' : 20 + cur_scroll
                                            })

                    $('header .logo').css({
                                              'width'  : 436 / (1 + cur_scroll / 390),
                                              'height' : 175 / (1 + cur_scroll / 590),
                                              'bottom' : 25
                                          })

                    $('.second_main_menu').css({
                                                   'opacity' : Math.min(1 - (cur_scroll / (60 - cur_scroll)), 0)
                                               })

                    $('.breadcrumbs').css({
                                              'opacity' : Math.min(1 - (cur_scroll / (60 - cur_scroll)), 0)
                                          })
                }

                if (cur_scroll >= 190) {
                    $('header').css({
                                        'top'    : -190,
                                        'height' : '318px'
                                    })
                    $('header .address_h').css({
                                                   'top' : 190
                                               })

                    $('header .search').css({
                                                'top' : 20 + 190
                                            })


                    $('header .logo').css({
                                              'width'  : 140,
                                              'height' : 88,
                                              'bottom' : 25
                                          })

                    $('.second_main_menu').css({
                                                   'opacity' : 0
                                               })

                    $('.breadcrumbs').css({
                                              'opacity' : 0
                                          })

                }

                if (cur_scroll == 0) {
                    $('header').css({
                                        'top'    : 0,
                                        'height' : '370px'
                                    })
                    $('header .address_h').css({
                                                   'top' : 0
                                               })


                    $('header .search').css({
                                                'top' : 20
                                            })
                    $('header .logo').css({
                                              'width'  : 436,
                                              'height' : 175,
                                              'bottom' : 130
                                          })
                    $('.second_main_menu').css({
                                                   'opacity' : 1
                                               })

                    $('.breadcrumbs').css({
                                              'opacity' : 1
                                          })
                }
            }

        })
    }
};

