$(document).ready(function () {
    buy.init();
});


var buy = {

    selector      : '#buyForm',
    popupSelector : {
        popup   : '#facebox',
        overlay : '#facebox_overlay'
    },

    init : function () {
        $(buy.selector).on('submit', function (event) {
            event.preventDefault();

            var dataGood = buy.getParamBuyFirst(this);

            buy.registerNewSesionData(buy.getParamBuyFirst(this), function () {
            });
            $('input.addtocart-button').attr('disabled', 'disabled');
            return false;
        });
//        buy.plusOne();
//        buy.confirmOrder();
    },

    plusOne : function () {
        $('a.pm_but').off('click').on('click', function (event) {
            var parentCont = $('div[class^=prow]').has(this).eq(0);
            var countProd = parentCont.find('.prod_kol_2').eq(0);
            var oldVal = intval($.trim(countProd.html()));

            if ($.trim($(this).html()) == '+') {
                oldVal++;
            } else {
                oldVal = ((oldVal == 1) ? 1 : (oldVal - 1));
            }

            var price = parentCont.attr('data-price');
            var sumPrice = parentCont.find('.sum_value').eq(0);
            var total = price * oldVal;

            sumPrice.html(total + ' Р');

            total = 0;
            $('.prod_price .sum_value').each(function () {
                total += intval($(this).html());
            });

            countProd.html(oldVal);

            $('div.prod_sum_block .sum_value').html(total + ' Р');
        });
    },

    confirmOrder : function () {
        $('#confirm_order, .next_step').off('click').on('click', function (event) {
            event.preventDefault();

            if ($('div.op_main div[class^=prow]').size()) {
                var current = 1;
                if (intval($(this).html()) > 0)
                    current = intval($(this).html()) - 1;

                var id = [];
                var count = [];
                $('div[class^=prow]').each(function () {
                    count.push(intval($('.prod_kol_2', this).eq(0).html()));
                    id.push(intval($(this).attr('data-key')));
                });

                var obj = buy['showStep_' + current](count, id);
                buy.showNextStep(obj.hide, obj.show, current, obj.opacityNum);
            }

            return false;
        });
    },

    showStep_1 : function (count, id) {
        var selector = {
            hide: Array( 'div.op_main', '.prod_sum' ),
            show: Array( 'div.edit_bill_to', 'div.edit_bill_to div.op_main'),
            opacityNum : 0
        };

        for (var i = 0; i < id.length; i++){

            $.ajax ({
                        url : baseUrl + 'buy/UpdateCountGood/id/' + id[i] + '/count/' + count[i],
                        success : function (data) {

                        }
                    })
        }

        return selector;
    },

    showStep_2 : function () {
        var selector = {
            hide: Array( 'div.edit_bill_to', 'div.edit_bill_to div.op_main'),
            show: Array( '' ),
            opacityNum : 0
        };

        var data = {
            user_firstname : $.trim($('#pinfo_name .op_title').eq(0).html()),
            user_lastname  : $.trim($('#pinfo_surname .op_title').eq(0).html()),
            user_email     : $.trim($('#pinfo_email .op_title').eq(0).html()),
            user_birthday  : $.trim($('#pinfo_birthdate .op_title').eq(0).html()),
            user_phone     : $.trim($('#pinfo_phone .op_title').eq(0).html())
        };

        for (var i = 0; i < id.length; i++){

            $.ajax ({
                        url : baseUrl + 'buy/UpdateCountGood/id/' + id[i] + '/count/' + count[i],
                        success : function (data) {

                        }
                    })
        }

        return selector;
    },

    showStep3 : function () {

    },

    showNextStep : function (selectorHide, selectorShow, number, numAnimate) {
        for (var i = 0; i < selectorHide.length; i++){
            $(selectorHide[i]).hide();
        }

        if (typeof selectorShow === "object") {
            for (var i = 0; i < selectorShow.length; i++) {
                $(selectorShow[i]).show();
            }
        }
        $('div.step_' + number).each(function () {
            if ($(this).hasClass('op_title_block'))
                $(this).attr('class', '').addClass('op_title_block step_' + intval(number + 1));
        });

        if (typeof selectorShow === 'object') {
            $(selectorShow[numAnimate]).animate({opacity : 1}, "slow");
        }


    },

    getParamBuyFirst : function (form) {
        var sizeBlock = $('#sizes_' + currentPhoto);
        var colorBlock = $('#color_' + currentPhoto);
        var descriptionBlock = $('#description_' + currentPhoto);

        var sel = buy.popupSelector.popup;
        var goodName = $.trim($('.product_model h2').text());
        $(sel).find('h4').eq(0).html(goodName + ' добавлен в корзину');

        var a = $('.product_title h1 a');
        var idProducer = basename(a.attr('href'));
        var photoUrl = basename($('#photoUrl' + currentPhoto + ' img').eq(0).attr('src'));

        buy.popupShow();

        return {
            good_id          : '"' + intval($('.productdetails-view').eq(0).attr('data')) + '"',
            size_id          : '"' + intval(sizeBlock.find('select').eq(0).val()) + '"',
            color_id         : '"' + intval(colorBlock.find('select').eq(0).val()) + '"',
            description_id   : '"' + intval(descriptionBlock.attr('data')) + '"',
            photo_url        : photoUrl,
            color            : $.trim(colorBlock.find('.color_selected').eq(0).text()),
            description      : $.trim(descriptionBlock.text()),
            good_size        : $.trim(sizeBlock.find('.size_selected').eq(0).text()),
            good_model       : $.trim($('.model_wrap .model_title').eq(0).text()),
            good_price       : $.trim($('div.price .price_number').eq(0).text()),
            good_name        : goodName,
            good_producer    : $.trim(a.text()),
            good_producer_id : idProducer,
            count            : '1'
        };
    },

    registerNewSesionData : function (dataOBJ, callback) {

        $.ajax({
                   url     : baseUrl + 'buy/registerData',
                   type    : 'POST',
                   data    : 'data=' + JSON.stringify(dataOBJ),
                   success : function (data) {
                       callback(data, dataOBJ);
                   }
               });

    },

    popupClose : function () {
        $(buy.popupSelector.popup).hide();
        $(buy.popupSelector.overlay).hide();
        return false;
    },

    popupShow : function () {
        $(buy.popupSelector.popup).show();
        $(buy.popupSelector.overlay).show();
        return false;
    }


};