var mask_inited = false;
//var users_last_name=jQuery('#Users_last_name');


function phone_mask() {
    setTimeout(function () {
        jQuery("input[name='phone']").mask("+7(999)999-9999", {'placeholder' : '*'});
    }, 100);
}

function email_mask() {


    jQuery("input[name='email']").on("keyup", function () {

        if (!validateEmail(jQuery(this).val())) {
            jQuery(this).css({
                                 'color' : 'red'
                             })
        } else {
            jQuery(this).css({
                                 'color' : ''
                             })
        }

    })

}
function initInputMask() {
    phone_mask();
    email_mask();
}

jQuery(document).ready(function () {
    initInputMask();
});

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($email)) {
        return false;
    } else {
        return true;
    }
}