var currentPhoto = 0;
var slidersInfo = {
    size  : {},
    color : {}
};
$(document).ready(function () {

    var s_items_cont_h = [];
    var size_is_open = false;

    var c_items_cont_h = [];
    var color_is_open = false;

    function init_selects() {
        if (!$('.size_wrap').hasClass('empty')) init_size_select();
        if (!$('.specs_color_wrap').hasClass('empty')) init_color_select();
    }

    function add_events() {
        if (!$('.size_wrap').hasClass('empty')) add_size_select_events();
        if (!$('.specs_color_wrap').hasClass('empty')) add_color_select_events();
        if (!$('.specs_details').hasClass('empty')) add_details_info_events();


        $('.addtocart-button').click(function () {
            var head_basket = $('.basket_h .cart_link');

            if (head_basket.hasClass('basket_empty')) {
                head_basket.attr('href', baseUrl + 'buy/cart');
                head_basket.removeClass('basket_empty');
            }

            if (head_basket.attr('data-add') != 'true') {
                var cur_q = parseInt(head_basket.children('.number').html());
                $('.cp_button').addClass('borderNew');
                cur_q++;
                head_basket.children('.number').html(cur_q);

                head_basket.attr('data-add', 'true');
            }
        })
    }

    function init_size_select() {

        var countPhoto = $('[id^=sizes_]').size();

        for (var j = 0; j < countPhoto; j++) {
            var sizesBlockID = "#sizes_" + j;

            var selectSizeBOX = $(sizesBlockID + ' select.size_select_box');
            selectSizeBOX.css({'display' : 'none'});
            var cp_size_item_count = selectSizeBOX.children('option').length;
            var option = selectSizeBOX.children('option');

            var selectSize = $(sizesBlockID + ' .size_select');

            var s_item_w = parseInt(selectSize.width());
            var s_item_h = 43;
            s_items_cont_h[j] = cp_size_item_count * s_item_h + 3 * 2; //+плюс ширина border
            var ssc = '<div class="size_select_items_cont" style="height:0; opacity: 0;"></div>';
            selectSize.append(ssc);

            for (var i = 0; i < cp_size_item_count; i++) {
                var ssi = '<div class=\"size_select_item\" data-value=\"' + $(option[i]).val() + '\" style=\"width:' + s_item_w + 'px; height:' + s_item_h + 'px;\">' + $(option[i]).text() + '</div>';
                $(sizesBlockID + ' .size_select_items_cont').append(ssi);
            }

            var sss = '<div class=\"size_selected\"></div>';
            selectSize.append(sss);
        }
    }

    function init_color_select() {

        $('select.color_select_box').css({'display' : 'none'});

        var countPhoto = $('[id^=sizes_]').size();

        for (var j = 0; j < countPhoto; j++) {
            var colorBlockID = "#color_" + j;

            var currentSelect = $(colorBlockID + ' select.color_select_box');
            var cp_color_item_count = currentSelect.children('option').length;
            var option = currentSelect.children('option');

            var currentColorDIV = $(colorBlockID + ' .color_select');
            var c_item_w = parseInt(currentColorDIV.width());
            var c_item_h = 43;

            c_items_cont_h[j] = cp_color_item_count * c_item_h + 3 * 2; //+плюс ширина border

            var csc = '<div class="color_select_items_cont" style="height:0; opacity: 0;"></div>';
            currentColorDIV.append(csc);

            for (var i = 0; i < cp_color_item_count; i++) {
                var csi = '<div class="color_select_item" data-value="' + $(option[i]).val() + '" style="width:' + c_item_w + 'px; height:' + c_item_h + 'px;">' + $(option[i]).text() + '</div>';
                $(colorBlockID + ' .color_select_items_cont').append(csi);
            }

            var css = '<div class="color_selected"></div>';
            currentColorDIV.append(css);

            var optionChild = $(colorBlockID + '.color_select_box option:nth-child(1)');
            var text = optionChild.text();
            $(colorBlockID + '.color_select_box > option').removeAttr('selected');
            optionChild.attr('selected', 'selected');

            $(colorBlockID + '.color_select > .color_selected').html(text);
        }
    }

    function add_size_select_events() {
        size_select_clicks();
        size_select_hover();
    }

    function size_select_hover() {
        $('.size_wrap').hover(function () {
            $(this).css({
                            'border-color' : '#be9872',
                            'z-index'      : '200'
                        });

            $(this).children('.size').css({
                                              'border-color' : '#be9872'
                                          })
        }, function () {
            if (!size_is_open) {
                $(this).css({
                                'border-color' : '#dadad3',
                                'z-index'      : 'auto'
                            });

                $(this).children('.size').css({
                                                  'border-color' : '#dadad3'
                                              })
            }
        })
    }

    function size_select_clicks() {

        // current size item click

        $('.size_select_item').off('click').on('click', function (e) {
            e.preventDefault();
            if (typeof slidersInfo.size[currentPhoto] === "undefined")
                slidersInfo.size[currentPhoto] = '';

            var value = parseInt($(this).data('value'));
            var text = $(this).html();

            slidersInfo.size[currentPhoto] = $.trim(text);

            $('#sizes_' + currentPhoto + ' .size_select_box > option').removeAttr('selected');
            $('#sizes_' + currentPhoto + ' .size_select_box [value=' + value + ']').attr('selected', 'selected');

            var sizeSelect = $('#sizes_' + currentPhoto + ' .size_select > .size_selected');
            sizeSelect.css({'background-image' : 'none'});
            sizeSelect.html(text);
        });


        // all area size click
        $('.size_wrap').click(function () {
            if (!size_is_open) {
                $('#sizes_' + currentPhoto + ' .size_select_items_cont').css({'height' : s_items_cont_h[currentPhoto], 'opacity' : '1'});
                size_is_open = true;
            }
            else {
                $('#sizes_' + currentPhoto + ' .size_select_items_cont').css({'height' : '0', 'opacity' : '0'});
                size_is_open = false;
            }
        })


    }

    function add_color_select_events() {
        color_select_clicks();
        color_select_hover();
    }

    function color_select_clicks() {

        $('.color_select_item').off('click').on('click', function (e) {
            e.preventDefault();
            var value = parseInt($(this).data('value'));
            var text = $(this).html();

            if (typeof slidersInfo.color[currentPhoto] === "undefined")
                slidersInfo.color[currentPhoto] = '';

            slidersInfo.color[currentPhoto] = $.trim(text);

            $('#color_' + currentPhoto + ' .color_select_box > option').removeAttr('selected');
            $('#color_' + currentPhoto + ' .color_select_box [value=' + value + ']').attr('selected', 'selected');

            $('#color_' + currentPhoto + ' .color_select > .color_selected').html(text);
        });


        // all area color click
        $('.specs_color_wrap').click(function () {
            if (!color_is_open) {
                $('#color_' + currentPhoto + ' .color_select_items_cont').css({'height' : c_items_cont_h[currentPhoto], 'opacity' : '1'});
                color_is_open = true;
            }
            else {
                $('#color_' + currentPhoto + ' .color_select_items_cont').css({'height' : '0', 'opacity' : '0'});
                color_is_open = false;
            }
        })

    }

    function color_select_hover() {
        $('.specs_color_wrap').hover(function () {
            $(this).css({
                            'border-color' : '#be9872',
                            'z-index'      : '200'
                        });

            $(this).children('.specs_color_title').css({
                                                           'border-color' : '#be9872'
                                                       })


        }, function () {
            if (!color_is_open) {
                $(this).css({
                                'border-color' : '#dadad3',
                                'z-index'      : 'auto'
                            });

                $(this).children('.size_title').css({
                                                        'border-color' : '#dadad3'
                                                    })
            }
        })

    }

    function add_details_info_events() {


        $('.specs_details').hover(function () {

            $('span[id^=description_]').each(function () {
                if (this.id !== 'description_' + currentPhoto) {
                    $(this).hide();
                }

                $('#description_' + currentPhoto).show();
            });

            var specs_detail_h = parseInt($('span.specs_detail_span').height()) + 55;
            $(this).css({
                            'border-color' : '#be9872',
                            'z-index'      : '200',
                            'height'       : specs_detail_h + 'px'
                        });
        }, function () {
            $(this).css({
                            'border-color' : '#dadad3',
                            'z-index'      : 'auto',
                            'height'       : '49px'
                        });
        })
    }


    init_selects();
    add_events();


// Slider
    var Base_Width;
    var Sliders;
    var Photo_Cont_Width = 0;
    var Photo_Cont_Height = 0;
//    var Photo_Aspect_Ratio = 900 / 450;
    var is_Anim = false;

    var zoom_Coef = 1.5;
    var z_w = 300;
    var z_h = 300;

    init_sliders();

    function init_sliders() {
        Sliders = $('.content_slider');
        Base_Width = Sliders.find('.slider_main_cont').width();
        $(window).resize(resize_slider);
        resize_slider();
        init_zoom();
        slider_events();
    }

    function init_zoom() {
        Sliders.find('.item').find('img').loupe({
                                                    width  : z_w,
                                                    height : z_h,
                                                    loupe  : 'slider_img_zoom'
                                                });

        update_zoom();

        var sw_z = '<div class="sw_z"></div>';
        $('.slider_img_zoom').prepend(sw_z);
    }

    function update_zoom() {
        var h = $('.content_slider').find('.item').find('img').height();
        h = zoom_Coef * h;
        $('.slider_img_zoom').children('img').css({'height' : h});
    }

    function resize_slider() {
        var Photo_Cont_Width_Old = Photo_Cont_Width;

        if (parseInt($(document.body).css('width')) > 990) {
            Photo_Cont_Width = 990;//Sliders.find('.slider_main_cont').width();
            Photo_Cont_Height = 450; //Photo_Cont_Width / Photo_Aspect_Ratio;
        }
        else if (parseInt($('body').css('width')) <= 990) {
            Photo_Cont_Width = 400;
            Photo_Cont_Height = 300;
        }

        Sliders.find('.item').css({'width' : Photo_Cont_Width});
        Sliders.find('.item').find('.photo_cont').css({
                                                          'width'  : Photo_Cont_Width,
                                                          'height' : Photo_Cont_Height
                                                      });

        Sliders.find('.item').find('.photo_cont').find('img').each(function () {
            /*        $(this).removeAttr('style');
             var iar = $(this).width() / $(this).height();
             if (iar > Photo_Aspect_Ratio) {
             var cur_photo_width = Photo_Cont_Height * iar;
             var marg_left = ((Photo_Cont_Width - cur_photo_width) / 2);
             $(this).css({
             'height':Photo_Cont_Height,
             'margin-left':marg_left
             })

             } else {
             var cur_photo_height = Photo_Cont_Width / iar;
             var marg_top = ((Photo_Cont_Height - cur_photo_height) / 2);
             $(this).css({
             'width':Photo_Cont_Width,
             'margin-top':marg_top
             })
             }*/

            $(this).css({
                            'height' : Photo_Cont_Height
                        })
        });

        Sliders.find('.arrow').each(function () {

            $(this).css({'top' : (Photo_Cont_Height / 2)})
        });

        Sliders.find('.content_band').each(function () {
            var items = $(this).find('.item');
            var items_count = items.length;
            var cur_left = parseInt($(this).css('left'));


            $(this).css({
                            'width' : Photo_Cont_Width * items_count * 2

                        });

            if (cur_left != 0) {
                is_Anim = true;
                $(this).animate({'left' : 0}, 100);
                setTimeout(Anim_false, 110);
            }
        });
        Sliders.children('.sl_p_cont').html('');
        build_counter();
        Sliders.find('.right').removeClass('hold');
        Sliders.find('.left').addClass('hold');

        if (Photo_Cont_Width !== Photo_Cont_Width_Old) {
            setTimeout(function () {
                update_zoom();
            }, 300);
        }
    }

    function build_counter() {
        Sliders.each(function () {
            var items_count = $(this).find('.item').length;
            var html_str = '<b>1</b>/' + items_count;
            $(this).find('.page_number').html(html_str);

            var app_str = '';
            for (var i = 0; i < items_count; i++) {
                app_str = '<span class="sl_pointer" rel="' + i + '"></span>';
                $(this).children('.sl_p_cont').append(app_str);
            }

            $($(this).children('.sl_p_cont').children('.sl_pointer')[0]).addClass('sl_p_active');
        })
    }

    function slider_events() {

        Sliders.find('.left').click(function () {
            left_arrow_click($(this).parent('.content_slider'))
        });
        Sliders.find('.right').click(function () {
            right_arrow_click($(this).parent('.content_slider'))
        });
        Sliders.find('.sl_pointer').click(function () {
            var cur_item = parseInt($(this).parent('.sl_p_cont').parent('.content_slider').find('b').html());
            var next_cur_item = parseInt($(this).attr('rel')) + 1;

            for (var i = 0; i < Math.abs(next_cur_item - cur_item); i++) {
                if (next_cur_item > cur_item) right_arrow_click($(this).parent('.sl_p_cont').parent('.content_slider'));
                if (next_cur_item < cur_item) left_arrow_click($(this).parent('.sl_p_cont').parent('.content_slider'));
            }

        });
    }

    function left_arrow_click(obj) {
        if (!is_Anim) {

            var cur_item = parseInt(obj.find('b').html());


            if (cur_item > 1) {
                obj.find('.right').removeClass('hold');
                obj.find('b').html(cur_item - 1);
                obj.children('.sl_p_cont').children('.sl_pointer').eq(cur_item - 1).removeClass('sl_p_active');
                obj.children('.sl_p_cont').children('.sl_pointer').eq(cur_item - 2).addClass('sl_p_active');
                obj.find('.content_band').each(function () {
                    var cur_left = parseInt($(this).css('left')) + Photo_Cont_Width;
                    is_Anim = true;
                    $(this).animate({'left' : cur_left}, 500);
                    setTimeout(Anim_false, 510);
                })
            }

            if ((cur_item - 1) <= 1) {
                obj.find('.left').addClass('hold')
            }

            var old = currentPhoto;
            currentPhoto = cur_item - 2;

            if (old != currentPhoto) {
                $('#sizes_' + old).hide();
                $('#color_' + old).hide();
                $('#sizes_' + currentPhoto).show();
                $('#color_' + currentPhoto).show();
            }

        }
    }

    function right_arrow_click(obj) {
        if (!is_Anim) {
            var items_count = obj.find('.item').length;
            var cur_item = parseInt(obj.find('b').html());


            if (cur_item < items_count) {
                obj.find('.left').removeClass('hold');
                obj.find('b').html(cur_item + 1);
                obj.children('.sl_p_cont').children('.sl_pointer').eq(cur_item - 1).removeClass('sl_p_active');
                obj.children('.sl_p_cont').children('.sl_pointer').eq(cur_item).addClass('sl_p_active');
                obj.find('.content_band').each(function () {
                    var cur_left = parseInt($(this).css('left')) - Photo_Cont_Width;
                    is_Anim = true;
                    $(this).animate({'left' : cur_left}, 500);
                    setTimeout(Anim_false, 510);

                })
            }

            if ((cur_item + 1) >= items_count) {
                obj.find('.right').addClass('hold')
            }

            var old = currentPhoto;
            currentPhoto = cur_item;

            if (old != currentPhoto) {
                $('#sizes_' + old).hide();
                $('#color_' + old).hide();
                $('#sizes_' + currentPhoto).show();
                $('#color_' + currentPhoto).show();
            }
        }
    }

    function Anim_false() {
        is_Anim = false;
    }
});
