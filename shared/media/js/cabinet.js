$(document).ready(function () {

    $('.pc_subscribe').on('click', function () {
        var val = 0;
        if ($(this).find('#check-personal-subscribe_pop_up-styler').eq(0).hasClass('checked'))
            val = 1;
        $('#check-personal-subscribe').val(val);
        if (val)
            $('#check-personal-subscribe-styler').addClass('checked');
        else
            $('#check-personal-subscribe-styler').removeClass('checked');

        $('#check-personal-subscribe_pop_up').val(val);
    });

    $('form').on('submit', function (event) {
        event.preventDefault();
        return false;
    });


    var n_input = jQuery('#personal-popup-name');
    var s_input = jQuery('#personal-popup-surname');
    var jn_input = jQuery('#jname_field');

    n_input.keyup(function () {
        set_jname();
    });

    s_input.keyup(function () {
        set_jname();
    });

    function set_jname() {
        var jname = n_input.val() + ' ' + s_input.val();
        jn_input.val(jname);
    }

    var add_phone_input = jQuery('#add-personal-popup-phone');
    var phone_input = jQuery('#personal-popup-phone');


    add_phone_input.keyup(function () {
        add_phone();
    });

    function add_phone() {
        var phone = add_phone_input.val();
        phone_input.val(phone);

    }

    jQuery(document).mouseup(function (e) {
        var container = jQuery("#personal_cabinet .pc_popup:visible");
        var containerDate = $('div.ui-datepicker');
        if (e.target != container[0] && !container.has(e.target).length && e.target !== containerDate[0] && !containerDate.has(e.target).length) {
            close_personal_popup();
        }
    });
});

var id = undefined;

function open_adress_popup(adress_num) {

    $('[id^=textError]').remove();

    $('#country_and_zip_new').val('');
    $('#city_new').val('');
    $('#street_new').val('');
    $('#metro_new').val('');
    $('#house_new').val('');
    $('#department_new').val('');

    id = adress_num;

    jQuery('#personal_cabinet .pc_popup').hide();

    jQuery('#add_adress_popup').find('.hidden_inputs').children('input').css({'display' : 'none'});

    var i_ids = ['#country_and_zip_' + adress_num , '#city_' + adress_num , '#street_' + adress_num , '#house_' + adress_num , '#department_' + adress_num];
    var ids_l = i_ids.length;

    for (var i = 0; i < ids_l; i++) {
        jQuery(i_ids[i]).css({'display' : 'block'});
    }

    jQuery('#add_adress_popup').show();
}

function delete_adress(adress_num) {
    var conf = confirm('Вы действительно желаете удалить адрес?');
    if (conf)
        $.ajax({
            url: 'buy/deleteAddress/id/' + adress_num,
            success : function (data) {
                if (!data) {
                    $('#addressTitle' + adress_num).remove();
                    if (!$('[id^=addressTitle]').size()) {
                        $('#addressEmpty').addClass('empty');
                    }
                }
            }
        });
}

function open_history_details(item) {
    if (jQuery(item).parent().hasClass('detailed')) {
        jQuery(item).parent().removeClass('detailed');
    } else {
        jQuery('#personal_cabinet .pc_history .pc_block').removeClass('detailed');
        jQuery(item).parent().addClass('detailed');
    }
}

function open_personal_popup(item) {
    jQuery('#personal_cabinet .pc_popup').hide();
    //alert(jQuery('#personal_cabinet .pc_personal').css('top'));
    switch (item) {
        case 1:
            jQuery('#personal_popup').show();
            break;
        case 2:
            jQuery('#phone_popup').show();
            break;
        case 3:
            jQuery('#birthday_popup').show();
            break;

        case 4:
            jQuery('#add_adress_popup').show();
            break;
    }
}

function close_personal_popup() {
    jQuery('#personal_cabinet .pc_popup').hide();
}

function myValidator(f, t, elem) {
    var input = $(f).find('input');
    var validate = true;
    for (var i in input) {
        if (input.hasOwnProperty(i) && typeof (input[i]) === 'HTMLElement') {
            if (!$(input[i]).val()) {
                validate = false;
            }
        }
    }

    var title = {};

    if (validate) {
        var data = $(f).serialize();
        var dataJSON = $(f).serializeArray();
        console.log(dataJSON);

        var action = '';

        if (t == 'saveUser') {
            action = 'buy/updateUserInfo';
            data = {
                'first_name' : dataJSON[0].value,
                'last_name' : dataJSON[1].value,
                'phone' : dataJSON[3].value,
                'email' : dataJSON[2].value,
                'birthday' : dataJSON[4].value,
                'subscribe' : dataJSON[5].value
            };
            data = "userInfo=" + JSON.stringify(data);
            title = $('div.pc_personal .pc_block .value');
        } else if (t == 'saveAddress') {
            action = 'buy/updateAddress';
            if (id === 'new') {
                action = 'buy/addAddress';
            }

            data = {
                'country'    : $('#country_and_zip_' + id).val(),
                'city'       : $('#city_' + id).val(),
                'department' : $('#department_' + id).val(),
                'street'     : $('#street_' + id).val(),
                'house'      : $('#house_' + id).val()
            };

            if (id) {
                data['id'] = id;
            }
        }

        $.ajax({
            url : baseUrl + action,
            data: data,
            type: 'POST',
            success : function (dataSend) {
                var tmpData = dataSend;
                try {
                    tmpData = JSON.parse(dataSend);
                }catch (e) {}

                if (typeof tmpData === "object")
                    dataSend = tmpData;

                if (typeof dataSend === "object" && typeof dataSend.error !== "undefined" && dataSend.error == 0) {
                    $('#add_adress_popup').hide();
                } else if (typeof data.country !== "undefined") {
                    $('[id^=textError]').remove();

                    var msg = 'Не все поля заполнены';
                    $('#add_adress_popup .pc_popup_block').eq(0).append($('<div/>', {id: 'textError', style: 'color: red;'}).html(msg));
                    return;
                }

                if (typeof data.country != "undefined") {
                    var div = $('#addressTitle' + id + ' div');
                    div.eq(-5).text($('#country_and_zip_' + id).val());
                    div.eq(-4).text($('#city_' + id).val());
                    div.eq(-1).text($('#department_' + id).val());
                    div.eq(-3).text($('#street_' + id).val());
                    div.eq(-2).text($('#house_' + id).val());
                } else {
                    for (var i in dataJSON) {
                        if (dataJSON.hasOwnProperty(i)) {
                            if (typeof(title[i]) !== 'undefined') {
                                console.log(title[i]);
                                $(title[i]).html(dataJSON[i].value);
                            }
                        }
                    }
                    $('#personal_popup').hide();
                }

                if (id == 'new') {
                    var dataSending = dataSend;
                    if (typeof dataSend === 'string')
                        dataSending = JSON.parse(dataSend);

                    var addrNum = intval($('[id^=addressTitle]').eq(-1).find('div.title').eq(0).text()) + 1;

                    var div = $('<span/>', {id: 'addressTitle' + dataSending.id}).addClass('')
                        .append($('<span/>', {onclick: 'open_adress_popup(' + dataSending.id + ')'}).addClass('small_button').html('Редактировать'))
                        .append($('<span/>', {onclick: 'delete_adress(' + dataSending.id + ')'}).addClass('small_button').html('Удалить'))
                        .append($('<div>').addClass('title').html(addrNum + ' адрес'))
                        .append($('<div>').html(dataSending.country))
                        .append($('<div>').html(dataSending.city))
                        .append($('<div>').html(dataSending.street))
                        .append($('<div>').html(dataSending.department));
                    var input = $('div.pc_popup_block div.hidden_inputs').eq(0);

                    var idTitle = {
                        'country_and_zip_' : {name: 'country', rus: 'Страна и Zip код'},
                        'city_'            : {name: 'city', rus: 'Город'},
                        'street_and_house_': {name: 'street', rus: 'Улица и дом'},
                        'metro_'           : {name: 'department', rus: 'Станция метро'}
                    };

                    for (var i in idTitle) {
                        if (idTitle.hasOwnProperty(i)) {
                            input.append($('<input/>', {
                                    id: i + dataSending.id,
                                    name: i + dataSending,
                                    type: 'text',
                                    'aria-invalid' : "false",
                                    'placeholder'  : idTitle[i].rus + ' (' + addrNum + ' адрес)'
                                }).val(dataSending[idTitle[i].name])
                            );
                        }
                    }
                    $('#addressBlock').append(div);
                    $('#addressEmpty').removeClass('empty');

                }
            },
            error : function (xhr, error) {
                console.log(xhr, error);
            }
        });
        return true;
    } else {
        var msg = 'Не заполнено обязательное поле';
        alert(msg);
    }
    return false;
}