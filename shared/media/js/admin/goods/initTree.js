$(document).ready(function () {
    var newCount = 0;
    var maxDepthLevelCategory = 2;
    initTree();

    $('#addCat').on('click', function () {
        var editModal = $('#editModal');
        editModal.find('input').val('');

        var param = {
            title : 'Добавление новой категории'
        };

        addModal(editModal, addCategoryToList, param);
        return false;
    });
});
var count = 0;

function initTree() {
    $('#tree').fancytree({
                             extensions      : ["filter", "dnd", "edit"/*, "glyph"*/],//,
                             checkbox        : true,
                             selectMode      : 2,
                             clickFolderMode : 3,
                             expanded        : false,
                             glyph: {
                                 map: {
                                     doc: "glyphicon glyphicon-file",
                                     docOpen: "glyphicon glyphicon-file",
                                     checkbox: "glyphicon glyphicon-unchecked",
                                     checkboxSelected: "glyphicon glyphicon-check",
                                     checkboxUnknown: "glyphicon glyphicon-share",
                                     error: "glyphicon glyphicon-warning-sign",
                                     expanderClosed: "glyphicon glyphicon-plus-sign",
                                     expanderLazy: "glyphicon glyphicon-plus-sign",
                                     // expanderLazy: "glyphicon glyphicon-expand",
                                     expanderOpen: "glyphicon glyphicon-minus-sign",
                                     // expanderOpen: "glyphicon glyphicon-collapse-down",
                                     folder: "glyphicon glyphicon-folder-close",
                                     folderOpen: "glyphicon glyphicon-folder-open",
                                     loading: "glyphicon glyphicon-refresh"
                                     // loading: "icon-spinner icon-spin"
                                 }
                             },

                             select          : function (event, data) {
                                 var span = data.node.span;
                                 var node = data.node;
                                 $(span).find('input[type^=hidden]').remove();
                                 if ($(span).hasClass('fancytree-selected')) {
                                     var cnt = $('input[type^=hidden]').size() + 1;
                                     var input = $('<input/>', {type: 'hidden', name: 'CategoryGoods[' + cnt + '][category_id]'}).val(node.key);
                                     $(span).prepend(input);
                                 } else {
                                     searchInSearchCat(node.key, true);
                                     $(span).find('input[type^=hidden]').val(false);
                                 }
                             },
                             dnd             : {
                                 preventVoidMoves      : true,
                                 preventRecursiveMoves : true,
                                 autoExpandMS          : 400,
                                 dragStart             : function (node, data) {
                                     return true;
                                 },
                                 select                : function(event, data) {
                                     // Display list of selected nodes
                                     var s = data.tree.getSelectedNodes().join(", ");
                                     $("#echoSelection1").text(s);
                                 },
                                 dragEnter             : function (node, data) {
                                     return true;
                                 },
                                 dragDrop              : function (node, data) {

                                     count = 0;
                                     var scan = recursiveScanNode(node, 1);
                                     if (scan == 3 && data.hitMode == 'over') {
                                         alert('Невозможно добавить! Максимальный уровень вложенности 2');
//                                         initTree();
                                         return true;
                                     }

                                     rotateElem(node.key, data.otherNode.key);

                                     $('.preload').show();

                                     setTimeout(function () {
                                         var tree = $('#tree');
                                         var treeFancy = tree.fancytree('getTree');
                                         $(":ui-fancytree").fancytree("destroy");
                                         initTree();

                                         var dataTree = tree.fancytree('getTree').toDict();

                                         $.ajax({
                                                    type    : "POST",
                                                    url     : adminUrl + "/" + controller + '/updateIndex',
                                                    data    : "data=" + JSON.stringify(dataTree),
                                                    success : function (data) {
                                                        $('.preload').hide();
                                                    },
                                                    error   : function (xhr, err) {
                                                        alert('ОШИБКА');
                                                        $('.preload').hide();
                                                    }
                                                });

                                     }, 400);
                                 }
                             }
                         });
    var cnt = 0;
    $("#tree").fancytree("getRootNode").visit(function (node) {
        node.setExpanded(true);
        var span = $(node.span);

        if (!span.find('[type^=checkbox]').size()) {
            cnt++;
            var checkBox = searchInSearchCat(node.key);
            if (checkBox) {
                $(span).addClass('fancytree-selected');
                $(span).find('.fancytree-checkbox').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                var input = $('<input/>', {type: 'hidden', name: 'CategoryGoods[' + cnt + '][category_id]'}).val(node.key);
                $(span).prepend(input);
            }
        }

    });
    changeElem();
    editButtonClick();
    deleteButtonClick();
}

function recursiveScanNode(node) {
    if (typeof node.parent !== "undefined" && node.parent !== null) {
        count++;
        recursiveScanNode(node.parent);
    }
    return count;
}

function rotateElem(key1, key2) {
    var elem1 = $('#' + key1).eq(0);
    var elem2 = $('#' + key2).eq(0);
    var ul = $('<ul/>');
    if (elem1.children('ul').size() == 0)
        elem1.append(ul);
    elem1.find('ul').append(elem2);
}

function addCategoryToList(event) {
    event.preventDefault();
    var inpVal = $(this).parent().find('input').val();
    if (!inpVal) {
        alert('Введите название категории!');
        return false;
    }

    $.ajax({
               url      : adminUrl + "category/add",
               data     : 'name=' + inpVal,
               type     : 'POST',
               dataType : 'JSON',
               success  : function (data) {
                   if (typeof data.position === 'undefined') {
                       alert('Название занято. Введите другое!');
                       return false;
                   }

                   var tree = $('#tree #treeData');
                   var li = tree.find('li').eq(-1).clone();
                   var btnCat = getBtnCat(data.id);
                   $(li).attr('id', data.id).text(data.name).append(btnCat);
                   tree.append(li);
                   closeModal();
                   initTree();
               },
               error    : function (xhr, err) {
                   console.log(xhr, err);
               }
           });

    return false;
}

function getBtnCat(id) {
    return '<div class="btnCat">\
    <span class="edit' + id + '">\
    <img title="Переименовать" src="/shared/media/css/admin/images/admin/edit.png">\
    </span>\
    <span class="delete' + id + '">\
    <img title="Удалить" src="/shared/media/css/admin/images/admin/delete.png">\
    </span>\
    </div>';
}

function deleteButtonClick() {
    $('span[class^=delete]').each(function () {
        $(this).off('click').on('click', function (event) {
            event.preventDefault();

            var elem = $(this).parent().parent();
            var id = $(this).attr('class').replace('delete', '');

            var confirmPromt = confirm("Удалить элемент " + elem.text().trim() + "?");

            if (confirmPromt) {
                $.ajax({
                           url     : adminUrl + "category/delete/id/" + id,
                           success : function (data) {
                               var elemLIDelete = $('#' + id);
                               console.log(elemLIDelete);
                               elemLIDelete.find('li').each(function () {
                                   $(elemLIDelete).before(this);
                               });
                               $(elemLIDelete).remove();
                               initTree();
                           }
                       })
            }

            return false;
        });
    });
}

function editButtonClick() {
    $('span[class^=edit]').each(function () {
        $(this).off('click').on('click', function (event) {
            event.preventDefault();

            var oldVal = $(this).parent().parent().text().trim();
            var editModal = $('#editModal');
            var inputElem = editModal.find('input');

            if (inputElem.size()) {
                editModal.find('input').val(oldVal);
            }

            addModal(editModal, addEditCategory, {title : 'Редактирование категории', elem: this, action: adminUrl + controller + '/update', oldVal: oldVal});

            return false;
        })
    });

}

function addEditCategory(event) {
    event.preventDefault();
    var inpVal = $(this).parent().find('input').val();
    var action = $('#editModal').parent().find('form').attr('action');
    if (typeof $(this).dialog('option', 'elem') !== 'undefined')
        action = $(this).dialog('option', 'action');
    if (!inpVal) {
        alert('Введите название категории!');
        return false;
    }

    var modal = this;

    var self   = $(this).dialog('option', 'elem');
    var id     = $(self).attr('class').replace('edit', '');
    var oldVal = $(this).dialog('option', 'oldVal');

    $.ajax({
               url     : action,
               data    : 'id=' + id + '&newName=' + inpVal,
               type    : 'POST',
               success : function (data) {
                   var htmlOld = $(self).parent().parent().html();
                   $(self).parent().parent().html(htmlOld.replace(oldVal, inpVal));
                   var modal = $('#editModal');
                   console.log(2222, modal);
                   closeModal();
                   $(modal).dialog('close');
                   modal.attr('class', '');
                   modal.find('input').val('');
                   $('.ui-dialog').dispatch();
               },
               error   : function (xhr, err) {
                   console.log(xhr, err);
               }
           });

    changeElem();

    return false;
}

function closeModal() {
    var modal = $('#editModal');
    $('.ui-widget-overlay').remove();
    $('.ui-dialog').remove();
    modal.attr('class', '');
    modal.find('input').val('');
}

function changeElem () {
    var cnt = 0;
    $('.ui-fancytree span.fancytree-node').each(function () {
        $(this).removeAttr('style');

        if ((cnt % 2) == 0) {
            $(this).css({'background-color' : '#F3F4F5!important'});
        }
        cnt++;
    });
}


function searchInSearchCat(id, del) {
    for (var i in jsonCat) {
        if (jsonCat.hasOwnProperty(i)) {
            if (parseInt(jsonCat[i].id) == id) {
                if (del) {
                    delete jsonCat[i];
                }
                return true;
            }
        }
    }

    return false;
}