function pekeUploadEvent(action, elem, id, callbackSuccess, text, callbackError) {

    $(elem).each(function () {
        self = this;

        if (typeof (callbackError) === 'undefined') {
            callbackError = function (file, err) {
                console.log(file, err);
            }
        }

        if (typeof callbackSuccess === 'undefined')
            callbackSuccess = function (file, data) {
                var parent = $(file.contestElem).parent();

                var input = $(parent.find('input[type^=hidden]'))[0];
                $(input).val(data.url);
                $(parent.find('img')[0]).attr('src', data.url);
            };

        if (typeof(text) === 'undefined') {
            text = "Изменить";
        }

        $(this).pekeUpload2({
                                btnText                    : text,
                                theme                      : 'bootstrap',
                                showPercent                : true,
                                showErrorAlerts            : true,
                                allowedExtensions          : 'jpg|jpeg|png',
                                invalidExtError            : 'Неверный формат файла',
                                maxSize                    : 10,
                                data                       : '{id: ' + id + '}',
                                sizeError                  : "Размер файла превышает установленные лимит в 10 Мб",
                                url                        : action,
                                dataType                   : 'JSON',
                                multiFileUpload            : false,
                                mimeType                   : 'image/*',
                                additionCheckCorrectAnswer : function (data) {
                                    if (data.status === true) {
                                        return true;
                                    }

                                    if (data.textError !== 'undefined')
                                        return data.textError;

                                    return false;
                                },
                                onFileSuccess              : function (file, data) {
                                    $(elem).html('Изменить');
                                    callbackSuccess(file, data);
                                },
                                onFileError                : function (file, error) {
                                    if (typeof callbackSuccess !== 'undefined')
                                        callbackError(file, error);
                                }
                            });
    });

}