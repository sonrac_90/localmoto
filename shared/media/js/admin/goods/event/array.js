var objectHelper = {
    searchInObjVal : function (needle, arr) {
        for (var i in arr) {
            if (arr.hasOwnProperty(i)) {
                if (i != 'length' && $(arr[i]).val() == needle) {
                    return true;
                }
            }
        }

        return false;
    }
};

String.prototype.ucShift = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};