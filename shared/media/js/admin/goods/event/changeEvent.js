var changeEvent = {


    selectorDefault : {
        changeProducer : {
            sendRequest    : false,
            title          : 'Добавление производителя',
            selector       : 'select',
            selectorParent : '.relPos',
            addData        : {
                name : 'newName',
                id   : 'id'
            },
            clearSelector  : [],
            action         : adminUrl + 'producer/add',
            errorTextIs    : 'Такой производитель уже существует',
            change         : function () {

            },
            afterAdd       : function (parent) {

            }
        }
    },

    addSelectVariant : function (elem, objSelect) {

        var modal = $('#editModal');

        $('input', modal).val('');

        function closeCall() {
            $(elem).val($(elem).attr('data-old'));
            $(this).dialog('close');
        }

        function callBackClick(event) {
            event.preventDefault();
            var modal = $(this);

            var inputVal = modal.find('input').eq(0).val();

            var option = $(modal).dialog('option');

            var url       = option.urlAction;
            var action    = option.action;
            var errorText = option.errorText;

            if (inputVal) {
                var additionParams = '';
                var parent = $('fieldset').has(elem).find(objSelect.selectorIDParent).eq(0);
                if (typeof action.id !== "undefined") {
                    if (parent.size())
                        additionParams = "&" + action.id + "=" + parent.val();
                }
                $.ajax({
                           url      : url,
                           type     : 'POST',
                           data     : action.name + "=" + inputVal + additionParams,
                           dataType : 'JSON',
                           success  : function (data) {
                               if (typeof data.error !== 'undefined') {
                                   alert(errorText + "\n" + data.error);
                                   return;
                               }

                               var options = $(elem).find('option');
                               options.each(function () {
                                   if ($(this).val() == '')
                                       $(this).remove();
                               });

                               $('option:selected', objSelect).removeAttr('selected');

                               var newOption = $('<option/>', {value : data.id, 'selected' : 'selected'}).html(data.name);

                               $(options.eq(-1)).before(newOption);
                               $(elem).val(data.id);
                               objSelect.afterAdd(parent);
                               $(elem).attr('data-old', data.id);
//                               $(modal).dialog('close');
                               closeModal();
                           },
                           error    : function (xhr, error) {
                               console.log(xhr, error);
                           }
                       });

                return;
            }
            alert('Введите значение в поле!');
        }

        param = {
            title     : objSelect.title,
            urlAction : objSelect.action,
            action    : objSelect.addData,
            errorText : objSelect.errorTextIs,
            close     : closeCall
        };

        addModal(modal, callBackClick, param);
    },

    changeSelect : function (selector, selectOBJ) {
        $(selector).off('change').on('change', function () {
            if ($(this).val() == -10) {
                changeEvent.addSelectVariant(this, selectOBJ);
            } else {
//                changeEvent.deleteCategoryFromDB($(this).attr('data-old'));
                $(this).attr('data-old', $(this).val());
            }
        });
    },

    changeAll : function () {
        var selector = changeEvent.selectorDefault;
        for (var i in selector) {
            if (selector.hasOwnProperty(i)) {
                var selectorFind = selector[i].selectorParent + ' ' + selector[i].selector;
                $(selectorFind).each(function () {
                    selector[i].change(this);
                    changeEvent.changeSelect(this, selector[i]);
                });
            }
        }
    },

    changeColor : function (selector, color) {
        colorChoose = true;

        if (color) {
            colorChoose = false;
        }
        $(selector).each(function () {
            var self = $(this).get(0);
            if (colorChoose) {
                color = $(this).parent().find('input.colorChoose').eq(0).val();
            }

            $(self).css({'color' : color, 'background-color' : color});

            $(self).ColorPicker({
                                    color    : color,
                                    onShow   : function (colpkr) {
                                        $(colpkr).fadeIn(500);
                                        return false;
                                    },
                                    onHide   : function (colpkr) {
                                        var elementInput = $(self).parent().find('input.colorChoose')[0];
                                        var colorN = $(elementInput).val();
                                        $(elementInput).val('');
                                        if (objectHelper.searchInObjVal(colorN, $('input.colorChoose'))) {
                                            alert('Такой цвет уже выбран');
                                            $(elementInput).val('');
                                            return true;
                                        }
                                        $(elementInput).val(colorN);
                                        $(colpkr).fadeOut(500);
                                        return false;
                                    },
                                    onChange : function (hsb, hex, rgb) {
                                        var parent = $(self).parent();
                                        $(parent).find(selector).val(hex).css({'backgroundColor' : '#' + hex, 'color' : '#' + hex});
                                    }
                                });
        });
    }

};