var callBacksEvent = {
    bindEvent : function (elem, eventType, callback, additionParams) {
        $(elem).each(function () {
            $(this).off(eventType).on(eventType, additionParams, function (event) {
                event.preventDefault();
                if (typeof callback == 'function')
                    callback($(this).eq(0), event);
                formControl();
                return false;
            })
        });
    }
};
