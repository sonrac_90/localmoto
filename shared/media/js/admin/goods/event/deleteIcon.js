/**
 * @type {{statusDelete: boolean, paramsDefault: {size: {action: string, parentContainer: string, selector: string, text: string}, color: {action: string, parentContainer: string, selector: string, text: string}, photo: {action: string, parentContainer: string, selector: string, text: string}, category: {action: string, parentContainer: string, selector: string, text: string}, subCategory: {action: string, parentContainer: string, selector: string, text: string}, subSubCategory: {action: string, parentContainer: string, selector: string, text: string}}, deleteSize: {bindClick: Function}, deleteColor: {bindClick: Function}, deletePhoto: {bindClick: Function}, deleteCategory: {bindClick: Function}, deleteSubCategory: {bindClick: Function}, deleteSubSubCategory: {bindClick: Function}, getSelectDataOption: Function, bindAllEventDelete: Function, deleteElem: Function}}
 */
var deleteElement = {

    /**
     * True - can delete, false - not delete
     */
    statusDelete : true,

    paramsDefault : {
        size  : { // Удаление размера
            action          : adminUrl + controller + "/" + "deleteSize/id/",
            parentContainer : ".nextSize",
            selector        : '.deleteSize',
            text            : 'размер'
        },
        color : { // Удаление цвета
            action          : adminUrl + controller + "/" + "deleteColor/id/",
            parentContainer : ".nextColor",
            selector        : '.deleteColors',
            text            : 'цвет'
        },

        photo : { // Удаление фото
            action          : adminUrl + controller + "/" + "deletePhoto/id/",
            parentContainer : ".photoInfo",
            selector        : '.deletePhoto',
            text            : 'фото'
        },

        category : { // Удаление категории
            action          : adminUrl + controller + "/" + "deleteSubCategory/id/",
            parentContainer : "fieldset",
            selector        : '.deleteCategory',
            text            : 'раздел'
        },

        subCategory : { // Удаление подкатегории
            action          : adminUrl + controller + "/" + "deleteSubCategory/id/",
            parentContainer : ".subcategoryChoose",
            selector        : '.deleteSubCategory',
            text            : 'категорию'
        },

        subSubCategory : { // Удаление категории подкатегории
            action          : adminUrl + controller + "/" + "deleteSubCategory/id/",
            parentContainer : ".next",
            selector        : '.deleteSubSubCategory',
            text            : 'подкатегорию'
        }

    },

    deleteSize : {
        /**
         * Bind click delete size
         * @param selector
         */
        bindClick : function (selector) {
            callBacksEvent.bindEvent(selector, 'click', function (elem, event) {
                event.preventDefault();
                deleteElement.deleteElem(elem, 'size', undefined, undefined, 'select');
                return false;
            });
        }
    },

    deleteColor : {
        /**
         * Bind click delete color
         * @param selector
         */
        bindClick : function (selector) {
            callBacksEvent.bindEvent(selector, 'click', function (elem, event) {
                event.preventDefault();
                deleteElement.deleteElem(elem, 'color', undefined, undefined, 'input.colorChoose');
                return false;
            });
        }
    },

    deletePhoto : {
        /**
         * Bind click delete photo
         * @param selector
         */
        bindClick : function (selector) {
            callBacksEvent.bindEvent(selector, 'click', function (elem, event) {
                event.preventDefault();
                deleteElement.deleteElem(elem, 'photo', undefined, undefined, '.photoInfo input.hiddenUrl');
                return false;
            });
        }
    },

    deleteCategory : {
        /**
         * Bind click delete category
         * @param selector
         */
        bindClick : function (selector) {
            callBacksEvent.bindEvent(selector, 'click', function (elem, event) {
                event.preventDefault();
                deleteElement.status = true;
                deleteElement.deleteElem(elem, 'category', function (element, parentContainer) {
                    if (deleteElement.status) {
                        var catChooise = $('.categoryChoose');
                        var optionNew = deleteElement.getSelectDataOption(parentContainer);
                        catChooise.find('select').each(function () {
                            var option = $(elem).find('option');
                            option.eq(-1).before(optionNew);
                        });
                    }
                }, true, '.categoryChoose select');
                return false;
            });
        }
    },

    deleteSubCategory : {
        /**
         * Bind click delete subCategory
         * @param selector
         * @param type
         * @param selectorFind
         */
        bindClick : function (selector, type, selectorFind) {
            if (typeof selectorFind === "undefined")
                selectorFind = 'select';

            if (typeof type === "undefined")
                type = 'subCategory';
            callBacksEvent.bindEvent(selector, 'click', function (elem, event) {
                event.preventDefault();
                deleteElement.status = true;
                deleteElement.deleteElem(elem, type, function (element, parentContainer) {
                    if (deleteElement.status) {
                        var optionNew = deleteElement.getSelectDataOption(parentContainer);
                        $(parentContainer).parent().find('select').each(function () {
                            var option = $(elem).find('option');
                            option.eq(-1).before(optionNew);
                        });
                    }
                }, undefined, selectorFind);

                return false;
            });
        }
    },

    deleteSubSubCategory : {
        /**
         * Bind click delete sub subCategory
         * @param selector
         */
        bindClick : function (selector) {
            deleteElement.deleteSubCategory.bindClick(selector, 'subSubCategory', '.subSubcategoryChoose .next select');
        }
    },

    /**
     * Get sets value select
     * @param parentContainer
     * @returns {*|jQuery}
     */
    getSelectDataOption : function (parentContainer) {
        var select = $(parentContainer).find('select').eq(0);
        var value = select.val();
        var name = select.text();

        return $('<option/>', {value : value}).text(name);
    },

    /**
     * Bind all click delete icon
     */
    bindAllEventDelete : function () {
        var param = deleteElement.paramsDefault;
        for (var i in param) {
            if (param.hasOwnProperty(i)) {
                var key = 'delete' + i.ucShift();
                deleteElement[key].bindClick(param[i].selector);
            }
        }
    },

    /**
     * Delete elem from DOM model
     * @param elem
     * @param typeDelete
     * @param callback
     * @param oneHelp
     * @param selectorFind
     * @returns {boolean}
     */
    deleteElem : function (elem, typeDelete, callback, oneHelp, selectorFind) {
        var cont = deleteElement.paramsDefault[typeDelete].parentContainer;
        var parentContainer = $(cont).has($(elem));
        if (oneHelp && $(selectorFind, parentContainer.parent()).size() <= 1 && typeof oneHelp !== "undefined") {
            alert('Невозможно удалить единственный ' + deleteElement.paramsDefault[typeDelete].text + '!');
            return false;
        }

        var text = deleteElement.paramsDefault[typeDelete].text;
        var idDelete = intval($(elem).attr('data'));
        idDelete = (idDelete == 0) ? '' : idDelete;

        var selectorValue = parentContainer.find(selectorFind);

        var additionText = selectorValue.eq(0).val();

        if (typeDelete.toLowerCase().indexOf('category') !== -1) {
            additionText = selectorValue.find('option[value^=' + selectorValue.val() + ']').text();
        }

        if (additionText)
            additionText = "( " + additionText + " )";

        var confirmDelete = confirm('Вы действительно хотите удалить ' + text + " " + idDelete + additionText);

        function callCallback(callBackFunction) {
            if (typeof callBackFunction == 'function') {
                callBackFunction(elem, parentContainer);
            }

            // Ищу контейнер, который содержит данный элемент и удаляю
            if ((cont.toLowerCase().indexOf('category') != -1) &&
                ($(parentContainer).parent().find('span.subcategoryChoose').size() <= 1)) {
                parentContainer.html('');
            } else {
                parentContainer.remove();
            }
        }

        if (confirmDelete) {

            if (typeof parentContainer !== 'object') {
                deleteElement.status = false;
                alert('Невозможно удалить, так как в DOM не обнаружено элемента');
                return false;
            }

            var goodID = parseInt($('#Goods_id').val());

            if (!isNaN(parseInt(idDelete))) { // Коллбек на удаление из базы
                $.ajax({
                           url      : deleteElement.paramsDefault[typeDelete].action + idDelete + '/goodID/' + goodID,
                           type     : 'POST',
                           dataType : 'JSON',
                           success  : function (data) {
                               if (!data.status) {
                                   deleteElement.status = false;
                                   alert('Произошла ошибка. Невозможно удалить ' +
                                         text + ' ' + idDelete + "\nПо следующей причине:\n" + data);
                                   return false;
                               }

                               callCallback(callback);
                           }
                       })
            } else {
                callCallback(callback);
            }
        }
    }
};
