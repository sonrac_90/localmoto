var createElement = {

    key     : undefined,
    idPhoto : undefined,

    selectorDefault : {

        photoAdd : {
            selector        : '#photoAdd',
            selectorFind    : '.photoInfo',
            solvedContainer : '#photoInfo',
            htmlTemplate    : function (key, photoID) {
                var deletePhoto = $('<div/>').addClass('deletePhoto').attr('data-key', key);
                deleteElement.deletePhoto.bindClick(deletePhoto);
                var inputFile = $('<input/>', {type : 'file', value : '', id : 'file' + key});
                var buttonSize = $('<select/>', {multiple: "", name: 'Goods[photos][' + key + '][sizes][]'}).attr('size', 10)
                        .append($('<option/>').text('XS'))
                        .append($('<option/>').text('S'))
                        .append($('<option/>').text('M'))
                        .append($('<option/>').text('L'))
                        .append($('<option/>').text('XL'))
                        .append($('<option/>').text('XXL'))
                        .append($('<option/>').text('XXXL'))
                        .append($('<option/>').text('BXL'))
                        .append($('<option/>').text('BXXL'))
                        .append($('<option/>').text('BXXXL'))
                    ;
                var buttonColor = $('<button/>').attr('data', key).addClass('addColor btn btn-success').attr('data-key', key).html('Добавить Цвет');
                var mainDiv = $('<div/>').attr('data-key', key).addClass('photoInfo')
                    // Объединение элементов
                    .append($('<fieldset/>')
                                // Вставляю легенду
                                .append($('<legend/>').html('Фотография ' + parseInt(key + 1))
                                            // Вставляю блок удаления фотографии
                                            .append(deletePhoto))
                                // Контейнер фотографии
                                .append($('<div/>').addClass('photo')
                                            // Сама фотография
                                            .append($('<img/>', {src : baseUrl + 'shared/media/css/admin/images/admin/no-img.png', width : 300, height : 300}))
                                            // Контейнер ошибки
                                            .append($('<div/>', {id : 'photo_' + key + '_Photo_url'}).addClass('errors divError'))
                                            // Передаваемое поле формы
                                            .append($('<input/>', {type : 'hidden', value : '', name : 'Goods[photos][' + key + '][url]'}).addClass('hiddenUrl'))
                                            .append(inputFile)
                                // Контейнер информации о фотографии
                            ).append($('<div/>').addClass('photoAddInfo')
                                         .append($('<div/>').addClass('sizeColor')
                                         // Объединение элементов (Размер)
                                             .append($('<fieldset/>')
                                                         // Размеры (легенда)
                                                         .append($('<legend/>').html('Размеры'))
                                                         // Контейнер информации о размерах
                                                         .append($('<div/>').addClass('sizesContainer')
                                                                     // Кнопка добавления размера
                                                                     .append(buttonSize)
                                                                     .append('<div class = "errors divError" id = "photo_' + key + '_size_0"></div>')

                                                     )
                                             )
                                             // Объединение элементов (Цвета)
                                             .append($('<fieldset/>')
                                                         // Размеры (легенда)
                                                         .append($('<legend/>').html('Цвета'))
                                                         // Контейнер информации о размерах
                                                         .append($('<div/>').addClass('colorsContainer')
                                                                     // Кнопка добавления цвета
                                                                     .append($('<textarea/>', {rows: 5, cols: 25, name: 'Goods[photos][' + key + '][color]'}))
                                                                     .append('<div class="errors divError" id="photo_' + key + '_color_ColorGoods_color"></div>')
                                                     )
                                             )
                                     )
                                         // Объединение элементов (Описание)
                                         .append($('<fieldset/>')
                                                     // Размеры (легенда)
                                                     .append($('<legend/>').html('Описание'))
                                                     // Контейнер информации о размерах
                                                     .append($('<div/>').addClass('relPos')
                                                                 // Поле описания
                                                                 .append($('<textarea/>', {name : 'Goods[photos][' + key + '][description][description]'}))
                                                                 .append($('<div/>', {id : 'photo_' + key + '_description_DescriptionGoods_description'}).addClass('errors divError'))
                                                     //<div class='errors divError' id='photo_<?=$key?>_description_DescriptionGoods_description'></div>
                                                 )
                                     )
                            )
                );
                pekeUploadEvent(adminUrl + '/' + controller + '/uploadPhoto', inputFile);
                return mainDiv;
            },
            createElem      : function () {
                var photo = createElement.selectorDefault.photoAdd;
                createElement.createPhotoElem(photo, createElement.selectorDefault.photoAdd.solvedContainer);
                var scrollBottom = $(window).scrollTop() + $(window).height() + 5000;
                $(document.body).scrollTo(scrollBottom);
            }
        },

        addColor : {
            selector        : '.addColor',
            selectorFind    : 'div.colorChoose',
            solvedContainer : '.nextColor',
            htmlTemplate    : function (key, keyPhoto) {
                var colorOBJ = createElement.selectorDefault.addColor;
                var color = colorOBJ.getRandomColor();
                var name = 'Goods[photos][' + keyPhoto + '][colors][' + key + '][color]';
                var divColor = $('<div/>').addClass('colorChoose').attr('data-photo', keyPhoto).attr('data-key', key);
                var divDelete = $('<div/>').addClass('deleteColors delIcon');
                deleteElement.deleteColor.bindClick(divDelete);
                var mainDiv = $('<div/>').addClass('nextColor')
                    .append(divColor)
                    .append($('<input/>', {type : 'hidden', name : name}).attr('data-color', key).addClass('colorChoose').attr('data-photo', keyPhoto))
                    .append(divDelete);
                colorOBJ.colorPickerCreate(divColor);
                changeEvent.changeColor($(colorOBJ.selectorFind, mainDiv));
                return mainDiv;
            },

            getRandomColor : function () {
                var letters = '0123456789ABCDEF'.split('');
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.round(Math.random() * 15)];
                }
                return color;
            },

            colorPickerCreate : function (selector) {
                var color = createElement.selectorDefault.addColor.getRandomColor();

                if ($(selector).parent() !== "undefined") {
                    $($(selector).parent().find('input.colorChoose')).val(color);

                }

                $(selector).css({'color' : color, 'background-color' : color});
            },
            createElem        : function (elem) {
                var color = createElement.selectorDefault.addColor;
                createElement.createPhotoElem(color, undefined, elem);
            }
        }
    },

    createPhotoElem : function (object, after, has) {
        var newElem = object.htmlTemplate(createElement.key, createElement.idPhoto, has);
        if (typeof after === 'undefined') {
            var form = $('form').eq(0);
            var obj = form.find(has);
            form.find(obj).eq(0).parent().before(newElem);
        } else {
            $('form').eq(0).find(after).eq(0).append(newElem);
            createElement.bindAllEventAdd();
        }
    },

    getKeyNext : function (selector, attr) {
        if (typeof attr === "undefined") {
            attr = 'data';
        }

        var elem = $(selector).eq(-1).attr(attr);

        if (isNaN(parseInt(elem)))
            return 0;
        return parseInt(parseInt(elem) + 1);
    },

    getPhotoID : function (selector, attr) {
        if (typeof attr === 'undefined') {
            attr = 'data-photo';
        }

        return $('.photoInfo').has($(selector)).attr(attr);
    },

    setKeyAndIdPhoto : function (elem, defSelector) {
        // Ищу ключ и idPhoto
        createElement.key = createElement.getKeyNext(defSelector.selectorFind, 'data-key');
        createElement.idPhoto = createElement.getPhotoID(elem, 'data-key');
    },

    bindAllEventAdd : function () {
        var defSelector = createElement.selectorDefault;
        for (var i in defSelector) {
            if (defSelector.hasOwnProperty(i)) {
                createElement.bindEventAdd(defSelector[i].selector, defSelector[i], {sel : defSelector[i]});
            }
        }

    },

    bindEventAdd : function (selector, defSelector, i) {
        callBacksEvent.bindEvent(selector,
                                 'click',
                                 createElement.createElementLast,
                                 i
        );
    },

    createElementLast : function (elem, event) {
        var sel = event.data.sel;
        createElement.setKeyAndIdPhoto(elem, sel);
        setTimeout(formControl, 50);
        sel.createElem(elem);
        setTimeout(showValidateResult.bindEventValidateForm, 100);
    }
};