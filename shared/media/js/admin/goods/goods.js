$(document).ready(function () {

    // Удаление элементов
    deleteElement.bindAllEventDelete();

    // ColorPicker
    changeEvent.changeColor('div.colorChoose');

    // Добавление элементов
    createElement.bindAllEventAdd();

    callBacksEvent.bindEvent('form', 'submit', function (event, elem) {

        if (!$('.fancytree-node input[type^=hidden]').size()) {
            alert('Не выбрано ни одной категории!');
            return false;
        }

        $('input, select, textarea, img').removeClass('redBorder');
        $('.divError').html('');
        validateData($('form').eq(0));
    });
    changeEvent.changeAll();

    pekeUploadEvent(adminUrl + '/' + controller + '/uploadPhoto', 'input[id^=file]');
    showValidateResult.bindEventValidateForm();
});