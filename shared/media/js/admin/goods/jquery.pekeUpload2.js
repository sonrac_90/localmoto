/*
 *  PekeUpload 2.0.0 - jQuery plugin
 *  written by Pedro Molina
 *  corrected by Doniy Serhey
 *  http://www.pekebyte.com/
 *
 *  Copyright (c) 2013 Pedro Molina (http://pekebyte.com)
 *  Dual licensed under the MIT (MIT-LICENSE.txt)
 *  and GPL (GPL-LICENSE.txt) licenses.
 *
 */
(function ($) {

    $.fn.pekeUpload2 = function (options) {

        // default configuration properties
        var defaults = {
            onSubmit                   : false,
            btnText                    : "Browse files...",
            url                        : "upload.php",
            theme                      : "custom",
            field                      : "file",
            data                       : null,
            multi                      : true,
            showFilename               : true,
            showPercent                : true,
            showErrorAlerts            : true,
            allowedExtensions          : "",
            invalidExtError            : "Invalid File Type",
            multiFileUpload            : false,
            mimeType                   : false,
            maxSize                    : 0,
            dataType                   : false,
            sizeError                  : "Size of the file is greather than allowed",
            onFileError                : function (file, error) {
            },
            onFileSuccess              : function (file, data) {
            },
            additionCheckCorrectAnswer : function (data) {
                return true;
            }
        };

        var options = $.extend(defaults, options);

        //Main function
        var obj;
        var file = {};
        this.each(function () {
            obj = $(this);
            //HTML code depends of theme
            if (options.theme == "bootstrap") {
                var html = '<a href="javascript:void(0)" class="btn btn-primary btn-upload"> <span class="icon-upload icon-white"></span> ' + options.btnText + '</a><div class="pekecontainer"></div>';
            }
            if (options.theme == "custom") {
                var html = '<a href="javascript:void(0)" class="btn-pekeupload">' + options.btnText + '</a><div class="pekecontainer"></div>';
            }
            if (options.multiFileUpload) {
                obj.attr('multiple', 'multiple');
            }

            if (options.mimeType) {
                if (options.mimeType.toLowerCase().indexOf('image') !== -1) {
//                    options.mimeType += ";capture=\"camera\"";
                    obj.attr('capture', 'camera');
                }
                obj.attr('accept', options.mimeType);
            }

            obj.after(html);
            obj.hide();
            //Event when clicked the newly created link
            obj.next('a').click(function () {
                obj.click();
            });
            //Event when user select a file
            obj.change(function (event) {
                var fileList = event.currentTarget.files;
                delete file;
                file = {};
                cnt = 0;

                for (var i in fileList) {
                    if (fileList.hasOwnProperty(i) && !isNaN(parseInt(i))) {
                        file[cnt] = {};
                        file[cnt].name = fileList[0].name.split('\\').pop();
                        file[cnt].size = (fileList[0].size / 1024) / 1024;
                        cnt++;
                    }
                }
                if (validateresult()) {
                    if (options.onSubmit == false) {
                        file.contestElem = this;
                        UploadFile();
                    }
                    else {
                        obj.next('a').next('div').prepend('<br /><span class="filename">' + file.name + '</span>');
                        obj.parent('form').bind('submit', function () {
                            obj.next('a').next('div').html('');
                            UploadFile();
                        });
                    }
                }
            });
        });

        //Function that uploads a file
        function UploadFile() {
            var error = true;
            if (options.theme == "bootstrap") {
                var htmlprogress = '<div class="file"><div class="filename"></div><div class="progress progress-striped"><div class="bar pekeup-progress-bar" style="width: 0%;"><span class="badge badge-info"></span></div></div></div>';
            }
            if (options.theme == "custom") {
                var htmlprogress = '<div class="file"><div class="filename"></div><div class="progress-pekeupload"><div class="bar-pekeupload pekeup-progress-bar" style="width: 0%;"><span></span></div></div></div>';
            }
            obj.next('a').next('div').prepend(htmlprogress);
            var formData = new FormData();
            for (var i in obj[0].files) {
                if (obj[0].files.hasOwnProperty(i))
                    formData.append(options.field + "[" + i + "]", obj[0].files[i]);
            }
            formData.append('data', options.data);
            $.ajax({
                       url         : options.url,
                       type        : 'POST',
                       data        : formData,
                       dataType    : options.dataType,
                       success     : function (data) {
                           var percent = 100;
                           obj.next('a').next('div').find('.pekeup-progress-bar:first').width(percent + '%');
                           obj.next('a').next('div').find('.pekeup-progress-bar:first').text(percent + "%");
                           if (options.multi == false) {
                               obj.attr('disabled', 'disabled');
                           }

                           error = options.additionCheckCorrectAnswer(data);

                           $('.pekecontainer .progress').remove();
                           if (typeof error === "boolean" && error)
                               options.onFileSuccess(file, data);
                           else {
                               if (typeof error === 'string') {
                                   showError(error);
                               }
                               options.onFileError(file, data);
                           }
                       },
                       xhr         : function () {  // custom xhr
                           myXhr = $.ajaxSettings.xhr();
                           if (myXhr.upload) { // check if upload property exists
                               myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // for handling the progress of the upload
                           }
                           return myXhr;
                       },
                       error       : function (xhr, error) {
                           $('.pekecontainer .progress').remove();
                           error = false;
                           options.onFileError(file, error);
                       },
                       cache       : false,
                       contentType : false,
                       processData : false
                   });
            return error;
        }

        //Function that updates bars progress
        function progressHandlingFunction(e) {
            if (e.lengthComputable) {
                var total = e.total;
                var loaded = e.loaded;
                if (options.showFilename == true) {
                    obj.next('a').next('div').find('.file').first().find('.filename:first').text(file.name);
                }
                if (options.showPercent == true) {
                    var percent = Number(((e.loaded * 100) / e.total).toFixed(2));
                    obj.next('a').next('div').find('.file').first().find('.pekeup-progress-bar:first').width(percent + '%');
                }
                obj.next('a').next('div').find('.file').first().find('.pekeup-progress-bar:first').html('<center>' + percent + "%</center>");
            }
        }

        //Validate master
        function validateresult() {
            var canUpload = true;

            if (options.allowedExtensions != "") {
                var validationresult = validateExtension();
                if (validationresult.length) {
                    canUpload = false;
                    showError(options.invalidExtError + '<br/>' + validationresult.join('<br/>'));
                }
            }
            if (options.maxSize > 0) {
                var validationresult = validateSize();
                if (validationresult.length) {
                    canUpload = false;
                    showError(options.sizeError + '<br/>' + validationresult.join('<br/>'));
                    options.onFileError(file, options.sizeError);
                }
            }
            return canUpload;
        }

        function showError(message) {
            if ((options.theme == "bootstrap") && (options.showErrorAlerts == true)) {
                obj.next('a').next('div').prepend('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button> ' + message + '</div>');
                bootstrapclosenotification();
            }
            if ((options.theme == "custom") && (options.showErrorAlerts == true)) {
                obj.next('a').next('div').prepend('<div class="alert-pekeupload"><button type="button" class="close" data-dismiss="alert">&times;</button> ' + message + '</div>');
                customclosenotification();
            }

        }

        //Validate extension of file
        function validateExtension() {
            var ext = '';
            var allowed = options.allowedExtensions.split("|");
            var resultValidate = [];
            var deleteElem = [];
            var i;

            for (i in file) {
                if (file.hasOwnProperty(i)) {
                    ext = file[i].name.split('.').pop().toLowerCase();
                    if ($.inArray(ext, allowed) == -1) {
                        deleteElem.push(i);
                        resultValidate.push(file[i].name);
                    }
                }
            }

            for (i = 0; i < deleteElem.length; i++) {
                delete file[deleteElem[i]];
            }

            return resultValidate;
        }

        //Validate Size of the file
        function validateSize() {
            var resultValidate = new Array();
            for (var i in file) {
                if (file.hasOwnProperty(i)) {
                    if (file[i].size > options.maxSize) {
                        resultValidate.push(file[i].name);
                    }
                }
            }

            return resultValidate;
        }

        //Function that allows close alerts of bootstap
        function bootstrapclosenotification() {
            obj.next('a').next('div').find('.alert-error').click(function () {
                $(this).remove();
            });
        }

        function customclosenotification() {
            obj.next('a').next('div').find('.alert-pekeupload').click(function () {
                $(this).remove();
            });
        }
    };

})(jQuery);