function showError(errors) {
    checkValidate.init(errors);
}


function validateData(form, submit) {
    var action = $(form).attr('action');
    var data = $(form).serialize();

    if (checkValidate.send)
        return false;

    checkValidate.send = true;

    $.ajax({
               url      : action,
               type     : 'POST',
               dataType : 'JSON',
               data     : data,
               success  : function (data) {
                   checkValidate.send = false;
                   showError(data);
               },
               error    : function (xhr, err) {
                   checkValidate.send = false;
                   console.log(xhr, err);
               }
           });
}

var checkValidate = {
    data     : null,
    send     : false,
    hasError : false,


    init : function (data) {
        checkValidate.hasError = false;
        checkValidate.data = data;
        checkValidate.checkPhotos(data.photo);
        checkValidate.checkGood(data.good);
        if (!checkValidate.hasError) {
            var form = $($('form')[0]);
            form.off('submit');
            form.submit();
        }
        checkValidate.send = false;
    },

    getLenObj : function (obj) {
        if (typeof (obj) == 'undefined') {
            return 0;
        }

        if (typeof(obj) !== 'object') {
            return 0;
        }

        if (typeof obj.length !== 'undefined') {
            return obj.length;
        }

        if (typeof Object.keys !== 'undefined')
            return Object.keys(obj).length;

        cnt = 0;
        for (var i in obj) {
            if (obj.hasOwnProperty(i))
                cnt++;
        }

        return cnt;
    },

    setHtml : function (id, html) {
        if ($(id).size()) {
            checkValidate.hasError = true;
            $(id).html(html);
            $(id).parent().find('input, textarea, select, img').each(function () {
                $(this).addClass('redBorder');
            });
        }
    },

    checkPhoto : function (photo, index) {
        for (var i in photo) {
            if (photo.hasOwnProperty(i)) {
                for (var j in photo[i]) {
                    if (typeof(photo[i][j]) !== 'object') {
                        var id = '#photo_' + index + '_' + i;
                        checkValidate.setHtml(id, photo[i].join('<br/>'));
                    }
                }
            }
        }

        if (typeof photo.description !== 'undefined') {
            checkValidate.checkDescription(photo.description, index);
        }

        if (typeof photo.color !== 'undefined') {
            checkValidate.checkColor(photo.color, index);
        }
        if (typeof photo.size !== 'undefined') {
            checkValidate.checkSizes(photo.size, index);
        }
    },

    checkDescription : function (description, numPhoto) {
        for (var i in description) {
            if (description.hasOwnProperty(i)) {
                var id = '#photo_' + numPhoto + '_description_' + i;
                checkValidate.setHtml(id, description[i].join('<br/>'));
            }
        }
    },

    checkColor : function (color, numPhoto) {
        for (var i in color) {
            if (color.hasOwnProperty(i)) {
                var id = '#photo_' + numPhoto + '_color_' + i;
                checkValidate.setHtml(id, color[i]);
            }
        }
    },

    checkSizes : function (size, numPhoto) {
        for (var i in size) {
            if (size.hasOwnProperty(i)) {
                var id = '#photo_' + numPhoto + '_size_' + i;
                console.log(id, size[i]);
                if (typeof size[i].GoodsSize_size !== "undefined")
                    checkValidate.setHtml(id, size[i].GoodsSize_size[0]);
            }
        }
    },

    checkPhotos : function (photos) {
        for (var i in photos) {
            if (photos.hasOwnProperty(i))
                checkValidate.checkPhoto(photos[i], i);
        }
    },

    checkGood : function (good) {
        for (var i in good) {
            if (good.hasOwnProperty(i)) {
                var id = '#good_' + i;
                checkValidate.setHtml(id, good[i]);
            }
        }
    }

};

var showValidateResult = {
    /**
     * Валидация формы
     */
    bindEventValidateForm : function () {
        showValidateResult.bindErrorShow($('form').eq(0).find('input, select, textarea, img'));
    },

    /**
     * Привязка событий отображения ошибок валидации формы
     * @param elem
     */
    bindErrorShow : function (elem) {
        elem.each(function () {
            $(this).removeClass('redBorder').off('mouseout').on('mouseout', function (event) {
                event.preventDefault();
                showValidateResult.mouseOutFormElem(this);
                return false;
            }).off('mousemove').on('mousemove', function (event) {
                event.preventDefault();
                showValidateResult.mouseMoveFromElem(this);
                return false;
            })
        });
    },

    /**
     * Скрытие всплывающего окна информации об ошибке
     * @param elem
     */
    mouseOutFormElem : function (elem) {
        $(elem).parent().find('.showErr').removeClass('showErr').addClass('errors');
    },

    /**
     * Открытие всплывающего окна информации об ошибке
     * @param elem
     */
    mouseMoveFromElem : function (elem) {

        var divErr = $(elem).parent().find('.divError');
        if (!divErr.html()) return false;

        var offset = $(elem).offset();

        divErr.css({top : event.pageY - offset.top + 20, left : event.pageX - offset.left + 20}).removeClass('errors').addClass('showErr');

    }
};