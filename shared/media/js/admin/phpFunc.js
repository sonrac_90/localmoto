/**
 * @example basename('/www/site/home.htm', '.htm'); - return 'home'
 * @example basename('/www/site/home.htm'); - return 'home.htm'
 * @param path
 * @param suffix
 * @returns {XML|string|void|*}
 */
function basename(path, suffix) {	// Returns filename component of path
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ash Searle (http://hexmen.com/blog/)
    // +   improved by: Lincoln Ramsay
    // +   improved by: djmix

    var b = path.replace(/^.*[\/\\]/g, '');
    if (typeof(suffix) == 'string' && b.substr(b.length - suffix.length) == suffix) {
        b = b.substr(0, b.length - suffix.length);
    }
    return b;
}

function pathinfo(path, options) {
    //  discuss at: http://phpjs.org/functions/pathinfo/
    // original by: Nate
    //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Brett Zamir (http://brett-zamir.me)
    //    input by: Timo
    //        note: Inspired by actual PHP source: php5-5.2.6/ext/standard/string.c line #1559
    //        note: The way the bitwise arguments are handled allows for greater flexibility
    //        note: & compatability. We might even standardize this code and use a similar approach for
    //        note: other bitwise PHP functions
    //        note: php.js tries very hard to stay away from a core.js file with global dependencies, because we like
    //        note: that you can just take a couple of functions and be on your way.
    //        note: But by way we implemented this function, if you want you can still declare the PATHINFO_*
    //        note: yourself, and then you can use: pathinfo('/www/index.html', PATHINFO_BASENAME | PATHINFO_EXTENSION);
    //        note: which makes it fully compliant with PHP syntax.
    //  depends on: basename
    //   example 1: pathinfo('/www/htdocs/index.html', 1);
    //   returns 1: '/www/htdocs'
    //   example 2: pathinfo('/www/htdocs/index.html', 'PATHINFO_BASENAME');
    //   returns 2: 'index.html'
    //   example 3: pathinfo('/www/htdocs/index.html', 'PATHINFO_EXTENSION');
    //   returns 3: 'html'
    //   example 4: pathinfo('/www/htdocs/index.html', 'PATHINFO_FILENAME');
    //   returns 4: 'index'
    //   example 5: pathinfo('/www/htdocs/index.html', 2 | 4);
    //   returns 5: {basename: 'index.html', extension: 'html'}
    //   example 6: pathinfo('/www/htdocs/index.html', 'PATHINFO_ALL');
    //   returns 6: {dirname: '/www/htdocs', basename: 'index.html', extension: 'html', filename: 'index'}
    //   example 7: pathinfo('/www/htdocs/index.html');
    //   returns 7: {dirname: '/www/htdocs', basename: 'index.html', extension: 'html', filename: 'index'}

    var opt = '',
        optName = '',
        optTemp = 0,
        tmp_arr = {},
        cnt = 0,
        i = 0;
    var have_basename = false,
        have_extension = false,
        have_filename = false;

    // Input defaulting & sanitation
    if (!path) {
        return false;
    }
    if (!options) {
        options = 'PATHINFO_ALL';
    }

    // Initialize binary arguments. Both the string & integer (constant) input is
    // allowed
    var OPTS = {
        'PATHINFO_DIRNAME'   : 1,
        'PATHINFO_BASENAME'  : 2,
        'PATHINFO_EXTENSION' : 4,
        'PATHINFO_FILENAME'  : 8,
        'PATHINFO_ALL'       : 0
    };
    // PATHINFO_ALL sums up all previously defined PATHINFOs (could just pre-calculate)
    for (optName in OPTS) {
        OPTS.PATHINFO_ALL = OPTS.PATHINFO_ALL | OPTS[optName];
    }
    if (typeof options !== 'number') { // Allow for a single string or an array of string flags
        options = [].concat(options);
        for (i = 0; i < options.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[options[i]]) {
                optTemp = optTemp | OPTS[options[i]];
            }
        }
        options = optTemp;
    }

    // Internal Functions
    var __getExt = function (path) {
        var str = path + '';
        var dotP = str.lastIndexOf('.') + 1;
        return !dotP ? false : dotP !== str.length ? str.substr(dotP) : '';
    };

    // Gather path infos
    if (options & OPTS.PATHINFO_DIRNAME) {
        var dirName = path.replace(/\\/g, '/')
            .replace(/\/[^\/]*\/?$/, ''); // dirname
        tmp_arr.dirname = dirName === path ? '.' : dirName;
    }

    if (options & OPTS.PATHINFO_BASENAME) {
        if (false === have_basename) {
            have_basename = this.basename(path);
        }
        tmp_arr.basename = have_basename;
    }

    if (options & OPTS.PATHINFO_EXTENSION) {
        if (false === have_basename) {
            have_basename = this.basename(path);
        }
        if (false === have_extension) {
            have_extension = __getExt(have_basename);
        }
        if (false !== have_extension) {
            tmp_arr.extension = have_extension;
        }
    }

    if (options & OPTS.PATHINFO_FILENAME) {
        if (false === have_basename) {
            have_basename = this.basename(path);
        }
        if (false === have_extension) {
            have_extension = __getExt(have_basename);
        }
        if (false === have_filename) {
            have_filename = have_basename.slice(0, have_basename.length - (have_extension ? have_extension.length + 1 :
                have_extension === false ? 0 : 1));
        }

        tmp_arr.filename = have_filename;
    }

    // If array contains only 1 element: return string
    cnt = 0;
    for (opt in tmp_arr) {
        cnt++;
    }
    if (cnt == 1) {
        return tmp_arr[opt];
    }

    // Return full-blown array
    return tmp_arr;
}

function dirname(path) {
    //  discuss at: http://phpjs.org/functions/dirname/
    // original by: Ozh
    // improved by: XoraX (http://www.xorax.info)
    //   example 1: dirname('/etc/passwd');
    //   returns 1: '/etc'
    //   example 2: dirname('c:/Temp/x');
    //   returns 2: 'c:/Temp'
    //   example 3: dirname('/dir/test/');
    //   returns 3: '/dir'

    return path.replace(/\\/g, '/')
        .replace(/\/[^\/]*\/?$/, '');
}

function realpath(path) {
    //  discuss at: http://phpjs.org/functions/realpath/
    // original by: mk.keck
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //        note: Returned path is an url like e.g. 'http://yourhost.tld/path/'
    //   example 1: realpath('../.././_supporters/pj_test_supportfile_1.htm');
    //   returns 1: 'file:/home/kevin/workspace/_supporters/pj_test_supportfile_1.htm'

    var p = 0,
        arr = [];
    /* Save the root, if not given */
    var r = this.window.location.href;
    /* Avoid input failures */
    path = (path + '')
        .replace('\\', '/');
    /* Check if there's a port in path (like 'http://') */
    if (path.indexOf('://') !== -1) {
        p = 1;
    }
    /* Ok, there's not a port in path, so let's take the root */
    if (!p) {
        path = r.substring(0, r.lastIndexOf('/') + 1) + path;
    }
    /* Explode the given path into it's parts */
    arr = path.split('/');
    /* The path is an array now */
    path = [];
    /* Foreach part make a check */
    for (var k in arr) { /* This is'nt really interesting */
        if (arr[k] == '.') {
            continue;
        }
        /* This reduces the realpath */
        if (arr[k] == '..') {
            /* But only if there more than 3 parts in the path-array.
             * The first three parts are for the uri */
            if (path.length > 3) {
                path.pop();
            }
        } /* This adds parts to the realpath */
        else {
            /* But only if the part is not empty or the uri
             * (the first three parts ar needed) was not
             * saved */
            if ((path.length < 2) || (arr[k] !== '')) {
                path.push(arr[k]);
            }
        }
    }
    /* Returns the absloute path as a string */
    return path.join('/');
}

/**
 * Преобразование number к числу или возврат 0 если это не число
 * @param number
 * @returns {*}
 */
function intval(number) {
    if (isNaN(parseInt(number)))
        return 0;
    return parseInt(number);
}

function count(mixed_var, mode, withKey) {
    var key, cnt = 0;

    if (mixed_var === null || typeof mixed_var === 'undefined') {
        return 0;
    } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
        return 1;
    }

    if (mode === 'COUNT_RECURSIVE') {
        mode = 1;
    }
    if (mode != 1) {
        mode = 0;
    }

    for (key in mixed_var) {
        if (mixed_var.hasOwnProperty(key)) {
            if (withKey)
                cnt++;
            if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor ===
                Object)) {
                cnt += this.count(mixed_var[key], 1);
            } else if (!withKey)
                cnt++;
        }
    }

    return cnt;
}