$(document).ready(function () {
    $('button#addAddress').on('click', function (event) {
        event.preventDefault();

        var fields = Array();

        fields['country'] = "Страна";
        fields['city'] = "Город";
        fields['street'] = "Улица";
        fields['department'] = "Дом";
        fields['c'] = "Квартира";

        var countAddr = getNumber();

        var field = $('<tr/>').append($('<td/>').text(countAddr));

        fieldSet = $('<div/>').addClass('nextAddress');

        for (var i in fields) {
            $(field).append($('<td/>').append($('<input/>', {type : 'text', value : '', maxLength : 255, id : 'Address_' + countAddr + '_' + i,
                            name            : 'Address[' + countAddr + '][' + i + ']'}))
                .append($('<div/>', {'style' : 'display: none;', id : "Address_" + i + "_em",
                            name             : 'Address[' + countAddr + '][' + i + ']'})
                            .addClass('errorMessage')));
        }

        $(field)
            .append($('<td/>')
                        .append($('<div/>').attr('data', "new" + countAddr).on('click', function (event) {
                                        event.preventDefault();
                                        deleteIconEvent($(this).attr('data'), $(this).parent());
                                        return false;
                                    }).addClass('deleteIcon')
                            .append($('<img/>', {src : adminUrl + "../shared/media/css/admin/images/admin/delete.png"})))
        );

        var id = userID;
        if (isNaN(parseInt(id)))
            id = '';
        $(field).append($('<input/>', {type : 'hidden', name : 'Address[' + countAddr + '][user_id]', id : 'Address_' + countAddr + '_user_id', value : id}));

        $('#user_update').find('table tbody').eq(0).append(field);

        formControl();

        return false;
    });

    $('.deleteIcon').on('click', function () {
        var id = $(this).attr('number');
        deleteIconEvent(id, $(this).parent());
    });
});

function deleteIconEvent(id, elem) {
    var promt = confirm('Удалить адрес?');
    if (!promt) {
        return false;
    }

    $(elem).parent().remove();
    if (!isNaN(parseInt(id)) && parseInt(id) > 0)
        $.post(adminUrl + 'users/deleteAddress/id/' + id, {}, function (data) {

        });
}

function findNumber(num) {
    var fNum = false;
    $('div[id^=addrBlock]').each(function () {
        var htmlText = $.trim($(this).find('.row').eq(0).text());
        var text = htmlText.replace('Адрес ', '');
        fNum = (text == num) ? true : fNum;
    });
    return fNum;
}

function getNumber() {
    var result = $.trim($('.numRowAddress').eq(-1).text());
    return (parseInt(result) ? parseInt(result) + 1 : 1);
}