(function () {

    var fixHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    };

    $.fn.extend({
                    sortableGrid : sortableGridTable
                });

    function sortableGridTable() {
        var ID = '#' + this.attr('id');
        if ($(ID + ' table.table tbody').size()) {

            $(ID + ' table.table tbody').sortable({
                                                      forcePlaceholderSize : true,
                                                      forceHelperSize      : true,
                                                      items                : 'tr',
                                                      update               : function () {
                                                          serial = $(ID + ' table.table tbody')
                                                              .sortable('serialize', {
                                                                            key       : 'items[]',
                                                                            attribute : 'class'
                                                                        });
                                                          $.ajax({
                                                                     'url'     : adminUrl + controller + '/changeIndex',
                                                                     'type'    : 'post',
                                                                     'data'    : serial,
                                                                     'success' : function (data) {
                                                                     },
                                                                     'error'   : function (request, status, error) {
                                                                         alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                                                                     }
                                                                 });
                                                      },
                                                      helper               : fixHelper
                                                  }).disableSelection();
        }
    }
})(jQuery);
