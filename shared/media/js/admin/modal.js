function addModal(elem, clickCallBack, addParam, succText) {

    if (typeof succText === "undefined")
        succText = 'Добавить';

    if (typeof addParam !== 'object')
        addParam = {};

    var defaultParam = {
        width         : 400,
        height        : 170,
        modal         : true,
        closeOnEscape : true,
//        hide          : {
//            effect   : 'explode',
//            duration : 500
//        },
//        show          : {
//            effect   : 'explode',
//            duration : 500
//        },
        buttons       : [
            {
                text    : 'Закрыть',
                click   : function (event) {
                    event.preventDefault();
                    $(elem).dialog('close');
                    return false;
                },
                'class' : 'btn btn-danger oneButton'
            },
            {
                text    : succText,
                click   : clickCallBack,
                'class' : 'btn btn-success twoButton'
            }
        ]
    };

    $.fn.extend(defaultParam, addParam);

    $(elem).dialog(defaultParam);
}

function updateYiiList(selector, callback) {
    $.fn.yiiListView.update(selector, {complete : function (jqXHR, status) {
        callback();
    }});
}