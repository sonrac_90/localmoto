$('document').ready(function () {
    bindProducerPage();
});

function bindProducerPage() {
    producerEdit('span[class^=edit]');
    producerDelete('span[class^=delete]');
    addProducer();
}

function producerDelete(selector) {
    $(selector).each(function () {
        $(this).off('click').on('click', function (event) {
            event.preventDefault();
            var elem = $('.producerList').has(this);
            var promtOK = confirm('Вы действительно хотите удалить производителя ' + $(elem).text());
            if (promtOK) {
                var id = $(this).attr('class').replace('delete', '');
                $.ajax({
                           url     : adminUrl + "/" + controller + "/delete/id/" + id,
                           success : function (data) {
                               updateYiiList('producerList', bindProducerPage);
                               return false;
                           }
                       });
                return false;
            }
        })
    });
    changeElem();
}

function producerEdit(selector) {
    $(selector).off('click').on('click', function (event) {
        event.preventDefault();

        var id = $(this).attr('class').replace('edit', '');

        var value = $(this).parent().parent().text().trim();

        var oldVal = value;

        var self = this;

        function editModal(event) {
            event.preventDefault();
            var inpVal = $(this).parent().find('input').val();
            var action = adminUrl + "/" + controller + "/update/id/" + id;
            if (!inpVal || inpVal == oldVal) {
                alert(((!inpVal) ? 'Введите' : 'Измените') + ' фирму-производитель!');
                return false;
            }

            var modal = this;

            $.ajax({
                       url     : action,
                       data    : 'id=' + id + '&newName=' + inpVal,
                       type    : 'POST',
                       success : function (data) {
                           var tmpData = '';
                           try {
                               tmpData = JSON.parse(data);
                           } catch (e) {
                           }

                           if (typeof tmpData === "object") {
                               alert(tmpData.join("\n"));
                               return;
                           }
                           $(modal).dialog('close');
                           $(modal).html('');
                           $(modal).attr('class', '');
                           $('.ui-dialog').remove();
                           updateYiiList('producerList', bindProducerPage);
                       },
                       error   : function (xhr, err) {
                           console.log(xhr, err);
                       }
                   });

            return false;
        }

        var modal = $('#modalProducer');
        modal.empty();
        modal.append($('<input/>').val(value));

        var param = {
            title : 'Редактирование производителя'
        };

        addModal(modal, editModal, param, 'Редактировать');
    });
}


function addProducer() {
    addEventProducer('#add');
}

function addEventProducer(selector) {
    $(selector).off('click').on('click', function (event) {
        event.preventDefault();

        var id = $(this).attr('class').replace('edit', '');

        var modal = $('#modalProducer');
        modal.empty();
        modal.append($('<input/>').val(''));

        var param = {
            'title' : 'Добавление производителя'

        };

        function saveProducer(event) {
            event.preventDefault();
            var inpVal = $(this).parent().find('input').val();
            var action = adminUrl + "/" + controller + "/add";
            if (!inpVal) {
                alert('Введите фирму-производитель!');
                return false;
            }

            var modal = this;

            $.ajax({
                       url     : action,
                       data    : 'id=' + id + '&newName=' + inpVal,
                       type    : 'POST',
                       success : function (data) {
                           var tmpData = '';
                           try {
                               tmpData = JSON.parse(data);
                           } catch (e) {
                           }

                           if (typeof tmpData['error'] !== "undefined") {
                               alert('Такой производитель уже существует');
                               return;
                           }

                           $(modal).dialog('close');
                           $(modal).html('');
                           $(modal).attr('class', '');
                           updateYiiList('producerList', bindProducerPage);
                           $('.ui-dialog').remove();
                       },
                       error   : function (xhr, err) {
                           console.log(xhr, err);
                       }
                   });

        }

        addModal(modal, saveProducer, param);
    });
    changeElem();
}

function changeElem () {
    var cnt = 0;
    $('.producerList').each(function () {
        $(this).removeClass('lodd');

        if ((cnt % 2) == 0) {
            $(this).addClass('lodd');
        }
        cnt++;
    })
}