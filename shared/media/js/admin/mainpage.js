$(document).ready(function () {

    function callBackSuccess(file, data) {
        var divIMG = $('.images').eq(0);
        if (!divIMG.find('img').size())
            divIMG.empty();

        var divDelete = $('<div/>').addClass('deleteImageSlider').html('Удалить');

        $(divIMG)
            .append($('<div/>').addClass('nextImage').attr({'data' : data.id, 'data-key' : data.position})
                        .append($('<img/>', {src : data.image_url, width : 100, height : 100})).attr('data', data.id)
                                .append(divDelete)
        );

        deleteOver();
        deleteSlider();
    }


    deleteSlider();
    deleteOver();

    pekeUploadEvent(adminUrl + controller + '/uploadSlider', $('#addImage'), '', callBackSuccess, 'Добавить изображение');

    if (typeof(CKEDITOR) !== 'undefined') {
        for (var name in CKEDITOR.instances) {
            CKEDITOR.instances[name].on("instanceReady", function () {
                // Set keyup event
                this.document.on("keyup", updateValue);
                // Set paste event
                this.document.on("paste", updateValue);
            });

            function updateValue() {
                CKEDITOR.instances[name].updateElement();
                $('#editor1').trigger('keyup');
            }
        }
    }

    $('.images').sortable({
                              update : function () {
                                  var id = {};
                                  var cnt = 0;
                                  $('.images').find('.nextImage').each(function () {
                                      cnt++;
                                      id[cnt] = $(this).attr('data');
                                      $(this).attr('data-key', cnt);
                                  });
                                  $.ajax({
                                             'url'     : adminUrl + controller + '/changeIndex',
                                             'type'    : 'post',
                                             'data'    : 'id=' + JSON.stringify(id),
                                             'success' : function (data) {
                                             },
                                             'error'   : function (request, status, error) {
                                                 alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
                                             }
                                         });
                              }
                          });

    CKEDITOR.replace('blockMain', {
        contestLanguage          : 'ru',
        contentsCss              : [
            '/shared/media/js/libs/ckeditor/contents.css',
            '/shared/media/css/base.css',
            '/shared/media/css/style.css',
            '/shared/media/css/article.css'
        ],
        scayt_sLang              : 'ru_RU',
        language                 : 'ru',
        contentsLanguage         : 'ru',
        charset                  : 'utf-8',
        templates_replaceContent : false,
//                autoUpdateElement        : false,
//                autoUpdateElementJquery  : false,
        autocomplete             : true,
        allowedContent           : true,
        entities                 : false,
        entities_latin           : false,
        entities_greek           : false,
        entities_additional      : false,
        basicEntities            : false,
        height                   : '500px',
        width                    : '990px'
    });
});


function deleteOver (selector) {
    if (typeof selector === "undefined")
        selector = '.nextImage';
    $(selector).off('mouseout').on('mouseout', function (event) {
        event.preventDefault();
        $(this).find('.deleteImageSlider').hide();
        return false;
    }).off('mouseover').on('mouseover', function (event) {
        event.preventDefault();
        $(this).find('.deleteImageSlider').show();
        return false;
    });
}

function deleteSlider (selector) {

    if (typeof selector === "undefined")
        selector = '.deleteImageSlider';

    $(selector).off('click').on('click', function (event) {
        event.preventDefault();
        var promt = confirm('Вы действительно хотите удалить изображение?');
        if (promt) {
            var parent = $(this).parent();

            var id = parent.attr('data');
            var self = this;
            $.ajax({
                       url     : adminUrl + controller + '/deleteSliderPhoto/id/' + id,
                       success : function (data) {
                           if (!data)
                               $(self).parent().remove();

                           if (!$('.slider img').size()) {
                               $('.slider').eq(0).find('.images').html('Нет изображений в слайдере');
                           }
                       },
                       error   : function (xhr, error) {
                           console.log(xhr, error);
                       }
                   })
        }
        return false;
    });

}