$(document).ready(function () {
    formControl();
    $('body').on('DOMSubtreeModified', function (event) {
        formControl();
    });
});

function formControl() {
    $('input, textarea, select').each(function () {
        if ($(this).attr('type') !== 'submit' && this.id != 'blockMain') {
            $(this).addClass('form-control');
            if (document.location.href.indexOf('goods') != -1) {
                var ins = $('ins');
                if (ins.size()) {
                    ins.remove();
                    $('div.icheckbox_minimal').hide();
                }
            }
        }
    });
}