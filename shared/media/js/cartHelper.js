$(document).ready(function () {

    var pers_bd_input = jQuery('#personal-popup-day');
    var pers_bm_input = jQuery('#personal-popup-month');
    var pers_by_input = jQuery('#personal-popup-year');

    var date_input = jQuery('#birthday_date_field');

    pers_bd_input.keyup(function () {
        set_date_1();
    });

    pers_bm_input.keyup(function () {
        set_date_1();
    });
    pers_by_input.keyup(function () {
        set_date_1();
    });

    function set_date_1() {
        var date = pers_bd_input.val() + '-' + pers_bm_input.val() + '-' + pers_by_input.val();
        valid_and_safe_date(date);

    }

    function valid_and_safe_date(date) {
        var sl = date.length;
        if ((date[0] == '-') || (date[sl - 1] == '-')) date_input.val('');
        else date_input.val(date);

    }

    $('#add_address').on('click', function (event) {
        event.preventDefault();

        $('#personal_popup.address').eq(0).css({top : '0px', bottom : '-27px!important'});
        open_personal_popup(4);
        return false;
    });
});

function validateAddress(data) {
    try {
        for (var i in data) {
            if (data.hasOwnProperty(i)) {
                if (data[i].value == '') {
                    return false;
                }
            }
        }
        return true;
    } catch (e) {
    }

    return false;
}

function getNewAddr(id, data) {
    return '<div class = "op_address_block op_selected" id = "attrID_' + id + '">\
    <div class = "op_address_right">\
    <a class = "small_button op_address_check" href = "javascript:void(0)"\
    onclick = "select_address(this)">\
        <span></span>\
    </a>\
    <a class = "small_button" href = "javascript: void(0);"> Редактировать</a>\
    </div>\
        <div class = "op_title"> <?= intval($numAddr + 1) ?> адрес</div>\
        <div class = "op_value" id="countryAddr">' + data[0].value + '</div>\
        <div class = "op_value" id="cityAddr">' + data[1].value + '</div>\
        <div class = "op_value" id="streetAddr">' + data[2].value + '</div>\
        <div class = "op_value" id="houseAddr">' + data[3].value + '</div>\
        <div class = "op_value" id="departmentAddr">' + data[4].value + '</div>\
    </div>';

}

function saveAddress(self) {
    $('[id^=textError]').remove();
    var form = $('#addressUpdate');
    var dataForm = form.serialize();
    var idAddr = intval($('#personal_popup.address').eq(0).attr('data'));
    dataForm += "&id=" + idAddr;
    var dataSerialize = form.serializeArray();

    if (validateAddress(dataSerialize)) {
        $.ajax({
                   url      : baseUrl + 'buy/addAddress',
                   data     : dataForm,
                   type     : 'POST',
                   dataType : 'JSON',
                   success  : function (data) {
                       console.log(data);
                       if (data.error == 0) {
                           $('.op_selected').removeClass('op_selected');
                           if (!idAddr) {
                               $('#addrBlock').find('.popup_address_begin').eq(-1).before(getNewAddr(data.id, dataSerialize));
                           } else {
                               var block = $('#attrID_' + data.id);
                               block.find('[id^=idAddr]').eq(0).text(data.id);
                               block.find('[id^=countryAddr]').eq(0).text(data.country);
                               block.find('[id^=cityAddr]').eq(0).text(data.city);
                               block.find('[id^=streetAddr]').eq(0).text(data.street);
                               block.find('[id^=houseAddr]').eq(0).text(data.house);
                               console.log(block, block.find('[id^=houseAddr]').get(0), data.house);
                               block.find('[id^=departmentAddr]').eq(0).text(data.department);
                           }
                           close_personal_popup();
                       } else {
                           $(self).parent().prepend($('<div/>', {id: 'textError', style : 'color: red'}).html(data.textError));
                       }
                   }
               });
    } else {
        $(self).parent().prepend($('<div/>', {id: 'textError', style: 'color: red'}).html('Заполните все поля'));
    }

    return false;
}

// Заказ товара
function createOrder() {
    $('[id^=textError]').remove();

    var getOrder = validateOrder();

    if (!getOrder.text) { // Регистрирую покупку
        $.ajax ({
            url : baseUrl + 'buy/addOrder',
            type: 'POST',
            data: 'userInfo=' + JSON.stringify(getOrder.userInfo) + '&address=' + JSON.stringify(getOrder.address),
            success : function (data) {
                var biker_text_num = Math.floor(Math.random() * bikers_excerptions.length);
                var divMain = $('<div/>', {id: 'order_process'})
                                .append($('<div/>').addClass('op_main')
                                      .append($('<div/>').addClass('op_complete_block')
                                             .append($('<div/>').addClass('block-1').html(bikers_excerptions[biker_text_num]['text']))
                                             .append($('<div/>').addClass('block-2').html((bikers_excerptions[biker_text_num]['biker'] !== '') ? '&mdash; ' + bikers_excerptions[biker_text_num]['biker'] : ''))
                                                                                                                                                       .append($('<div/>').addClass('block-3').html('Ваш заказ оформлен<br>спасибо, что вы с нами<br>').append($('<a/>', {href: baseUrl}).html('Продолжить')))
                                      )
                                ).append($('<div/>').addClass('clearfix')
                );

                $('div.main').eq(0).html('').append(divMain);
            },
            error: function (xhr, error) {
                console.log(xhr, error);
            }
        });
        return false;
    }

    $('.checkout-button-top').append($('<div/>', {id: 'textError', style: 'color: red;'}).html(getOrder.text));
    return false;

}

function validateOrder() {
    // Проверяю заполненность полей информации о пользователе
    var textMessage = {
        user_firstname : 'Имя',
        user_lastname  : 'Фамилия',
        user_email     : 'Email',
        user_birthday  : 'Дата рождения',
        user_phone     : 'Телефон'
    };

    var message = '';

    var userInfo = {
        user_firstname : $.trim($('#pinfo_name .op_value').eq(0).html()),
        user_lastname  : $.trim($('#pinfo_surname .op_value').eq(0).html()),
        user_email     : $.trim($('#pinfo_email .op_value').eq(0).html()),
        user_birthday  : $.trim($('#pinfo_birthdate .op_value').eq(0).html()),
        user_phone     : $.trim($('#pinfo_phone .op_value').eq(0).html())
    };

    for (var i in userInfo) {
        if (userInfo.hasOwnProperty(i)) {
            if (userInfo[i] == '' && typeof textMessage[i] === 'string') {
                if (!message)
                    message += "Не заполнено обязательные поля информации о пользователе:<br/>" + textMessage[i];
                else
                    message += ", " + textMessage[i];
            }
        }
    }

    var addressBlock = $('.op_selected').eq(0);

    var address = {pickUp : false};
    if (addressBlock.size()) {

        if (!addressBlock.hasClass('popup_address_begin')) { // Проверяю наличие адресса
            address = {
                pickUp     : false,
                country    : $.trim(addressBlock.find('div[id^=countryAddr]').eq(0).text()),
                city       : $.trim(addressBlock.find('div[id^=cityAddr]').eq(0).text()),
                street     : $.trim(addressBlock.find('div[id^=streetAddr]').eq(0).text()),
                department : $.trim(addressBlock.find('div[id^=departmentAddr]').eq(0).text())
            };

            var textMessageAddr = {
                country    : 'Страна и почтовый индекс',
                city       : 'Город',
                street     : 'Улица',
                house      : 'Дом',
                department : 'Номер дома'
            };
            var msgIs = false;
            for (i in address) {
                if (address.hasOwnProperty(i)) {
                    if (typeof textMessageAddr[i] == 'string' && address[i] == '') {
                        if (!msgIs) {
                            message += "<br/><br/>Неверно заполнены поля формы адреса доставки:<br/>" + textMessageAddr[i];
                        } else {
                            message += ", " + textMessageAddr[i];
                        }
                    }
                }
            }
        } else {
            address = {
                'pickUp' : true
            }
        }
    } else {
        message += ' Не выбрано ни одного адреса доставки!';
    }

    return {
        userInfo : userInfo,
        address  : address,
        text     : message
    };
}

function select_address(item) {
    jQuery('.op_selected').removeClass('op_selected');
//    $('.op_address_check').removeClass('op_address_check');
    jQuery(item).parent().parent().addClass('op_selected');
}


function myValidator(elem) {
    var input = $(f).find('input');
    var mainBlock = $('div.op_address_block').has(elem).eq(0);
    var validate = true;
    for (var i in input) {
        if (input.hasOwnProperty(i) && typeof (input[i]) === 'HTMLElement') {
            if (!$(input[i]).val()) {
                validate = false;
            }
        }
    }

    var title = {};

    if (validate) {
        var data = $(f).serialize();
        var dataJSON = $(f).serializeArray();

        var action = '';

        if (t == 'saveUser') {
            action = 'buy/updateUserInfo';
            data = {
                'first_name' : dataJSON[0].value,
                'last_name' : dataJSON[1].value,
                'phone' : dataJSON[3].value,
                'email' : dataJSON[2].value,
                'birthday' : dataJSON[4].value,
                'subscribe' : dataJSON[5].value
            };
            data = "userInfo=" + JSON.stringify(data)
            title = $('div.pc_personal .pc_block .value');
        } else if (t == 'saveAddress') {
            action = 'buy/updateAddress';
            if (id === 'new') {
                action = 'buy/addAddress';
            }

            data = {
                'country'    : $('#country_and_zip_' + id).val(),
                'city'       : $('#city_' + id).val(),
                'department' : $('#department_' + id).val(),
                'street'     : $('#street_' + id).val(),
                'house'      : $('#house_' + id).val()
            };

            if (id) {
                data['id'] = id;
            }
        }

        $.ajax({
                   url : baseUrl + action,
                   data: data,
                   type: 'POST',
                   success : function (dataSend) {
                       for (var i in dataJSON) {
                           if (dataJSON.hasOwnProperty(i)) {
                               if (typeof(title[i]) !== 'undefined') {
                                   $(title[i]).html(dataJSON[i].value);
                               }
                           }
                       }

                       if (id == 'new') {
                           var dataSending = dataSend;
                           if (typeof dataSend === 'string')
                               dataSending = JSON.parse(dataSend);

                           var addrNum = intval($('[id^=addressTitle]').eq(-1).find('div.title').eq(0).text()) + 1;

                           var div = $('<span/>', {id: 'addressTitle' + dataSending.id}).addClass('')
                               .append($('<span/>', {onclick: 'open_adress_popup(' + dataSending.id + ')'}).addClass('small_button').html('Редактировать'))
                               .append($('<span/>', {onclick: 'delete_adress(' + dataSending.id + ')'}).addClass('small_button').html('Удалить'))
                               .append($('<div>').addClass('title').html(addrNum + ' адрес'))
                               .append($('<div>').html(dataSending.country))
                               .append($('<div>').html(dataSending.city))
                               .append($('<div>').html(dataSending.street))
                               .append($('<div>').html(dataSending.department));
                           var input = $('div.pc_popup_block div.hidden_inputs').eq(0);

                           var idTitle = {
                               'country_and_zip_' : {name: 'country', rus: 'Страна и Zip код'},
                               'city_'            : {name: 'city', rus: 'Город'},
                               'street_and_house_': {name: 'street', rus: 'Улица и дом'},
                               'metro_'           : {name: 'department', rus: 'Станция метро'},
                           };

                           for (var i in idTitle) {
                               if (idTitle.hasOwnProperty(i)) {
                                   input.append($('<input/>', {
                                                    id: i + dataSending.id,
                                                    name: i + dataSending,
                                                    type: 'text',
                                                    'aria-invalid' : "false",
                                                    'placeholder'  : idTitle[i].rus + ' (' + addrNum + ' адрес)'
                                                }).val(dataSending[idTitle[i].name])
                                   );
                               }
                           }
                           $('#addressBlock').append(div);
                           $('#addressEmpty').removeClass('empty');

                           $('#add_adress_popup').hide();
                       }
                   },
                   error : function (xhr, error) {
                       console.log(xhr, error);
                   }
               });
        return true;
    } else {
        var msg = 'Не все поля заполнены';
        $(elem).parent().prepend($('<div/>', {id: 'textError', style: 'color: red;'}).html(msg));

    }
    return false;
}