//(function($){
var bikers_excerptions = phraseBikers;

jQuery(document).ready(function () {

    $('#recPass').on('click', function (event) {
        var self = this;
        $('[id^=textError]').remove();
        event.preventDefault();
        $.ajax({
            url : baseUrl + 'site/passRecovery',
            data: 'email=' + jQuery('#passremind-email').val(),
            type: 'POST',
            success : function (data) {
                if (data) {
                    $(self).parent().prepend($('<div/>', {id: 'textError', style: 'color: red;'}).html('Пароль выслан вам на email ' + data));
                } else {
                    $(self).parent().prepend($('<div/>', {id: 'textError', style: 'color: red;'}).html('Такого пользователя нет. Зарегистрируйтесь!'));
                }
            }
        })
        return false;
    });

    $('.subscribe form').each(function () {
        $(this).on('submit', function (event) {
            $('[id^=textError]').remove();
            var self = this;
            var input = $('#subscribe_input');

            if (!input.val() || input.css('color') == 'red' || input.css('color') == 'rgb(255, 0, 0)') {
                $(self).parent().prepend($('<div/>', {id: 'textError', style: 'color: red;'}).html('Неверно введен EMAIL!'));
                return false;
            }

            $.ajax({
                url: baseUrl + 'site/subscribe',
                data: 'email=' + input.val(),
                type: 'POST',
                dataType: 'JSON',
                success : function (data) {
                    if (data.error == 0)
                        input.val('Вы подписаны на обновления');
                    else
                        input.val('ОШИБКА!');
                }
           });

            return false;
        });
    });

    // Успешная регистрация
    jQuery('#auth-menu .reg-ok-button').click(function (e) {
        e.preventDefault();
        auth_menu_reg();
    });

    // Успешное напоминание пароля
    jQuery('#auth-menu .passremind-ok-button').click(function () {
        auth_menu_hide();
    });

    jQuery('#auth-menu .reg-button').click(function (e) {
        e.preventDefault();
        auth_menu_show('reg');
    });

    jQuery('#loginForm').submit(function (event) {
        event.preventDefault();
        return false;
    });

    jQuery('input.auth-ok-button').off('click').on('click', function (event) {
        var form = $('form').has(this);
        $.ajax ({
            url : baseUrl + 'site/login',
            dataType: 'JSON',
            'data' : form.serialize(),
            'type' : 'POST',
            success : function (data){
                if (data.error == 0) {
                    document.location.reload();
                }else {
                    $('#auth-menu [class^=block-]').addClass('t-invalid');
                }
            },
            error : function () {

            }
        });

        return false;
    });

    // Закрыть меню
    jQuery('#auth-menu .menu-slider, #auth-menu .close-button').click(function () {
        auth_menu_hide();
    });

    // После ввода каждой буквы
    jQuery('#auth-menu input[type=text], #auth-menu input[type=password]').keyup(function () {
        auth_menu_text_changed(this);
    });

    // Полное имя
    jQuery('#reg-name, #reg-surname').keyup(function () {
        jQuery('#reg-fullname').val(jQuery('#reg-name').val() + ' ' + jQuery('#reg-surname').val());
    });

    // Емэйл - это также логин и подтверждение емейла
    jQuery('#reg-email').keyup(function () {
        jQuery('#reg-username').val(jQuery('#reg-email').val());
        jQuery('#reg-email1').val(jQuery('#reg-email').val());
        jQuery('#reg-email2').val(jQuery('#reg-email').val());
    });

    // Placeholder for IE and Opera
    if (jQuery.browser.msie || jQuery.browser.opera) {
        jQuery('input[placeholder], textarea[placeholder]').placeholder();
    }

    jQuery('input[type=checkbox].jq-checkbox').styler();
});

jQuery(document).mouseup(function (e) {
    var container = jQuery("#auth-menu");
    if (e.target != container[0] && !container.has(e.target).length) {
        auth_menu_hide();
    }
});

function auth_menu_text_changed(obj) {
    if (jQuery(obj).val().length > 0) {
        jQuery(obj).addClass('text-present');
    } else {
        jQuery(obj).removeClass('text-present');
    }

    if (jQuery('#auth-menu').hasClass('auth')) {
        auth_menu_auth_valid(obj);
    } else if (jQuery('#auth-menu').hasClass('reg')) {
        auth_menu_reg_valid(obj);
    } else if (jQuery('#auth-menu').hasClass('pass-remind')) {
        auth_menu_passremind_valid(obj);
    }
}


function auth_menu_auth_valid(obj) {
    var valid = true;
    jQuery('#auth-menu .b-item').removeClass('t-invalid');
    if (typeof obj !== "undefined") {
        var text = $(obj).val();
        if (!$(obj).val().length || $(obj).css('color') == 'red' || $(obj).css('color') == 'rgb(255, 0, 0)') {
            $(obj).parent().addClass('t-invalid');
        } else {
            $(obj).parent().removeClass('t-invalid');
        }
    }

    if (jQuery('#auth-email').val().length == 0) {
        valid = false;
//        jQuery('#auth-email').parent().addClass('t-invalid');
    }
    if (jQuery('#auth-password').val().length == 0) {
        valid = false;
//        jQuery('#auth-password').parent().addClass('t-invalid');
    }

    if (valid) jQuery('#auth-menu .auth-ok-button').removeAttr('disabled'); else jQuery('#auth-menu .auth-ok-button').attr('disabled', 'disabled');
}

function auth_menu_reg_valid(obj) {
    var valid = true;
    jQuery('#auth-menu .b-item').removeClass('t-invalid');
    if (typeof obj !== "undefined") {
        if ($(obj).attr('id') == 'reg-password' || $(obj).attr('id') == 'reg-password-confirm') {
            if (jQuery('#reg-password').val().length == 0 || jQuery('#reg-password-confirm').val().length == 0 || jQuery('#reg-password').val() !== jQuery('#reg-password-confirm').val() ) {
                jQuery('#reg-password').parent().addClass('t-invalid');
                jQuery('#reg-password-confirm').parent().addClass('t-invalid');
            } else {
                jQuery('#reg-password').parent().removeClass('t-invalid');
                jQuery('#reg-password-confirm').parent().removeClass('t-invalid');
            }
        } else if (!$(obj).val().length || $(obj).css('color') == 'red' || $(obj).css('color') == 'rgb(255, 0, 0)') {
            $(obj).parent().addClass('t-invalid');
        } else {
            $(obj).parent().removeClass('t-invalid');
        }
    }

    if (jQuery('#reg-name').val().length == 0) {
        valid = false;
        jQuery('#reg-name').parent().addClass('t-invalid');
    }
    if (jQuery('#reg-surname').val().length == 0) {
        valid = false;
//        jQuery('#reg-surname').parent().addClass('t-invalid');
    }
    if (jQuery('#reg-email').val().length == 0) {
        valid = false;
//        jQuery('#reg-email').parent().addClass('t-invalid');
    }
    if (jQuery('#reg-password').val().length == 0 || jQuery('#reg-password-confirm').val().length == 0 || jQuery('#reg-password').val() !== jQuery('#reg-password-confirm').val()) {
        valid = false;
    }

    if (valid) jQuery('#auth-menu .reg-ok-button').removeAttr('disabled'); else jQuery('#auth-menu .reg-ok-button').attr('disabled', 'disabled');
}

function auth_menu_passremind_valid() {
    var valid = true;
    jQuery('#auth-menu .b-item').removeClass('t-invalid');

    if (jQuery('#passremind-email').val().length == 0) {
        valid = false;
        jQuery('#passremind-email').parent().addClass('t-invalid');
    }

    if (valid && jQuery('#passremind-email').css('color') !== 'red') {
        jQuery('#recPass').removeAttr('disabled');
    } else {
        jQuery('#recPass').attr('disabled', 'disabled');
    }
}

function auth_menu_reg() {
    var ajax_data = {
        'name'     : jQuery('#reg-fullname').val(),
        'username' : jQuery('#reg-username').val(),
        'email'    : jQuery('#reg-email1').val(),
        'email2'   : jQuery('#reg-email2').val(),
        'passwd'   : jQuery('#reg-password').val(),
        'passwd2'  : jQuery('#reg-password-confirm').val(),
        'subscribe'  : jQuery('#check-auth-subscribe').val()
    };
    jQuery('#regForm input[type="hidden"]').each(function (index, jqitem) {
        ajax_data[jQuery(jqitem).attr('name')] = jQuery(jqitem).val();
    });

    $('#auth-menu div[class^=invalid-text]').hide();

    jQuery.ajax({
                    'type'     : 'POST',
                    'url'      : jQuery('#regForm').attr('action'),
                    'dataType' : 'json',
                    'data'     : ajax_data,
                    'timeout'  : 10000,
                    'cache'    : false,
                    'success'  : function (json_data) {
                        if (parseInt(json_data.error) === 0) {
                            jQuery('#regForm input[type="text"], #regForm input[type="password"]').val('').removeClass('text-present');
                            jQuery('#regForm input[type="submit"]').attr('disabled', 'disabled');

                            var biker_text_num = Math.floor(Math.random() * bikers_excerptions.length);
                            jQuery('#auth-menu .reg-complete-block .block-1 span').html(bikers_excerptions[biker_text_num]['text']);
                            jQuery('#auth-menu .reg-complete-block .block-2 span').html((bikers_excerptions[biker_text_num]['biker'] !== '') ? '&mdash; ' + bikers_excerptions[biker_text_num]['biker'] : '');
                            auth_menu_show('reg-ok');
                        }else {
                            if (typeof json_data.textError !== 'undefined')
                                jQuery('#auth-menu .block-3 .invalid-text').html(json_data.textError).show();
                        }
                    }
                });
}


function auth_menu_show(menu_type) {
    jQuery('#auth-menu').attr('class', menu_type).animate({
                                                              'top' : '0px'
                                                          }, 'fast');
}

function auth_menu_hide() {
    $('.t-invalid').removeClass('t-invalid');
    jQuery('#auth-menu').removeAttr('class').animate({
                                                         'top' : '-500px'
                                                     }, 'fast');
}
//})(window.jQuery);