(function ($) {
    var srp_Item_Width;
    var srp_Band_Width;
    var srp_CW;
    var srp_Items_Count;
    var srp_View_IC;
    var srp_Cur_Left;
    var srp_Count_Photo;

    $(document).ready(function () {
        srp_init_vars();
        srp_build_band();
        srp_events();

        $(window).resize(function () {
            if ((parseInt($('body').css('width')) > 990 && srp_View_IC === 1) || (parseInt($('body').css('width')) <= 990 && srp_View_IC === srp_Count_Photo)) {
                setTimeout(function () {
                    srp_init_vars();
                    srp_build_band();
                }, 100);
            }
        });
    });

    function srp_init_vars() {
        srp_Count_Photo = $('.content_slider .item').size();
        srp_View_IC = (parseInt($('body').css('width')) > 990) ? srp_Count_Photo : 1;
        srp_Cur_Left = 0;
        srp_CW = (parseInt($('body').css('width')) > 990) ? 990 : 400; //$('.wrapper').width();
        $('.wrapper').css('width', srp_CW);
        srp_Item_Width = srp_CW / srp_View_IC;
        srp_Items_Count = $('.rp_sl_item').length;
        srp_Band_Width = srp_Item_Width * srp_Items_Count;
    }

    function srp_build_band() {
        if (srp_Items_Count == 0) {
            $('.lm-product-related-products').css({'display' : 'none'})
        }

        $('.rp_sl_item').css({'width' : srp_Item_Width});
        $('.rp_sl_band').css({'width' : srp_Band_Width});
        $('.rp_sl_item img').css({'width' : srp_Item_Width * 0.7});

        var sl_items = $('.rp_sl_item')
        for (var i = srp_Items_Count; i >= 0; i--) {
            $(sl_items[i]).css({'left' : i * srp_Item_Width})
            $(sl_items[i]).css({'opacity' : '1'})
        }
    }

    function srp_events() {
        srp_arrows_hover();
        srp_prod_area_hover();
        srp_arrows_click();
    }

    function srp_arrows_hover() {
        $('.left_arrow').hover(
            function () {
                $(this).children('img').animate({'right' : '15px'}, 100)
            },
            function () {
                $(this).children('img').animate({'right' : '0px'}, 100)
            });
        $('.right_arrow').hover(
            function () {
                $(this).children('img').animate({'left' : '15px'}, 100)
            },
            function () {
                $(this).children('img').animate({'left' : '0px'}, 100)
            });
    }

    function srp_prod_area_hover() {
        $('.rp_sl_item').hover(
            function () {
                var cl = parseFloat($(this).css('left')) + srp_Cur_Left;
                $('.hover_pointer').stop();
                $('.hover_pointer').animate({'left' : cl}, 500);
                $('.hover_pointer').css({'opacity' : '1'});
            },
            function () {
                $('.hover_pointer').css({
                                            'opacity' : '0'
                                        });
            });
    }

    function srp_arrows_click() {
        $('.left_arrow').click(function () {
            srp_move_band('left');
        })
        $('.right_arrow').click(function () {
            srp_move_band('right');
        })
    }

    function srp_move_band(dir) {
        if (dir == 'right') {
            if (Math.abs(srp_Cur_Left) >= 0) {
                srp_Cur_Left = srp_Cur_Left - srp_Item_Width;
                srp_Cur_Left = Math.max(-(srp_Band_Width - srp_View_IC * srp_Item_Width), srp_Cur_Left);
                $('.rp_sl_band').animate({'left' : srp_Cur_Left}, 500);
            }
        }

        if (dir == 'left') {
            if (Math.abs(srp_Cur_Left) <= (srp_Band_Width - srp_View_IC * srp_Item_Width)) {
                srp_Cur_Left = srp_Cur_Left + srp_Item_Width;
                srp_Cur_Left = Math.min(0, srp_Cur_Left);
                $('.rp_sl_band').animate({'left' : srp_Cur_Left}, 500);
            }
        }
    };

})(window.jQuery);
