function open_personal_popup(item, data) {
    jQuery('#personal_popup.pc_popup').hide();
    //alert(jQuery('#personal_cabinet .pc_personal').css('top'));
    switch (item) {
        case 1:
            $('#personal-popup-name').html($.trim($('#pinfo_name').html()));
            $('#personal-popup-surname').html($.trim($('#pinfo_surname').html()));
            $('#personal-popup-email').html($.trim($('#pinfo_email').html()));
            $('#personal-popup-phone').html($.trim($('#pinfo_phone').html()));
            $('#birthday_date_field').html($.trim($('#pinfo_birthdate').html()));
            jQuery('#personal_popup').show();
            break;
        case 2:
            jQuery('#phone_popup').show();
            break;
        case 3:
            jQuery('#birthday_popup').show();
            break;

        case 4:
            var idAddr = '';
            if (typeof data === "object") {
                $('#personal-popup-country').val($.trim(data.country));
                $('#personal-popup-city').val($.trim(data.city));
                $('#personal-popup-street').val($.trim(data.street));
                $('#personal-popup-house').val($.trim(data.house));
                $('#personal-popup-department').val($.trim(data.department));
                idAddr = data.id;
            }

            jQuery('#personal_popup.address').eq(0).attr('data', intval(idAddr)).show();
            break;
    }
}

function editAddress (elem) {
    if (elem) {
        var divAddress = $('div.op_address_block').has(elem).eq(0);
        var data  = {
            id         : $.trim(divAddress.find('[id^=idAddr]'        ).eq(0).text()),
            country    : $.trim(divAddress.find('[id^=countryAddr]'   ).eq(0).text()),
            city       : $.trim(divAddress.find('[id^=cityAddr]'      ).eq(0).text()),
            street     : $.trim(divAddress.find('[id^=streetAddr]'    ).eq(0).text()),
            house      : $.trim(divAddress.find('[id^=houseAddr]'     ).eq(0).text()),
            department : $.trim(divAddress.find('[id^=departmentAddr]').eq(0).text())
        };
        open_personal_popup(4, data);
    }

    return false;
}

function close_personal_popup() {
    jQuery('#personal_popup.pc_popup').hide();
}
jQuery(document).mouseup(function (e) {
    var container = jQuery("#personal_popup.pc_popup:visible");
    var containerDate = $('div.ui-datepicker');
    if (e.target != container[0] && !container.has(e.target).length && e.target !== containerDate[0] && !containerDate.has(e.target).length) {
        close_personal_popup();
    }
});

(function ($) {
    var save_time_out_id = null;

    $(document).ready(function () {
        init_cart_app();
    })

    function init_cart_app() {
        init_paginator();
        init_plus_minus();
        init_save_data_buts();
    }

    function init_plus_minus() {
        $('.pm_but').click(function () {
            var sym = $(this).attr('data-pm');
            var row = $(this).attr('data-row');
            change_quantity(sym, row);
        })
    }

    function change_quantity(sym, row) {
        var prod_obj = $('.prow' + row);
        var quan_obj = prod_obj.find('.prod_kol_2');

        var cur_q = parseInt(prod_obj.attr('data-quantity'));
        var cur_p = parseInt(prod_obj.attr('data-price'));

        if (sym == '+') {
            cur_q++;
        } else if (sym == '-') {
            cur_q--
        }

        cur_q = Math.max(cur_q, 1);

        quan_obj.html(cur_q);
        prod_obj.attr('data-quantity', cur_q);
        prod_obj.find('.input_quan').val(cur_q);

        prod_obj.children('.prod_price').children('.sum_value').html(cur_q * cur_p + ' р');
        prod_obj.attr('data-total_price', cur_q * cur_p);

        change_total();

        clearTimeout(save_time_out_id);
        save_time_out_id = setTimeout(function () {
            save_sums(prod_obj.find('form.quan_change_form'));
        }, 300)
    }

    function change_total() {
        var prod_objs = $('.op_product');
        var po_l = prod_objs.length;

        var prod_sum_obj = $('.prod_sum');

        var discount = parseInt(prod_sum_obj.attr('data-discount'));
        var discount_per = parseFloat(prod_sum_obj.attr('data-desc_per'));

        var total_sum = 0;
        var total_sum_disc = 0;

        for (var i = 0; i < po_l; i++) {
            total_sum += parseInt($(prod_objs[i]).attr('data-total_price'));
        }

        if (discount_per != 0) {
            discount = Math.floor(discount_per * total_sum);
        }

        total_sum_disc = total_sum - discount;

        prod_sum_obj.children('.prod_sum_1').children('.sum_value').html(total_sum + ' р');
        prod_sum_obj.children('.prod_sum_2').children('.sum_value').html(discount + ' р');
        prod_sum_obj.children('.prod_sum_3').children('.sum_value').html(total_sum_disc + ' р');
    }

    function save_sums(form) {
        var datas = form.serialize();
        var input = $(form).find('input').eq(0);
        $.ajax({
                   url     : $(form).attr('action') + 'id/' + input.attr('data') + '/count/' + input.val(),
                   type    : 'POST',
                   data    : datas,
                   success : function (data) {

                   },
                   error   : function (data) {

                   }
               })
    }

    function save_form(form) {
        var datas = jQuery(form).serialize();
        jQuery.ajax({
                        type    : 'POST',
                        data    : datas,
                        success : function (data) {

                        },
                        error   : function (data) {

                        }
                    })
    }

    function init_paginator() {

        $('.order_process').find('.next_step').click(function () {
            next_step_event(this);
        });

    }

    function next_step_event(obj) {
        var obj_id = $(obj).attr('id');
        var title = $('.op_title_block');

        switch (obj_id) {
            case 'confirm_order':
                show_page('.edit_bill_to');
                title.removeClass('step_1');
                title.addClass('step_2');
                break;
            case 'bill_next':
                if (validateUserInfo()) {
                    show_page('.edit_ship_to');
                    title.removeClass('step_1');
                    title.removeClass('step_2');
                    title.removeClass('step_3');

                    title.addClass('step_3');
                } else {
                    $('[id^=textError]').remove();
                    $('#bill_next').parent().prepend($('<div/>', {id: 'textError', style: 'color: red;'}).html('Заполните все поля'));
                }
                break;
            case 'ship_next':
                show_page('.payments');
                title.removeClass('step_2');
                title.addClass('step_3');
                break;
            case 'title_step_1':
                show_page('.product_list');
                title.removeClass('step_1');
                title.removeClass('step_2');
                title.removeClass('step_3');

                title.addClass('step_1');
                break;
            case 'title_step_2':
                show_page('.edit_bill_to');
                title.removeClass('step_1');
                title.removeClass('step_2');
                title.removeClass('step_3');

                title.addClass('step_2');
                break;
            case 'title_step_3':
                show_page('.edit_ship_to');
                title.removeClass('step_1');
                title.removeClass('step_2');
                title.removeClass('step_3');

                title.addClass('step_3');
                break;
        }
    };

    function show_page(sel) {
        var prev = $('.order_page.on');
        var next = $(sel);

        prev.css({'opacity' : 0});

        setTimeout(function () {
            prev.css({'display' : 'none'}).toggleClass('on');
            next.css({'display' : 'block'}).toggleClass('on');

            setTimeout(function () {
                next.css({'opacity' : 1});
            }, 10)

        }, 205)

    }

    function init_save_data_buts() {

        pers_info_save();

    }

    function getUserrInfo ( ) {
        return {
            first_name : $('#personal-popup-name').val(),
            last_name  : $('#personal-popup-surname').val(),
            email      : $('#personal-popup-email').val(),
            phone      : $('#personal-popup-phone').val(),
            birthday   : $('#birthday_date_field').val()
        };
    }

    function validateUserInfo () {
        var valResult = true;
        var res = getUserrInfo();
        for (var i in res) {
            if (res.hasOwnProperty(i)) {
                if ($.trim(res[i]) == '')
                    valResult = false;
            }
        }

        return valResult;
    }

    function pers_info_save() {
        $("#personal_save").click(function () {

            var phone = $('#personal-popup-phone').val();
            var birthday = $('#birthday_date_field');

            var dataUpdateUserInfo = getUserrInfo();

            $('#pinfo_name').children('.op_value').html(dataUpdateUserInfo.first_name);
            $('#pinfo_surname').children('.op_value').html(dataUpdateUserInfo.last_name);
            $('#pinfo_email').children('.op_value').html(dataUpdateUserInfo.email);

            var Pphone = $('#pinfo_phone');
            if (phone != '')  Pphone.removeClass('nodata');
            else Pphone.addClass('nodata');
            Pphone.children('.op_value').html(phone);

            var pBirth = $('#pinfo_birthdate');
            if (birthday.val().length)
                pBirth.removeClass('nodata');
            else
                pBirth.addClass('nodata');
            pBirth.children('.op_value').html(birthday.val());

            save_form('#all_update_user_info');

            $.ajax({
                       url      : baseUrl + 'buy/updateUserInfo',
                       dataType : 'JSON',
                       type     : 'POST',
                       data     : 'userInfo=' + JSON.stringify(dataUpdateUserInfo),
                       success  : function () {

                       }
                   })
        })
    }

    function save_form(form) {
        var datas = jQuery(form).serialize();
        jQuery.ajax({
                        type    : 'POST',
                        data    : datas,
                        success : function (data) {

                        },
                        error   : function (data) {

                        }
                    })
    }

})(window.jQuery);
