/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50535
Source Host           : localhost:3306
Source Database       : localmoto

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2014-04-18 21:22:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for motolocal_address
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_address`;
CREATE TABLE `motolocal_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'Пользователь',
  `city` varchar(255) NOT NULL COMMENT 'Город',
  `street` varchar(255) NOT NULL COMMENT 'Улица',
  `department` varchar(255) NOT NULL COMMENT 'Дом',
  `date_add` date DEFAULT NULL COMMENT 'Дата добавления',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `motolocal_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_address
-- ----------------------------
INSERT INTO `motolocal_address` VALUES ('1', '1', 'odessa123', 'ss', '11', '0000-00-00');
INSERT INTO `motolocal_address` VALUES ('2', '1', 'odessa', 'ss', '11', '0000-00-00');
INSERT INTO `motolocal_address` VALUES ('10', '1', 'asd', 'asd', 'asd', '2003-04-14');
INSERT INTO `motolocal_address` VALUES ('21', '1', 'asdasd', 'asdas', 'asdasd', '2003-04-14');
INSERT INTO `motolocal_address` VALUES ('22', '1', 'asdas', 'asdasd', 'asdasd', '2003-04-14');
INSERT INTO `motolocal_address` VALUES ('23', '1', 'asdasd', 'asd', 'asd', '2003-04-14');

-- ----------------------------
-- Table structure for motolocal_category
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_category`;
CREATE TABLE `motolocal_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT NULL COMMENT 'Родитель',
  `date_create` datetime NOT NULL COMMENT 'Дата создания',
  `parent_position` int(11) DEFAULT NULL COMMENT 'Позиция в подкатегории',
  `position` int(11) DEFAULT '1' COMMENT 'Позиция',
  PRIMARY KEY (`id`),
  KEY `position` (`position`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_category
-- ----------------------------
INSERT INTO `motolocal_category` VALUES ('89', 'Street', null, '0000-00-00 00:00:00', null, '9');

-- ----------------------------
-- Table structure for motolocal_color_goods
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_color_goods`;
CREATE TABLE `motolocal_color_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `color` varchar(255) NOT NULL COMMENT 'Цвет',
  `id_photo` int(11) NOT NULL COMMENT 'ID фото',
  PRIMARY KEY (`id`),
  KEY `id_photo` (`id_photo`),
  CONSTRAINT `id_photo` FOREIGN KEY (`id_photo`) REFERENCES `motolocal_photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_color_goods
-- ----------------------------
INSERT INTO `motolocal_color_goods` VALUES ('25', '#5AB7C5', '18');
INSERT INTO `motolocal_color_goods` VALUES ('26', '#EC264B', '18');
INSERT INTO `motolocal_color_goods` VALUES ('27', '#8334A6', '18');
INSERT INTO `motolocal_color_goods` VALUES ('28', '#3C7237', '18');

-- ----------------------------
-- Table structure for motolocal_description_goods
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_description_goods`;
CREATE TABLE `motolocal_description_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `description` text NOT NULL COMMENT 'Описание',
  `photo_id` int(11) NOT NULL COMMENT 'ID фото',
  PRIMARY KEY (`id`),
  KEY `photoid` (`photo_id`),
  CONSTRAINT `photoid` FOREIGN KEY (`photo_id`) REFERENCES `motolocal_photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_description_goods
-- ----------------------------
INSERT INTO `motolocal_description_goods` VALUES ('4', 'Описание товара #1 T-Shirt', '18');

-- ----------------------------
-- Table structure for motolocal_goods
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_goods`;
CREATE TABLE `motolocal_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `price` int(20) NOT NULL COMMENT 'Цена',
  `model` varchar(255) NOT NULL COMMENT 'Модель',
  `sex_characters` enum('0','1','2') DEFAULT '2' COMMENT 'М/Ж',
  `producer_id` int(11) NOT NULL COMMENT 'Производитель',
  `is_active` tinyint(2) DEFAULT '1' COMMENT 'Состояние (1- видимый, 2-скрытый)',
  PRIMARY KEY (`id`),
  KEY `producer_id` (`producer_id`),
  CONSTRAINT `producer_id` FOREIGN KEY (`producer_id`) REFERENCES `motolocal_producer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_goods
-- ----------------------------
INSERT INTO `motolocal_goods` VALUES ('7', '#1 T-Shirt', '3400', '#1 T-Shirt', '2', '40', '1');

-- ----------------------------
-- Table structure for motolocal_goods_category
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_goods_category`;
CREATE TABLE `motolocal_goods_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL COMMENT 'Товар',
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  PRIMARY KEY (`id`),
  KEY `cat_id` (`category_id`),
  KEY `good_id` (`goods_id`),
  CONSTRAINT `cat_id` FOREIGN KEY (`category_id`) REFERENCES `motolocal_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `good_id` FOREIGN KEY (`goods_id`) REFERENCES `motolocal_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_goods_category
-- ----------------------------
INSERT INTO `motolocal_goods_category` VALUES ('60', '7', '89');

-- ----------------------------
-- Table structure for motolocal_goods_size
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_goods_size`;
CREATE TABLE `motolocal_goods_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `size` varchar(25) NOT NULL COMMENT 'Размер',
  `photo_id` int(11) NOT NULL COMMENT 'ID фото',
  PRIMARY KEY (`id`),
  KEY `photo_id` (`photo_id`),
  CONSTRAINT `photo_id` FOREIGN KEY (`photo_id`) REFERENCES `motolocal_photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_goods_size
-- ----------------------------
INSERT INTO `motolocal_goods_size` VALUES ('11', 'XS', '18');
INSERT INTO `motolocal_goods_size` VALUES ('12', 'S', '18');
INSERT INTO `motolocal_goods_size` VALUES ('13', 'M', '18');
INSERT INTO `motolocal_goods_size` VALUES ('14', 'L', '18');
INSERT INTO `motolocal_goods_size` VALUES ('15', 'XL', '18');
INSERT INTO `motolocal_goods_size` VALUES ('16', 'XXL', '18');
INSERT INTO `motolocal_goods_size` VALUES ('17', 'XXXL', '18');
INSERT INTO `motolocal_goods_size` VALUES ('18', 'BXL', '18');

-- ----------------------------
-- Table structure for motolocal_photo
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_photo`;
CREATE TABLE `motolocal_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `url` varchar(255) NOT NULL COMMENT 'Изображение',
  `goods_id` int(11) NOT NULL COMMENT 'Товар',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  CONSTRAINT `goods_id` FOREIGN KEY (`goods_id`) REFERENCES `motolocal_goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_photo
-- ----------------------------
INSERT INTO `motolocal_photo` VALUES ('18', '18.png', '7');

-- ----------------------------
-- Table structure for motolocal_producer
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_producer`;
CREATE TABLE `motolocal_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT 'Проивзодитель',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_producer
-- ----------------------------
INSERT INTO `motolocal_producer` VALUES ('40', 'Bell');

-- ----------------------------
-- Table structure for motolocal_users
-- ----------------------------
DROP TABLE IF EXISTS `motolocal_users`;
CREATE TABLE `motolocal_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `email` varchar(255) NOT NULL COMMENT 'EMail',
  `password` varchar(255) DEFAULT NULL COMMENT 'Пароль',
  `role` enum('admin','user','moderator','manager') DEFAULT 'user' COMMENT 'Роль',
  `username` varchar(255) NOT NULL COMMENT 'Ник',
  `first_name` varchar(255) NOT NULL COMMENT 'Имя',
  `last_name` varchar(255) NOT NULL COMMENT 'Фамилия',
  `middle_name` varchar(255) DEFAULT NULL COMMENT 'Отчество',
  `phone` varchar(25) NOT NULL COMMENT 'Телефон',
  `city` varchar(255) DEFAULT NULL COMMENT 'Город',
  `register_date` date NOT NULL COMMENT 'Дата регистрации',
  `last_visit` date DEFAULT NULL COMMENT 'Последнее посещение',
  `email_activate` tinyint(2) DEFAULT '0' COMMENT 'Активация EMail',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of motolocal_users
-- ----------------------------
INSERT INTO `motolocal_users` VALUES ('1', 'admin@lm.ru', 'c4ca4238a0b923820dcc509a6f75849b', 'admin', 'sonrac', 'Admin', 'Admin', 'Admin', '79654824564', 'Moscow', '0000-00-00', '0000-00-00', '0');
